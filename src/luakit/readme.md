---
title: "Status of the Project Inventory"
author: "baizabal.jesus@gmail.com"
extensions:
  - image_ueberzug
  - qrcode
  - render
styles:
  style: monokai
  table:
    column_spacing: 15
  margin:
    top: 3
    bottom: 0
  padding:
    top: 3
    bottom: 3
---

## Install styles

in

~/.local/share/luakit/

## Activate noscript

- add in /etc/xdg/luakit/rc.lua

```bash
require "noscript"
local noscript = require "noscript"
noscript.enable_scripts = false
```
