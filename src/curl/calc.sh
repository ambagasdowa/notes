curl -X POST \
  'http://www.dneonline.com/calculator.asmx?wsdl=' \
  -H 'Accept: */*' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Type: text/xml' \
  -H 'Host: www.dneonline.com' \
  -H 'Postman-Token: 6e5ff401-3454-4505-addb-218e7f5ecadf,4101c904-c74a-4cb9-8d6d-a8776fe6502f' \
  -H 'User-Agent: PostmanRuntime/7.15.0' \
  -H 'accept-encoding: gzip, deflate' \
  -H 'cache-control: no-cache' \
  -H 'content-length: 314' \
  -d '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
  <soap12:Body>
    <Add xmlns="http://tempuri.org/">
      <intA>2</intA>
      <intB>1</intB>
    </Add>
  </soap12:Body>
</soap12:Envelope>'
