#!/bin/bash
emails=($(grep -oP '(?<=mensaje>)[^<]+' "xtract.xml"))

for i in ${!emails[*]}
do
  echo "$i" "${emails[$i]}"
  # instead of echo use the values to send emails, etc
done

# https://stackoverflow.com/questions/17333755/extract-xml-value-in-bash-script

# example
# for email in $(cat /my_path/spam.xml | grep -oP '(?<=email>)[^<]+'); do echo "$email"; done


#example 2

#
#
# As Charles Duffey has stated, XML parsers are best parsed with a proper XML parsing tools. For one time job the following should work.
#
# grep -oPm1 "(?<=<title>)[^<]+"
#
# Test:
#
# $ echo "$data"
# <item>
#   <title>15:54:57 - George:</title>
#   <description>Diane DeConn? You saw Diane DeConn!</description>
# </item>
# <item>
#   <title>15:55:17 - Jerry:</title>
#   <description>Something huh?</description>
# $ title=$(grep -oPm1 "(?<=<title>)[^<]+" <<< "$data")
# $ echo "$title"
# 15:54:57 - George:

# example 3
#
# XMLStarlet or another XPath engine is the correct tool for this job.
#
# For instance, with data.xml containing the following:
#
# <root>
#   <item>
#     <title>15:54:57 - George:</title>
#     <description>Diane DeConn? You saw Diane DeConn!</description>
#   </item>
#   <item>
#     <title>15:55:17 - Jerry:</title>
#     <description>Something huh?</description>
#   </item>
# </root>
#
# ...you can extract only the first title with the following:
#
# xmlstarlet sel -t -m '//title[1]' -v . -n <data.xml
