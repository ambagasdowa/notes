#
#
# Here's a full working example.
# If it's only extracting email addresses you could just do something like:
# 1) Suppose XML file spam.xml is like
#
# <spam>
# <victims>
#   <victim>
#     <name>The Pope</name>
#     <email>pope@vatican.gob.va</email>
#     <is_satan>0</is_satan>
#   </victim>
#   <victim>
#     <name>George Bush</name>
#     <email>father@nwo.com</email>
#     <is_satan>1</is_satan>
#   </victim>
#   <victim>
#     <name>George Bush Jr</name>
#     <email>son@nwo.com</email>
#     <is_satan>0</is_satan>
#   </victim>
# </victims>
# </spam>
#
# 2) You can get the emails and process them with this short bash code:
#
# #!/bin/bash
# emails=($(grep -oP '(?<=email>)[^<]+' "/my_path/spam.xml"))
#
# for i in ${!emails[*]}
# do
#   echo "$i" "${emails[$i]}"
#   # instead of echo use the values to send emails, etc
# done
#
# Result of this example is:
#
# 0 pope@vatican.gob.va
# 1 father@nwo.com
# 2 son@nwo.com
#
# Important note:
# Don't use this for serious matters. This is OK for playing around, getting quick results, learning grep, etc. but you should definitely look for, learn and use an XML parser for production (see Micha's comment below).
# shareedit
# edited Aug 26 '16 at 15:49
# answered Jun 6 '14 at 18:01
# aesede
# 4,19522 gold badges2828 silver badges3131 bronze badges
#
#     That was exactly what I was looking for. It is working as supposed to, but I fail to understand the -o and -P arguments and the expression you are using for the grep. Can you explain it? Just trying to learn something new. – thexpand Jul 13 '14 at 20:35
#     2
#     Hi, the -o or --only-matching means "only show the matching part", in this case the emails. The -P or --perl-regexp means "use a regular expression as if it was Perl". You can see this and all other options just by doing grep --help in the command line. Also you can do man grep for the full manual. – aesede Jul 14 '14 at 20:23
#     1
#     Also worth noticing that this is a quick and dirty way of getting emails from an XML document. You could get same result with this in the command line: for email in $(cat /my_path/spam.xml | grep -oP '(?<=email>)[^<]+'); do echo "$email"; done If you aim to use it for production you should definetly use an XML parser. In my case I use Python scripts with lxml – aesede Oct 16 '15 at 18:50
#     Why are you using grep? You must not use a regex to parse xml: stackoverflow.com/questions/1732348/… – Micha Wiedenmann Mar 9 '16 at 7:56
#     1
#     Yeah @MichaWiedenmann that post is a classic, a must read for anybody! Notice that I don't recommend my solution for production, just for quick and dirty things from the command line. You should always use an XML parser for real life stuff. – aesede Mar 9 '16 at 13:42
#

curl -X POST \
  'https://gst.webcopiloto.com/ws/wgst/apicopiloto/ApiUnidades.asmx?wsdl=' \
  -H 'Accept: */*' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Type: text/xml' \
  -H 'Host: gst.webcopiloto.com' \
  -H 'Postman-Token: 609c7d7f-60e5-4e81-b4b6-cf65e4458826,4f1ca9b3-286e-41d5-9a57-8a463f6be655' \
  -H 'User-Agent: PostmanRuntime/7.15.0' \
  -H 'accept-encoding: gzip, deflate' \
  -H 'cache-control: no-cache' \
  -H 'content-length: 443' \
  -H 'cookie: __cfduid=da406b22479ceccf77e3e0f923c3e95781561133550' \
  -b __cfduid=da406b22479ceccf77e3e0f923c3e95781561133550 \
  -d '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
  <soap12:Body>
    <historicoUnidades xmlns="http://tempuri.org/">
      <token>Gst2019C0p1l0tO</token>
      <fechaInicial>2019-06-21T10:00:00</fechaInicial>
      <fechaFinal>2019-06-21T10:10:00</fechaFinal>
    </historicoUnidades>
  </soap12:Body>
</soap12:Envelope>'
