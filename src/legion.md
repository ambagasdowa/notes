---
title: "ProjectName ..."
author: "baizabal.jesus@gmail.com"
extensions:
  - image_ueberzug
  - qrcode
  - render
styles:
  style: solarized-dark
  table:
    column_spacing: 18
  margin:
    top: 1
    bottom: 0
  padding:
    top: 1
    bottom: 1
---

# In Legion

-start virtual output with

```bash
intel-virtual-output
```

then

xheck with xrandr

xrandr --output VIRTUAL5 --left-of eDP1 --mode VIRTUAL5.459-1920x1080
