---
title: "Bitlbee Install"
author: "baizabal.jesus@gmail.com"
extensions:
  - image_ueberzug
  - qrcode
  - render
styles:
  style: solarized-dark
  table:
    column_spacing: 15
  margin:
    top: 3
    bottom: 0
  padding:
    top: 3
    bottom: 3
---

These are Debian/Ubuntu package repositories. New packages are built nightly (if new changes are available).

Adding the APT repository

Add a line like this to your /etc/apt/sources.list

deb http://code.bitlbee.org/debian/<BRANCH>/<VERSION>/<ARCHITECTURE>/ ./

Replace branch + version + architecture with the corresponding values. Also, the ./ at the end is important.

Branches
master Main branch, you probably want this
develop Slightly more experimental
parson, hip-cat, ... Other feature branches, even more experimental
Versions
wheezy Debian 7.0 (old-old-stable)
jessie Debian 8.0 (old-stable)
stretch Debian 9.0 (stable)
testing Debian testing
trusty Ubuntu 14.04 LTS (trusty tahr), Linux Mint 17
xenial Ubuntu 16.04 LTS (xenial xerus)
zesty Ubuntu 17.04 (zesty zapus)
artful Ubuntu 17.10 (artful aardvark)
.... Any non-EOLed Ubuntu release should normally work, we're just bad at updating this page in time
Architectures
i386
amd64
raspbian Obviously only for Debian releases, not Ubuntu
armhf
armel Not for Ubuntu (no official port anymore after Precise)

For example, if you have debian jessie amd64, you'd write the following:

deb http://code.bitlbee.org/debian/master/jessie/amd64/ ./

GPG key

You can add the signing key (signed by E3304051, wilmer@gaast.net, which has a pretty reasonable trust path) to your apt configs by copy-pasting the following into a shell
window:

wget -O- https://code.bitlbee.org/debian/release.key | sudo apt-key add -

### Configuration

- Install bins
  - sudo apt install bitlbee-libpurple
  - sudo apt install telegram-purple

> Init bitlbee

- start irssi
- in window `/connect localhost`
- in window 2 `account add telegram <[+code-country] [phone-number]>`
- in window 3 `enter login code of telegram `
