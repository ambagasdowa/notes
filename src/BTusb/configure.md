The firmware for rtl8761b is missing in your Linux distribution if you see these errors:

Direct firmware load for rtl_bt/rtl8761b_fw.bin failed with error -2
firmware file rtl_bt/rtl8761b_fw.bin not found
Execute these commands to install the missing firmware:

cd /tmp

# Fetch rtl8761b_config and rtl8761b_fw from https://github.com/Realtek-OpenSource/android_hardware_realtek

wget https://raw.githubusercontent.com/Realtek-OpenSource/android_hardware_realtek/rtk1395/bt/rtkbt/Firmware/BT/rtl8761b_config
wget https://raw.githubusercontent.com/Realtek-OpenSource/android_hardware_realtek/rtk1395/bt/rtkbt/Firmware/BT/rtl8761b_fw
mv rtl8761b_config /lib/firmware/rtl_bt/rtl8761b_config.bin
mv rtl8761b_fw /lib/firmware/rtl_bt/rtl8761b_fw.bin
sudo modprobe btusb
sudo systemctl start bluetooth.service
hciconfig -a # will show that the bluetooth-device is up
If you are looking for a more comfortable solution, here's a similar, script-based solution that automatically installs missing bluetooth-firmware, including rtl8761b: https://unix.stackexchange.com/a/643707/88252

#### How To Connect Devices Using The Command Line

If the graphical method doesn't work for your Bluetooth device, or if your device does not show up in the search results, there is another way to complete the pairing using the bluetoothctl command-line utility provided with Ubuntu MATE. In this example, we connect a Bluetooth 3.0 Keyboard.

1.  From the menu, open **MATE Terminal**.
2.  In the terminal, type  
    bluetoothctl  
    and press **Enter**.
3.  The terminal will display:  
    agent registered
4.  Now type  
    scan on  
    and press **Enter** to begin the discovery process.
5.  Note the MAC address of the keyboard or device you want to connect. You will it need to activate the Bluetooth pairing on the keyboard.
6.  Let's assume the MAC address of the keyboard is ab:cd:ef:12:34:56
7.  Stop the Bluetooth pairing on the keyboard otherwise, the following option won't work. To do this, type  
    scan off  
    and press **Enter**.
8.  Type  
    trust ab:cd:ef:12:34:56  
    and press **Enter**.
9.  Type  
    pair ab:cd:ef:12:34:56  
    and press **Enter**.
10. Bluetooth pairing begins. If a prompt for a pin appears, use the pin provided by your device maker and press the **Enter** key on your keyboard. The pin for many devices is four zeroes. (0000)
11. Type exit  
    to quit the utility.
