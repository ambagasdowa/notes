cat file | xclip -sel clip

curl -sH "Accept: application/vnd.github.v3+json" https://api.github.com/users/ambagasdowa/repos?per_page=1000

Example with httpie

- The number of results per page (max 100).

https https://api.github.com/users/ambagasdowa/repos per_page==100 page==1 "Authorization:token `cat --plain credentials/github/token/working_api`" Accept:application/vnd.github+json | grep -o 'git@[^"]\*'

- clones scdbf
  https https://api.github.com/repos/ambagasdowa/scdbf/traffic/clones per==week "Authorization:token `cat --plain credentials/github/token/working_api`" Accept:application/vnd.github+json

- Search
  https https://api.github.com/search/repositories q==kitty "Authorization:token `cat --plain credentials/github/token/working_api`" Accept:application/vnd.github+json | grep -o 'git@[^"]\*'
