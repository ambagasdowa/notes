---
title: "Status of the Project Inventory"
author: "baizabal.jesus@gmail.com"
extensions:
  - image_ueberzug
  - qrcode
  - render
styles:
  style: solarized-dark
  table:
    column_spacing: 15
  margin:
    top: 3
    bottom: 0
  padding:
    top: 3
    bottom: 3
---

6

I am using this in my ~/.Xmodmap:

! who needs CapsLock anyway
clear Lock
keycode 66 = Escape
It only disables CapsLock so basically [CapsLock] and [Esc] act the same way on my system.

I also have following line in ~/.xinitrc to load my customized keyboard map whenever X starts.

if [ -s ~/.Xmodmap ]; then
xmodmap ~/.Xmodmap
fi
You will find more solutions on the arch-forum.

The following solutions are just copied from the above link for reference.

This should be put in ~/.Xmodmap to switch both Keys. You also have to load your modified .Xmodmap file from .xinitrc.

remove Lock = Caps_Lock
add Lock = Escape
keysym Caps_Lock = Escape
keysym Escape = Caps_Lock
Or if you prefer another program instead of xmodmap, add following line to ~/.xinitrc:

setxkbmap -option caps:escape

Step 1: Find the KeyCode (number assigned to key) and Keysym (name of key) for your desired keys to swap

xmodmap -pk
In my case 118 is KeyCode for Insert key and 119 is for Delete key.

Step 2: Swap the keys by issuing following command:

xmodmap -e "keycode 118 = Delete"
xmodmap -e "keycode 119 = Insert"

Now your keys are swapped but this action is not persistent and only works until you reboot. If you want to make your changes persistent then you need to write a script and make it auto execute at startup or you can make a .desktop file and make it run at every startup. I prefer .desktop approach.
