#!/bin//bash


source <(curl -sL https://raw.githubusercontent.com/nextcloud/vm/master/lib.sh)

# Rewrite Vars
## VARIABLES

# Dirs
SCRIPTS=/var/scripts
NCPATH=/var/www/cloud.uruk/public_html
HTML=/var/www
POOLNAME=ncdata
NCDATA="$HTML/data.uruk/data/"
SNAPDIR=/var/snap/spreedme
GPGDIR=/tmp/gpg
SHA256_DIR=/tmp/sha256
BACKUP=/mnt/NCBACKUP
RORDIR=/opt/es/
OPNSDIR=/opt/opensearch
NC_APPS_PATH=$NCPATH/apps
VMLOGS=/var/log/nextcloud


PGDB_USER="cloud.uruk"
PGDB_PASS="@cloud.uruk#"
GUIUSER="Ambagasdowa"
GUIPASS="@EnumaElish#"


# Ubuntu OS
DISTRO=$(lsb_release -sr)
CODENAME=$(lsb_release -sc)
KEYBOARD_LAYOUT=$(localectl status | grep "Layout" | awk '{print $3}')


# Install Nextcloud
print_text_in_color "$ICyan" "Installing Nextcloud..."

#git clone https://github.com/nextcloud/server.git public_html && cd "$NCPATH" && git submodule update --init

#sudo chown -R www-data:www-data "$NCPATH" && sudo chmod -R 775 "$NCPATH"

nextcloud_occ maintenance:install \
--data-dir="$NCDATA" \
--database=mysql \
--database-name=nextcloud \
--database-user="$PGDB_USER" \
--database-pass="$PGDB_PASS" \
--admin-user="$GUIUSER" \
--admin-pass="$GUIPASS"
echo
print_text_in_color "$ICyan" "Nextcloud version:"
nextcloud_occ status
sleep 3
echo

## Prepare cron.php to be run every 5 minutes
#crontab -u www-data -l | { cat; echo "*/5  *  *  *  * php -f $NCPATH/cron.php > /dev/null 2>&1"; } | crontab -u www-data -
#
## Change values in php.ini (increase max file size)
## max_execution_time
#sed -i "s|max_execution_time =.*|max_execution_time = 3500|g" "$PHP_INI"
## max_input_time
#sed -i "s|max_input_time =.*|max_input_time = 3600|g" "$PHP_INI"
## memory_limit
#sed -i "s|memory_limit =.*|memory_limit = 512M|g" "$PHP_INI"
## post_max
#sed -i "s|post_max_size =.*|post_max_size = 1100M|g" "$PHP_INI"
## upload_max
#sed -i "s|upload_max_filesize =.*|upload_max_filesize = 1000M|g" "$PHP_INI"


## Set logging
#nextcloud_occ config:system:set log_type --value=file
#nextcloud_occ config:system:set logfile --value="$VMLOGS/nextcloud.log"
#rm -f "$NCDATA/nextcloud.log"
#nextcloud_occ config:system:set loglevel --value=2
#install_and_enable_app admin_audit
#nextcloud_occ config:app:set admin_audit logfile --value="$VMLOGS/audit.log"
#nextcloud_occ config:system:set log.condition apps 0 --value admin_audit
#
## Set SMTP mail
#nextcloud_occ config:system:set mail_smtpmode --value="smtp"
#
## Forget login/session after 30 minutes
#nextcloud_occ config:system:set remember_login_cookie_lifetime --value="1800"
#
## Set logrotate (max 10 MB)
#nextcloud_occ config:system:set log_rotate_size --value="10485760"
#
## Set trashbin retention obligation (save it in trashbin for 60 days or delete when space is needed)
#nextcloud_occ config:system:set trashbin_retention_obligation --value="auto, 60"
#
## Set versions retention obligation (save versions for 180 days or delete when space is needed)
#nextcloud_occ config:system:set versions_retention_obligation --value="auto, 180"
#
## Set activity retention obligation (save activity feed for 120 days, defaults to 365 days otherwise)
#nextcloud_occ config:system:set activity_expire_days --value="120"
#
## Remove simple signup
#nextcloud_occ config:system:set simpleSignUpLink.shown --type=bool --value=false
#
## Set chunk_size for files app to 100MB (defaults to 10MB)
#nextcloud_occ config:app:set files max_chunk_size --value="104857600"
#

# Enable OPCache for PHP
# https://docs.nextcloud.com/server/14/admin_manual/configuration_server/server_tuning.html#enable-php-opcache
#phpenmod opcache
#{
#echo "# OPcache settings for Nextcloud"
#echo "opcache.enable=1"
#echo "opcache.enable_cli=1"
#echo "opcache.interned_strings_buffer=16"
#echo "opcache.max_accelerated_files=10000"
#echo "opcache.memory_consumption=256"
#echo "opcache.save_comments=1"
#echo "opcache.revalidate_freq=1"
#echo "opcache.validate_timestamps=1"
#} >> "$PHP_INI"
#
## PHP-FPM optimization
## https://geekflare.com/php-fpm-optimization/
#sed -i "s|;emergency_restart_threshold.*|emergency_restart_threshold = 10|g" /etc/php/"$PHPVER"/fpm/php-fpm.conf
#sed -i "s|;emergency_restart_interval.*|emergency_restart_interval = 1m|g" /etc/php/"$PHPVER"/fpm/php-fpm.conf
#sed -i "s|;process_control_timeout.*|process_control_timeout = 10|g" /etc/php/"$PHPVER"/fpm/php-fpm.conf
#
## PostgreSQL values for PHP (https://docs.nextcloud.com/server/latest/admin_manual/configuration_database/linux_database_configuration.html#postgresql-database)
#{
#echo ""
#echo "[PostgresSQL]"
#echo "pgsql.allow_persistent = On"
#echo "pgsql.auto_reset_persistent = Off"
#echo "pgsql.max_persistent = -1"
#echo "pgsql.max_links = -1"
#echo "pgsql.ignore_notice = 0"
#echo "pgsql.log_notice = 0"
#} >> "$PHP_FPM_DIR"/conf.d/20-pdo_pgsql.ini
#
## Install PECL dependencies
#install_if_not php"$PHPVER"-dev
#
## Install Redis (distributed cache)
#run_script ADDONS redis-server-ubuntu
#
#
## Install smbclient
## php"$PHPVER"-smbclient does not yet work in PHP 7.4
#install_if_not libsmbclient-dev
#yes no | pecl install smbclient
#if [ ! -f $PHP_MODS_DIR/smbclient.ini ]
#then
#    touch $PHP_MODS_DIR/smbclient.ini
#fi
#if ! grep -qFx extension=smbclient.so $PHP_MODS_DIR/smbclient.ini
#then
#    echo "# PECL smbclient" > $PHP_MODS_DIR/smbclient.ini
#    echo "extension=smbclient.so" >> $PHP_MODS_DIR/smbclient.ini
#    check_command phpenmod -v ALL smbclient
#fi
#
## Enable igbinary for PHP
## https://github.com/igbinary/igbinary
#if is_this_installed "php$PHPVER"-dev
#then
#    if ! yes no | pecl install -Z igbinary
#    then
#        msg_box "igbinary PHP module installation failed"
#        exit
#    else
#        print_text_in_color "$IGreen" "igbinary PHP module installation OK!"
#    fi
#{
#echo "# igbinary for PHP"
#echo "session.serialize_handler=igbinary"
#echo "igbinary.compact_strings=On"
#} >> "$PHP_INI"
#    if [ ! -f $PHP_MODS_DIR/igbinary.ini ]
#    then
#        touch $PHP_MODS_DIR/igbinary.ini
#    fi
#    if ! grep -qFx extension=igbinary.so $PHP_MODS_DIR/igbinary.ini
#    then
#        echo "# PECL igbinary" > $PHP_MODS_DIR/igbinary.ini
#        echo "extension=igbinary.so" >> $PHP_MODS_DIR/igbinary.ini
#        check_command phpenmod -v ALL igbinary
#    fi
#restart_webserver
#fi





if [ -n "$PROVISIONING" ]
then
    choice="Calendar Contacts IssueTemplate PDFViewer Extract Text Mail Deck Group-Folders"
else
    choice=$(whiptail --title "$TITLE - Install apps or software" --checklist \
"Automatically configure and install selected apps or software
$CHECKLIST_GUIDE" "$WT_HEIGHT" "$WT_WIDTH" 4 \
"Calendar" "" ON \
"Contacts" "" ON \
"IssueTemplate" "" ON \
"PDFViewer" "" ON \
"Extract" "" ON \
"Text" "" ON \
"Mail" "" ON \
"Deck" "" ON \
"Group-Folders" "" OFF 3>&1 1>&2 2>&3)
fi


case "$choice" in
    *"Calendar"*)
        install_and_enable_app calendar
    ;;&
    *"Contacts"*)
        install_and_enable_app contacts
    ;;&
    *"IssueTemplate"*)
        # install_and_enable_app issuetemplate
        rm -rf "$NCPATH"apps/issuetemplate
        nextcloud_occ app:install --force --keep-disabled issuetemplate
        sed -i "s|20|${CURRENTVERSION%%.*}|g" "$NCPATH"/apps/issuetemplate/appinfo/info.xml
        nextcloud_occ_no_check app:enable issuetemplate
    ;;&
    *"PDFViewer"*)
        install_and_enable_app files_pdfviewer
    ;;&
    *"Extract"*)
        if install_and_enable_app extract
        then
            install_if_not unrar
            install_if_not p7zip
            install_if_not p7zip-full
        fi
    ;;&
    *"Text"*)
        install_and_enable_app text
    ;;&
    *"Mail"*)
        install_and_enable_app mail
    ;;&
    *"Deck"*)
        install_and_enable_app deck
    ;;&
    *"Group-Folders"*)
        install_and_enable_app groupfolders
    ;;&
    *)
    ;;
esac


# Cleanup
apt-get autoremove -y
apt-get autoclean




