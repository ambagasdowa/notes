---
title: "Status of the Project Inventory"
author: "baizabal.jesus@gmail.com"
extensions:
  - image_ueberzug
  - qrcode
  - render
styles:
  style: solarized-dark
  table:
    column_spacing: 15
  margin:
    top: 3
    bottom: 0
  padding:
    top: 3
    bottom: 3
---

# clone under webroot

```sh
git clone git@github.com:nextcloud/server.git --branch stable24 public_html
# cd public_html
git submodule update --init
```

3\. Use the `occ` command to complete your installation. This takes the place of running the graphical Installation Wizard:

```
$ cd /var/www/nextcloud/
$ sudo -u www-data php occ  maintenance:install --database \
"mysql" --database-name "nextcloud"  --database-user "cloud.uruk" --database-pass \
"@cloud.uruk#" --admin-user "Ambagasdowa" --admin-pass "@EnumaElish#" --data-dir /var/www/data.uruk/data/
Nextcloud is not installed - only a limited number of commands are available
Nextcloud was successfully installed
```

- git@github.com:nextcloud/viewer.git
  - `make`
- git@github.com:nextcloud/files_videoplayer.git
- git@github.com:nextcloud/photos.git
  - `make dev-setup` to install the dependencies.
  - Then to build the Javascript whenever you make changes, run `make build-js`.
  - Enable the app through the app management of your Nextcloud.
- git@github.com:nextcloud/text.git
  - `make`
- git@github.com:nextcloud/serverinfo.git
- git@github.com:nextcloud/deck.git
  - `make install-deps`
  - `make build`
- git@github.com:nextcloud/calendar.git
- git@github.com:nextcloud/impersonate.git
- git@github.com:nextcloud/files_texteditor.git
- git@github.com:nextcloud/files_filter.git
- git@github.com:nextcloud/files_pdfviewer.git
  - `npm ci && npm run dev`
- git@github.com:nextcloud/files_rightclick.git
- git@github.com:OliverParoczai/nextcloud-unroundedcorners.git

### NODE

> some notes about node and npm version

> upgrade npm in debian 11
> sudo npm i -g npm@latest

> to see current prefix of npm

    npm get prefix

> You can set npm prefix e.g.:
> npm config set prefix /usr/local

Note that you must change to the root Nextcloud directory, as in the example above, to run `occ maintenance:install`, or the installation will fail with a PHP fatal error message.

### FILES

scan files
sudo -u www-data php occ files:scan --all

Options:
--path limit rescan to the user/path given
--all will rescan all files of all known users
--quiet suppress any output
--verbose files and directories being processed are shown additionally during scanning
--unscanned scan only previously unscanned files

---

Supported databases are:

```
- sqlite (SQLite3 - Nextcloud Community edition only)
- mysql (MySQL/MariaDB)
- pgsql (PostgreSQL)
- oci (Oracle 11g currently only possible if you contact us at https://nextcloud.com/enterprise as
```

> continue config data in config/config.php
