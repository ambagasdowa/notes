#!/bin/bash
# sed -n 9p file ## extract the nth line
icon=`echo -e "\uF028"`
volume=`amixer get Master | grep '%' | head -n 1 | cut -d '[' -f 2 | cut -d '%' -f 1`
#printf "\uf8ff"
echo "${icon} ${volume}"
