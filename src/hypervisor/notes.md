# Build a nat in local over dsl or fiber

```sh
/etc/network/interfaces
```

```bash

source /etc/network/interfaces.d/*

auto lo
iface lo inet loopback


#### Configuration for VM as nat ####
allow-hotplug eno1
auto eno1
iface eno1 inet static
   address 192.168.0.224
   netmask 255.255.255.0
   gateway 192.168.0.1
dns-search invalid
dns-nameservers 1.1.1.1 8.8.8.8

auto vmbr0
iface vmbr0 inet static
   address 10.14.17.1
#  netmask 255.255.255.0
   bridge_ports none
   bridge_stp off
#  bridge_waitport 0
   bridge_fd 0

# Add a gateway route in the virtual machine bridge interface
   up ip route add 10.14.17.0/24 via 10.14.17.1 dev vmbr0

# Set the NAT and the ip forward
   post-up echo 1 > /proc/sys/net/ipv4/ip_forward
   post-up iptables -t nat -A POSTROUTING -s '10.14.17.0/24' -o eno1 -j MASQUERADE
   post-down iptables -t nat -D POSTROUTING -s '10.14.17.0/24' -o eno1 -j MASQUERADE

#  post-up   iptables -t raw -I PREROUTING -i fwbr+ -j CT --zone 1
#  post-down iptables -t raw -D PREROUTING -i fwbr+ -j CT --zone 1

```

## Set the dhcp server

```sh
# dhcpd.conf
#
# Sample configuration file for ISC dhcpd
#
# option definitions common to all supported networks...
option domain-name "Kalacmul.uruk";
option domain-name-servers 1.1.1.1, 8.8.8.8;


subnet 10.14.17.0 netmask 255.255.255.0 {
    range 10.14.17.100 10.14.17.200;
    option subnet-mask 255.255.255.0;
    option broadcast-address 10.14.17.255;
    option routers 10.14.17.1;
    option domain-name-servers 1.1.1.1, 8.8.8.8 ;
}


default-lease-time 600;
max-lease-time 7200;


```

## Some Export issues with hypervisor

for making a backup use

### Change directory where backups are stored

cd /var/lib/vz/dump

### create backup

vzdump 100

### this will create backup for vm id 100

In restore backup maybe found a problem like storage not found

failed: storage 'some-storage' does not exist

fix with the parameter of --storage

qmrestore /media/usb/vzdump-qemu-128-2020_11_05-09_56_08.vma 106 --storage local-zfs
