# Clean from standard vim
sudo apt-get remove --purge vim vim-runtime vim-gnome vim-tiny vim-common vim-gui-common
# Vim dependency
sudo apt-get install liblua5.1-dev luajit libluajit-5.1 python-dev libperl-dev libncurses5-dev ruby-dev

# if you want gVim add also this 
sudo apt-get install libgnome2-dev libgnomeui-dev libgtk2.0-dev libatk1.0-dev libbonoboui2-dev libcairo2-dev libx11-dev libxpm-dev libxt-dev

# Clean and prepare folders
sudo rm -rf /usr/local/share/vim
sudo rm /usr/bin/vim
sudo mkdir /usr/include/lua5.1/include

# Download it and compile
cd /usr/local/src/
git clone https://github.com/vim/vim
cd vim/src
make distclean

./configure --with-features=huge \
            --enable-rubyinterp \
            --with-ruby-command=$(which ruby) \
            --enable-largefile \
            --disable-netbeans \
            --enable-pythoninterp \
            --with-python-config-dir=/usr/lib/python2.7/config \
            --enable-perlinterp \
            --enable-luainterp \
            --with-luajit \
            --enable-gui=auto \
            --enable-fail-if-missing \
            --with-lua-prefix=/usr/include/lua5.1 \
            --enable-cscope

make
sudo make install

For Python 3.5 replace python-dev with python3-dev in the sudo apt-get install line and in the ./configure statement replace the 2.7 row and the row above with the following:

--enable-python3interp=yes \    
--with-python3-config-dir=/usr/lib/python3.5/config \

For the Python config directories, you could also use the output of python-config or python3-config:

--with-python-config-dir=$(python-config --configdir) \
# or
--with-python3-config-dir=$(python3-config --configdir) \

