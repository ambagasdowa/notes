#!/bin/bash
dir=$(pwd)
echo "we are in ${dir}"
cd $dir

./configure --with-features=huge --enable-multibyte --enable-rubyinterp=yes --with-ruby-command=/usr/bin/ruby --enable-largefile --enable-python3interp=yes --with-python3-config-dir=$(python3-config --configdir) --enable-perlinterp=yes --enable-luainterp=yes --with-luajit --with-lua-prefix=/usr/include/lua5.4 --enable-cscope --disable-darwin --disable-selinux --enable-tclinterp --disable-gui --prefix=/usr/local
