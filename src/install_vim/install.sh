#!/bin/bash
#
#
#

#dpkg
#Put a package on hold:
echo "<package-name> hold" | sudo dpkg --set-selections
#Remove the hold:
echo "<package-name> install" | sudo dpkg --set-selections
#apt
#Hold a package:
sudo apt-mark hold <package-name>
#Remove the hold:
sudo apt-mark unhold <package-name>
#Show all packages on hold:
sudo apt-mark showhold
#aptitude
#Hold a package:
sudo aptitude hold <package-name>
#Remove the hold:
sudo aptitude unhold <package-name>

#The package that is installed isn't vim and is actually vim-tiny, vim-athena, vim-gtk, vim-gtk3, or something else. To find out if this is the case, use the following command:

dpkg-query -l | grep vim

echo "Clean from Default standard Vim"

sudo apt-get remove --purge vim vim-runtime vim-gnome gvim vim-tiny vim-common \ 
                            vim-gui-common vim.nox

echo "Installing Requisites"

sudo apt-get install libncurses5-dev libgnome2-dev libgnomeui-dev \
    libgtk2.0-dev libatk1.0-dev \ #libbonoboui2-dev \
    libcairo2-dev libx11-dev libxpm-dev libxt-dev python-dev \
    python3-dev ruby-dev lua5.4 liblua5.4-dev libperl-dev git \
    intltool 
  
# if you want gVim add also this 
#sudo apt-get install libgnome2-dev libgnomeui-dev libgtk2.0-dev libatk1.0-dev libbonoboui2-dev libcairo2-dev libx11-dev libxpm-dev libxt-dev

# Clean and prepare folders
sudo rm -rf /usr/local/share/vim
sudo rm /usr/bin/vim
sudo mkdir /usr/include/lua5.1/include

# Download it and compile
#cd /usr/local/src/
git clone https://github.com/vim/vim
cd vim/src
make distclean



echo " Stating Config "

./configure --with-features=huge \
            --enable-multibyte \
            --enable-rubyinterp=yes \
            --with-ruby-command=$(which ruby) \
            --enable-largefile \
            --enable-python3interp=yes \
            --with-python3-config-dir=$(python3-config --configdir) \
            --enable-perlinterp=yes \
            --enable-luainterp=yes \
#            --with-luajit \
            --with-lua-prefix=/usr/include/lua5.1 \
            --enable-cscope \
            --disable-darwin \
            --disable-selinux \
#            --enable-pythoninterp \
            --enable-tclinterp \
#            --enable-xim \  # only with gui
#            --enable-fontset \  # only with gui
            --disable-gui \ #make sudo apt-get install checkinstall sudo checkinstall
            --prefix=/usr/local

echo " Configure is Done"

sudo apt install checkinstall
cd ~/vim
sudo checkinstall

#Once it is configured, you can compile the program with:
#make
#An optional command exists to run some self-checks (don't ask me what they do exactly I always skip them :-)):
#make check
#Finally install Vim to /usr/local/ (this command requires the root privileges):
#sudo make install
#To remove the now useless files which were created for the compilation you can then run:
#make clean
#make distclean

#Set vim as your default editor with update-alternatives.

sudo update-alternatives --install /usr/bin/editor editor /usr/local/bin/vim 1
sudo update-alternatives --set editor /usr/local/bin/vim
sudo update-alternatives --install /usr/bin/vi vi /usr/local/bin/vim 1
sudo update-alternatives --set vi /usr/local/bin/vim

#Enjoy!
