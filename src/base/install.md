# firts steps

- install net iso with base install with git [] {https://blog.desdelinux.net/como-crear-live-usbs-multiboot/}
- install chromium
- install darkreader and vimium
- Install bootiso {jsamr/bootiso}
- install nvidia drivers if apply [nvidia]{ https://www.nvidia.com/en-us/drivers/unix/}
- install vim in vim_install dir instructions
- install ranger
- install sakura , terminology or kitty
- install FiraMonaco [url]{https://github.com/GianCastle/FiraMonaco}
- install programming fonts [url]{https://github.com/ProgrammingFonts/ProgrammingFonts}
- install font preview from [url]{https://github.com/sdushantha/fontpreview}
- update font cache
- buy a new ac adapter [amazon]{https://www.amazon.com/-/es/adaptador-cargador-alimentaci%C3%B3n-Powerbook-pulgadas/dp/B01HGD3NCU/ref=psdc_11041841_t3_B08YK65SJD}



#installing sc-im 

autoconf
automake
gettext
libtool

The missing AM_ICONV should be provided by gettext

ibtoolize: Consider adding `AC_CONFIG_MACRO_DIR([m4])' to configure.ac and
> libtoolize: rerunning libtoolize, to keep the correct libtool macros
> in-tree.
> libtoolize: Consider adding `-I m4' to ACLOCAL_AMFLAGS in Makefile.am.



AX_CXX_COMPILE_STDCXX_11 macro not found

sudo apt install autoconf-archive was the solution for me



En Linux: Multiboot

1.- Formateá el pendrive en una sola partición:

[INS::INS]

En un terminal escribí:

sudo su
fdisk -l

…y tomá nota de cuál es tu pendrive.

[INS::INS]

fdisk /dev/sdx

Recordá reemplazar la x con la letra de tu pendrive (ver paso anterior).

Luego…

d (para borrar la actual partición)
n (para crear una partición nueva)
p (para partición primaria)
1 (Crea la primera partición)
Enter (para usar el primer cilindro)
Enter de nuevo (para usar el valor por defecto para el último cilindro)
a (para activa)
1 (para marcar la primera partición como arrancable)
w (para escribir cambios y cerrar fdisk)

2.- Creá un sistema de archivos FAT32 en el pendrive:

umount /dev/sdx1 #(para desmontar la partición del pendrive)
mkfs.vfat -F 32 -n MULTIBOOT /dev/sdx1 #(para formatear la partición como fat32)

3.- Instalá Grub2 en el pendrive:

[INS::INS]

mkdir /media/MULTIBOOT #(crea el directorio para el punto de montaje)
mount /dev/sdx1 /media/MULTIBOOT #(monta el pendrive)
grub-install --force --no-floppy --root-directory=/media/MULTIBOOT /dev/sdx #(instala Grub2)
cd /media/MULTIBOOT/boot/grub #(cambias de directorio)
wget pendrivelinux.com/downloads/multibootlinux/grub.cfg #(descargas el fichero grub.cfg )

4.- Testeá que tu pendrive arranque con Grub2:

Reiniciá la computadora, y entrá en el BIOS. Configurá el orden de arranque para que inicie desde el USB – Boot from USB o similar. Salvá los cambios y
reiniciá. Si todo va bien aparecerá un menú de GRUB.

5.- Añadiendo ISOs:

cd /media/MULTIBOOT #(si el Pendrive está aún montado allí)

Seguí las instrucciones para cada distro, cambiando el nombre de la ISO al que por defecto se usa en el grub.cfg que descargaste, por ejemplo, para
xubuntu.iso renombrar a ubuntu.iso

### install of nvidia drivers

```
		#local installation of fonts
		~/.local/share/fonts/
		# Update the font cache
		sudo fc-cache -f -v
```

```
 wget net iso[url]
```
