#!/bin/bash
# for a disable touchpad we found two methods 

if xinput list-props 11 | grep "Device Enabled (155):.*1" >/dev/null
then
	xinput disable 11
#	xinput set-prop 11 "Device Enabled" 0
    	#notify-send -u low -i mouse "Trackpad disabled"
			#dunstify -a "changeVolume" -u low -i audio-volume-muted -h string:x-dunst-stack-tag:$msgTag "Volume muted"
			dunstify -h string:x-dunst-stack-tag:test 'Trackpad Disabled'
else
	xinput enable 11
#	xinput set-prop 11 "Device Enabled" 1
	#notify-send -u low -i mouse "Trackpad enabled"
	dunstify -h string:x-dunst-stack-tag:test 'Trackpad Enabled'
fi

