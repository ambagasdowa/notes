---
title: "Status of the Project Inventory"
author: "baizabal.jesus@gmail.com"
extensions:
  - image_ueberzug
  - qrcode
  - render
styles:
  style: solarized-dark
  table:
    column_spacing: 15
  margin:
    top: 3
    bottom: 0
  padding:
    top: 3
    bottom: 3
---

# Nvidia artifacts on lenovo Y540

url : <https://bbs.archlinux.org/viewtopic.php?pid=2064752#p2064752>

Hi all, I was using Win10 on my laptop before, but I installed Arch in parallel with it. The laptop only supports 144hz, on Win10 everything works fine, but on Arch I have fake video artifacts (I see frame gap when scrolling or while watching videos). Output xrandr:
xrandr
Screen 0: minimum 8 x 8, current 1920 x 1080, maximum 32767 x 32767
DP-0 disconnected primary (normal left inverted right x axis y axis)
DP-1 disconnected (normal left inverted right x axis y axis)
HDMI-0 disconnected (normal left inverted right x axis y axis)
DP-2 connected 1920x1080+0+0 (normal left inverted right x axis y axis) 344mm x 193mm
1920x1080 144.00\*+
DP-3 disconnected (normal left inverted right x axis y axis)
DP-4 disconnected (normal left inverted right x axis y axis)
First time i thought that the problem in hz, but after working in nvidea-settings hz seems fine (144). I tried to
xrandr --output DP-2 --mode 1920x1080 --rate 144.00
But no change.

After surfing the internet I found out that the command in terminal
nvidia-settings --assign CurrentMetaMode="nvidia-auto-select +0+0 { ForceFullCompositionPipeline = On }"
solving the problem. How? I don't know, but it works.
