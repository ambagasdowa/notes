# Connection with python

```python
import pyodbc
import pandas as pd
# Example:
    cnxn = pyodbc.connect(
        'Trusted_Connection=no;DRIVER={ODBC Driver 17 for SQL Server};SERVER=10.8.0.235;DATABASE=sistemas;UID=zam;PWD=lis'
    )
```

```bash
#idPerson = 2
```

```python
cursor = cnxn.cursor()
#cursor.execute('SELECT id, xml FROM prueba WHERE id=?', (idPerson,))
cursor.execute("SELECT * FROM INFORMATION_SCHEMA.COLUMNS")
for row in cursor.fetchall():
    print(row)
```

> Also You can use pyodbc via FreeTDS. To create a FreeTDS connection Install FreeTDS on your Linux apt-get install tdsodbc freetds-bin, configure FreeTDS / etc/odbcinst.ini like this:

```ini
[FreeTDS]
Description = FreeTDS
Driver = /usr/lib/x86_64-linux-gnu/odbc/libtdsodbc.so
Setup = /usr/lib/x86_64-linux-gnu/odbc/libtdsS.so
```

```bash
# and turn it on
odbcinst - i - d - f / etc/odbcinst.ini
```

> After that, you can use pyodbc

```python
CONN = pyodbc.connect('DRIVER={FreeTDS};'
'Server=<server>;'
'Database=<database>;'
'UID=<domain name>\\<username>;'
'PWD=<password>;'
'TDS_Version=8.0;'
'Port=1433;')
sql = "SELECT \* FROM INFORMATION_SCHEMA.COLUMNS"
df = pd.read_sql(sql, CONN)
# It's works much faster









# Notes 

<https://stackoverflow.com/questions/45352760/mssql-data-insertion-with-python-and-pypyodbc-params-must-be-in-a-list-tuple>```
<https://www.munisso.com/2012/06/07/4-ways-to-get-identity-ids-of-inserted-rows-in-sql-server/>
<https://www.munisso.com/2012/06/07/4-ways-to-get-identity-ids-of-inserted-rows-in-sql-server/>
<https://stackoverflow.com/questions/48837384/how-to-create-tuple-with-a-loop-in-python>
<https://www.dataquest.io/blog/python-tuples/>
<https://www.datacamp.com/tutorial/python-tuples-tutorial>

