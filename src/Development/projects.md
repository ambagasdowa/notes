---
title: "Projects"
author: "baizabal.jesus@gmail.com"
extensions:
  - image_ueberzug
  - qrcode
  - render
styles:
  style: solarized-dark
  table:
    column_spacing: 18
  margin:
    top: 1
    bottom: 0
  padding:
    top: 0
    bottom: 0
---

# Viable Projects

Some projects that i think to develop

- Bookatme Based on **lookatme** markdown viewer project
