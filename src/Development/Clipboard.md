---
title: "Project Name"
author: "baizabal.jesus@gmail.com"
extensions:
  - image_ueberzug
  - qrcode
  - render
styles:
  style: solarized-dark
  table:
    column_spacing: 15
  margin:
    top: 3
    bottom: 0
  padding:
    top: 3
    bottom: 3
---

# Copy and Paste

source : <https://unix.stackexchange.com/questions/139191/whats-the-difference-between-primary-selection-and-clipboard-buffer>

> how it works

```bash
echo primary | xclip -sel p
echo secondary | xclip -sel p
echo clipboard | xclip -sel c

# Ctrl + Shift + v: clipboard
# Middle click: primary
# Paste selection: (Shift + Insert): primary
```
