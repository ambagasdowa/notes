---
title: "Status of the Project Inventory"
author: "baizabal.jesus@gmail.com"
extensions:
  - image_ueberzug
  - qrcode
  - render
styles:
  style: solarized-dark
  table:
    column_spacing: 15
  margin:
    top: 3
    bottom: 0
  padding:
    top: 3
    bottom: 3
---

# TODO

My own todo list

## Development

- Bicicle app
- stores app
- education app
- truck app
- app services development
- api services
- construction app
- hardware services app
- peaje,cruces,casetas app

## Inner platform

- voucher app
- nas,printer,images,vms, servers over android
- bible app
- game platform
- carpenters app
- Platform Services Development web site
- game in python or js but better in c,c++
