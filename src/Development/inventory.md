---
title: "Status of the Project Inventory"
author: "baizabal.jesus@gmail.com"
extensions:
  - image_ueberzug
  - qrcode
  - render
styles:
  style: monokai
  table:
    column_spacing: 15
  margin:
    top: 3
    bottom: 0
  padding:
    top: 3
    bottom: 3
---

## Assets Inventory for GST

## Equipos gobernados

    windows[bloqueo,shutdown,reboot] , android [bloqueo][@Baizabal]

## Clasificacion de equipos [Inventory]

## Habilitar lo necesario para la app de Envio de Alertas en equipos

    Clasificacion **Areas,dispositivo,areas**

## Log[info basica del equipo][inventory]

## Geoposicionamiento[@Baizabal]

---

## 2nd etapa administracion de permisos para instalar apps[windows, @android]

---

## Requirements

- Terminal Governance `predefined cmd capabilities` `Shutdown,LogOff,Restart`
- Geoposition `lat,long position of the terminal`

---

## Server Configuration

> Create your server's self signed SSL Certificate.
> If you use your server as a business, it had better buy and use Formal Certificate.

Certificates is mandatory for deployment capabilities of OCSInventoryServer

[Prerequisites on OCS deployment](https://github.com/OCSInventory-NG/Wiki/blob/master/english/05.Deployment/Prerequisites.md)

![7](/home/ambagasdowa/captures/requisites_agent.png)

### Create self signed SSL Certificate

`root@dlp:~#`[cd](https://www.server-world.info/en/command/html/cd.html) `/etc/ssl/private`

```bash
root@dlp:/etc/ssl/private# openssl genrsa -aes128 -out server.key 2048

Generating RSA private key, 2048 bit long modulus (2 primes)
...........................+++++
............+++++
e is 65537 (0x010001)
Enter pass phrase for server.key:              # set passphrase
Verifying - Enter pass phrase for server.key:  # confirm

# remove passphrase from private key

root@dlp:/etc/ssl/private# openssl rsa -in server.key -out server.key


Enter pass phrase for server.key:

# input passphrase

writing RSA key

root@dlp:/etc/ssl/private# openssl req -new -days 3650 -key server.key -out server.csr

```

You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.

```bash
Country Name (2 letter code) [AU]:JP # country code
State or Province Name (full name) [Some-State]:Hiroshima # state
Locality Name (eg, city) []:Hiroshima # city
Organization Name (eg, company) [Internet Widgits Pty Ltd]:GTS # company
Organizational Unit Name (eg, section) []:Server World # department
Common Name (e.g. server FQDN or YOUR name) []:dlp.srv.world # server's FQDN
Email Address []:root@srv.world # admin email address

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:
An optional company name []:



root@dlp:/etc/ssl/private# openssl x509 -in server.csr -out server.crt -req -signkey server.key -days 3650


Signature ok
subject=C = JP, ST = Hiroshima, L = Hiroshima, O = GTS, OU = Server World, CN = dlp.srv.world, emailAddress = root@srv.world
Getting Private key


root@dlp:/etc/ssl/private#

total 12
-rw-r--r-- 1 root root 1334 Aug 19 19:31 server.crt
-rw-r--r-- 1 root root 1062 Aug 19 19:30 server.csr
-rw------- 1 root root 1675 Aug 19 19:30 server.key

```

---

## Server Configuration

> Setup apache server with ssl capabilities

### Modifying the Default Apache SSL Virtual Host File

Next, let’s modify `/etc/apache2/sites-available/default-ssl.conf`, the default Apache SSL Virtual Host file. If you are using a different server block file, substitute its name in the commands below.

Before we go any further, let’s back up the original SSL Virtual Host file:

```
sudo cp /etc/apache2/sites-available/default-ssl.conf /etc/apache2/sites-available/default-ssl.conf.bak

```

Now, open the SSL Virtual Host file to make adjustments:

```
sudo nano /etc/apache2/sites-available/default-ssl.conf

```

Inside, with most of the comments removed, the Virtual Host block should look something like this by default:

/etc/apache2/sites-available/default-ssl.conf

```
<IfModule mod_ssl.c>
        <VirtualHost _default_:443>
                ServerAdmin webmaster@localhost

                DocumentRoot /var/www/html

                ErrorLog ${APACHE_LOG_DIR}/error.log
                CustomLog ${APACHE_LOG_DIR}/access.log combined

                SSLEngine on

                SSLCertificateFile      /etc/ssl/certs/ssl-cert-snakeoil.pem
                SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key

                <FilesMatch "\.(cgi|shtml|phtml|php)$">
                                SSLOptions +StdEnvVars
                </FilesMatch>
                <Directory /usr/lib/cgi-bin>
                                SSLOptions +StdEnvVars
                </Directory>

        </VirtualHost>
</IfModule>
```

We will be making some minor adjustments to the file. We will set the normal things we’d want to adjust in a Virtual Host file (ServerAdmin email address, ServerName, etc.), and adjust the SSL directives to point to our certificate and key files. Again, if you’re using a different document root, be sure to update the `DocumentRoot` directive.

After making these changes, your server block should look similar to this:

/etc/apache2/sites-available/default-ssl.conf

```
<IfModule mod_ssl.c>
        <VirtualHost _default_:443>
                ServerAdmin your_email@example.com
                ServerName server_domain_or_IP

                DocumentRoot /var/www/html

                ErrorLog ${APACHE_LOG_DIR}/error.log
                CustomLog ${APACHE_LOG_DIR}/access.log combined

                SSLEngine on

                SSLCertificateFile      /etc/ssl/certs/apache-selfsigned.crt
                SSLCertificateKeyFile /etc/ssl/private/apache-selfsigned.key

                <FilesMatch "\.(cgi|shtml|phtml|php)$">
                                SSLOptions +StdEnvVars
                </FilesMatch>
                <Directory /usr/lib/cgi-bin>
                                SSLOptions +StdEnvVars
                </Directory>

        </VirtualHost>
</IfModule>
```

Save and close the file when you are finished.

#### (Recommended) Modifying the HTTP Host File to Redirect to HTTPS

As it stands now, the server will provide both unencrypted HTTP and encrypted HTTPS traffic. For better security, it is recommended in most cases to redirect HTTP to HTTPS automatically. If you do not want or need this functionality, you can safely skip this section.

To adjust the unencrypted Virtual Host file to redirect all traffic to be SSL encrypted, open the `/etc/apache2/sites-available/000-default.conf` file:

```
sudo nano /etc/apache2/sites-available/000-default.conf

```

Inside, within the `VirtualHost` configuration blocks, add a `Redirect` directive, pointing all traffic to the SSL version of the site:

/etc/apache2/sites-available/000-default.conf

```
<VirtualHost *:80>
        . . .

        Redirect "/" "https://your_domain_or_IP/"

        . . .
</VirtualHost>
```

Save and close the file when you are finished.

### Step 4 — Enabling the Changes in Apache

Now that we’ve made our changes and adjusted our firewall, we can enable the SSL and headers modules in Apache, enable our SSL-ready Virtual Host, and then restart Apache to put these changes into effect.

Enable `mod_ssl`, the Apache SSL module, and `mod_headers`, which is needed by some of the settings in our SSL snippet, with the `a2enmod` command:

```
sudo a2enmod ssl
sudo a2enmod headers

```

Next, enable your SSL Virtual Host with the `a2ensite` command:

```
sudo a2ensite default-ssl

```

You will also need to enable your `ssl-params.conf` file, to read in the values you’ve set:

```
sudo a2enconf ssl-params

```

At this point, the site and the necessary modules are enabled. We should check to make sure that there are no syntax errors in our files. Do this by typing:

```
sudo apache2ctl configtest

```

If everything is successful, you will get a result that looks like this:

```
OutputSyntax OK
```

As long as your output has `Syntax OK` in it, then your configuration file has no syntax errors and you can safely restart Apache to implement the changes:

```
sudo systemctl restart apache2

```

With that, your self-signed SSL certificate is all set. You can now test that your server is correctly encrypting its traffic.

## Step 6 — Changing to a Permanent Redirect

If your redirect worked correctly and you are sure you want to allow only encrypted traffic, you should modify the unencrypted Apache Virtual Host again to make the redirect permanent.

Open your server block configuration file again:

```
sudo nano /etc/apache2/sites-available/000-default.conf

```

Find the `Redirect` line we added earlier. Add `permanent` to that line, which changes the redirect from a 302 temporary redirect to a 301 permanent redirect:

/etc/apache2/sites-available/000-default.conf

```
<VirtualHost *:80>
        . . .

        Redirect permanent "/" "https://your_domain_or_IP/"

        . . .
</VirtualHost>
```

Save and close the file.

Check your configuration for syntax errors:

```
sudo apache2ctl configtest

```

If this command doesn’t report any syntax errors, restart Apache:

```
sudo systemctl restart apache2

```

This will make the redirect permanent, and your site will only serve traffic over HTTPS.

### Conclusion

You have configured your Apache server to use strong encryption for client connections. This will allow you serve requests securely, and will prevent outside parties from reading your traffic.

sources:

- [debianWiki](https://wiki.debian.org/Self-Signed_Certificate)
- [serverWorld](https://www.server-world.info/en/note?os=Debian_11&p=ssl&f=1)
- [digitalocean](https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-apache-in-debian-9)

---

## Notes

- The source path were a file is uploaded in OCS server is
  > /var/lib/ocsinventory-reports/download
- The default path configured in deployment section is
  > http[s]://localhost/download

![5](/home/ambagasdowa/captures/agent_donw_link.png)

- Add an Alias directive

![9](/home/ambagasdowa/captures/alias.png)

![9](/home/ambagasdowa/captures/down_ocs.png)

#### url for testing vlc

[VLC](https://get.videolan.org/vlc/3.0.17.4/win64/vlc-3.0.17.4-win64.msi)

#### How to connect to mariadb server via ssh tunnel

```bash
mycli --ssh-user remote_usr --ssh-password remote_usr_pass --ssh-host remote_ip -u db_user -p db_pass
```

#### Download a file with ps

    Get-Alias -Definition Invoke-WebRequest

> $uri = https://go.microsoft.com/fwlink/?LinkId=2085155
> wget -Uri $uri -OutFile "C:\temp\netframework.exe" -Verbose

#### Install ocs agent msi

- Install silently on specific computers (ALL.TXT is a text file that lists target computer names, one per line), using domain administrator credentials:

> psexec @ALL.TXT -s -u Domain\Administrator -p Password \\Server\NetLogon\OCS-NG-Windows-Agent-Setup.exe /S /NOSPLASH /SERVER=http://my_ocs_server/ocsinventory

```bash
psexec \\COMPUTER_NAME -s \\Server\NetLogon\OCS-NG-Windows-Agent-Setup.exe /S /NOSPLASH /SERVER=http://my_ocs_server/ocsinventory
#on local
OCS-NG-Windows-Agent-Setup.exe /S /NOSPLASH /SERVER=http://my_ocs_server/ocsinventory
```

#### Install nano

You can install nano in powershell via choco - It's a low friction way to get text editing capabilities into powershell:

1.  Install Choco

```
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```

2.  Install Nano

```
choco install nano
```

3.  Profit

```
nano myfile.txt
```

Best part is it becomes part of the path, and stays working across reboots etc :)

---

## Briefing notes

### Update on the project

- Windows clients needs a gateway for commands :
  - netcat as persisten backdoor
  - ssh secure connection
- Response : ssh server as gateway for commands : success
- [powershell](https://github.com/PowerShell/Win32-OpenSSH/wiki/Install-Win32-OpenSSH-Using-MSI)

- Download the ssh server

```bash:
 wget https://github.com/PowerShell/Win32-OpenSSH/releases/download/v8.9.1.0p1-Beta/OpenSSH-Win64-v8.9.1.0.msi
```

- Install it

```bash:
 msiexec /i <path to openssh.msi> ADDLOCAL=Server
```

- Add firewall rules

```bash:
 netsh advfirewall firewall add rule name=sshd dir=in action=allow protocol=TCP localport=22
```

#### Para desactivar el firewall mediante comandos netsh:

1.  Abra un símbolo del sistema administrativo.
2.  Tipo

    netsh advfirewall set allprofiles state off

#### Para apagar el firewall utilizando Windows PowerShell en Windows Server 2012 o versiones superiores, incluido Windows Server Core:

1.  Abra Windows PowerShell.
2.  Tipo

    Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled False

#### About policies

## List execution policies by scope

In the first place, you may want to see what policies are active on your computer. To list the policies you need to open a powershell window and type:

`Get-ExecutionPolicy -List`

The output will look like this :

```
Scope          ExecutionPolicy
-----          ---------------
MachinePolicy  Undefined
UserPolicy     Undefined
Process        Undefined
CurrentUser    AllSigned
LocalMachine   RemoteSigned
```

## Set execution policies by scope

To make the OCS Agent work, you need to set the `Process` and `LocalMachine` to `Unrestricted`

The command is the following :

`Set-ExecutionPolicy -Scope {MyScope} -ExecutionPolicy {Policy Level}`

In our case: _`Set-ExecutionPolicy -Scope LocalMachine -ExecutionPolicy Unrestricted`_ `Set-ExecutionPolicy -Scope Process -ExecutionPolicy Unrestricted`

#### Read the SSL Certificate information from a remote server

You may want to monitor the validity of an SSL certificate from a remote server, without having the certificate.crt text file locally on your server? You can use the same openssl for that.

To connect to a remote host and retrieve the public key of the SSL certificate, use the following command.

$ openssl s_client -showcerts -connect ma.ttias.be:443

This will connect to the host ma.ttias.be on port 443 and show the certificate.

    openssl s_client -servername <NAME> -connect <HOST:PORT> 2>/dev/null | openssl x509 -noout -dates


    openssl s_client  -connect baizabal.xyz:443 2>/dev/null | openssl x509 -noout -dates

---

- Command example

  - ssh node_two@10.14.17.109 'systeminfo | find "System Boot Time"'
  - ssh node_two@10.14.17.109 'shutdown -s -t 00'

- Ocs Plugins as a lack of funtionalities and limitation is only for information or executing commands with an intervals of time
- Otherwise the installation of plugins was a success
- Ocs Plugin is going to be just a gateway for send commands over shell_exec php function or even better a nodejs deployment : work in progress
- Another solution is with a python backend whith fastapi and paramiko for ssh connection
- Geo position can be in plugin format and can be executed in time intervals in clients
- Add a layer for accurate localization in clients and to whatch in a map : research for vector maps : work in progress

---

## Next steps

- Research the structure of a ocs plugin
- Example , Uptime , runningprocess are good start
- even speedtest can work
- Analize a nodejs deployment
- Structure of OCS Example plugin
  ![Example Plugin](./img/exampleplugin.png)

---

## Roadmap

Roadmap to Requirements

### Execute Commands in guests

- Installing ssh server in guest add capabilities
  for basic admin in guest another
  option is PS but require more advance implementation

- Build an api for sending commands to guest over a secure channel[success - fastapi]
  - shutdown -r -t 00
  - ipconfig | find "IPv4"
  - net user Username NewPassword

![remote-api](/home/ambagasdowa/captures/api_remote_command.png)

---

#### GeoPosition Locations in Guests

    - Due the capabilities in the windows workstations guests only have one way
        for quering the geolocation
    - GPS (if available)
    - Available wi-fi networks and signal strengths
    - Available cell towers and signal strengths
    - IP Address lookup

![ps_query](/home/ambagasdowa/captures/ipGeoLocationPowerShell.png)

---

#### Ip Address lookup can be reach with maxmind geolocation database

for example

```bash
mmdblookup --file GeoLite2-City.mmdb --ip 201.108.211.142 | cat -l json
```

![result](/home/ambagasdowa/captures/geoip.png)

---

## TODO

- Trigger that's format url to maps
- Remove maps url from agents
- Remote commands on android devices
- View link to nom2001

---

# ISSUES

> AndroidAgent

requirements :

- apk 003
- sim instalada
- todos los permisos
- gps activado
- ip externa

| modelo  | version android | error si tira error | si es posible video o imagen | datos o wifi |
| ------- | --------------- | ------------------- | ---------------------------- | ------------ |
| samsung | 10xxx           | drop by server      |                              | datos        |

---

### Models

Diagram of the

```mermaid-30

%%{init: {'theme': 'dark', 'themeVariables': { 'background':'#eeeeee','primaryColor': '#333333','secondaryColor':'#aaaaaa', 'edgeLabelBackground':'#ffffee', 'tertiaryColor': '#fff0f0'}}}%%

        graph TD
          A[Christmas] -->|Get money| B(Go shopping)
          B --> C{Let me think}
          B --> G[/Another/]
          C ==>| One | D[ fa:fa-laptop Laptop ]
          C -->| Two | E[ fa:fa-mobile iPhone ]
          C -->| Three | F[ fa:fa-car Car ]
          subgraph section
            C
            D
            E
            F
            G
          end
```

---

```mermaid

%%{init: {'theme': 'base', 'themeVariables': { 'darkMode':'true','background':'#ffffff',primaryColor:'#cccccc'}}}%%

gantt
    dateFormat                YYYY-MM-DD
    title                     Roadmap
    excludes                  :excludes the named dates/days from being included in a charted task..
    section InventoryGST
    Implementation :     active,  des2, 2022-06-06, 2022-06-17
    Requirements   :     done,    des3, 2022-06-06, 2022-06-08
    Development    :              des4, after des3, 3d
    Testing        :              des5, after des4, 1d
    Release        :              des6, after des5, 1d
    Maintenance    :     active,  des1, after des6,

```

---

## Code

```mermaid

classDiagram
Class01 <|-- AveryLongClass : Cool
<<Interface>> Class01
Class09 --> C2 : Where am i?
Class09 --* C3
Class09 --|> Class07
Class07 : equals()
Class07 : Object[] elementData
Class01 : size()
Class01 : int chimp
Class01 : int gorilla
class Class10 {
  <<service>>
  int id
  size()
}

```

---

```mermaid

stateDiagram-v2
[*] --> Still
Still --> [*]
Still --> Moving
Moving --> Still
Moving --> Crash
Crash --> [*]

```

---

```mermaid
pie
"Dogs" : 386
"Cats" : 85
"Rats" : 15
```

---

```mermaid

journey
    title My working day
    section Go to work
      Make tea: 5: Me
      Go upstairs: 3: Me
      Do work: 1: Me, Cat
    section Go home
      Go downstairs: 5: Me
      Sit down: 3: Me
```

---

```mermaid

    requirementDiagram

    requirement test_req {
    id: 1
    text: the test text.
    risk: high
    verifymethod: test
    }

    functionalRequirement test_req2 {
    id: 1.1
    text: the second test text.
    risk: low
    verifymethod: inspection
    }

    performanceRequirement test_req3 {
    id: 1.2
    text: the third test text.
    risk: medium
    verifymethod: demonstration
    }

    interfaceRequirement test_req4 {
    id: 1.2.1
    text: the fourth test text.
    risk: medium
    verifymethod: analysis
    }

    physicalRequirement test_req5 {
    id: 1.2.2
    text: the fifth test text.
    risk: medium
    verifymethod: analysis
    }

    designConstraint test_req6 {
    id: 1.2.3
    text: the sixth test text.
    risk: medium
    verifymethod: analysis
    }

    element test_entity {
    type: simulation
    }

    element test_entity2 {
    type: word doc
    docRef: reqs/test_entity
    }

    element test_entity3 {
    type: "test suite"
    docRef: github.com/all_the_tests
    }


    test_entity - satisfies -> test_req2
    test_req - traces -> test_req2
    test_req - contains -> test_req3
    test_req3 - contains -> test_req4
    test_req4 - derives -> test_req5
    test_req5 - refines -> test_req6
    test_entity3 - verifies -> test_req5
    test_req <- copies - test_entity2
```
