# NOTES Operations

#    POST: to create data.
#    GET: to read data.
#    PUT: to update data.
#    DELETE: to delete data.

import os
from typing import Optional
from fastapi import FastAPI


diesel = FastAPI(
    title='Development UES API Documentation', docs_url="/api", openapi_url="/api/v1"
)


@diesel.get('/')
async def read_root():
    return {"Hello": "Python"}


@diesel.get('/items/{item_id}')
async def read_item(item_id: int, q: Optional[str] = None):
    # NOTE can query a database in hir
    # For Example
    # request = await some_sql_query()

    newQuery = "Response String is -> " + q

    os.system("echo Hello from the other side!" + newQuery)

    return{"item_id": item_id, "q": newQuery}
