---
title: "Status of the Project Inventory"
author: "baizabal.jesus@gmail.com"
extensions:
  - image_ueberzug
  - qrcode
  - render
styles:
  style: monokai
  table:
    column_spacing: 15
  margin:
    top: 3
    bottom: 0
  padding:
    top: 3
    bottom: 3
---

::: {.alert-bar .js-alert-bar .high-alert}
::: {.alert-bar\_\_content}
Classes are running in-person (socially distanced) and live online.
Secure your seat today
:::
:::

::: {.site-nav .site-nav-outer}
::: {.site-nav-bar\_\_brand}
Noble Desktop

Noble Desktop
:::

::: {.site-nav-bar\_\_primary}

- [Coding](javascript:;){.site-nav-bar**item
  .site-nav-bar**item--primary .site-nav-bar\_\_item--dropdown}
  ::: {.nav-dropdown-container style="display: none;"}
  ::: {.primary-nav-dropdown}

  - [](/topics/web-development-courses-nyc)
    ::: {.menu-item\_\_icon .front-end-web}
    :::

    Web Development

  - [](/topics/python-classes-nyc)
    ::: {.menu-item\_\_icon .python}
    :::

    Python

  - [](/topics/javascript-classes-nyc)
    ::: {.menu-item\_\_icon .jquery}
    :::

    JavaScript

  - [](/topics/fintech-bootcamps-nyc)
    ::: {.menu-item\_\_icon .financial-modeling}
    :::

    FinTech

  - [](/topics/sql-courses)
    ::: {.menu-item\_\_icon .web-dev-bootcamp}
    :::

    SQL

  - [](/nextgen/topics/summer-classes-live-online)
    ::: {.menu-item\_\_icon .comp-sci}
    :::

    High School Coding

  - [](/classes/ios-app-development-bootcamp)
    ::: {.menu-item\_\_icon .ios}
    :::

    iOS Development

  - [](/topics/data-science-training)
    ::: {.menu-item\_\_icon .data-science}
    :::

    Data Science

  - [](/topics/web-development-certificates-nyc)
    ::: {.menu-item\_\_icon .web}
    :::

    Web Certificates

  - [](/topics/html-email-classes)
    ::: {.menu-item\_\_icon .email}
    :::

    HTML Email

  - [](/topics/wordpress-classes-nyc)
    ::: {.menu-item\_\_icon .wordpress}
    :::

    WordPress

  - [](/topics/machine-learning-training-nyc)
    ::: {.menu-item\_\_icon .machine-learning}
    :::

    Machine Learning

  - [](/classes/react-bootcamp)
    ::: {.menu-item\_\_icon .react}
    :::

    React

  - [](/topics/cybersecurity-classes-nyc)
    ::: {.menu-item\_\_icon .cybersecurity}
    :::

    Cybersecurity

  ```{=html}
  <!-- -->
  ```

  - All Coding Classes & Bootcamps
    :::
    :::

- [Design](javascript:;){.site-nav-bar**item
  .site-nav-bar**item--primary .site-nav-bar\_\_item--dropdown}
  ::: {.nav-dropdown-container style="display: none;"}
  ::: {.primary-nav-dropdown}

  - [](/topics/graphic-design-courses)
    ::: {.menu-item\_\_icon .graphicdesign}
    :::

    Graphic Design

  - [](/topics/web-design-classes)
    ::: {.menu-item\_\_icon .web-design-bootcamp}
    :::

    Web Design

  - [](/topics/photoshop-classes-nyc)
    ::: {.menu-item\_\_icon .ps}
    :::

    Photoshop

  - [](/topics/after-effects-classes-nyc)
    ::: {.menu-item\_\_icon .ae}
    :::

    After Effects

  - [](/topics/premiere-pro-classes-nyc)
    ::: {.menu-item\_\_icon .pr}
    :::

    Premiere Pro

  - [](/topics/indesign-classes-nyc)
    ::: {.menu-item\_\_icon .id}
    :::

    InDesign

  - [](/topics/illustrator-classes-nyc)
    ::: {.menu-item\_\_icon .ai}
    :::

    Illustrator

  - [](/topics/adobe-classes-nyc)
    ::: {.menu-item\_\_icon .cc}
    :::

    Creative Cloud

  - [](/topics/video-editing-classes)
    ::: {.menu-item\_\_icon .video}
    :::

    Video Editing

  - [](/topics/motion-graphics-classes)
    ::: {.menu-item\_\_icon .motion-graphics}
    :::

    Motion Graphics

  - [](/topics/visual-design-classes)
    ::: {.menu-item\_\_icon .visual-design}
    :::

    Visual Design

  - [](/topics/ux-design-classes-nyc)
    ::: {.menu-item\_\_icon .ux}
    :::

    UX Design

  - [](/classes/figma-bootcamp)
    ::: {.menu-item\_\_icon .figma}
    :::

    Figma

  - [](/topics/sketch-classes-nyc)
    ::: {.menu-item\_\_icon .sketch}
    :::

    Sketch

  - [](/topics/adobe-xd-classes-nyc)
    ::: {.menu-item\_\_icon .xd}
    :::

    Adobe XD

  - [](/topics/autocad-courses-nyc)
    ::: {.menu-item\_\_icon .autocad}
    :::

    AutoCAD

  ```{=html}
  <!-- -->
  ```

  - All Design Classes & Certificates
    :::
    :::

- [Business](javascript:;){.site-nav-bar**item
  .site-nav-bar**item--primary .site-nav-bar\_\_item--dropdown}
  ::: {.nav-dropdown-container style="display: none;"}
  ::: {.primary-nav-dropdown}

  - [](/topics/digital-marketing-courses)
    ::: {.menu-item\_\_icon .digital-marketing}
    :::

    Digital Marketing

  - [](/topics/seo-training-nyc)
    ::: {.menu-item\_\_icon .seo}
    :::

    SEO

  - [](/topics/google-analytics-training-nyc)
    ::: {.menu-item\_\_icon .google-analytics}
    :::

    Google Analytics

  - [](/topics/google-ads-training-nyc)
    ::: {.menu-item\_\_icon .google-ads}
    :::

    Google Ads

  - [](/topics/social-media-marketing-training-nyc)
    ::: {.menu-item\_\_icon .social-media}
    :::

    Social Media

  - [](/topics/data-analytics-training)
    ::: {.menu-item\_\_icon .data-science}
    :::

    Data Analytics

  - [](/topics/excel-courses)
    ::: {.menu-item\_\_icon .excel}
    :::

    Excel

  - [](/topics/tableau-training-nyc)
    ::: {.menu-item\_\_icon .tableau}
    :::

    Tableau

  - [](/topics/powerpoint)
    ::: {.menu-item\_\_icon .pp}
    :::

    PowerPoint

  - [](/classes/financial-modeling-bootcamp)
    ::: {.menu-item\_\_icon .financial-modeling}
    :::

    Financial Modeling

  - [](/topics/finance-classes-nyc)
    ::: {.menu-item\_\_icon .financial-modeling}
    :::

    Finance

  - [](/topics/project-management-courses-nyc)
    ::: {.menu-item\_\_icon .project-management}
    :::

    Project Management

  ```{=html}
  <!-- -->
  ```

  - All Business Classes & Certificates
    :::
    :::

- [Certificates](javascript:;){.site-nav-bar**item
  .site-nav-bar**item--primary .site-nav-bar**item--dropdown}
  ::: {.nav-dropdown-container style="display: none;"}
  ::: {.primary-nav-dropdown} - [](/certificates/graphic-design)
  ::: {.menu-item**icon .graphicdesign}
  :::

          Graphic Design
      -   [](/certificates/motion-graphics)
          ::: {.menu-item__icon .video}
          :::

          Motion Graphics
      -   [](/classes/ux-ui-design-program)
          ::: {.menu-item__icon .ux}
          :::

          UX & UI Design
      -   [](/certificates/web-design)
          ::: {.menu-item__icon .visual-design}
          :::

          Web Design
      -   [](/certificates/social-media-marketing)
          ::: {.menu-item__icon .social-media}
          :::

          Social Media
      -   [](/certificates/digital-marketing)
          ::: {.menu-item__icon .digital-marketing}
          :::

          Digital Marketing
      -   [](/certificates/visual-design)
          ::: {.menu-item__icon .visual-design}
          :::

          UI Design
      -   [](/certificates/digital-design)
          ::: {.menu-item__icon .graphicdesign}
          :::

          Digital Design
      -   [](/certificates/video-editing)
          ::: {.menu-item__icon .video}
          :::

          Video Editing
      -   [](/certificates/data-science)
          ::: {.menu-item__icon .data-science}
          :::

          Data Science
      -   [](/certificates/front-end-web-development)
          ::: {.menu-item__icon .front-end-web}
          :::

          Front-End Web
      -   [](/certificates/full-stack-development)
          ::: {.menu-item__icon .full-stack}
          :::

          Full-Stack Web
      -   [](/certificates/javascript-development)
          ::: {.menu-item__icon .jquery}
          :::

          JavaScript Development
      -   [](/certificates/software-engineering-bootcamp)
          ::: {.menu-item__icon .comp-sci}
          :::

          Software Engineering
      -   [](/certificates/python-developer)
          ::: {.menu-item__icon .python}
          :::

          Python Developer
      -   [](/certificates/fintech-bootcamp-nyc)
          ::: {.menu-item__icon .financial-modeling}
          :::

          FinTech
      -   [](/certificates/cybersecurity)
          ::: {.menu-item__icon .cybersecurity}
          :::

          Cybersecurity
      -   [](/certificates/data-analytics)
          ::: {.menu-item__icon .tableau}
          :::

          Data Analytics

      ```{=html}
      <!-- -->
      ```
      -   Find & Compare Certificates by Topic
      :::
      :::

  :::

- ::: {.nav-search .site-nav-bar**item}
  Type forward slash (\"/\") to open the search bar
  /
  ::: {.nav-search**results}
  :::

  ::: {.nav-search\_\_message}
  :::
  :::

- [Corporate](/corporate-training){.site-nav-bar**item
  .site-nav-bar**item--secondary .site-nav-bar**item--link
  .site-nav-bar**item--dropdown}
  ::: {.nav-dropdown-container style="display: none;"}
  ::: {.corporate-dropdown}

  - [](/corporate-training/excel)
    ::: {.lazyload .menu-item\_\_icon bg="/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBa01jIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--41a45634c49843cf2e4787bb229885bece86e4b9/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdDRG9MWm05eWJXRjBTU0lJY0c1bkJqb0dSVlE2QzNKbGMybDZaVWtpQnpVMEJqc0dWRG9NY1hWaGJHbDBlVWtpQnpjd0Jqc0dWQT09IiwiZXhwIjpudWxsLCJwdXIiOiJ2YXJpYXRpb24ifX0=--0be16de403e70cc73d76b944e514f1bc104091f2/1_excel_icon.png"}
    :::

    Excel

  - [](/corporate-training/sql)
    ::: {.lazyload .menu-item\_\_icon bg="/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBa1VjIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--1f60fb9a74564c0c92ff31a2b0ba6a605dd905e1/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdDRG9MWm05eWJXRjBTU0lJY0c1bkJqb0dSVlE2QzNKbGMybDZaVWtpQnpVMEJqc0dWRG9NY1hWaGJHbDBlVWtpQnpjd0Jqc0dWQT09IiwiZXhwIjpudWxsLCJwdXIiOiJ2YXJpYXRpb24ifX0=--0be16de403e70cc73d76b944e514f1bc104091f2/2_sql_icon.png"}
    :::

    SQL

  - [](/corporate-training/python)
    ::: {.lazyload .menu-item\_\_icon bg="/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBa1ljIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--8d45738484f9cf9cf961477ec1f9f382e4b91c6a/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdDRG9MWm05eWJXRjBTU0lJY0c1bkJqb0dSVlE2QzNKbGMybDZaVWtpQnpVMEJqc0dWRG9NY1hWaGJHbDBlVWtpQnpjd0Jqc0dWQT09IiwiZXhwIjpudWxsLCJwdXIiOiJ2YXJpYXRpb24ifX0=--0be16de403e70cc73d76b944e514f1bc104091f2/3_python_icon.png"}
    :::

    Python

  - [](/corporate-training/data-science)
    ::: {.lazyload .menu-item\_\_icon bg="/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBa2tjIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--6ef5bb970e85682d336f6dbc55f55a853e93db8f/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdDRG9MWm05eWJXRjBTU0lJY0c1bkJqb0dSVlE2QzNKbGMybDZaVWtpQnpVMEJqc0dWRG9NY1hWaGJHbDBlVWtpQnpjd0Jqc0dWQT09IiwiZXhwIjpudWxsLCJwdXIiOiJ2YXJpYXRpb24ifX0=--0be16de403e70cc73d76b944e514f1bc104091f2/5_data_science_icon.png"}
    :::

    Data Science

  - [](/corporate-training/graphic-design)
    ::: {.lazyload .menu-item\_\_icon bg="/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBa3NjIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--5411ad2f4282b0e11df1127160fd2b94efb2438e/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdDRG9MWm05eWJXRjBTU0lJY0c1bkJqb0dSVlE2QzNKbGMybDZaVWtpQnpVMEJqc0dWRG9NY1hWaGJHbDBlVWtpQnpjd0Jqc0dWQT09IiwiZXhwIjpudWxsLCJwdXIiOiJ2YXJpYXRpb24ifX0=--0be16de403e70cc73d76b944e514f1bc104091f2/7_graphic_design_icon.png"}
    :::

    Graphic Design

  - [](/corporate-training/web-design)
    ::: {.lazyload .menu-item\_\_icon bg="/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBazBjIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--2c1ad3f5deac40621665f7ca971e52af21aa3a5e/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdDRG9MWm05eWJXRjBTU0lJY0c1bkJqb0dSVlE2QzNKbGMybDZaVWtpQnpVMEJqc0dWRG9NY1hWaGJHbDBlVWtpQnpjd0Jqc0dWQT09IiwiZXhwIjpudWxsLCJwdXIiOiJ2YXJpYXRpb24ifX0=--0be16de403e70cc73d76b944e514f1bc104091f2/8_web_design_icon.png"}
    :::

    Web Design

  - [](/corporate-training/photoshop)
    ::: {.lazyload .menu-item\_\_icon bg="/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBazhjIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--0988f4e7973256ad2c7dd0ba4b8e9495d39f1677/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdDRG9MWm05eWJXRjBTU0lJY0c1bkJqb0dSVlE2QzNKbGMybDZaVWtpQnpVMEJqc0dWRG9NY1hWaGJHbDBlVWtpQnpjd0Jqc0dWQT09IiwiZXhwIjpudWxsLCJwdXIiOiJ2YXJpYXRpb24ifX0=--0be16de403e70cc73d76b944e514f1bc104091f2/ps_2x.png"}
    :::

    Photoshop

  - [](/corporate-training/after-effects)
    ::: {.lazyload .menu-item\_\_icon bg="/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBbEFjIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--4b156a156a976178e7729047d1fceabcfe12b8b0/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdDRG9MWm05eWJXRjBTU0lJY0c1bkJqb0dSVlE2QzNKbGMybDZaVWtpQnpVMEJqc0dWRG9NY1hWaGJHbDBlVWtpQnpjd0Jqc0dWQT09IiwiZXhwIjpudWxsLCJwdXIiOiJ2YXJpYXRpb24ifX0=--0be16de403e70cc73d76b944e514f1bc104091f2/ae_2x.png"}
    :::

    After Effects

  - [](/corporate-training/video-editing)
    ::: {.lazyload .menu-item\_\_icon bg="/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBbEljIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--dceab4c70b3065778faa23ca26414ee251a48d8f/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdDRG9MWm05eWJXRjBTU0lJY0c1bkJqb0dSVlE2QzNKbGMybDZaVWtpQnpVMEJqc0dWRG9NY1hWaGJHbDBlVWtpQnpjd0Jqc0dWQT09IiwiZXhwIjpudWxsLCJwdXIiOiJ2YXJpYXRpb24ifX0=--0be16de403e70cc73d76b944e514f1bc104091f2/9_video_editing_icon.png"}
    :::

    Video Editing

  - [](/corporate-training/digital-marketing)
    ::: {.lazyload .menu-item\_\_icon bg="/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBbFFjIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--ca2947dc243b0f68d99ba19239aeab90f176c8e4/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdDRG9MWm05eWJXRjBTU0lJY0c1bkJqb0dSVlE2QzNKbGMybDZaVWtpQnpVMEJqc0dWRG9NY1hWaGJHbDBlVWtpQnpjd0Jqc0dWQT09IiwiZXhwIjpudWxsLCJwdXIiOiJ2YXJpYXRpb24ifX0=--0be16de403e70cc73d76b944e514f1bc104091f2/55_digital_marketing_icon.png"}
    :::

    Digital Marketing

  - [](/corporate-training/data-analytics)
    ::: {.lazyload .menu-item\_\_icon bg="/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBa0lxIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--c31820ee5554979cf89b03b4b47095365e16fc9b/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdDRG9MWm05eWJXRjBTU0lJY0c1bkJqb0dSVlE2QzNKbGMybDZaVWtpQnpVMEJqc0dWRG9NY1hWaGJHbDBlVWtpQnpjd0Jqc0dWQT09IiwiZXhwIjpudWxsLCJwdXIiOiJ2YXJpYXRpb24ifX0=--0be16de403e70cc73d76b944e514f1bc104091f2/tableau@2x.png"}
    :::

    Data Analytics

  - [](/corporate-training/adobe)
    ::: {.lazyload .menu-item\_\_icon bg="/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBbFljIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--3e53eabb3e1dd944ee6a88676c11db1a3eb1811c/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdDRG9MWm05eWJXRjBTU0lJY0c1bkJqb0dSVlE2QzNKbGMybDZaVWtpQnpVMEJqc0dWRG9NY1hWaGJHbDBlVWtpQnpjd0Jqc0dWQT09IiwiZXhwIjpudWxsLCJwdXIiOiJ2YXJpYXRpb24ifX0=--0be16de403e70cc73d76b944e514f1bc104091f2/13_adobe_icon.png"}
    :::

    Adobe

  - [](/corporate-training/microsoft-office)
    ::: {.lazyload .menu-item\_\_icon bg="/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBbGNjIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--13cf958034acdf6b74625d9866cc8a40fa53a285/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdDRG9MWm05eWJXRjBTU0lJY0c1bkJqb0dSVlE2QzNKbGMybDZaVWtpQnpVMEJqc0dWRG9NY1hWaGJHbDBlVWtpQnpjd0Jqc0dWQT09IiwiZXhwIjpudWxsLCJwdXIiOiJ2YXJpYXRpb24ifX0=--0be16de403e70cc73d76b944e514f1bc104091f2/31_microsoft_office_icon.png"}
    :::

    Microsoft Office

  - [](/corporate-training/project-management)
    ::: {.lazyload .menu-item\_\_icon bg="/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBbUVjIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--e6403e06922af03a7baa5c65ee39c7f5ffb86a24/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdDRG9MWm05eWJXRjBTU0lJY0c1bkJqb0dSVlE2QzNKbGMybDZaVWtpQnpVMEJqc0dWRG9NY1hWaGJHbDBlVWtpQnpjd0Jqc0dWQT09IiwiZXhwIjpudWxsLCJwdXIiOiJ2YXJpYXRpb24ifX0=--0be16de403e70cc73d76b944e514f1bc104091f2/32_project_management_icon.png"}
    :::

        Project Management

    :::
    :::

- [Compare](/compare){.site-nav-bar**item
  .site-nav-bar**item--secondary .site-nav-bar\_\_item--link}

- [Schedule](/schedule){.site-nav-bar**item
  .site-nav-bar**item--secondary .site-nav-bar\_\_item--link}

-

[ ](javascript:;){.site-nav-bar**item
.site-nav-bar**item--drawer-toggle-button .nav-util--offset-top}

::: {.site-nav-drawer**scroll-wrapper}
::: {.site-nav-drawer**inner}
::: {.site-nav-drawer**section .site-nav-drawer**section--account}
:::

::: {.site-nav-drawer\_\_accordion}

- ::: {.site-nav-drawer\_\_accordion-header}
  Coding
  :::

  ::: {.site-nav-drawer\_\_accordion-panel}

  - [](/topics/web-development-courses-nyc)
    ::: {.menu-item\_\_icon .front-end-web}
    :::

    Web Development

  - [](/topics/python-classes-nyc)
    ::: {.menu-item\_\_icon .python}
    :::

    Python

  - [](/topics/javascript-classes-nyc)
    ::: {.menu-item\_\_icon .jquery}
    :::

    JavaScript

  - [](/topics/fintech-bootcamps-nyc)
    ::: {.menu-item\_\_icon .financial-modeling}
    :::

    FinTech

  - [](/topics/sql-courses)
    ::: {.menu-item\_\_icon .web-dev-bootcamp}
    :::

    SQL

  - [](/nextgen/topics/summer-classes-live-online)
    ::: {.menu-item\_\_icon .comp-sci}
    :::

    High School Coding

  - [](/classes/ios-app-development-bootcamp)
    ::: {.menu-item\_\_icon .ios}
    :::

    iOS Development

  - [](/topics/data-science-training)
    ::: {.menu-item\_\_icon .data-science}
    :::

    Data Science

  - [](/topics/web-development-certificates-nyc)
    ::: {.menu-item\_\_icon .web}
    :::

    Web Certificates

  - [](/topics/html-email-classes)
    ::: {.menu-item\_\_icon .email}
    :::

    HTML Email

  - [](/topics/wordpress-classes-nyc)
    ::: {.menu-item\_\_icon .wordpress}
    :::

    WordPress

  - [](/topics/machine-learning-training-nyc)
    ::: {.menu-item\_\_icon .machine-learning}
    :::

    Machine Learning

  - [](/classes/react-bootcamp)
    ::: {.menu-item\_\_icon .react}
    :::

    React

  - [](/topics/cybersecurity-classes-nyc)
    ::: {.menu-item\_\_icon .cybersecurity}
    :::

    Cybersecurity

  ```{=html}
  <!-- -->
  ```

  - All Coding Classes & Bootcamps
    :::

- ::: {.site-nav-drawer\_\_accordion-header}
  Design
  :::

  ::: {.site-nav-drawer\_\_accordion-panel}

  - [](/topics/graphic-design-courses)
    ::: {.menu-item\_\_icon .graphicdesign}
    :::

    Graphic Design

  - [](/topics/web-design-classes)
    ::: {.menu-item\_\_icon .web-design-bootcamp}
    :::

    Web Design

  - [](/topics/photoshop-classes-nyc)
    ::: {.menu-item\_\_icon .ps}
    :::

    Photoshop

  - [](/topics/after-effects-classes-nyc)
    ::: {.menu-item\_\_icon .ae}
    :::

    After Effects

  - [](/topics/premiere-pro-classes-nyc)
    ::: {.menu-item\_\_icon .pr}
    :::

    Premiere Pro

  - [](/topics/indesign-classes-nyc)
    ::: {.menu-item\_\_icon .id}
    :::

    InDesign

  - [](/topics/illustrator-classes-nyc)
    ::: {.menu-item\_\_icon .ai}
    :::

    Illustrator

  - [](/topics/adobe-classes-nyc)
    ::: {.menu-item\_\_icon .cc}
    :::

    Creative Cloud

  - [](/topics/video-editing-classes)
    ::: {.menu-item\_\_icon .video}
    :::

    Video Editing

  - [](/topics/motion-graphics-classes)
    ::: {.menu-item\_\_icon .motion-graphics}
    :::

    Motion Graphics

  - [](/topics/visual-design-classes)
    ::: {.menu-item\_\_icon .visual-design}
    :::

    Visual Design

  - [](/topics/ux-design-classes-nyc)
    ::: {.menu-item\_\_icon .ux}
    :::

    UX Design

  - [](/classes/figma-bootcamp)
    ::: {.menu-item\_\_icon .figma}
    :::

    Figma

  - [](/topics/sketch-classes-nyc)
    ::: {.menu-item\_\_icon .sketch}
    :::

    Sketch

  - [](/topics/adobe-xd-classes-nyc)
    ::: {.menu-item\_\_icon .xd}
    :::

    Adobe XD

  - [](/topics/autocad-courses-nyc)
    ::: {.menu-item\_\_icon .autocad}
    :::

    AutoCAD

  ```{=html}
  <!-- -->
  ```

  - All Design Classes & Certificates
    :::

- ::: {.site-nav-drawer\_\_accordion-header}
  Business
  :::

  ::: {.site-nav-drawer\_\_accordion-panel}

  - [](/topics/digital-marketing-courses)
    ::: {.menu-item\_\_icon .digital-marketing}
    :::

    Digital Marketing

  - [](/topics/seo-training-nyc)
    ::: {.menu-item\_\_icon .seo}
    :::

    SEO

  - [](/topics/google-analytics-training-nyc)
    ::: {.menu-item\_\_icon .google-analytics}
    :::

    Google Analytics

  - [](/topics/google-ads-training-nyc)
    ::: {.menu-item\_\_icon .google-ads}
    :::

    Google Ads

  - [](/topics/social-media-marketing-training-nyc)
    ::: {.menu-item\_\_icon .social-media}
    :::

    Social Media

  - [](/topics/data-analytics-training)
    ::: {.menu-item\_\_icon .data-science}
    :::

    Data Analytics

  - [](/topics/excel-courses)
    ::: {.menu-item\_\_icon .excel}
    :::

    Excel

  - [](/topics/tableau-training-nyc)
    ::: {.menu-item\_\_icon .tableau}
    :::

    Tableau

  - [](/topics/powerpoint)
    ::: {.menu-item\_\_icon .pp}
    :::

    PowerPoint

  - [](/classes/financial-modeling-bootcamp)
    ::: {.menu-item\_\_icon .financial-modeling}
    :::

    Financial Modeling

  - [](/topics/finance-classes-nyc)
    ::: {.menu-item\_\_icon .financial-modeling}
    :::

    Finance

  - [](/topics/project-management-courses-nyc)
    ::: {.menu-item\_\_icon .project-management}
    :::

    Project Management

  ```{=html}
  <!-- -->
  ```

  - All Business Classes & Certificates
    :::

- ::: {.site-nav-drawer\_\_accordion-header}
  Certificates
  :::

  ::: {.site-nav-drawer\_\_accordion-panel}

  - [](/certificates/graphic-design)
    ::: {.menu-item\_\_icon .graphicdesign}
    :::

    Graphic Design

  - [](/certificates/motion-graphics)
    ::: {.menu-item\_\_icon .video}
    :::

    Motion Graphics

  - [](/classes/ux-ui-design-program)
    ::: {.menu-item\_\_icon .ux}
    :::

    UX & UI Design

  - [](/certificates/web-design)
    ::: {.menu-item\_\_icon .visual-design}
    :::

    Web Design

  - [](/certificates/social-media-marketing)
    ::: {.menu-item\_\_icon .social-media}
    :::

    Social Media

  - [](/certificates/digital-marketing)
    ::: {.menu-item\_\_icon .digital-marketing}
    :::

    Digital Marketing

  - [](/certificates/visual-design)
    ::: {.menu-item\_\_icon .visual-design}
    :::

    UI Design

  - [](/certificates/digital-design)
    ::: {.menu-item\_\_icon .graphicdesign}
    :::

    Digital Design

  - [](/certificates/video-editing)
    ::: {.menu-item\_\_icon .video}
    :::

    Video Editing

  - [](/certificates/data-science)
    ::: {.menu-item\_\_icon .data-science}
    :::

    Data Science

  - [](/certificates/front-end-web-development)
    ::: {.menu-item\_\_icon .front-end-web}
    :::

    Front-End Web

  - [](/certificates/full-stack-development)
    ::: {.menu-item\_\_icon .full-stack}
    :::

    Full-Stack Web

  - [](/certificates/javascript-development)
    ::: {.menu-item\_\_icon .jquery}
    :::

    JavaScript Development

  - [](/certificates/software-engineering-bootcamp)
    ::: {.menu-item\_\_icon .comp-sci}
    :::

    Software Engineering

  - [](/certificates/python-developer)
    ::: {.menu-item\_\_icon .python}
    :::

    Python Developer

  - [](/certificates/fintech-bootcamp-nyc)
    ::: {.menu-item\_\_icon .financial-modeling}
    :::

    FinTech

  - [](/certificates/cybersecurity)
    ::: {.menu-item\_\_icon .cybersecurity}
    :::

    Cybersecurity

  - [](/certificates/data-analytics)
    ::: {.menu-item\_\_icon .tableau}
    :::

    Data Analytics

  ```{=html}
  <!-- -->
  ```

  - Find & Compare Certificates by Topic
    :::

- ::: {.site-nav-drawer\_\_accordion-header}
  Corporate Training
  :::

  ::: {.site-nav-drawer\_\_accordion-panel}

  - [![](){.lazyload .menu-item**icon .site-nav-drawer**menu-item}
    Excel](/corporate-training/excel)
  - [![](){.lazyload .menu-item**icon .site-nav-drawer**menu-item}
    SQL](/corporate-training/sql)
  - [![](){.lazyload .menu-item**icon .site-nav-drawer**menu-item}
    Python](/corporate-training/python)
  - [![](){.lazyload .menu-item**icon .site-nav-drawer**menu-item}
    Data Science](/corporate-training/data-science)
  - [![](){.lazyload .menu-item**icon .site-nav-drawer**menu-item}
    Graphic Design](/corporate-training/graphic-design)
  - [![](){.lazyload .menu-item**icon .site-nav-drawer**menu-item}
    Web Design](/corporate-training/web-design)
  - [![](){.lazyload .menu-item**icon .site-nav-drawer**menu-item}
    Photoshop](/corporate-training/photoshop)
  - [![](){.lazyload .menu-item**icon .site-nav-drawer**menu-item}
    After Effects](/corporate-training/after-effects)
  - [![](){.lazyload .menu-item**icon .site-nav-drawer**menu-item}
    Video Editing](/corporate-training/video-editing)
  - [![](){.lazyload .menu-item**icon .site-nav-drawer**menu-item}
    Digital Marketing](/corporate-training/digital-marketing)
  - [![](){.lazyload .menu-item**icon .site-nav-drawer**menu-item}
    Data Analytics](/corporate-training/data-analytics)
  - [![](){.lazyload .menu-item**icon .site-nav-drawer**menu-item}
    Adobe](/corporate-training/adobe)
  - [![](){.lazyload .menu-item**icon .site-nav-drawer**menu-item}
    Microsoft Office](/corporate-training/microsoft-office)
  - [![](){.lazyload .menu-item**icon .site-nav-drawer**menu-item}
    Project Management](/corporate-training/project-management)

  ```{=html}
  <!-- -->
  ```

  - All Corporate Training
    :::

::: {.site-nav-drawer\_\_accordion-header}
More
:::

::: {.site-nav-drawer\_\_accordion-panel}

- [Compare](/compare)
- [Schedule](/schedule)
- [Classes Near Me](/classes-near-me)
- [FAQ](/faq)
- [Blog](/blog)
- [Workbooks](/books)
- [Free Seminars](/seminars)
- [High School Classes](https://nextgenbootcamp.com)
- [Resources](/learn)
- [Student Testimonials](/testimonials)
- [Student Showcase](/student-showcase)
- [Job Board](/jobs)
- [Evaluation](/evaluations/new)
- [Course Catalog](/courses)
- [Instructors](/instructors)
  :::
  :::

::: {.site-nav-drawer**section .site-nav-drawer**section--contact}

### Contact Us {#contact-us .text-md .font-body .margin-top-0}

- (212) 226-4149
- hello\@nobledesktop.com
  :::
  :::
  :::

::: {.drawer-background-overlay}
:::
:::

::: {.header-background header-style="static_image"}
::: {.header-background\_\_image-container}
![](){sizes="(max-width: 400px) 384px, 1280px"
srcset="https://www.nobledesktop.com/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBaDRjIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--3273ee745fa3295ef6835b34540e3c15c065f126/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdDRG9MWm05eWJXRjBPZ2hxY0djNkMzSmxjMmw2WlVraUNEWTBNQVk2QmtWVU9neHhkV0ZzYVhSNVNTSUhOVEFHT3doVSIsImV4cCI6bnVsbCwicHVyIjoidmFyaWF0aW9uIn19--fd2e73ce84ba5f73a76beff539155af6d5c598d7/git-branches-merge.png 768w, https://www.nobledesktop.com/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBaDRjIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--3273ee745fa3295ef6835b34540e3c15c065f126/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdDRG9MWm05eWJXRjBPZ2hxY0djNkMzSmxjMmw2WlVraUNUSTFOakFHT2daRlZEb01jWFZoYkdsMGVVa2lCelV3QmpzSVZBPT0iLCJleHAiOm51bGwsInB1ciI6InZhcmlhdGlvbiJ9fQ==--7a247d8759fd4fe71602cd6e8392505f2a0c464f/git-branches-merge.png 2560w"}
:::

::: {.header-background\_\_image-overlay}
:::
:::

::: {.container}
::: {#article-heading}
Git Branches: List, Create, Switch to, Merge, Push, & Delete
============================================================

Git Tips & Commands
:::
:::

::: {role="main"}
::: {.entry-section .blog-entry-layout-grid}
::: {.entry-meta\_\_date}
October 6, 2021
:::

::: {.entry-meta\_\_more}
Read more in [Git](/learn/git)
:::

::: {.entry-meta\_\_share}
Share

::: {.share-icons}
Share on Twitter

Share on Facebook

Share on LinkedIn
:::
:::

### Related Resources

- [How to Create a Git Repository: git
  init](/learn/git/create-git-repository)
- [Push to a Remote Repository: git push](/learn/git/push)
- [Pull From a Remote Repository: git pull & git
  fetch](/learn/git/pull-fetch)

```{=html}
<!-- -->
```

- [](/topics/web-development-courses-nyc){.resource-product
  .resource-product--with-icon}
  ::: {.resource-product**icon-wrapper}
  ![](){.resource-product**icon .lazyload}
  :::

  ::: {.resource-product**text}
  ::: {.resource-product**title}
  Web Development Courses
  :::
  :::

- [](/classes/front-end-web-development){.resource-product}
  ::: {.resource-product**image-wrapper}
  ![](){.resource-product**image .lazyload}
  :::

  ::: {.resource-product**text}
  ::: {.resource-product**title}
  Front-End Web Development Certificate
  :::

  ::: {.resource-product**quick-details}
  ::: {.resource-product**duration}
  108 hours
  :::

  ::: {.resource-product\_\_price}
  \$3,495
  :::
  :::
  :::

- [](/topics/web-design-classes){.resource-product}
  ::: {.resource-product**text}
  ::: {.resource-product**title}
  Web Design Classes
  :::
  :::
- [](/classes/web-design){.resource-product}
  ::: {.resource-product**image-wrapper}
  ![](){.resource-product**image .lazyload}
  :::

  ::: {.resource-product**text}
  ::: {.resource-product**title}
  Web Design Certificate
  :::

  ::: {.resource-product**quick-details}
  ::: {.resource-product**duration}
  162 hours
  :::

  ::: {.resource-product\_\_price}
  \$4,995
  :::
  :::
  :::

::: {.entry-content .padding-y-lg .max-width-sm .text-component}
::: {.entry-intro .text-component}
Git lets you branch out from the original code base. This lets you more
easily work with other developers, and gives you a lot of flexibility in
your workflow.
:::

![](/image/gitresources/git-branches-merge.png)

Here\'s an example of how Git branches are useful. Let\'s say you need
to work on a new feature for a website. You create a new branch and
start working. You haven\'t finished your new feature, but you get a
request to make a rush change that needs to go live on the site today.
You switch back to the master branch, make the change, and push it live.
Then you can switch back to your new feature branch and finish your
work. When you\'re done, you merge the new feature branch into the
master branch, and both the new feature and rush change are kept!

## For All the Commands Below

The commands below assume you\'ve navigated to the folder for the Git
repo.

### See What Branch You\'re On

- Run this command:
  - **git status**

### List All Branches

**NOTE:** The current local branch will be marked with an asterisk (\*).

- To see **local branches**, run this command:
  - **git branch**
- To see **remote branches**, run this command:
  - **git branch -r**
- To see **all local and remote branches**, run this command:
  - **git branch -a**

### Create a New Branch

- Run this command (replacing **my-branch-name** with whatever name
  you want):
  - **git checkout -b my-branch-name**
- You\'re now ready to commit to this branch.

### Switch to a Branch In Your Local Repo

- Run this command:
  - **git checkout my-branch-name**

### Switch to a Branch That Came From a Remote Repo

1.  To get a list of all branches from the remote, run this command:
    - **git pull**
2.  Run this command to switch to the branch:
    - **git checkout \--track origin/my-branch-name**

### Push to a Branch

- If your local branch **does not exist** on the remote, run either of
  these commands:
  - **git push -u origin my-branch-name**
  - **git push -u origin HEAD**

**NOTE:** HEAD is a reference to the top of the current branch, so it\'s
an easy way to push to a branch of the same name on the remote. This
saves you from having to type out the exact name of the branch!

- If your local branch **already exists** on the remote, run this
  command:
  - **git push**

### Merge a Branch

1.  You\'ll want to make sure your working tree is clean and see what
    branch you\'re on. Run this command:
    - **git status**
2.  First, you must check out the branch that you want to merge another
    branch into (changes will be merged into this branch). If you\'re
    not already on the desired branch, run this command:
    - **git checkout master**
    - **NOTE:** Replace **master** with another branch name as needed.
3.  Now you can merge another branch into the current branch. Run this
    command:
    - **git merge my-branch-name**
    - **NOTE:** When you merge, there may be a conflict. Refer to
      **Handling Merge Conflicts** (the next exercise) to learn what
      to do.

### Delete Branches

- To delete a **remote branch**, run this command:
  - **git push origin \--delete my-branch-name**
- To delete a **local branch**, run either of these commands:
  - **git branch -d my-branch-name**
  - **git branch -D my-branch-name**
- **NOTE:** The -d option only deletes the branch if it has already
  been merged. The -D option is a shortcut for \--delete \--force,
  which deletes the branch irrespective of its merged status.

## Grow Your Skills With Hands-on Classes

Learn Git with hands-on training:

- [Coding Classes NYC](/topics/coding-classes-nyc)
- [Python Classes NYC](/topics/python-classes-nyc)
- [Coding Classes Near Me](/classes-near-me/all/coding) or [Coding
  Classes Live Online](/classes-near-me/live-online/coding)
- [Python Classes Near Me](/classes-near-me/all/python) or [Python
  Classes Live Online](/classes-near-me/live-online/python)
  :::
  :::
  :::

::: {.related-entries-outer .padding-y-lg}
::: {.section .featured-entries}
::: {.container}
Related Resources {#related-resources-1 .margin-top-0 .margin-bottom-lg}

---

::: {.entries-grid .layout-3-entries}
[](/learn/git/create-git-repository){.entry-card}

::: {.thumbnail-container}
![](/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBdm9VIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--b3c5831be81af802233fbd3f405d57f1bd8bda38/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdDRG9MWm05eWJXRjBTU0lJY0c1bkJqb0dSVlE2QzNKbGMybDZaVWtpQ0Rnd01BWTdCbFE2REhGMVlXeHBkSGxwUVE9PSIsImV4cCI6bnVsbCwicHVyIjoidmFyaWF0aW9uIn19--a04cb320879366508952130c02d9150d899fde5b/terminal.png){.entry-thumbnail}
:::

::: {.card-content .text-component}

### How to Create a Git Repository: git init

A Git repository (or repo for short) contains all of the project les and
the entire revision history. Learn the Git command to make a repository.
:::

[](/learn/git/push){.entry-card}

::: {.thumbnail-container}
![](/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBaE1WIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--cc42af01496320f96802fa31531ab60d77fefcbb/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdDRG9MWm05eWJXRjBTU0lJY0c1bkJqb0dSVlE2QzNKbGMybDZaVWtpQ0Rnd01BWTdCbFE2REhGMVlXeHBkSGxwUVE9PSIsImV4cCI6bnVsbCwicHVyIjoidmFyaWF0aW9uIn19--a04cb320879366508952130c02d9150d899fde5b/Screen_Shot_2020-01-17_at_1.44.13_PM.png){.entry-thumbnail}
:::

::: {.card-content .text-component}

### Push to a Remote Repository: git push

After you have a remote repository set up, you upload (push) your files
and revision history to it.
:::

[](/learn/git/pull-fetch){.entry-card}

::: {.thumbnail-container}
![](/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBaFFWIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--c0789be579be64fbb30765d7f569772b3e674770/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdDRG9MWm05eWJXRjBTU0lJY0c1bkJqb0dSVlE2QzNKbGMybDZaVWtpQ0Rnd01BWTdCbFE2REhGMVlXeHBkSGxwUVE9PSIsImV4cCI6bnVsbCwicHVyIjoidmFyaWF0aW9uIn19--a04cb320879366508952130c02d9150d899fde5b/Screen_Shot_2020-01-17_at_1.57.57_PM.png){.entry-thumbnail}
:::

::: {.card-content .text-component}

### Pull From a Remote Repository: git pull & git fetch

After someone else makes changes to a remote repo, you can download
(pull) their changes into your local repo.
:::
:::
:::
:::
:::

Yelp

Facebook

LinkedIn

YouTube

Twitter

Instagram

::: {.container}
::: {.grid}
::: {.col-sm-6 .col-mdp-offset-1 .col-lg-2 .col-lg-offset-6}
::: {.contact}

### Contact Us

Office Hours:\
9am--6pm, Mon--Fri

(212) 226-4149

hello\@nobledesktop.com

Text us for customer support during business hours:

(212) 287-9140
:::
:::

::: {.col-sm-6 .col-mdp-offset-1 .col-lg-2 .col-lg-offset-8}
::: {.location}

### Location

#### In-Person in NYC {#in-person-in-nyc .margin-top-0}

185 Madison Avenue 3rd Floor\
New York, NY 10016

Campus Info

#### Live Online from Anywhere {#live-online-from-anywhere .margin-top-md}

Live Online Info
:::
:::

::: {.col-sm-10 .col-md-6 .col-mdp-offset-7 .col-lg-5 .col-lg-offset-1}
Noble Desktop is today's primary center for learning and career
development. Since 1990, our project-based classes and certificate
programs have given professionals the tools to pursue creative careers
in design, coding, and beyond. Noble Desktop is licensed by the New York
State Education Department.

::: {.social}
:::
:::

::: {.col-sm-6 .col-md-offset-1}
::: {.badges}
Adobe Certified Training Center
:::
:::

::: {.win-free-class .col-md-6 .col-lg-3 .col-lg-offset-10}

### Win a Free Class!

Sign up to get tips, free giveaways, and more in our weekly newsletter.

::: {.username_1660399697}
If you are a human, ignore this field
:::

::: {.input-flex-container}
:::

::: {.error-container}
:::
:::

::: {.copyright}
© 1998--2022 Noble Desktop - [Privacy &
Terms](/privacy){.privacy-policy}
:::
:::
:::
