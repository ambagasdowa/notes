---
title: "ProjectName ..."
author: "baizabal.jesus@gmail.com"
extensions:
  - image_ueberzug
  - qrcode
  - render
styles:
  style: solarized-dark
  table:
    column_spacing: 18
  margin:
    top: 1
    bottom: 0
  padding:
    top: 1
    bottom: 1
---

# IPTABLES

# iptables - Drop NAT rules based on rule/name, NOT rule number

https://unix.stackexchange.com/questions/606494/iptables-drop-nat-rules-based-on-rule-name-not-rule-number

Given any rule with `-I` (insert) or `-A` (append), you can repeat the rule definition with `-D` to delete it.
For your particular example, this will delete the first matching rule in the `OUTPUT` chain for the `nat` table

```
iptables -t nat -D OUTPUT -p tcp -o lo --dport 3306 -j DNAT --to-destination RMT_IP:3306
example
sudo iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 8081 -j DNAT --to-destination 192.168.20.236:80
to
sudo iptables -t nat -D PREROUTING -i eth0 -p tcp --dport 8081 -j DNAT --to-destination 192.168.20.236:80
```

If you need to easily toggle a rule on or off, how about making a separate chain for it?

```
iptables -t nat -N MySQLnat
iptables -t nat -A MySQLnat -j DNAT --to-destination RMT_IP:3306
```

Then create your rules in a slightly modified way:

```
iptables -t nat -A POSTROUTING -j MASQUERADE
iptables -t nat -A PREROUTING -p tcp --dport 3306 -j MySQLnat
iptables -t nat -I OUTPUT -p tcp -o lo --dport 3306 -j MySQLnat
```

Now you can easily erase the MySQL-specific DNAT rules for MySQL by emptying your custom chain:

```
iptables -t nat -F MySQLnat
```

An empty chain is the same as a chain that has just `-j RETURN` in it, so it does nothing and then continues processing the chain that jumped into the empty chain.

(In your position, I would be wary about deleting the conditionless `-j MASQUERADE` rule in the POSTROUTING chain, unless I was very sure nothing else depends on it.)

And when you need the MySQL DNAT again, simply put the contents of the chain back in:

```
iptables -t nat -A MySQLnat -j DNAT --to-destination RMT_IP:3306
```
