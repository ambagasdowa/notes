#!/usr/bin/env python3
"""
Example code on how to toggle Wi-Fi on and off:
python3 toggle_wifi.py http://admin:PASSWORD@192.168.8.1/ 1
python3 toggle_wifi.py http://admin:PASSWORD@192.168.8.1/ 0
"""
from huawei_lte_api.enums.device import ModeEnum
from huawei_lte_api.enums.client import ResponseEnum
from huawei_lte_api.Client import Client
from huawei_lte_api.Connection import Connection
# from argparse import ArgumentParser
import os.path
import pprint
import sys
from typing import Any, Callable
sys.path.insert(0, os.path.join(os.path.dirname(__file__), os.path.pardir))


# parser = ArgumentParser()
# parser.add_argument('url', type=str)
# # parser.add_argument('enabled', type=bool)
# parser.add_argument('--username', type=str)
# parser.add_argument('--password', type=str)
# args = parser.parse_args()

url = 'http://admin:Telcel1974@192.168.255.1/'
# args.username = 'admin'
# args.password = "Telcel1974"


def dump(method: Callable[[], Any]) -> None:
    print("==== %s" % method.__qualname__)
    try:
        pprint.pprint(method())
    except Exception as e:
        print(str(e))
    print("")


# with Connection(args.url, username=args.username, password=args.password) as connection:
with Connection(url) as connection:
    client = Client(connection)

    dump(client.device.information)
    dump(client.device.autorun_version)
    dump(client.device.device_feature_switch)
    dump(client.device.basic_information)
    dump(client.device.basicinformation)

    # if client.wlan.wifi_network_switch(args.enabled) == ResponseEnum.OK.value:
    #     print('Wi-Fi was toggled successfully')
    # else:
    #     print('Error')


# 8 rancheros [4 rojos, 4 verdes]
# 3 mole
# 3 verdes
# 3 frijol
# 5 chile seco
