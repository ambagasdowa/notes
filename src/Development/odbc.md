---
title: "Status of the Project Inventory"
author: "baizabal.jesus@gmail.com"
extensions:
  - image_ueberzug
  - qrcode
  - render
styles:
  style: solarized-dark
  table:
    column_spacing: 15
  margin:
    top: 3
    bottom: 0
  padding:
    top: 3
    bottom: 3
---

# Install odbc stuff

# Install pre-requesite packages

sudo apt-get install unixodbc unixodbc-dev freetds-dev freetds-bin tdsodbc

Point odbcinst.ini to the driver in /etc/odbcinst.ini:

[FreeTDS]
Description = v0.91 with protocol v7.2
Driver = /usr/lib/x86_64-linux-gnu/odbc/libtdsodbc.so

Create your DSNs in odbc.ini:

[dbserverdsn]
Driver = FreeTDS
Server = dbserver.domain.com
Port = 1433
TDS_Version = 7.2

...and your DSNs in freetds.conf:

[global] # TDS protocol version, use: # 7.3 for SQL Server 2008 or greater (tested through 2014) # 7.2 for SQL Server 2005 # 7.1 for SQL Server 2000 # 7.0 for SQL Server 7
tds version = 7.2
port = 1433

    # Whether to write a TDSDUMP file for diagnostic purposes
    # (setting this to /tmp is insecure on a multi-user system)

; dump file = /tmp/freetds.log
; debug flags = 0xffff

    # Command and connection timeouts

; timeout = 10
; connect timeout = 10

    # If you get out-of-memory errors, it may mean that your client
    # is trying to allocate a huge buffer for a TEXT field.
    # Try setting 'text size' to a more reasonable limit
    text size = 64512

# A typical Microsoft server

[dbserverdsn]
host = dbserver.domain.com
port = 1433
tds version = 7.2

After completing this, you can test your connection by attempting to connect with tsql (to test the FreeTDS layer) and isql (for the unixODBC through FreeTDS stack).

> notes
> Ubuntu 20.04 PHP 7.4 using the 19.10 drivers for sqlsrv

We have a real old SQL server out there . The SQL Server show's version 10.50.2550.0, i think it's SQL Server 2008 R2. I had to use TLSv1 to connect to the server. I also had to do a "systemctl restart apache2" to get it to take affect. TLSv1.1 did not work with my MSSQL server version.

Error message: Connection failed: SQLSTATE[08001]: [Microsoft][odbc driver 17 for sql server]SSL Provider: [error:1425F102:SSL routines:ssl_choose_client_version:unsupported protocol]Database Connection Error

edit: /etc/ssl/openssl.cnf
1st line in the file added
openssl_conf = default_conf

End of file added

[default_conf]
ssl_conf = ssl_sect

[ssl_sect]
system_default = system_default_sect

[system_default_sect]
MinProtocol = TLSv1
CipherString = DEFAULT@SECLEVEL=1

Not 100% sure why i had to restart apache2 for it to take effect, but I had to.

systemctl restart apache2

reloaded the page and it works

# ID

If you're using SQLAlchemy with an engine, then you can retrieve the PyODBC cursor like this before running the query and fetching the table ID.

    connection = sql_alchemy_engine.raw_connection()
    cursor = connection.cursor()
    result = cursor.execute(
        """
        INSERT INTO MySchema.MyTable (Col1, Col2) OUTPUT INSERTED.MyTableId
        VALUES (?, ?);
        """,
        col1_value,
        col2_value,
    )
    myTableId = cursor.fetchone()[0]
    cursor.commit()
    print("my ID is:", myTableId)
