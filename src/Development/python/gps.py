#! /usr/bin/env python3
"""
example  Python gpsd client
run this way: python3 example1.py.txt
"""

# # import gps               # the gpsd interface module
# import os
# from gps import *
# from time import *
# import time
# import threading


# session = gps.gps(mode=gps.WATCH_ENABLE)

# try:
#     while 0 == session.read():
#         if not (gps.MODE_SET & session.valid):
#             # not useful, probably not a TPV message
#             continue

#         print('Mode: %s(%d) Time: ' %
#               (("Invalid", "NO_FIX", "2D", "3D")[session.fix.mode],
#                session.fix.mode), end="")
#         # print time, if we have it
#         if gps.TIME_SET & session.valid:
#             print(session.fix.time, end="")
#         else:
#             print('n/a', end="")

#         if ((gps.isfinite(session.fix.latitude) and
#              gps.isfinite(session.fix.longitude))):
#             print(" Lat %.6f Lon %.6f" %
#                   (session.fix.latitude, session.fix.longitude))
#         else:
#             print(" Lat n/a Lon n/a")

# except KeyboardInterrupt:
#     # got a ^C.  Say bye, bye
#     print('')

# # Got ^C, or fell out of the loop.  Cleanup, and leave.
# session.close()
# exit(0)

from gpsdclient import GPSDClient

# get your data as json strings:
with GPSDClient(host="127.0.0.1") as client:
    for result in client.json_stream():
        print(result)

# or as python dicts (optionally convert time information to `datetime` objects)
with GPSDClient() as client:
    for result in client.dict_stream(convert_datetime=True, filter=["TPV"]):
        print("Latitude: %s" % result.get("lat", "n/a"))
        print("Longitude: %s" % result.get("lon", "n/a"))

# you can optionally filter by report class
with GPSDClient() as client:
    for result in client.dict_stream(filter=["TPV", "SKY"]):
        print(result)
