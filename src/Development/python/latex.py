from __future__ import division
from sympy import init_printing
from sympy import *
init_printing()


x, y, z, t = symbols('x y z t')
k, m, n = symbols('k m n', integer=True)
f, g, h = symbols('f g h', cls=Function)
init_printing()  # doctest: +SKIP


formula = latex(Integral(sqrt(1/x), x))


print(formula)


pprint(Integral(sqrt(1/x), x), use_unicode=True)
