from functools import singledispatch


@singledispatch
def func(arg1, arg2):
    print("default implementation of func - ", arg1, arg2)


@func.register(str)
def func_impl_1(arg1, arg2):
    print("Implementation of func with first argument as string - ", arg1, arg2)


@func.register(int)
@func.register(float)
def func_impl_2(arg1, arg2):
    print("Implementation of func with first argument as integer or float - ", arg1, arg2)


print("The different implementations of the func are")
print(func.registry)

print("String implementation:")
print(func.registry[str])
