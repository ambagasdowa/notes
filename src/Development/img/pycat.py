import ueberzug.lib.v0 as ueberzug
import time
import sys


# def main():
#
#    for filename in sys.argv[1:]:
#
#        with ueberzug.Canvas() as c:
#            paths = [filename]
#            demo = c.create_placement(
#                'demo', x=0, y=0, scaler=ueberzug.ScalerOption.COVER.value)
#            demo.path = paths[0]
#            demo.visibility = ueberzug.Visibility.VISIBLE
#
#            for i in range(30):
#                with c.lazy_drawing:
#                    demo.x = i * 3
#                    demo.y = i * 3
#                    demo.path = paths[i % 2]
#                time.sleep(1/30)
#
#            time.sleep(2)
#
#
# if __name__ == "__main__":
#    main()


if __name__ == '__main__':
    with ueberzug.Canvas() as c:
        paths = ['/home/ambagasdowa/captures/provider.png',
                 '/home/ambagasdowa/captures/agent_msi.png']
        demo = c.create_placement(
            'demo', x=0, y=0, scaler=ueberzug.ScalerOption.COVER.value)
        demo.path = paths[0]
        demo.visibility = ueberzug.Visibility.VISIBLE

        for i in range(30):
            with c.lazy_drawing:
                demo.x = i * 3
                demo.y = i * 3
                demo.path = paths[i % 2]
            time.sleep(1/30)

    time.sleep(2)
