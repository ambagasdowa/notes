---
title: "Status of the Project Inventory"
author: "baizabal.jesus@gmail.com"
extensions:
  - image_ueberzug
  - qrcode
  - render
styles:
  style: solarized-dark
  table:
    column_spacing: 15
  margin:
    top: 3
    bottom: 0
  padding:
    top: 3
    bottom: 3
---

DotNet6 + Debian

Para Debian 11

- Paquetes con las claves de firma (llaves de los repositorios)

```bash
wget https://packages.microsoft.com/config/debian/11/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb
rm packages-microsoft-prod.deb
```

- Instalación del SDK

```bash
sudo apt update && \
  sudo apt install -y dotnet-sdk-6.0
```

- Instalación del Runtime

```bash
sudo apt update && \
  sudo apt install -y aspnetcore-runtime-6.0
```

- Instalación del Core Runtime de ASP.NET

```bash
sudo apt install -y dotnet-runtime-6.0
```
