
local lunajson = require 'lunajson'

local jsonstr = '{"USD_MXN":19.7895,"EUR_MXN":20.969708}'
local t = lunajson.decode(jsonstr)
print(t.USD_MXN) -- prints 1.5
print(t.EUR_MXN) -- prints 1.5
print(lunajson.encode(t)) -- prints {"Hello":["lunajson",1.5]}
