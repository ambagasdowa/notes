-- trigger example of writing a cell content to sqlite3 database
-- use sqlite3 to create in /tmp/db.sql3
-- CREATE TABLE account ( date DATETIME, val REAL);
-- watch account table when defining trigger in scim
-- place this file in /usr/local/share/scim/lua
-- define trigger as follow : trigger a5 "mode=W type=LUA file=trg_sql3.lua function=trg"
-- when ever something writen to a5, it will be inserted into account table


http = require("socket.http")
mime = require("mime")
lunajson = require 'lunajson'
date = require 'date'

function split(str, sep)
   local result = {}
   local regex = ("([^%s]+)"):format(sep)
   for each in str:gmatch(regex) do
      table.insert(result, each)
   end
   return result
end


function initBankAccounts(c,r)

-- type (v)
-- Returns the type of its only argument, coded as a string.
-- The possible results of this function are "nil" (a string, not the value nil), 
-- "number", "string", "boolean", "table", "function", "thread", and "userdata".
--


    local mark = os.date("%d-%m-%Y %X")

    file = assert(io.open('/tmp/log.txt', 'a+'))
    file:write(string.format("Initializing log at %s \n",mark))

    
    tblfields='ID,Beneficiary,Clabe,Bank,CardNumber,Account'
    
    tbl=split(tblfields,',')
    
    for x,a in ipairs(tbl) do
        file:write(string.format("%d %s \n",x,a))
    end



    init =1
    limit=120
    step = 1

    for i=init,limit,step do

        cv = sc.lgetnum(0,i)

        cs = sc.lgetstr(0,i)
        -- Read the cell and eval if is not null and its a number
        if cv ~= nil and cv > 0 then
            trg(0,i)

            file:write(string.format("%s -- Check value %s is %d  \n",mark,sc.colrow2a(0,i),cv))
        end
        -- Read the label of the cell
        if cs ~= nil and cs ~= '' then
            -- tgr(0,i,'Sheet1') 
            file:write(string.format("%s -- Check string %s is %s \n",mark,sc.colrow2a(0,i),cs))
        end

    end
    file:flush()

end


function searchBankAccount(c, r )

    -- if st ~= nil and type(st) == "string"  then

    --     page = st
    --     val=sc.lgetstr(c,r)

    -- else

        page='BankAccounts'
        val=sc.lgetnum(c,r)

    -- end
    -- Set the default
    -- print(string.format(" Page %s, row = %d ",page,r))

        tblrange='A1:F19'
        tblfields='ID, Beneficiary, Clabe, Bank, CardNumber, Account'
        rcol=1 -- Means B
        initrow=1
        endrow=19
        xmpty=''


        for xc=c,2,1 do
            -- set the row for write 
            xc = xc+1
            xcol = sc.colrow2a(rcol,val)

            if val == 0 then
                beneficiary = "\"\""
            else
                --beneficiary=string.format("@substr({\"%s\"}!%s,1,@slen({\"%s\"}!%s))",page,xcol,page,xcol)
                beneficiary=string.format("{\"%s\"}!%s",page,xcol)
            end 

            xcell = sc.colrow2a(xc,r)
                sc.sc(string.format(" LABEL %s = %s",xcell,beneficiary))
            -- Go to next column 
            rcol = rcol + 1

        end

    sc.sc("RECALC")

end

function searchInvoice(c, r )


        page='Invoices'
        val=sc.lgetnum(c,r)


        tblrange='A5:L99'
        tblfields='ID,ItemSat,NextInvoice,Description,CodeName,Qty,Provider,UnitPrice,Amount,PF,Active'
        rcol=1 -- Means B
        initrow=5
        endrow=99
        xmpty=''


        for xc=c,2,1 do
            -- set the row for write 
            xc = xc+1
            xcol = sc.colrow2a(rcol,val)

            if val == 0 then
                beneficiary = "\"\""
            else
                --beneficiary=string.format("@substr({\"%s\"}!%s,1,@slen({\"%s\"}!%s))",page,xcol,page,xcol)
                beneficiary=string.format("{\"%s\"}!%s",page,xcol)
            end 

            xcell = sc.colrow2a(xc,r)
                sc.sc(string.format(" LABEL %s = %s",xcell,beneficiary))
            -- Go to next column 
            rcol = rcol + 1

        end

    sc.sc("RECALC")

end


