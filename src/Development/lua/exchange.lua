
-- load required modules
http = require("socket.http")
mime = require("mime")
lunajson = require 'lunajson'
date = require "date"
--require "lfs"



function split(str, sep)
   local result = {}
   local regex = ("([^%s]+)"):format(sep)
   for each in str:gmatch(regex) do
      table.insert(result, each)
   end
   return result
end

function file_exists(filename)

    if not io.open(filename, "r") then

      str = 'File :'..filename..' doesn\'t exist'
      log = assert(io.open('/tmp/log.txt', 'a+'))
      log:write(str)
      log:flush()
     return false

   else
      str = 'File :'..filename..' exist'
      log = assert(io.open('/tmp/exchange.log', 'a+'))
      log:write(string.format("%s \n",str))
      log:flush()
     return true
   end
end


function trg(c,r)
-- body = {"USD_MXN":19.786504,"EUR_MXN":20.972489}
   local mark = os.date("%Y-%m-%d")
   xfile = "/tmp/exchange_"..mark..".json"
   log = assert(io.open('/tmp/exchange.log', 'a+'))

   if ( file_exists(xfile) ) then
      file = assert(io.open(xfile, "rb"))
   else
      file = assert(io.open(xfile,'a+'))
      body, z,l,h = http.request("https://free.currconv.com/api/v7/convert?q=USD_MXN,EUR_MXN&compact=ultra&apiKey=75e40f4f336389035fd9")
     -- body = '{"USD_MXN":19.786504,"EUR_MXN":20.972489}'
      file:write(body)
      file:flush()
   end

   local jsonString = file:read "*a"
    
      local exchange = lunajson.decode(jsonString)
      usdmxn = exchange.USD_MXN
      eurmxn = exchange.EUR_MXN
      sc.lsetstr(1,0,'USD')
      sc.lsetnum(2,0,usdmxn)
      sc.lsetstr(3,0,'EUR')
      sc.lsetnum(4,0,eurmxn)

   log:write(string.format("%d %d  \n",c,r))
   log:write(usdmxn)
   log:flush()
   log:close()
   file:close()
end



