#!/usr/bin/python3.9

import pygame
from sys import exit



class Player(pygame.sprite.Sprite):
    def __init__(self,pos_x,pos_y):
        super().__init__()
        self.attack_animation = False
        self.sprites = []
        self.sprites.append(pygame.image.load('gif/source/tifa-0.png'))
        self.sprites.append(pygame.image.load('gif/source/tifa-1.png'))
        self.sprites.append(pygame.image.load('gif/source/tifa-2.png'))
        self.sprites.append(pygame.image.load('gif/source/tifa-3.png'))
        self.sprites.append(pygame.image.load('gif/source/tifa-4.png'))
        self.sprites.append(pygame.image.load('gif/source/tifa-5.png'))
        self.sprites.append(pygame.image.load('gif/source/tifa-6.png'))
        self.sprites.append(pygame.image.load('gif/source/tifa-7.png'))
        self.sprites.append(pygame.image.load('gif/source/tifa-8.png'))
        self.sprites.append(pygame.image.load('gif/source/tifa-9.png'))
        self.sprites.append(pygame.image.load('gif/source/tifa-10.png'))
        self.sprites.append(pygame.image.load('gif/source/tifa-11.png'))

        self.current_sprite = 0 
        self.image = self.sprites[self.current_sprite]

        self.rect = self.image.get_rect()
        self.rect.topleft = [pos_x,pos_y]
    

    def animate(self):
        self.is_animating = True




    def update(self):
        self.current_sprite += 1
        if self.current_sprite >= len(self.sprites):
            self.current_sprite = 0
            self.is_animating = False

        self.image = self.sprites[self.current_sprite]



#initializing pygame
pygame.init()


#Colours
white = ((255,255,255))
blue = ((0,0,255))
green = ((0,255,0))
red = ((255,0,0))
black = ((0,0,0))
orange = ((255,100,10))
yellow = ((255,255,0))
blue_green = ((0,255,170))
marroon = ((115,0,0))
lime = ((180,255,100))
pink = ((255,100,180))
purple = ((240,0,255))
gray = ((127,127,127))
magenta = ((255,0,230))
brown = ((100,40,0))
forest_green = ((0,50,0))
navy_blue = ((0,0,100))
rust = ((210,150,75))
dandilion_yellow = ((255,200,0))
highlighter = ((255,255,100))
sky_blue = ((0,255,255))
light_gray = ((200,200,200))
dark_gray = ((50,50,50))
tan = ((230,220,170))
coffee_brown =((200,190,140))
moon_glow = ((235,245,255))
background_blue = ((192, 232, 224))


# Creating Sprites and Groups

moving_sprites = pygame.sprite.Group()
player = Player(10,10)
moving_sprites.add(player)




# Draw Canvas
screen = pygame.display.set_mode((800,400))

pygame.display.set_caption('AmbaGame')
clock = pygame.time.Clock()
test_font = pygame.font.Font('font/Pixeltype.ttf', 50)


# Surfaces
sky_surface = pygame.image.load('graphics/Sky.png').convert()
ground_surface = pygame.image.load('graphics/ground.png').convert()

score_surf = test_font.render('Elephantom Universe', False, moon_glow)
score_rect = score_surf.get_rect(center = (400,50))

#Snail Enemy
snail_surf = pygame.image.load('graphics/snail/snail1.png').convert_alpha()
snail_rect = snail_surf.get_rect(bottomright = (600,300))

#Sprites

#player_surf = pygame.image.load('graphics/Player/player_walk_1.png').convert_alpha()
#player_surf = pygame.image.load('gif/source.gif').convert_alpha()
#player_rect = player_surf.get_rect( midbottom = (80,300))


while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
                pygame.quit()
                exit()
        
#        if event.type == pygame.MOUSEMOTION
#            if player_rect.collidepoint(event.pos): print('buum')
            
screen.blit(sky_surface,(0,0))
screen.blit(ground_surface,(0,300))

pygame.draw.rect(screen,background_blue,score_rect)
pygame.draw.rect(screen,background_blue,score_rect,10)
#   pygame.draw.ellipse(screen,brown,score_rect,10)


screen.blit(score_surf,score_rect)




#moving enemy
snail_rect.x -= 2
if snail_rect.right <= 0 : snail_rect.left = 800

screen.blit(snail_surf,snail_rect)
screen.blit( player_surf, player_rect )

# Moving Player
moving_sprites.draw(screen)
moving_sprites.update()

# Controller 
print(pygame.key.get_pressed())

# Collisions 
if player_rect.colliderect(snail_rect):
    print('Collision')

mouse_pos = pygame.mouse.get_pos()
if snail_rect.collidepoint(mouse_pos):
    print(pygame.mouse.get_pressed())

    #draw all our elements 
    #update everything
pygame.display.update()
clock.tick(60)



