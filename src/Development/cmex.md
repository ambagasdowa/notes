---
title: "Cmex Project WS"
author: "baizabal.jesus@gmail.com"
extensions:
  - image_ueberzug
  - qrcode
  - render
styles:
  style: solarized-dark
  table:
    column_spacing: 15
  margin:
    top: 3
    bottom: 0
  padding:
    top: 3
    bottom: 3
---

# Cotizacion:

> Project
> cmex tralix / Copiloto --> Michelin

## Etapas:

- consumo de datos: 50%
  - Complejidad : medium
- Integracion en ZAM:50%
  - Complejidad : very Hard
- Insertion in Solomon: extra
  - insert in deskimport to desk

## Projection Costs:

$ [160 ~ 180]

## test Command

```bash
  https -vvv -d transportescp.xsa.com.mx:9050/5365d430-32dc-4f0a-8725-905aeb373c1b/descargasCfdi representacion==XML pageSize==50
```
