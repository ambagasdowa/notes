Here are some alternative suggestions (mostly in `awk`), including some
advanced use cases, like extracting version numbers for software packages.

Just note that with a slightly different input some of these may fail,
therefore anyone using these should validate on their expected input and
adapt the regex expression if required.

```bash
f='/path/to/complex/file.1.0.1.tar.gz'

# Filename : 'file.1.0.x.tar.gz'
    echo "$f" | awk -F'/' '{print $NF}'

# Extension (last): 'gz'
    echo "$f" | awk -F'[.]' '{print $NF}'

# Extension (all) : '1.0.1.tar.gz'
    echo "$f" | awk '{sub(/[^.]*[.]/, "", $0)} 1'

# Extension (last-2): 'tar.gz'
    echo "$f" | awk -F'[.]' '{print $(NF-1)"."$NF}'

# Basename : 'file'
    echo "$f" | awk '{gsub(/.*[/]|[.].*/, "", $0)} 1'

# Basename-extended : 'file.1.0.1.tar'
    echo "$f" | awk '{gsub(/.*[/]|[.]{1}[^.]+$/, "", $0)} 1'

# Path : '/path/to/complex/'
    echo "$f" | awk '{match($0, /.*[/]/, a); print a[0]}'
    # or
    echo "$f" | grep -Eo '.*[/]'

# Folder (containing the file) : 'complex'
    echo "$f" | awk -F'/' '{$1=""; print $(NF-1)}'

# Version : '1.0.1'
    # Defined as 'number.number' or 'number.number.number'
    echo "$f" | grep -Eo '[0-9]+[.]+[0-9]+[.]?[0-9]?'

    # Version - major : '1'
    echo "$f" | grep -Eo '[0-9]+[.]+[0-9]+[.]?[0-9]?' | cut -d. -f1

    # Version - minor : '0'
    echo "$f" | grep -Eo '[0-9]+[.]+[0-9]+[.]?[0-9]?' | cut -d. -f2

    # Version - patch : '1'
    echo "$f" | grep -Eo '[0-9]+[.]+[0-9]+[.]?[0-9]?' | cut -d. -f3

# All Components : "path to complex file 1 0 1 tar gz"
    echo "$f" | awk -F'[/.]' '{$1=""; print $0}'

# Is absolute : True (exit-code : 0)
    # Return true if it is an absolute path (starting with '/' or '~/'
    echo "$f" | grep -q '^[/]\|^~/'
```

> Example

1_40 Visiones de María Valtorta La Vida Oculta El Evangelio como me ha sido Revelado.m4a
2_40 Visiones de María Valtorta La Vida Oculta El Evangelio como me ha sido Revelado.m4a
21_40 María Valtorta Tercer Año de la Vida Pública de Jesús.m4a
39_40 MARÍA VALTORTA — GLORIFICACIÓN DE JESÚS Y MARÍA — PARTE II.m4a
40_40 ÚLTIMA PARTE DE MARÍA VALTORTA — GLORIFICACIÓN DE JESÚS Y MARÍA — PARTE III.m4a

ls -1v |grep .m4a | grep -Eo '[0-9]+[_]?[0-9]+[_]?'| cut -d\_ -f1

```bash
1
2
#...
38
39
40

```

python3 -m music_tag --set "Title:Maria Valtorta" \*.m4a

find ./path/to/file(s).zip -iname \*.zip -type f -print -exec unzip -d /dir/to/extract '{}' \;

/home/ambagasdowa/github/taglib/build/bin/tagwriter -a "Iker Jimenez" -A "Milenio 3" -g "Misterio" m3_25julio2005_346.wma:e

~% FILE="example.tar.gz"

~% echo "${FILE%%.\*}"
example

~% echo "${FILE%.\*}"
example.tar

~% echo "${FILE#\*.}"
tar.gz

~% echo "${FILE##\*.}"
gz

First, get file name without the path:

filename=$(basename -- "$fullfile")
extension="${filename##*.}"
filename="${filename%.\*}"
Alternatively, you can focus on the last '/' of the path instead of the '.' which should work even if you have unpredictable file extensions:

filename="${fullfile##\*/}"
You may want to check the documentation :

On the web at section [ShellParamerter](http://www.gnu.org/software/bash/manual/html_node/Shell-Parameter-Expansion.html "3.5.3 Shell Parameter Expansion")
In the bash manpage at section called "Parameter Expansion"

As a followup to mouviciel's answer, you could also do this as a for loop, instead of using xargs. I often find xargs cumbersome, especially if I need to do something more complicated in each iteration.

for f in $(find /tmp -name '_.pdf' -or -name '_.doc'); do rm $f; done
As a number of people have commented, this will fail if there are spaces in filenames. You can work around this by temporarily setting the IFS (internal field seperator) to the newline character. This also fails if there are wildcard characters \[?\* in the file names. You can work around that by temporarily disabling wildcard expansion (globbing).

IFS=$'\n'; set -f
for f in $(find /tmp -name '*.pdf' -or -name '*.doc'); do rm "$f"; done
unset IFS; set +f
If you have newlines in your filenames, then that won't work either. You're better off with an xargs based solution:

find /tmp \( -name '_.pdf' -or -name '_.doc' \) -print0 | xargs -0 rm
(The escaped brackets are required here to have the -print0 apply to both or clauses.)

GNU and \*BSD find also has a -delete action, which would look like this:

find /tmp \( -name '_.pdf' -or -name '_.doc' \) -delete

```bash
IFS=$'\n'; set -f ;
    for i in $(find ./ -iname *.gdi) ;
        do chdman createcd --input "$i" --output /media/storage/data/Roms/Dreamcast/chd/"${i%%.*}.chd" ;
    done ;
unset IFS; set +f
```

```bash
IFS=$'\n'; set -f ;
	for i in $(find ./ -iname *.gdi) ;
		do chdman createcd --input "$i" --output chd/"${i%\/*.*}.chd" ; # takes dir name
	done ;
unset IFS; set +f
```

$ find /home/data -type f -print0
We also use -type f to specify that we want to search for only files and not folders.

The above command will display a list of all relative file paths. We use the output of above command in a for loop to iterate through these files and work with them. In the following code, you can fill the part between do…done with code to work with files. We use -print0 option to display all filenames even if it includes spaces and other special characters. If you use only -print option, it will not work with files with spaces & special characters.

for i in $(find . -type f -print0)
do
#code to perform task on each file
done
If you want to find specific types of files such as pdf files, you can use -name option in your find command as shown below. For the name option, you can specify the file extension name formats using wildcard characters.

for i in $(find . -type f -print0 -name "\*.pdf")
do

    #code to perform task on each file

done
Similarly, here is an example to search for pdf and .doc files in your folder.

for i in $(find . -type f -print0 -name "\*.pdf" or -name ".doc")
do

    #code to perform task on each file

done
You can run the above command directly on terminal, or add it as a part of your shell script.
