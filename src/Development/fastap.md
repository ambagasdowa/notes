---
title: "FastApi Notes"
author: "baizabal.jesus@gmail.com"
extensions:
  - image_ueberzug
  - qrcode
  - render
styles:
  style: solarized-dark
  table:
    column_spacing: 15
  margin:
    top: 3
    bottom: 0
  padding:
    top: 3
    bottom: 3
---

# Starting FastApi

```bash
uvicorn main:app --reload
# Open your browser at http://127.0.0.1:8000.
```

# Configuring ORM SqlAlchemy

# Integration with Panamericano Project
