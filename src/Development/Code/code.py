#!/usr/bin/python3.9


# Un object es una entidad que agrupa un estado y una funcionalidad relacionadas.
#
# El estado del objeto se define a travez de variables llamadas atributos ,
#    mientras que la
#
#    Funcionalidad se modela a travez de funciones a las que se les conoce con el nombre de metodos del objeto.
#
#    Una Clase , no es mas que una plantilla generica a partir de la cual instanciar los objetos
#
#    Plantilla es la que define que atributos y metodos tendran los objetos de esa clase

#
#    Object => Car
#    Attributes => brand , number od doors , type of gas
#    Methods => start,stop,Aceleration,


# Testing class

class firtsCode:
    def __init__(self, howMany):
        self.howMany = howMany
        print("Initialize the object class , this is the init method with param  ", howMany)

    def printing(self):
        for item in range(0, self.howMany):
            print(
                "This is a method inside of an class that print this item ", item, " times")

    def __hide(self):
        print("this never going to be show")


# initializing
code = firtsCode(8)
# execute a method from the class
code.printing()
# trying ...
# code._firstCode__hide()


# NOTE Multiple Herency

class terrestre:
    def desplazar(self):
        print(" El animal anda ... ")


class acuatico:
    def desplazar(self):
        print(" El animal nada ... ")


class cocodrile(terrestre, acuatico):
    pass


# NOTE Execute ...
# instanciating
coco = cocodrile()
# execute
coco.desplazar()


# Functional programming
# Superior Order Funtions or Lambda(anonimas)

def saludar(lang):
    def saludar_es():
        print('Hola!')

    def saludar_en():
        print('Hi!')

    def saludar_fr():
        print('Salut!')

    lang_func = {"es": saludar_es, "en": saludar_en, "fr": saludar_fr}

    return lang_func[lang]


# NOTE Execute ...
saludar("es")()

saludar('fr')()


# map(function,sequence[,sequence, ...])

def cuadrado(n):
    return n ** 2


l = [1, 2, 3, 4, 5, 6]
l2 = map(cuadrado, l)

for mapper in l2:
    print('map ', mapper)

# Filter(function,sequence)


def es_par(n):
    return (n % 2.0 == 0)


l = [1, 2, 3, 4, 5, 6, 7, 8, 9]
l2 = filter(es_par, l)


for par in l2:
    print('Filtering', par)


# reduce(function,sequence[,initial])

# def sumar(x, y):
#    return x + y
#
#
#l = [1, 2, 3]
#l2 = reduce(sumar, l)
#
#print('reduce', l2)


# NOTE lambda functions

l = [1, 2, 3, 4, 5, 6, 7, 8]
# lambda param:expression
l2 = filter(lambda n: n % 2.0 == 0, l)


for dot in l2:
    print('lambda ', dot)



#https://fastapi.tiangolo.com/async/#in-a-hurry

#Asynchronous Code¶
#
#Asynchronous code just means that the language 💬 has a way to tell the computer / program 🤖 that at some point in the code, it 🤖 will have to wait for something else to finish somewhere else. Let's say that something else is called "slow-file" 📝.
#
#So, during that time, the computer can go and do some other work, while "slow-file" 📝 finishes.
#
#Then the computer / program 🤖 will come back every time it has a chance because it's waiting again, or whenever it 🤖 finished all the work it had at that point. And it 🤖 will see if any of the tasks it was waiting for have already finished, doing whatever it had to do.
#
#Next, it 🤖 takes the first task to finish (let's say, our "slow-file" 📝) and continues whatever it had to do with it.
#
#That "wait for something else" normally refers to I/O operations that are relatively "slow" (compared to the speed of the processor and the RAM memory), like waiting for:
#
#    the data from the client to be sent through the network
#    the data sent by your program to be received by the client through the network
#    the contents of a file in the disk to be read by the system and given to your program
#    the contents your program gave to the system to be written to disk
#    a remote API operation
#    a database operation to finish
#    a database query to return the results
#    etc.
#
#As the execution time is consumed mostly by waiting for I/O operations, they call them "I/O bound" operations.
#
#It's called "asynchronous" because the computer / program doesn't have to be "synchronized" with the slow task, waiting for the exact moment that the task finishes, while doing nothing, to be able to take the task result and continue the work.
#
#Instead of that, by being an "asynchronous" system, once finished, the task can wait in line a little bit (some microseconds) for the computer / program to finish whatever it went to do, and then come back to take the results and continue working with them.
#
#For "synchronous" (contrary to "asynchronous") they commonly also use the term "sequential", because the computer / program follows all the steps in sequence before switching to a different task, even if those steps involve waiting.
#Concurrency and Burgers¶
#
#This idea of asynchronous code described above is also sometimes called "concurrency". It is different from "parallelism".
#
#Concurrency and parallelism both relate to "different things happening more or less at the same time".
#
#But the details between concurrency and parallelism are quite different.
#
#To see the difference, imagine the following story about burgers:
#Concurrent Burgers¶
#
#You go with your crush 😍 to get fast food 🍔, you stand in line while the cashier 💁 takes the orders from the people in front of you.
#
#Then it's your turn, you place your order of 2 very fancy burgers 🍔 for your crush 😍 and you.
#
#You pay 💸.
#
#The cashier 💁 says something to the cook in the kitchen 👨‍🍳 so they know they have to prepare your burgers 🍔 (even though they are currently preparing the ones for the previous clients).
#
#The cashier 💁 gives you the number of your turn.
#
#While you are waiting, you go with your crush 😍 and pick a table, you sit and talk with your crush 😍 for a long time (as your burgers are very fancy and take some time to prepare ✨🍔✨).
#
#As you are sitting at the table with your crush 😍, while you wait for the burgers 🍔, you can spend that time admiring how awesome, cute and smart your crush is ✨😍✨.
#
#While waiting and talking to your crush 😍, from time to time, you check the number displayed on the counter to see if it's your turn already.
#
#Then at some point, it finally is your turn. You go to the counter, get your burgers 🍔 and come back to the table.
#
#You and your crush 😍 eat the burgers 🍔 and have a nice time ✨.
#



#Imagine you are the computer / program 🤖 in that story.
#
#While you are at the line, you are just idle 😴, waiting for your turn, not doing anything very "productive". But the line is fast because the cashier 💁 is only taking the orders (not preparing them), so that's fine.
#
#Then, when it's your turn, you do actual "productive" work 🤓, you process the menu, decide what you want, get your crush's 😍 choice, pay 💸, check that you give the correct bill or card, check that you are charged correctly, check that the order has the correct items, etc.
#
#But then, even though you still don't have your burgers 🍔, your work with the cashier 💁 is "on pause" ⏸, because you have to wait 🕙 for your burgers to be ready.
#
#But as you go away from the counter and sit at the table with a number for your turn, you can switch 🔀 your attention to your crush 😍, and "work" ⏯ 🤓 on that. Then you are again doing something very "productive" 🤓, as is flirting with your crush 😍.
#
#Then the cashier 💁 says "I'm finished with doing the burgers" 🍔 by putting your number on the counter's display, but you don't jump like crazy immediately when the displayed number changes to your turn number. You know no one will steal your burgers 🍔 because you have the number of your turn, and they have theirs.
#
#So you wait for your crush 😍 to finish the story (finish the current work ⏯ / task being processed 🤓), smile gently and say that you are going for the burgers ⏸.
#

#Then you go to the counter 🔀, to the initial task that is now finished ⏯, pick the burgers 🍔, say thanks and take them to the table. That finishes that step / task of interaction with the counter ⏹. That in turn, creates a new task, of "eating burgers" 🔀 ⏯, but the previous one of "getting burgers" is finished ⏹.
