local lunajson = require 'lunajson'
local jsonstr = '{"Hello":["lunajson",1.5]}'
local t = lunajson.decode(jsonstr)
print(t.Hello[2]) -- prints 1.5
print(lunajson.encode(t)) -- prints {"Hello":["lunajson",1.5]}

-- read remote file from website
http = require("socket.http")
local body, code = socket.http.request("https://10.14.17.105:3000/items/228/1702")
if not body then error(code) end
local file = assert(io.open('file.json', 'w'))
file:write(body)
file:close()

-- read local file 
local open = io.open
local file = open("file.json", "rb")
if not file then return nil end
local jsonString = file:read "*a"
file:close()

-- parse json with lunajson
local json = require 'lunajson'
local t = lunajson.decode(jsonString)
print(t)
