#!/bin/bash
var_datum=$(date +"%Y%m%d")
input=“impfin.csv” # or any other file name, you can edit it with text editor (gedit,geany, notepad++)
#var_apache_user=www-data
var_path_nextcloud=/var/www/nextcloud
var_result_file="${var_datum}_user_create.txt"
while read -r line
do
echo “Rang: ${line}”
var_password=$(pwgen 8 -c -n -N 1)
set -e
export OC_PASS=$var_password
echo “${var_password} ${OC_PASS}”
var_username=$(echo “${line}” | cut -d";" -f2)
var_name=$(echo “${line}” | cut -d";" -f1)
var_group1=$(echo “${line}” | cut -d";" -f3)
var_email=$(echo “${line}” | cut -d";" -f7)
var_quota=$(echo “${line}” | cut -d";" -f8)

# echo "====DEBUG=====";
# echo "${}"

sh ${var_apache_user} -c “php ${var_path_nextcloud}/occ user:add ${var_username} --password-from-env --group=‘users’ --display-name=’${var_name}’”
if [ “${var_quota}” != “” ] ;then
sh ${var_apache_user} -c " php ${var_path_nextcloud}/occ user:setting ${var_username} files quota ‘${var_quota}’";
fi
sh ${var_apache_user} -c " php ${var_path_nextcloud}/occ user:setting ${var_username} settings email ‘${var_email}’";
echo “${var_username};${var_password}” >> “${var_result_file}”
done < “$input”
exit 0
