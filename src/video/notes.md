---
title: "Video"
author: "baizabal.jesus@gmail.com"
extensions:
  - image_ueberzug
  - qrcode
  - render
styles:
  style: solarized-dark
  table:
    column_spacing: 18
  margin:
    top: 1
    bottom: 0
  padding:
    top: 1
    bottom: 1
---

# Grab the desktop

```bash
 <https://www.addictivetips.com/ubuntu-linux-tips/record-your-screen-from-the-linux-command-line/>

    ffmpeg -f x11grab -y -r 30 -s 1920x1080 -i :0.0 -vcodec huffyuv out.avi

### Compress

<https://unix.stackexchange.com/questions/28803/how-can-i-reduce-a-videos-size-with-ffmpeg>

    ffmpeg -i input.mp4 -vcodec libx265 -crf 28 output.mp4

### For telegram and other

    ffmpeg -i out.avi -c:v libx264 -profile:v baseline -level 3.0 -pix_fmt yuv420p out.mp4

    # This was really driving me nuts: It's important that the file extension is ".mp4". If you upload a video with ".m4v" extension you'll not see a preview window and the video is opened in an external player.

    # So here is my final command to reencode and resize a video and send it to the bot using curl:


ffmpeg -i input -an -c:v libx264 -crf 26 -vf scale=640:-1 out.mp4
curl -v -F chat_id=CHATID -F video=@out.mp4 -F caption=foobar https://api.telegram.org/bot<TOKEN>/sendVideo
```

https://askubuntu.com/questions/648603/how-to-create-an-animated-gif-from-mp4-video-via-command-line

#!/bin/bash

mkdir 30gif
for f in *.mp4; do
duration=$(ffprobe -loglevel error -show_entries format=duration -of default=nk=1:nw=1 "$f")
ffmpeg -i "$f" -filter_complex "[0:v]select='lt(mod(t,${duration}/10),3)',setpts=N/(FRAME_RATE*TB),scale=560:340:force_original_aspect_ratio=decrease,pad=560:340:(ow-iw)/2:(oh-ih)/2,setsar=1,split[v0][v1];[v0]palettegen[p];[v1][p]paletteuse[v]" -map "[v]" "30gif/${f%.mp4}.gif"
done

ffmpeg -i bms.mp4 -r 15 -vf scale=860:-1 bms.gif

https://github.com/videojs/video.js

## ALSA

[source](https://github.com/mpv-player/mpv/issues/4614)

## Introduction [¶](https://trac.ffmpeg.org/wiki/Capture/V4L2_ALSA#Introduction "Link to this section")

This page explain how to use a capture card in Linux to capture both Audio and Video

What you need to keep in mind is that video and audio use different ways to reach ffmpeg in a Linux system. Audio will use [ALSA](https://en.wikipedia.org/wiki/Advanced_Linux_Sound_Architecture) and Video will use [V4L2](https://en.wikipedia.org/wiki/Video4Linux)

## Video stream [¶](https://trac.ffmpeg.org/wiki/Capture/V4L2_ALSA#Videostream "Link to this section")

Find your capture card

```
v4l2-ctl --list-devices

```

Chances are that you will end up using /dev/video0 is you only have a single capture card in your machine.

Check the supported video resolutions and frame rates :

```
ffmpeg  -hide_banner -f v4l2 -list_formats all -i /dev/video0

```

or

```
v4l2-ctl --list-formats-ext

```

Based on the discovered information, the capture section of ffmpeg will look like :

```
ffmpeg \
        -f v4l2 -framerate 25 -video_size 960x540 -i /dev/video0

```

## Audio stream [¶](https://trac.ffmpeg.org/wiki/Capture/V4L2_ALSA#Audiostream "Link to this section")

Find your capture card

```
arecord -L

```

Note that I'm using a big "L" here to have symbolic names instead of index numbers.

You want to select the audio stream that is coming directly from your capture card. Those streams always begin with "hw:"

Based on the discovered information, the capture section of ffmpeg will look like :

```
ffmpeg \
        -f alsa -ac 2 -i hw:CARD=HDMI,DEV=0

```

Note the "-ac 2" to select stereo input.

## Compression [¶](https://trac.ffmpeg.org/wiki/Capture/V4L2_ALSA#Compression "Link to this section")

There is a lot to be said about compression that you can find elsewhere on this wiki. If you are in a hurry, try with those settings (PAL/25fps region, adapt if you are in NTSC/30fps region).

```
ffmpeg \
        -c:v libx264 -b:v 1600k -preset ultrafast \
        -x264opts keyint=50 -g 25 -pix_fmt yuv420p \
        -c:a aac -b:a 128k \

```

Depending on your needs, you will change the bitrate and the preset:

- 1.6Mbps is good for SD/HD-ready streaming on the net
- preset ultrafast is not loading your CPU too much
- if you want to record the stream, raise the bandwidth as much as you can at the time of recording, then compress using slower preset after the capture

## Destinations [¶](https://trac.ffmpeg.org/wiki/Capture/V4L2_ALSA#Destinations "Link to this section")

### File [¶](https://trac.ffmpeg.org/wiki/Capture/V4L2_ALSA#File "Link to this section")

Just finish your ffmpeg command with a filename :

```
ffmpeg \
        -f v4l2 -framerate 25 -video_size 960x540 -i /dev/video0 \
        -f alsa -ac 2 -i hw:CARD=HDMI,DEV=0 \
        -c:v libx264 -b:v 1600k -preset ultrafast \
        -x264opts keyint=50 -g 25 -pix_fmt yuv420p \
        -c:a aac -b:a 128k \
        file.mp4

```

### Streaming on your web site [¶](https://trac.ffmpeg.org/wiki/Capture/V4L2_ALSA#Streamingonyourwebsite "Link to this section")

If you have a web server, you can send your captured video to it using [HTTP Live Streaming (HLS)](https://en.wikipedia.org/wiki/HTTP_Live_Streaming) :

```
ffmpeg \
        -f v4l2 -framerate 25 -video_size 960x540 -i /dev/video0 \
        -f alsa -ac 2 -i hw:CARD=HDMI,DEV=0 \
        -c:v libx264 -b:v 1600k -preset ultrafast \
        -x264opts keyint=50 -g 25 -pix_fmt yuv420p \
        -c:a aac -b:a 128k \
        -f ssegment -segment_list playlist.m3u8 -segment_list_flags +live \
        -segment_time 10 out%03d.ts

```

### Streaming to your LAN [¶](https://trac.ffmpeg.org/wiki/Capture/V4L2_ALSA#StreamingtoyourLAN "Link to this section")

You can send your captured video to a multicast address, so that it can be viewed on many devices on your LAN. The advantage of multicast is that your encoder will only send the video stream once on the network, whatever the number of viewers you have.

```
ffmpeg \
        -f v4l2 -framerate 25 -video_size 960x540 -i /dev/video0 \
        -f alsa -ac 2 -i hw:CARD=HDMI,DEV=0 \
        -c:v libx264 -b:v 1600k -preset ultrafast \
        -x264opts keyint=50 -g 25 -pix_fmt yuv420p \
        -c:a aac -b:a 128k \
        -f rtp_mpegts rtp://239.0.0.2:5001?ttl=2

```

With the settings above, you can use any rtp-compatible client to view the stream. For instance, in VLC, go to Media > Open Network Stream and use this address :

```
rtp://239.0.0.2:5001

```

[Last modified](https://trac.ffmpeg.org/wiki/Capture/V4L2_ALSA?action=diff&version=1 "Version 1 by Jean-François S.") [3 years ago](https://trac.ffmpeg.org/timeline?from=2019-11-06T21%3A44%3A48%2B02%3A00&precision=second "See timeline at Nov 6, 2019, 9:44:48 PM") Last modified on Nov 6, 2019, 9:44:48 PM

## NOTES

Do you really want no compression? Uncompressed video would be very large. Or do you want no transcoding? Or lossless?

No compression can be achieved as mentioned at https://superuser.com/a/1302502/128124

First determine your available formats with:

v4l2-ctl --list-formats-ext

You can then also check:

ffmpeg -f v4l2 -list_formats all -i /dev/video0

OR

ffmpeg -sources |grep video

OR

v4l2-ctl --list-devices

##

Must choose proper format

This camera offers up to 90 fps using its MJPEG encoder, but only up to 30 using raw video, so you have to tell it which format you want with the -input_format input option:

ffmpeg -f v4l2 -framerate 90 -video_size 1280x720 -input_format mjpeg -i /dev/video1 out.mkv

# If your computer is too slow to encode at realtime

[Stream copy](https://ffmpeg.org/ffmpeg.html#Stream-copy) it first then re-encode later at your leisure to your desired format:

```
ffmpeg -f v4l2 -framerate 90 -video_size 1280x720 -input_format mjpeg -i /dev/video1 -c copy mjpeg.mkv
```

Then something like:

```
ffmpeg -i mjpeg.mkv -c:v libx264 -crf 23 -preset medium -pix_fmt yuv420p out.mkv
```

Also see:

- [FFmpeg v4l2 docs](https://ffmpeg.org/ffmpeg-devices.html#video4linux2_002c-v4l2) and [FFmpeg Wiki: Webcam](https://trac.ffmpeg.org/wiki/Capture/Webcam)

- [FFmpeg Wiki: H.264 Encoding](https://trac.ffmpeg.org/wiki/Encode/H.264)

## Extra

How to play on local in MPV camera AV output from remote via SSH?

You need to tell ffmpeg which output container format / muxer (`-f`) to use when using a pipe:

```
ssh -p 22 192.168.1.100 'ffmpeg -i /dev/video0 -c:v libx264 -c:a aac -b:v 1M -b:a 150k -f mpegts -' | mpv -
```
