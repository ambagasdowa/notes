#!/bin/bash
# NOTE of testing
# The following calls
# myscript -vfd ./foo/bar/someFile -o /fizz/someOtherFile
# myscript -v -f -d -o/fizz/someOtherFile -- ./foo/bar/someFile
# myscript --verbose --force --debug ./foo/bar/someFile -o/fizz/someOtherFile
# myscript --output=/fizz/someOtherFile ./foo/bar/someFile -vfd
# myscript ./foo/bar/someFile -df -v --output /fizz/someOtherFile
# all return
#
# verbose: y, force: y, debug: y, in: ./foo/bar/someFile, out: /fizz/someOtherFile
#
# URL_SOURCE https://stackoverflow.com/a/29754866/5090696

#PAT_ghLegion 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
#ssh-keygen -t ed25519 -C "your_email@example.com"
#Note: If you are using a legacy system that doesn't support the Ed25519 algorithm, use:
#$ ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
#gh ssh-key add key-file --title "personal laptop"

#Test the connection 
#$ ssh -T git@github.com

#Here's where a few examples would have helped. To understand the man page I simply experimented with the echo command and several shell variables. This is what it all means:
#
#    Given:
#        foo=/tmp/my.dir/filename.tar.gz
#
#    We can use these expressions:
#
#    path = ${foo%/*}
#        To get: /tmp/my.dir (like dirname)
#    file = ${foo##*/}
#        To get: filename.tar.gz (like basename)
#    base = ${file%%.*}
#        To get: filename
#    ext = ${file#*.}
#        To get: tar.gz
#
#    Note that the last two depend on the assignment made in the second one
#
#Here we notice two different "operators" being used inside the parameters (curly braces). Those are the # and the % operators. We also see them used as single characters and in pairs. This gives us four combinations for trimming patterns
#off the beginning or end of a string:
#
#${variable%pattern}
#    Trim the shortest match from the end
#${variable##pattern}
#    Trim the longest match from the beginning
#${variable%%pattern}
#    Trim the longest match from the end
#${variable#pattern}
#    Trim the shortest match from the beginning
#
#It's important to understand that these use shell "globbing" rather than "regular expressions" to match these patterns. Naturally a simple string like "txt" will match sequences of exactly those three characters in that sequence -- so the
#difference between "shortest" and "longest" only applies if you are using a shell wild card in your pattern.
#
#A simple example of using these operators comes in the common question of copying or renaming all the *.txt to change the .txt to .bak (in MS-DOS' COMMAND.COM that would be REN *.TXT *.BAK).
#
#This is complicated in Unix/Linux because of a fundamental difference in the programming API's. In most Unix shells the expansion of a wild card pattern into a list of filenames (called "globbing") is done by the shell -- before the
#command is executed. Thus the command normally sees a list of filenames (like "foo.txt bar.txt etc.txt") where DOS (COMMAND.COM) hands external programs a pattern like *.TXT.



getopt --test > /dev/null
if [[ $? -ne 4 ]]; then
    echo "I’m sorry, `getopt --test` failed in this environment."
    exit 1
fi

OPTIONS=dfo:v
LONGOPTIONS=debug,force,output:,verbose

# -temporarily store output to be able to check for errors
# -activate advanced mode getopt quoting e.g. via “--options”
# -pass arguments only via   -- "$@"   to separate them correctly
PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@")
if [[ $? -ne 0 ]]; then
    # e.g. $? == 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi
# use eval with "$PARSED" to properly handle the quoting
eval set -- "$PARSED"

# now enjoy the options in order and nicely split until we see --
while true; do
    case "$1" in
        -d|--debug)
            d=y
            shift
            ;;
        -f|--force)
            f=y
            shift
            ;;
        -v|--verbose)
            v=y
            shift
            ;;
        -o|--output)
            outFile="$2"
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

# handle non-option arguments
if [[ $# -ne 1 ]]; then
    echo "$0: Needs already install ranger sc-im and i3-wm rsync nitrogen feh ."
    echo "run : install.sh install"
    exit 4
fi

# config_files
files=("i3/config" "sc-im/scimrc" "ranger/rc.conf" "ranger/scope.sh" "ranger/rifle.conf" "rclone/rclone.conf" "w3m/config" "dunst/dunstrc")

files_root=(".vim/" ".vimrc")

# where we are?
root_dir=`pwd`'/src/'
repo_font_dir="${HOME}/gitlab/notes/src"
config_dir="${HOME}/.config/"
fonts_dir="${HOME}/.local/share/fonts"

config_root="${HOME}/"


function main () {
    echo "initialize";

  sudo apt install i3 i3blocks xinit ranger vim mpv rclone exa terminology sakura rofi nitrogen zathura python3 rsync libdbus-glib-1-2 libdbus-glib-1-dev libdbus-glib-1-dev-bin xbanish 
  sudo apt install ripgrep checkinstall jq lookatme
  sudo apt install librsvg2-bin grabc xdot graphviz

  ln -s $HOME/gitlab/notes/src/bash/bash_aliases $HOME/.bash_aliases
  ln -s $HOME/gitlab/notes/src/bash/profile $HOME/.profile
  ln -s $HOME/gitlab/notes/src/xinitrc $HOME/.xinitrc
  #ln -s $HOME/gitlab/notes/src/w3m/config $HOME/.config/w3m/

  sudo cp $HOME/gitlab/notes/src/BTusb/rtl8761b_config.bin /lib/firmware/rtl_bt/
  sudo cp $HOME/gitlab/notes/src/BTusb/rtl8761b_fw.bin /lib/firmware/rtl_bt/


# NOTE firts generate the files on ~/.config/ dir
    generate




# NOTE Now the particular configs
    ranger_config
    vim_config
    fonts
    
}



function build_dir () {

    echo "check if $1 exists ?" 
    if [ ! -d $1 ]; then
        echo "The dir ${1} isn't exists then make it "
        mkdir -p $1
    fi
}


function backup () {
    mv $config_dir$1 $config_dir$1.old
}


function set_links () {

    if [ -f $config_dir$1 ]; then 
        rm $config_dir$1
    fi
#  build the new file 
    ln -s $root_dir$1 $config_dir$1
}




function generate () {
# set link files
    count=${#files[@]}

    echo "found ${count} files "

    for i in ${files[@]}
    do
        echo "Processing links for $i" 
        backup $i
        #echo "What we see $config_dir${i%/*}"
	build_dir $config_dir${i%/*}
        set_links $i
    done
}





## NOTE Individual configurations

function fonts(){

    echo " Installing Fonts ... "

#get some extra fonts 
   # git clone git@github.com:ryanoasis/nerd-fonts.git
   # git clone https://github.com/ryanoasis/nerd-fonts.git ~/github/NerdFonts


    if [ ! -f $fonts_dir'/nfont/MonacoRegularLigaturesNerdFontComplete.ttf' ]; then 

	echo "NO FONT DIR FOUND"    
        build_dir $fonts_dir   
        rsync -axPz $repo_font_dir'/fonts_patched/' $fonts_dir

    fi


    echo " Updating Cache "
    fc-cache -f -v 2>&1

}

# extra config in ranger 

function ranger_config () {    
    if [ ! -d $config_dir'/ranger/plugins/' ];then 


        if [[ $v -ne 1 ]]; then
            echo "deleting ranger inner files and restore new ones "
        fi

        rm -r $config_dir'/ranger/*'
        ranger --copy-config=all
        if [[ $v -ne 1 ]]; then
            echo "cloning devicons repo inside ranger config dir ..."
        fi
        git clone https://github.com/cdump/ranger-devicons2 $HOME/.config/ranger/plugins/devicons2
 #Add/change `default_linemode devicons2` in your `~/.config/ranger/rc.conf`
    fi
}


function vim_config() {

	if [ ! -d $HOME'/github/v-ide/' ];then
  
		git clone git@github.com:ambagasdowa/v-ide.git $HOME/github/v-ide
		ln -s $HOME/github/v-ide/vimrc $HOME/.vimrc
		ln -s $HOME/github/v-ide/vim $HOME/.vim

	fi		
}


#init run ...
    main 
#echo "verbose: $v, force: $f, debug: $d, in: $1, out: $outFile"
