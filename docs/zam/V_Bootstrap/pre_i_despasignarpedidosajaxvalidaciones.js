﻿//Nombre    : pre_i_despasignarpedidosajaxvalidaciones.js
//Objetivo  : Funcionalidad
var astrcontrol;
//rherrera
var astrOriLon;
var astrOriLat;
var astrDesLon;
var astrDesLat;
var astrOriRegLat;
var astrOriRegLon;
var astrDesRegLon;
var astrDesRegLat;
var wayPoints = new Array();
var wayPoints_regreso = new Array();
var rendimientoUnidad;
//rherrera GAAL
var locations = new Array();
var gasolineras = new Array();
var gaso = [];
var distancia = [];
var lat = null;
var lon = null;
var wayp = [];
var map = null;
var renderOp = {
    draggable: true,
    preserveViewport: true
};
var directionServices = new google.maps.DirectionsService();
var directionsDisplay = new google.maps.DirectionsRenderer(renderOp);
var service = new google.maps.DistanceMatrixService();

var ArrGaso = new Array();
var ArrGasoConDistancia = new Array();
var icont = 0;
var intContSteps = 0;

//rherrera GAAL
if (document.getElementById('hdnObtenerGasolineras').value == 'S') {
    pridesp01_pre_i_despasignarpedidos.getListaGasolineras(getGasolineras_CallBack);
    initialize();
}

function getGasolineras_CallBack(data) {
    var dataGasolinera = JSON.parse(data.value);
    for (var i = 0; i < dataGasolinera.length; i++) {
        var gas = [];

        gas.push(dataGasolinera[i].id_gasolinera);
        gas.push(dataGasolinera[i].desc_gasolinera);
        gas.push(dataGasolinera[i].domicilio);
        gas.push("A");
        gas.push(dataGasolinera[i].coordenadas);
        gas.push(dataGasolinera[i].lat)
        gas.push(dataGasolinera[i].lng)
        gas.push("../imagenes/gas.png");
        locations.push(gas)
    }
};

function modifunidad() {
    if (document.form1.txtUnidad.value == '') {
        document.form1.imgTracto.src = '../Imagenes/spacer.GIF';
    }else{
        document.form1.imgTracto.src = '../Imagenes/tractor.JPG';
    }
}

function modifrem1() {
    if (document.form1.txtRemolque1.value == '') {
        document.form1.imgRemolque1.src = '../Imagenes/spacer.GIF';
    }else{
        document.form1.imgRemolque1.src = '../Imagenes/remolque.JPG';
    }
}

function modifdolly() {
    if (document.form1.txtDolly.value == '') {
        document.form1.imgDolly.src = '../Imagenes/spacer.GIF';
    }else{
        document.form1.imgDolly.src = '../Imagenes/dolly.JPG';
    }
}

function modifrem2() {
    if (document.form1.txtRemolque2.value == '') {
        document.form1.imgRemolque2.src = '../Imagenes/spacer.GIF';
    }else{
        document.form1.imgRemolque2.src = '../Imagenes/remolque.JPG';
    }
}

function BuscaTractor() {
    var strURL; var winUnidad;
    //FOLIO: 24266, GAFIGUEROA 18/04/2018
    if (document.getElementById("HdnCambiosOpeAlmex").value == "S") {
        strURL = '../busquedas/busqueda_unidades.aspx?Campo1=form1.txtUnidad&Campo2=form1.txtPermisionario&Opcion=ASIGN';
        winUnidad = window.open(strURL, 'Busqueda', 'width=640,height=480,status=no,scrollbars=no');
        winUnidad.ventanaReferencia = window;
        winUnidad.Referencia = document.form1.txtUnidad;
    }
    else{
        var ParCC = document.getElementById('txtHdnParCapCarga').value;
        strURL = '../busqueda_estandar.aspx?Search=/srv_i_busquedas/ctrlsearch_unidadesdisp.ascx&field=form1.txtUnidad&field2=form1.txtPermisionario&Opc=ASIGN&CapCarga=' + ParCC;
        winUnidad = window.open(strURL, 'Busqueda', 'width=640,height=480,status=no,scrollbars=no');
        winUnidad.ventanaReferencia = window;
        winUnidad.Referencia = document.form1.txtUnidad;
    }
}

function BuscaLinea(as_campo) {
    var strURL; var winBuscLinea;
    strURL = '../busqueda_estandar.aspx?Search=/srv_i_busquedas/ctrlsearch_lineas.ascx&Campo=' + as_campo;
    winBuscLinea = window.open(strURL, 'Busqueda', 'width=640,height=480,top=150,left=150,status=no,scrollbars=yes');
    winBuscLinea.focus();
}

function EditarFianza(as_linea,as_remolque,num_rem,bolabrefianza) {
    var strURL; var winFianza; var arrControl = new Array()
    var astrpedido = ''; var arrNacionalidad = new Array()
    //Obtengo la nacionalidad del Remolque cuando no tiene Linea para saber si es propio Americano.
    //Jdlcruz, GASA 29-Nov-2012, Folio: 17748
    if (as_linea == '') {
        arrNacionalidad = pridesp01_pre_i_despasignarpedidos.GetNacionalidadRemolque(as_remolque);

        if (arrNacionalidad.error != null) {
            alert("Error:" + arrNacionalidad.error);
            return;
        }

        if (arrNacionalidad.value[0] == 'Error') {
            document.getElementById('lblErrorAjax').innerHTML = arrNacionalidad.value[1];
            return arrControl;
        }
    }
    else {
        arrNacionalidad[0] = 0
    }

    //si es remolque americano valida la fianza
    if (as_linea != '' || arrNacionalidad.value[0] == 2) {
        var astrasigna = document.getElementById('txt_hdAsignacion').value
        if (astrasigna == '') {
            astrasigna = '0'
            var rowIndex; var oRow;
            rowIndex = window.opener.igtbl_getGridById('uwgPedidos').ActiveRow
            oRow = window.opener.igtbl_getRowById(rowIndex)
            if (oRow != null) {
               astrpedido = '' + oRow.getCellFromKey("id_pedido").getValue();
            }
         }
         arrControl = pridesp01_pre_i_despasignarpedidos.GetControlRemolque(as_linea, as_remolque, astrasigna, astrpedido);
        if (arrControl.error != null) {
            alert("Error:" + arrControl.error);
            return;
        }
        var aidcontrol = ''
        aidcontrol = '' + arrControl.value[0]
        astrcontrol = aidcontrol;
        if (arrControl.error != null) { return; }
        if (arrControl.value[0] != 'Error') {
            if (bolabrefianza == true) {
                strURL = 'pre_i_despfianza.aspx?NumRem=' + num_rem + '&Control=' + aidcontrol;
                winFianza = window.open(strURL, 'FianzaControl', 'width=500,height=320,status=no,scrollbars=no,left=100,top=100');
                winFianza.focus();
            }
        }
        else {
            document.getElementById('lblErrorAjax').innerHTML = arrControl.value[1];
            return arrControl;
        }
    }
    else {
        return arrControl;
    }
    return arrControl;
}

function BuscaOperador(as_campo, as_descr) {
    var strURL; var winOperador;
    //FOLIO: 24301, GAFIGUEROA 20/04/2018
    if (document.getElementById("HdnCambiosOpeAlmex").value == "S") {
        var ParIdFlota = document.getElementById("idFlota").value;
        strURL = '../busquedas/busqueda_personal.aspx?Campo=' + as_campo + '&Descr=' + as_descr + '&TipoEmp=O&IdFlota=' + ParIdFlota;
        winOperador = window.open(strURL, 'BuscaOperador', 'width=640,height=480,top=150,left=150,status=no,scrollbars=no');
        winOperador.focus();
    }
    else {
        //GGUERRA FOLIO:21597
        var ParIdFlota = document.getElementById("idFlota").value;
        strURL = '../busqueda_estandar.aspx?Search=/srv_i_busquedas/ctrlsearch_personalxtipo.ascx&Campo=' + as_campo + '&Descr=' + as_descr + '&TipoE=O&IdFlota=' + ParIdFlota;
        winOperador = window.open(strURL, 'BuscaOperador', 'width=640,height=480,top=150,left=150,status=no,scrollbars=no');
        winOperador.focus();
    }
}

function BuscaRuta(as_campo, as_desc) {
    var strURL; var winRuta;
    //FOLIO: 24266, GAFIGUEROA 18/04/2018
    if (document.getElementById("HdnCambiosOpeAlmex").value == "S") {
        //var oAsignacion = '&Opcion=ASIGN&Campo2=form1.txtSeguimiento&Campo3=form1.txtSeguimientoDesc';
        strURL = '../busquedas/busqueda_traficorutas.aspx?Campo=' + as_campo + '&Descr=' + as_desc + '&Opcion=ASIGN';
        winRuta = window.open(strURL, 'Ruta', 'width=640,height=480,top=150,left=150,status=no,scrollbars=no');
        winRuta.focus();
    }
    else{
        //rherrera GAAL
        if (as_campo == 'form1.txtRuta') {
            strURL = '../busqueda_estandar.aspx?Search=/srv_i_busquedas/ctrlsearch_rutas.ascx&Campo=' + as_campo + '&Desc=' + as_desc;
            winRuta = window.open(strURL, 'Ruta', 'width=640,height=480,top=150,left=150,status=no,scrollbars=no');
            winRuta.focus();
        }
        else{
            if (document.getElementById("txtRutaRegreso").disabled != true && document.getElementById('hdnObtenerGasolineras').value == 'S') {
                strURL = '../busqueda_estandar.aspx?Search=/srv_i_busquedas/ctrlsearch_rutas.ascx&Campo=' + as_campo + '&Desc=' + as_desc;
                winRuta = window.open(strURL, 'Ruta', 'width=640,height=480,top=150,left=150,status=no,scrollbars=no');
                winRuta.focus();
            }
        }
    }
}


function BuscaRemolque(as_rem, as_linea) {
    var strURL; var winBuscRem;
    var ParCC = document.getElementById('txtHdnParCapCarga').value;
    strURL = '../busqueda_estandar.aspx?Search=/srv_i_busquedas/ctrlsearch_remolques.ascx&Rem=' + as_rem + '&Linea=' + as_linea + '&Opc=ASIGN&CapCarga=' + ParCC;
    winBuscRem = window.open(strURL, 'BuscaRem', 'width=640,height=510,status=no,scrollbars=yes');
    winBuscRem.focus();
}

function BuscaDolly(as_campo) {
    var strURL; var winDolly;
    var ParCC = document.getElementById('txtHdnParCapCarga').value;
    strURL = '../busqueda_estandar.aspx?Search=/srv_i_busquedas/ctrlsearch_unidades.ascx&Campo=' + as_campo + '&Tipo=D&Opc=ASIGN&CapCarga=' + ParCC;
    winDolly = window.open(strURL, 'BuscaDolly', 'width=640,height=480,top=150,left=150,status=0,scrollbars=0');
    winDolly.focus();
}

function BuscaKit() {
    var strURL; var winKit;
    strURL = '../busquedas/busqueda_kitunidad.aspx';
    winKit = window.open(strURL, 'BuscaKit', 'width=640,height=480,top=150,left=150,status=0,scrollbars=0');
    winKit.focus();
}

//se agrega busqueda de ventana chrysler
function BuscaVentana(txtID, txtDesc) {
    var Url, WinVentana, Idpedidopk;

    Idpedidopk = document.getElementById('hdn_pedidopk').value;

    Url = '../busquedas/busqueda_ventanas_chrysler.aspx?Idpedidopk=' + Idpedidopk + '&Id=' + txtID + '&Desc=' + txtDesc;
    WinVentana = window.open(Url, 'VentanasChrysler', 'width=760,height=360,top=150,left=150,status=0,scrollbars=0')
    WinVentana.focus();
}

function wibSalir_Click(oButton, oEvent) {
    var arryGas;
    var intAsignacion = document.form1.txtAsignacion2.value;
    var strSubArray = new Array();
    window.close();
}

//Valida Kit
function ValidaKit(strKit) {
    if (strKit == '') {
        document.getElementById("txtKit").value = '';
    }else{
        pridesp01_pre_i_despasignarpedidos.ValidaKit(strKit, Kit_CallBack);
    }
}

function Kit_CallBack(response) {
    if (response.error != null) { return; }
    if (response.value[0] == 'Error') {
        document.getElementById("txtRemolque1").value = ""
        document.getElementById("txtDolly").value = ""
        document.getElementById("txtRemolque2").value = ""
        document.getElementById("txtKit").value = ""
        document.getElementById("txtUnidad").value = ""
        document.getElementById("txtKit").focus();
        lblErrorAjax.innerText = response.value[1]
    }else {
        document.getElementById("txtRemolque1").value = response.value[0]
        document.getElementById("txtDolly").value = response.value[1]
        document.getElementById("txtRemolque2").value = response.value[2]
        document.getElementById("txtUnidad").value = response.value[3]
        lblErrorAj.ax.innerText = ""
    }
    modifrem1()
    modifrem2()
    modifunidad()
}

//Valida Ruta
function ValidaRuta(intRuta, strUnidad, dFecha) {
    if (intRuta == '') {
        document.getElementById("txtRuta").value = '';
    }else {
        dFecha = FormatoFecha(dFecha, document.getElementById('txtFechaDefault').value)
        pridesp01_pre_i_despasignarpedidos.ValidaRuta(intRuta, strUnidad, dFecha, Ruta_CallBack);
    }
}

//AMONTIEL FOLIO:25313  13/12/2018
function ValidaVelocidad() {
    if (document.getElementById('hdn_razon_social').value == 'S') {
        lblErrorAjax.innerText = "";
        document.getElementById("btnGuarda").disabled = false;
    }
}

//rherrera GAAL
//Valida Ruta Regreso
function ValidaRutaRegreso(intRuta, strUnidad, dFecha) {
    if (intRuta == '') {
        document.getElementById("txtRutaRegreso").value = '';
    } else {
        dFecha = FormatoFecha(dFecha, document.getElementById('txtFechaDefault').value)
        pridesp01_pre_i_despasignarpedidos.ValidaRuta(intRuta, strUnidad, dFecha, RutaRegreso_CallBack);
    }
}

function Ruta_CallBack(response) {
    if (response.error != null) { return; }
    if (response.value[0] == 'Error') {
        document.getElementById("txtRuta").focus();
        document.getElementById("txtRuta").value = ""
        document.getElementById("txtRutaDesc").value = ""
        lblErrorAjax.innerText = response.value[1]
    } else {
        if (response.value[1] == 'Aviso') {
            lblErrorAjax.innerText = response.value[2]
        } else {
            document.getElementById("txtRutaDesc").value = response.value[0]
            lblErrorAjax.innerText = ""
        }
    }
}
//rherrera GAAL
function RutaRegreso_CallBack(response) {
    if (response.error != null) { return; }
    if (response.value[0] == 'Error') {
        document.getElementById("txtRutaRegreso").focus();
        document.getElementById("txtRutaRegreso").value = ""
        document.getElementById("txtRutaRegresoDesc").value = ""
        lblErrorAjax.innerText = response.value[1]
    } else {
        if (response.value[1] == 'Aviso') {
            lblErrorAjax.innerText = response.value[2]
        } else {
            document.getElementById("txtRutaRegresoDesc").value = response.value[0]
            lblErrorAjax.innerText = ""
        }
    }
}


function GetData(numberOp) {
  var response;
  // alert(document.getElementById('txtOperador1').value.length);
  // alert(numberOp);

  pridesp01_pre_i_despasignarpedidos.GetData(numberOp,GetData_CallBack);

}

function GetData_CallBack(response) {
  var response=response.value;
  // alert(response);
  // document.getElementById('node-id').innerHTML = response;
  var pieces = response.split(",");
  var blocks = parseInt(pieces[0]);
  var restDays = pieces[1];
  var namae = pieces[2];
  var id_operador = pieces[3];
  var folio = pieces[4];
  var vence = pieces[5];
  // alert("block => " + blocks);
  // alert(typeof(blocks));
  // alert(vence);
    if(blocks == 0) {
    alert("Licencia expirada desde " + restDays + " dias");
    document.getElementById("btnGuarda").disabled = true;
    // lblError.innerText = "";
    lblErrorAjax.innerText = "Licencia del operador "+ namae +" expirada desde " + restDays + " dias";
  } else if (blocks == 1) {
    alert("Licencia Vence en " + restDays + " dias");
    document.getElementById("btnGuarda").disabled = true;
    // lblError.innerText = "";
    lblErrorAjax.innerText = "La Licencia del Operador tiene menos de 10 dias para vencer";
  } else if (blocks == 2) {
    alert("Licencia sin Actualizar");
    document.getElementById("btnGuarda").disabled = true;
    // lblError.innerText = "";
    lblErrorAjax.innerText = "La Licencia del Operador necesita Actualizarse";
  } else if (blocks == 3) { // Entre 10 y 30 dias
    alert("Licencia Vence en " + restDays + " dias");
    // if button is disabled just enable
    if (document.getElementById("btnGuarda").disabled == true) {
      document.getElementById("btnGuarda").disabled = false;
    }
    document.getElementById('txtOperador1Nombre').length=0;
    document.getElementById('txtOperador1Nombre').value=namae;
    // lblError.innerText = "";
    lblErrorAjax.innerText = "La Licencia del Operador Vence en " + restDays + " dias";
  } else if (blocks == 4) {
    alert("El Operador esta dado de Baja");
    document.getElementById("btnGuarda").disabled = true;
    // lblError.innerText = "";
    lblErrorAjax.innerText = "El Operador " + namae + " esta dado de baja";
  } else { // mayor a un mes
    // if button is disabled just enable
    if (document.getElementById("btnGuarda").disabled == true) {
      document.getElementById("btnGuarda").disabled = false;
    }
    // Clear current data
    document.getElementById('txtOperador1Nombre').length=0;
    // set the new value
    document.getElementById('txtOperador1Nombre').value=namae;
    lblErrorAjax.innerText = ""
    // lblError.innerText = ""
  }
} // NOTE End Callback

//Valida Operador
function ValidaOperador(intOperador, NoOperador) {
    if (intOperador == '') {
        switch (NoOperador) {
            case 1:
                document.getElementById("txtOperador1Nombre").value = ""
                break;
            case 2:
                document.getElementById("txtOperador2Nombre").value = ""
                break;
            default:
        }
    }else{
        pridesp01_pre_i_despasignarpedidos.ValidaOperador(intOperador, NoOperador, Operador_CallBack);
    }
}

function Operador_CallBack(response) {
    lblErrorAjax.innerText = ""
    if (response.error != null) { return; }
    if (response.value[0] == 'Error') {
        switch (response.value[2]) {
            case 1:
                document.getElementById("txtOperador1").focus();
                document.getElementById("txtOperador1").value = '';
                document.getElementById("txtOperador1Nombre").value = ""
                break;
            case 2:
                document.getElementById("txtOperador2").value = '';
                document.getElementById("txtOperador2Nombre").value = ""
                document.getElementById("txtOperador2").focus();
                break;
            default:
        }
        lblErrorAjax.innerText = response.value[1]
    }else {
        switch (response.value[0]) {
            case 1:
                document.getElementById("txtOperador1Nombre").value = response.value[1]
                break;
            case 2:
                document.getElementById("txtOperador2Nombre").value = response.value[1]
                break;
            default:
        }
        lblErrorAjax.innerText = ""
    }
}

// MSALVARADO AEO
//=======================aqui
function ValidadEvidenciaOperador(int_operador, no_operador) {
    pridesp01_pre_i_despasignarpedidos.ValidadEvidenciaOperador(int_operador, no_operador, OperadorEvidencia_CallBack);
}
function OperadorEvidencia_CallBack(response) {
    if (response.error != null) { return; }
    if (response.value[0] == 'Error') {
        lblErrorAjax.innerText = response.value[1]
        window.close()
    }
    else {
        if (response.value[0] = 1) {
            //valida si el operador tiene viajes pendientes
            if (response.value[1] >= 1) {
                window.opener.alert("El Operador : " + response.value[2] + " tiene " + response.value[1] + " evidencia(s) pendiente(s)");
                lblErrorAjax.innerText = ""

            }
        } else {
            //valida si el operador tiene viajes pendientes
            if (response.value[1] >= 1) {
                window.opener.alert("El Operador : " + response.value[2] + " tiene " + response.value[1] + " evidencia(s) pendiente(s)");
                lblErrorAjax.innerText = ""

            }

        }

    }
}

//Valida Seguimiento
function ValidaSeguimiento(strSeguimiento) {
    if (strSeguimiento == '') {
        document.getElementById("txtSeguimiento").value = '';
        document.getElementById("txtSeguimientoDesc").value = '';
    }else{
        pridesp01_pre_i_despasignarpedidos.ValidaSeguimiento(strSeguimiento, Seguimiento_CallBack);
    }
}

function Seguimiento_CallBack(response) {
    if (response.error != null) { return; }
    if (response.value[0] == 'Error') {
        document.getElementById("txtSeguimiento").value = ""
        document.getElementById("txtSeguimiento").focus();
        document.getElementById("txtSeguimientoDesc").value = ""
        lblErrorAjax.innerText = response.value[1]
    }else{
        document.getElementById("txtSeguimientoDesc").value = response.value[0]
        lblErrorAjax.innerText = ""
        //cambios tpx  09/09/2014 : evita que tomo el tipo de operacion en base al seguimiento
        if (document.getElementById("hdn_tipoopertpx").value != "S" && document.getElementById("hdn_valida_tipoOperacion").value != "S") {
            ActualizaTipoOperacion()
         }
    }
}

//Valida Hora
function ValidaHora(strHora, strHraInicio) {
    if (strHora == '') {
        strHora = '0'
    }else {
        strHraInicio = FormatoFecha(strHraInicio, document.getElementById('txtFechaDefault').value)
        pridesp01_pre_i_despasignarpedidos.ValidaHora(strHora, strHraInicio, Hora_CallBack);
    }
}

function Hora_CallBack(response) {
    if (response.error != null) { return; }
    if (response.value[0] == 'Error') {
        lblErrorAjax.innerText = response.value[1]
    }else{
        igedit_getById('wdcFinViaje').setText(response.value[0]);
        lblErrorAjax.innerText = ""
    }
}

//Valida Fechas
function ValidaFechas(strHoraIni, strHoraFin) {
    strHoraIni = FormatoFecha(strHoraIni, document.getElementById('txtFechaDefault').value)
    strHoraFin = FormatoFecha(strHoraFin, document.getElementById('txtFechaDefault').value)
    pridesp01_pre_i_despasignarpedidos.ValidaFechas(strHoraIni, strHoraFin, Fecha_CallBack);
}

function FormatoFecha(fecha, format) {
    if (document.getElementById('txtFechaDefault').value == '') {
        alert("Es posible que experimente problemas con los campos de las fechas, favor de configurar correctamente en el archivo de configuración el strDateIISTimeFormat y strDateIISFormat indicando el formato de fecha de tu servicio de IIS")
    }
    var strSubArray;
    strSubArray = fecha.split("/")
    switch (format) {
        case 'dd/MM/yyyy HH:mm':
            fecha = strSubArray[0] + "/" + strSubArray[1] + "/" + strSubArray[2]
            break;
        case 'MM/dd/yyyy HH:mm':
            fecha = strSubArray[1] + "/" + strSubArray[0] + "/" + strSubArray[2]
            break;
        default:
    }
    return fecha;
}

function Fecha_CallBack(response) {
    if (response.error != null) { return; }
    if (response.value[0] == 'Error') {
        lblErrorAjax.innerText = response.value[1]
    }else {
        document.getElementById('txtHrsEstandar').value = response.value[0]
        lblErrorAjax.innerText = ""
    }
}

function ValidaUnidad(unidad) {
    try {
        if (unidad == '') {
            if (document.getElementById("txtUnidad") != null) {
                document.getElementById("txtUnidad").value = '';
                if (document.getElementById('txtHdnParCapCarga').value == 'S') {
                    document.getElementById('txtIdTipoUTracto').value = 0;
                    document.getElementById('txtTipoUTracto').value = '';
                }
                modifunidad()
                if (document.getElementById('txtHdnParCapCarga').value == 'S') {
                    ObtieneCapacidadCarga();
                }
            }
        }else {
            var aint_id_asigna = document.getElementById("txt_hdAsignacion").value;
            var astrListaP = document.getElementById("txtListaPedidos").value;
            var astrListaPA = document.getElementById("txtListaPedidosAreas").value;
            var intRuta = document.getElementById("txtRuta").value;
            if (aint_id_asigna == '') { aint_id_asigna = '0'; }
            if (intRuta == '') { intRuta = '0'; }
            pridesp01_pre_i_despasignarpedidos.ValidaUnidad(intRuta, unidad, aint_id_asigna, astrListaP, astrListaPA, Unidad_CallBack);
            setFocus('txtRemolque1');
            modifunidad();
        }
    } catch (e) {
        alert("Error Ajax:" + e.message);
    }
}

function Unidad_CallBack(response) {
    try {
        if (response.error != null) {
            alert("Error Ajax:" + response.error); return;
        }
        if (response.value[0] == 'Error') {
            if (document.getElementById("txtUnidad") != null) {
                document.getElementById("txtUnidad").focus();
                document.getElementById("txtUnidad").value = '';
                document.getElementById("txtPermisionario").value = '';
                if (document.getElementById('txtHdnParCapCarga').value == 'S') {
                    document.getElementById('txtIdTipoUTracto').value = 0;
                    document.getElementById('txtTipoUTracto').value = '';
                    //ObtieneCapacidadCarga();
                }
                lblErrorAjax.innerText = response.value[1]
            }
        } else {
            if (response.value[0] == "Advertencia") {
                lblErrorAjax.innerText = response.value[0]
                document.getElementById('txtPermisionario').value = response.value[3]
            } else {
                lblErrorAjax.innerText = ""
            }
            if (response.value[0] == "InfoTipoUnidad") {
                document.getElementById('txtIdTipoUTracto').value = response.value[1]
                document.getElementById('txtTipoUTracto').value = response.value[2]
                document.getElementById('txtPermisionario').value = response.value[3]
                ObtieneCapacidadCarga();
            } else {
                lblErrorAjax.innerText = ""
                document.getElementById('txtPermisionario').value = response.value[3]
            }
            if (response.value[4] == "Aviso") {
                lblErrorAjax.innerText = response.value[5]
            } else {
                lblErrorAjax.innerText = ""
            } //jcrodríguez folio 23451 18/09/2017 se agregovalidacion de funcionalidad de aeo
            if (response.value[0] == "DatosOper") {
                document.getElementById("txtOperador1").value = response.value[1]
                document.getElementById("txtOperador1Nombre").value = response.value[2]
            } else {
                lblErrorAjax.innerText = ""
            }
        }
    } catch (e) {
        alert("Error Ajax:" + e.message);
    }
}

function ObtieneCapacidadCarga() {
    try {
        var array = new Array();
        if (document.getElementById('lblError') != null) {
            document.getElementById('lblError').innerHTML = '';
        }
        if (document.getElementById('hfDINET').value != "1") {
            return;
        }
        var TUT = document.getElementById('txtIdTipoUTracto').value;
        var TUR1 = document.getElementById('txtIdTipoUR1').value;
        var TUD = document.getElementById('txtIdTipoUD').value;
        var TUR2 = document.getElementById('txtIdTipoUR2').value;
        array[0] = document.getElementById('txtUnidad').value;
        array[1] = document.getElementById('txtRemolque1').value;
        array[2] = document.getElementById('txtDolly').value;
        array[3] = document.getElementById('txtRemolque2').value;
        var PesoCargar = document.getElementById('txtCargaKG').value;
        var VolCargar = document.getElementById('txtCargarM3').value;
        pridesp01_pre_i_despasignarpedidos.ObtieneCapacidadCarga(TUT, TUR1, TUR2, TUD, PesoCargar, VolCargar, array, ObtieneCapacidadCarga_CallBack);
    } catch (e) {
        alert("Error Ajax:" + e.message);
    }
}

function ObtieneCapacidadCarga_CallBack(response) {
    try {
        if (response.error != null) {
            document.getElementById('lblConfigMTC').innerHTML = "Configuración MTC: ";
            alert("Error Ajax:" + response.error); return;
        }
        if (response.value[0] == 'Error') {
            lblErrorAjax.innerText = ""
            lblErrorAjax.innerText = "La Capacidad de Carga o de Volumen es menor a la Capacidad de Carga registrada en el Catálogo de Configuración de MTC o el Volumen configurado en el Tipo de Unidad."//response.value[1]
            var oGuardar = document.getElementById('DivGuardar');
            oGuardar.style.display = 'none';
            document.getElementById('txtCapacidadKG').value = response.value[1];
            document.getElementById('txtCapacidadM3').value = response.value[2];
            document.getElementById('txtIdTipoUTracto').value = response.value[4];
            document.getElementById('txtTipoUTracto').value = response.value[5];
            document.getElementById('txtIdTipoUR1').value = response.value[6];
            document.getElementById('txtTipoURem1').value = response.value[7];
            document.getElementById('txtIdTipoUR1').value = response.value[8];
            document.getElementById('txtTipoURem2').value = response.value[9];
            document.getElementById('txtIdTipoUD').value = response.value[10];
            document.getElementById('txtTipoUDolly').value = response.value[11];
            document.getElementById('lblConfigMTC').innerHTML = response.value[3];
        } else {
            lblErrorAjax.innerText = ""
            var oGuardar = document.getElementById('DivGuardar');
            oGuardar.style.display = 'block';
            if (response.value[0] == "InfoCapacidad") {
                document.getElementById('txtCapacidadKG').value = response.value[1];
                document.getElementById('txtCapacidadM3').value = response.value[2];
                document.getElementById('lblConfigMTC').innerHTML = response.value[3];
                document.getElementById('txtIdTipoUTracto').value = response.value[4];
                document.getElementById('txtTipoUTracto').value = response.value[5];
                document.getElementById('txtIdTipoUR1').value = response.value[6];
                document.getElementById('txtTipoURem1').value = response.value[7];
                document.getElementById('txtIdTipoUR1').value = response.value[8];
                document.getElementById('txtTipoURem2').value = response.value[9];
                document.getElementById('txtIdTipoUD').value = response.value[10];
                document.getElementById('txtTipoUDolly').value = response.value[11];
            }
        }
    } catch (e) {
        alert("Error Ajax:" + e.message);
    }
}

//Valida linea
function ValidaLineasyRemolques(strLinea, strRem, intRemNum) {
    try {
        var Arguments = new Array()
        if (strLinea == "" && strRem == "") {
            if (intRemNum == 1) {
                if (document.getElementById('txtHdnParCapCarga').value == 'S') {
                    document.getElementById('txtIdTipoUR1').value = 0;
                    document.getElementById('txtTipoURem1').value = '';
                    ObtieneCapacidadCarga()
                }
            }
            if (intRemNum == 2) {
                if (document.getElementById('txtHdnParCapCarga').value == 'S') {
                    document.getElementById('txtIdTipoUR2').value = 0;
                    document.getElementById('txtTipoURem2').value = '';
                    ObtieneCapacidadCarga()
                }
            }
            lblErrorAjax.innerText = ""
        }else {
            //txtLineaRem1
            if (document.getElementById('txtLineaRem1') == null || document.getElementById('txtLineaRem1') == 'undefined') {
                Arguments[0] = '';
            } else {
                Arguments[0] = document.getElementById('txtLineaRem1').value;
            }
            //txtRemolque1
            if (document.getElementById('txtRemolque1') == null || document.getElementById('txtRemolque1') == 'undefined') {
                Arguments[1] = '';
            } else {
                Arguments[1] = document.getElementById('txtRemolque1').value;
            }
            //txtDolly
            if (document.getElementById('txtDolly') == null || document.getElementById('txtDolly') == 'undefined') {
                Arguments[2] = '';
            } else {
                Arguments[2] = document.getElementById('txtDolly').value;
            }
            //txtRemolque2
            if (document.getElementById('txtRemolque2') == null || document.getElementById('txtRemolque2') == 'undefined') {
                Arguments[3] = '';
            } else {
                Arguments[3] = document.getElementById('txtRemolque2').value;
            }
            pridesp01_pre_i_despasignarpedidos.ValidaLineasyRemolques(strLinea, strRem, intRemNum, Arguments, ValidaLineasyRemolques_CallBack);
        }
    } catch (e) {
        alert("Error Ajax:" + e.message);
    }
}

function ValidaLineasyRemolques_CallBack(response) {
    if (response.error != null) { alert("Error Ajax:" + response.error); return; }
    if (response.value[0] == 'Error') {
        lblErrorAjax.innerText = response.value[1]
        if (response.value[2] == 1) {
            document.getElementById('txtFianza1').value = ''
            setFocus('txtRemolque1')
            document.getElementById('txtRemolque1').value = ""
            if (document.getElementById('txtHdnParCapCarga').value == 'S') {
                document.getElementById('txtIdTipoUR1').value = 0;
                document.getElementById('txtTipoURem1').value = '';
            }
            modifrem1()
            if (document.getElementById('txtHdnParCapCarga').value == 'S') {
                //ObtieneCapacidadCarga()
            }
        } else {
            document.getElementById('txtFianza2').value = ''
            setFocus('txtRemolque2')
            document.getElementById('txtRemolque2').value = ""
            if (document.getElementById('txtHdnParCapCarga').value == 'S') {
                document.getElementById('txtIdTipoUR2').value = 0;
                document.getElementById('txtTipoURem2').value = '';
            }
            modifrem2()
            if (document.getElementById('txtHdnParCapCarga').value == 'S') {
                //ObtieneCapacidadCarga()
            }
        }
    } else {
        lblErrorAjax.innerText = ""
        if (response.value[0] == 1) {
            if (response.value[1] == "InfoRem1") {
                document.getElementById('txtIdTipoUR1').value = response.value[2]
                document.getElementById('txtTipoURem1').value = response.value[3]
                document.getElementById('txtFianza1').value = response.value[4]
            }
            setFocus('txtDolly')
            modifrem1()
            if (document.getElementById('txtHdnParCapCarga').value == 'S') {
                ObtieneCapacidadCarga();
            }
            document.getElementById('txtFianza1').value = response.value[4]
        } else {
            if (response.value[1] == "InfoRem2") {
                document.getElementById('txtIdTipoUR2').value = response.value[2]
                document.getElementById('txtTipoURem2').value = response.value[3]
                document.getElementById('txtFianza2').value   = response.value[4]
            }
            setFocus('txtOperador1')
            modifrem2()
            if (document.getElementById('txtHdnParCapCarga').value == 'S') {
                ObtieneCapacidadCarga();
            }
            document.getElementById('txtFianza2').value = response.value[4]
        }
    }
}

//Valida Dolly
function ValidaDolly(dolly) {
    if (dolly != '') {
        pridesp01_pre_i_despasignarpedidos.ValidaDolly(dolly, Dolly_CallBack);
    }
}

function Dolly_CallBack(response) {
    if (response.error != null) { alert("Error Ajax:" + response.error); return; }
    if (response.value[0] == 'Error') {
        document.getElementById('txtDolly').focus();
        document.getElementById('txtDolly').value = ""
        lblErrorAjax.innerText = response.value[1]
        if (document.getElementById('txtHdnParCapCarga').value == 'S') {
            document.getElementById('txtIdTipoUD').value = 0;
            document.getElementById('txtTipoUDolly').value = '';
        }
        setFocus('txtDolly')
        if (document.getElementById('txtHdnParCapCarga').value == 'S') {
            //ObtieneCapacidadCarga();
        }
    } else {
        if (response.value[0] == "InfoTipoUnidad") {
            document.getElementById('txtIdTipoUD').value = response.value[1]
            document.getElementById('txtTipoUDolly').value = response.value[2]

        } else {
            lblErrorAjax.innerText = ""
        }
        setFocus('txtRemolque2')
        if (document.getElementById('txtHdnParCapCarga').value == 'S') {
            ObtieneCapacidadCarga();
        }
    }
}

function setFocus(id) {
    try {
        var oCampo = document.getElementById(id);
        if (oCampo != null) {
            oCampo.focus();
        }
    } catch (e) {
        alert(e.message);
    }
}

function MuestraCargando(bolMuestra) {
    if (bolMuestra) {
        try {
            _backgroundElement.style.display = 'block';
            _backgroundElement.style.position = 'absolute';
            _backgroundElement.style.left = '0px';
            _backgroundElement.style.top = '0px';
            var clientBounds = this._getClientBounds(); //obtiene las medidas del navegador.
            var clientWidth = clientBounds.width;
            var clientHeight = clientBounds.height;
            _backgroundElement.style.width = Math.max(Math.max(document.documentElement.scrollWidth, document.body.scrollWidth), clientWidth) + 'px';
            _backgroundElement.style.height = Math.max(Math.max(document.documentElement.scrollHeight, document.body.scrollHeight), clientHeight) + 'px';
            var intCentro = Math.max(Math.max(document.documentElement.scrollHeight, document.body.scrollHeight), clientHeight) / 2
            _backgroundElement.style.zIndex = 10000;
            _backgroundElement.innerHTML = '<div  style="padding-top:' + intCentro + 'px;" align="center"><img src="../imagenes/loading.gif" /><div><h3>Por favor espere, cargando...</h3></div></div>';
            _backgroundElement.className = "Bground";
        } catch (e) {

        }
    }else {
        _backgroundElement.style.display = 'none';
    }
}

//OBTIENE LA INFORAMCIÓN DEL ALTO Y ANCHO DEL NAVEGADOR.
function _getClientBounds() {
    var clientWidth;
    var clientHeight;
    switch (Sys.Browser.agent) {
        case Sys.Browser.InternetExplorer:
            clientWidth = document.documentElement.clientWidth;
            clientHeight = document.documentElement.clientHeight;
            break;
        case Sys.Browser.Safari:
            clientWidth = window.innerWidth;
            clientHeight = window.innerHeight;
            break;
        case Sys.Browser.Opera:
            clientWidth = Math.min(window.innerWidth, document.body.clientWidth);
            clientHeight = Math.min(window.innerHeight, document.body.clientHeight);
            break;
        default:  // Sys.Browser.Firefox, etc.
            clientWidth = Math.min(window.innerWidth, document.documentElement.clientWidth);
            clientHeight = Math.min(window.innerHeight, document.documentElement.clientHeight);
            break;
    }
    return new Sys.UI.Bounds(0, 0, clientWidth, clientHeight);
}

//Guardar
function Guardar() {
    //jmartin folio: 24492  09/06/2018
    /*--------------------------------------------------------------------------------------------------------------------------------------

                                                                I M P O R T A N T E

            Cada vez que se redimensione el arreglo (Array1), además del valor que utilizarán, agregar un default para esa nueva posición.

            e.g:
                // usuario folio: XXXXXX dd/MM/yyyy
                if(document.getElementById("hdnParametro").value == 'S'){
                    Array1[59] = '123';
                }
                else{
                    Array1[59] = '0';
                }
      --------------------------------------------------------------------------------------------------------------------------------------
    */

    if (document.getElementById('txtHdnValidaArmado').value == 1 && (document.getElementById('txtRemolque1').value != '' && document.getElementById('txtRemolque2').value != '' && document.getElementById("txtDolly").value == '')) {
        lblErrorAjax.innerText = 'Debe capturar un Dolly, ya que se tienen 2 remolques'
        return;
    }
    document.getElementById("btnGuarda").disabled = true;
    MuestraCargando(true)
    var Array1 = new Array(); var rowIndex;
    var oRow; var intAsignacion; var grid;
    if (document.getElementById('txtIdGrid').value == 'uwgUnidades') {
        rowIndex = window.opener.igtbl_getGridById('uwgUnidades').ActiveRow
        oRow = window.opener.igtbl_getRowById(rowIndex)
        grid = window.opener.igtbl_getGridById('uwgPedidos');
        Array1[16] = document.getElementById("idFlota").value; //window.opener.document.form1.WebPanel1_txtFlota.value
    } else {
        rowIndex = window.opener.opener.igtbl_getGridById('uwgUnidades').ActiveRow
        oRow = window.opener.opener.igtbl_getRowById(rowIndex)
        grid = window.opener.opener.igtbl_getGridById('uwgPedidos');
        Array1[16] = document.getElementById("idFlota").value; //window.opener.opener.document.form1.WebPanel1_txtFlota.value
    }
    if (oRow != null) {
        intAsignacion = oRow.getCellFromKey("id_asignacion").getValue();
    } else {
        intAsignacion = '';
    }
    Array1[0] = document.getElementById("txt_hdAsignacion").value
    document.getElementById("txtActualiza").value = document.getElementById("txt_hdAsignacion").value
    if (document.getElementById('txtUnidad').value == '') {
        document.getElementById('txtUnidad').value = '0'
    }
    Array1[1] = document.getElementById('txtUnidad').value;

    //Recorre los pedidos seleccionados
    if (grid != null) {
        var strLista = ""; var strLista2 = ""; var strLista3 = ""; var aiAreaUsuario
        var strURL; var winAsignar; var lRow;
        for (lRow in grid.SelectedRows) {
            var oRow2; var value;
            if (document.getElementById('txtIdGrid').value == 'uwgUnidades') {
                oRow2 = window.opener.igtbl_getRowById(lRow);
                aiAreaUsuario = window.opener.document.form1.txtAreaUsuario.value;
            } else {
                oRow2 = window.opener.opener.igtbl_getRowById(lRow);
                aiAreaUsuario = window.opener.opener.document.form1.txtAreaUsuario.value;
            }
            value = oRow2.getCellFromKey("id_pedido").getValue();
            if (value != null) { strLista = strLista + value + "[;]"; }
            value = oRow2.getCellFromKey("id_area").getValue();
            if (value != null) { strLista2 = strLista2 + value + "[;]"; }
            value = oRow2.getCellFromKey("id_pedidopk").getValue();
            if (value != null) { strLista3 = strLista3 + value + "[;]"; }
            if (oRow2.getCellFromKey("id_statusseg").getValue() != null) {
                var aiStatusSeg = oRow2.getCellFromKey("id_statusseg").getValue();
                if (aiStatusSeg == 5 && oRow2.getCellFromKey("id_area").getValue() != aiAreaUsuario) {
                    alert("El pedido tiene status Transbordo y debe pertenecer a la misma área del usuario para asignarlo nuevamente.");
                    return;
                }
            }
        }
        if (oRow2 == undefined) {
            var oRow3; var value; var continuaSinPedidos = false;
            if (grid.getActiveCell() == undefined)
            { continuaSinPedidos = true; }
            if (continuaSinPedidos == false) {
                oRow3 = grid.getActiveCell().Row;
                value = oRow3.getCellFromKey("id_pedido").getValue();
                if (value != null) { strLista = strLista + value + "[;]"; }
                value = oRow3.getCellFromKey("id_area").getValue();
                if (value != null) { strLista2 = strLista2 + value + "[;]"; }
                value = oRow3.getCellFromKey("id_pedidopk").getValue();
                if (value != null) { strLista3 = strLista3 + value + "[;]"; }
                if ((strLista == "") || (strLista2 == "") || (strLista3 == "")) {
                    alert("Debe seleccionar uno o mas Pedidos.");
                    return;
                }
            }
        }
    }
    Array1[2] = strLista;
    Array1[3] = strLista2;
    Array1[4] = strLista3;
    if (document.getElementById("txtRuta").value == '') {
        document.getElementById("txtRuta").value = '0';
    }
    Array1[5] = document.getElementById("txtRuta").value
    Array1[6] = FormatoFecha(igedit_getById('wdcInicioViaje').getText(), document.getElementById('txtFechaDefault').value)
    Array1[7] = FormatoFecha(igedit_getById('wdcFinViaje').getText(), document.getElementById('txtFechaDefault').value)
    Array1[8] = document.getElementById("txtIngreso").value
    if (document.getElementById("txtHrsEstandar").value == '') {
        document.getElementById("txtHrsEstandar").value = '0'
    }
    Array1[9] = document.getElementById("txtHrsEstandar").value
    Array1[10] = document.getElementById("txtKit").value
    Array1[11] = document.getElementById("txtLineaRem1").value
    Array1[12] = document.getElementById("txtRemolque1").value
    Array1[13] = document.getElementById("txtDolly").value
    Array1[14] = document.getElementById("txtLineaRem2").value
    Array1[15] = document.getElementById("txtRemolque2").value
    Array1[17] = document.getElementById("ddlConfigViaje").value;
    Array1[18] = document.getElementById("txtOperador1").value
    Array1[19] = document.getElementById("txtOperador2").value
    Array1[20] = document.getElementById("txtObservaciones").value
    Array1[21] = document.getElementById("txtSeguimiento").value
    Array1[22] = document.getElementById("txtLineaRem1Original").value
    Array1[23] = document.getElementById("txtRemolque1Original").value
    Array1[24] = document.getElementById("txtLineaRem2Original").value
    Array1[25] = document.getElementById("txtRemolque2Original").value
    Array1[26] = document.getElementById("txtUnidadOriginal").value
    Array1[27] = document.getElementById("txtFechaDefault").value

    //jmartin folio: 23121	17/10/2017
    //jmartin folio: 24492  09/06/2018
	Array1[53] = dvClasfMvto.style.display=='block' ?  document.getElementById('ddlClasf').value : 0;

    document.form1.txtAsignacion2.value = intAsignacion;
    // Asignación seleccionada del grid Unidades de Despacho
    Array1[28] = document.form1.txtAsignacion2.value;
    if (Array1[28] == "") {
        Array1[28] = "null"
    }
    //Se agrega la informacion de la Capacidad de Carga KG y M3, emartinez 20/Jun/2012
    if (document.getElementById('txtHdnParCapCarga').value == 'S') {
        Array1[29] = document.getElementById("txtCargarM3").value
        Array1[30] = document.getElementById("txtCargaKG").value
    } else {
        Array1[29] = "0"
        Array1[30] = "0"
    }

    //ERAMIREZ F:24499 05/06/2018
    if (document.getElementById('hdnShowOrigenDestinoVacio').value == 'S'){
        Array1[31] = document.getElementById("txtOrigen1").value;
        Array1[32] = document.getElementById("txtDestino1").value;
    } else {
        Array1[31] = document.getElementById("txtRemitente").value;
        Array1[32] = document.getElementById("txtDestinatario").value;
    }


    Array1[33] = document.getElementById("lblRemDestHabil").value;


        // Si esta el cambio de fianzas en la asignacion de pedidos, valida
           if (document.getElementById('DivFianza1').style.visibility == 'visible') {
                var arrFianza = new Array()
                var arrNacionalidad = new Array()
                //valida la fianza de los remolques americanos
                if (Array1[11] != '') { //Si tiene capturada Linea 1
                    // valida que el remolque este activo, siempre y cuando sea impo
                    arrFianza = EditarFianza(Array1[11], Array1[12], '1', false)
                    if (arrFianza.value[0] == 'Error') {
                         MuestraCargando(false);
                         document.getElementById("btnGuarda").disabled = false;
                         return;
                    }
                    else{
                        //Valida que este capturada la fianza
                        //Se agrega la validacion del tipo de servicio 'E', Jdlcruz, GASA 29-Nov-2012, Folio: 17748.
                        if (arrFianza.value[1] == 'I' || arrFianza.value[1] == 'E') {
                           if (document.getElementById('txtFianza1').value == '') {
                                document.getElementById('lblErrorAjax').innerText = 'El Remolque ' + Array1[12] + ' no tiene una fianza capturada. Favor de capturarla'
                                MuestraCargando(false);
                                document.getElementById("btnGuarda").disabled = false;
                                return;
                            }
                            else {
                                if (FianzaNoRtornada(arrFianza.value[2]) == false) {
                                    document.getElementById('lblErrorAjax').innerText = 'El Remolque ' + Array1[12] + ' tiene una fianza retornada. Favor de cambiarla'
                                    MuestraCargando(false);
                                    document.getElementById("btnGuarda").disabled = false;
                                    return;
                                }
                            }
                        }
                        else {
                            if (FianzaNoRtornada(arrFianza.value[2]) == false) {
                                document.getElementById('lblErrorAjax').innerText = 'El Remolque ' + Array1[12] + ' tiene una fianza retornada. Favor de cambiarla'
                                MuestraCargando(false);
                                document.getElementById("btnGuarda").disabled = false;
                                return;
                            }
                        }
                    }
                }
                if (Array1[14] != '') { //Si tiene capturada Linea 2
                        arrFianza =  EditarFianza(Array1[14], Array1[15], '1', false)
                        if (arrFianza.value[0] == 'Error') {
                             MuestraCargando(false);
                             document.getElementById("btnGuarda").disabled = false;
                             return;
                        }
                        else {
                            //Se agrega la validacion del tipo de servicio 'E', Jdlcruz, GASA 29-Nov-2012, Folio: 17748.
                            if (arrFianza.value[1] == 'I' || arrFianza.value[1] == 'E') {
                                 if (document.getElementById('txtFianza2').value == '') {
                                    document.getElementById('lblErrorAjax').innerText = 'El Remolque ' + Array1[15] + ' no tiene una fianza capturada. Favor de capturarla'
                                    MuestraCargando(false);
                                    document.getElementById("btnGuarda").disabled = false;
                                    return;
                                }
                                else {
                                    if (FianzaNoRtornada(arrFianza.value[2]) == false) {
                                        document.getElementById('lblErrorAjax').innerText = 'El Remolque ' + Array1[12] + ' tiene una fianza retornada. Favor de cambiarla'
                                        MuestraCargando(false);
                                        document.getElementById("btnGuarda").disabled = false;
                                        return;
                                    }
                                }
                            }
                            else {
                                if (FianzaNoRtornada(arrFianza.value[2]) == false) {
                                    document.getElementById('lblErrorAjax').innerText = 'El Remolque ' + Array1[12] + ' tiene una fianza retornada. Favor de cambiarla'
                                    MuestraCargando(false);
                                    document.getElementById("btnGuarda").disabled = false;
                                    return;
                                }
                            }
                        }
                }

                //Obtengo la nacionalidad para validar la fianza de los remolques propios americanos.
                //Jdlcruz, GASA 29-Nov-2012, Folio: 17748.
                if (Array1[11] == '' && Array1[12] != '') { //Si no tiene capturada la Linea 1 y pero si capturado el remolque 1
                    //Obtengo la nacionalidad del remolque.
                    arrNacionalidad = pridesp01_pre_i_despasignarpedidos.GetNacionalidadRemolque(Array1[12]);
                    if (arrNacionalidad.error != null) {
                        alert("Error:" + arrNacionalidad.error);
                        return;
                    }
                    if (arrNacionalidad.value[0] != 'Error') {
                        //Si la nacionalidad es Americana (2) raliza la validacion de remolque activo y fianza obligada.
                        if (arrNacionalidad.value[0] == 2) {
                            // valida que el remolque este activo, siempre y cuando sea impo
                            arrFianza = EditarFianza(Array1[11], Array1[12], '1', false)
                            if (arrFianza.value[0] == 'Error') {
                                MuestraCargando(false);
                                document.getElementById("btnGuarda").disabled = false;
                                return;
                            }
                            else {
                                //Valida que este capturada la fianza
                                if (arrFianza.value[1] == 'I' || arrFianza.value[1] == 'E') {
                                    if (document.getElementById('txtFianza1').value == '') {
                                        document.getElementById('lblErrorAjax').innerText = 'El Remolque ' + Array1[12] + ' no tiene una fianza capturada. Favor de capturarla'
                                        MuestraCargando(false);
                                        document.getElementById("btnGuarda").disabled = false;
                                        return;
                                    }
                                    else {
                                        if (FianzaNoRtornada(arrFianza.value[2]) == false) {
                                            document.getElementById('lblErrorAjax').innerText = 'El Remolque ' + Array1[12] + ' tiene una fianza retornada. Favor de cambiarla'
                                            MuestraCargando(false);
                                            document.getElementById("btnGuarda").disabled = false;
                                            return;
                                        }
                                    }
                                }
                                else {
                                    if (FianzaNoRtornada(arrFianza.value[2]) == false) {
                                        document.getElementById('lblErrorAjax').innerText = 'El Remolque ' + Array1[12] + ' tiene una fianza retornada. Favor de cambiarla'
                                        MuestraCargando(false);
                                        document.getElementById("btnGuarda").disabled = false;
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    else {
                        document.getElementById('lblErrorAjax').innerHTML = arrNacionalidad.value[1];
                        return arrControl;
                    }
                }

                //Obtengo la nacionalidad para validar la fianza de los remolques propios americanos.
                //Jdlcruz, GASA 29-Nov-2012, Folio: 17748.
                if (Array1[14] == '' && Array1[15] != '') { //Si no tiene capturada la Linea 2 y pero si capturado el remolque 2
                    //Obtengo la nacionalidad del remolque.
                    arrNacionalidad = pridesp01_pre_i_despasignarpedidos.GetNacionalidadRemolque(Array1[15]);
                    if (arrNacionalidad.error != null) {
                        alert("Error:" + arrNacionalidad.error);
                        return;
                    }
                    if (arrNacionalidad.value[0] != 'Error') {
                        //Si la nacionalidad es Americana (2) raliza la validacion de remolque activo y fianza obligada.
                        if (arrNacionalidad.value[0] == 2) {
                            // valida que el remolque este activo, siempre y cuando sea impo
                            arrFianza = EditarFianza(Array1[14], Array1[15], '1', false)
                            if (arrFianza.value[0] == 'Error') {
                                MuestraCargando(false);
                                document.getElementById("btnGuarda").disabled = false;
                                return;
                            }
                            else {
                                //Valida que este capturada la fianza
                                if (arrFianza.value[1] == 'I' || arrFianza.value[1] == 'E') {
                                    if (document.getElementById('txtFianza1').value == '') {
                                        document.getElementById('lblErrorAjax').innerText = 'El Remolque ' + Array1[15] + ' no tiene una fianza capturada. Favor de capturarla'
                                        MuestraCargando(false);
                                        document.getElementById("btnGuarda").disabled = false;
                                        return;
                                    }
                                    else {
                                        if (FianzaNoRtornada(arrFianza.value[2]) == false) {
                                            document.getElementById('lblErrorAjax').innerText = 'El Remolque ' + Array1[12] + ' tiene una fianza retornada. Favor de cambiarla'
                                            MuestraCargando(false);
                                            document.getElementById("btnGuarda").disabled = false;
                                            return;
                                        }
                                    }
                                }
                                else {
                                    if (FianzaNoRtornada(arrFianza.value[2]) == false) {
                                        document.getElementById('lblErrorAjax').innerText = 'El Remolque ' + Array1[12] + ' tiene una fianza retornada. Favor de cambiarla'
                                        MuestraCargando(false);
                                        document.getElementById("btnGuarda").disabled = false;
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    else {
                        document.getElementById('lblErrorAjax').innerHTML = arrNacionalidad.value[1];
                        return arrControl;
                    }
                }
            } // Termina cambio de fianzas

            //================================hrflores 20/06/2013 Cambio Contenedores de Pedidos================================//
            Array1[34] = document.getElementById("txtParamContenedores").value;
            Array1[35] = document.getElementById("txtIdPedido_Contenedores1").value;
            Array1[36] = document.getElementById("txtareaPedido_Contenedores1").value;
            Array1[37] = document.getElementById("txtIdPedido_Contenedores2").value;
            Array1[38] = document.getElementById("txtareaPedido_Contenedores2").value;

            Array1[39] = document.getElementById("txtIdContenedor1").value;
            Array1[40] = document.getElementById("txtIdContenedor1b").value;


            Array1[41] = document.getElementById("txtIdContenedor2").value;
            Array1[42] = document.getElementById("txtIdContenedor2b").value;

            //=================================================================================================================//
            //megallegos 08/08/2016 - SIMSA Folio: 22277
            var strTipoOp_AsigVV = document.getElementById("hdnTipoOp_AsigVV").value;
            if (strTipoOp_AsigVV == "S") {
                Array1[43] = document.getElementById("ddlTipoOp").value;
            } else {
                Array1[43] = document.getElementById("ddlOperacion").value;
            }

            //rherrera ETF 24/11/14
            var paramETF = document.getElementById("hdnParamTresFronteras").value;
            if( paramETF == "S" ){
                var bolCheckUrgente = document.getElementById("chkPedidoUrgente").checked;
                if (bolCheckUrgente == true) {
                    Array1[44] = '1';
                }
                else {
                    Array1[44] = '0';
                }
            }

            //ereyes 02/12/2015: OS Xpress Internacional Viajes vacios Auto
            if (document.getElementById("hdn_viajevacioauto").value == "S") {
                var TipoSegDetona = document.getElementById("hdn_tiposegdetonador").value;
                //1 :Validar que el tipo de seguimiento alla sido capturado
                if (document.getElementById("txtSeguimiento").value == "") {
                    document.getElementById('lblErrorAjax').innerText = 'Favor de capturar el tipo de seguimiento';
                    MuestraCargando(false)
                    return;
                } else {
                    //1 :Validar el tipo de seguimiento sea exportacion
                    if (document.getElementById("txtSeguimiento").value == TipoSegDetona) {
                        //valida que el ultimo destino coincida con el origen de al ruta actual si no coincide abrir ventana de creacion de viaje vacio.
                        var arreglo = new Array();
                        var arrRegreso = new Array();
                        var Unidad = document.getElementById('txtUnidad').value + "";
                        var RutaNueva = document.getElementById("txtRuta").value + "";
                        var Operador1 = document.getElementById("txtOperador1").value + "";
                        var Linea1 = document.getElementById("txtLineaRem1").value + "";
                        var Rem1 = document.getElementById("txtRemolque1").value + "" ;

                        arrRegreso = pridesp01_pre_i_despasignarpedidos.UltimoDestinoNuevoOrigen(Unidad, RutaNueva);
                        if (arrRegreso.value[0] == "Error") {
                            alert(arrRegreso.value[1]);
                            MuestraCargando(false)
                            document.getElementById("btnGuarda").disabled = false;
                            return;
                        } else {
                             if (arrRegreso.value[0] > 0 ) {
                                 //compara el utimo destino sea igual al nuevo origen para poder permitir continuar
                                 if (arrRegreso.value[0] != arrRegreso.value[1]) {
                                     //abrir ventana de nueva asignacion de viaje vacio
                                     MuestraCargando(false)
                                     var strURL = "";
                                     var winVacio;
                                     strURL = '../pridesp01/pre_i_despviajevacioauto.aspx?Accion=Nuevo&Lista=vacio[;]&Unidad=' + Unidad + '&Lista2=vacio[;]&Flota=0&Lista3=vacio[;]&IdGrid=uwgUnidades&UltDestino=' + arrRegreso.value[0] + '&OrigenRNueva=' + arrRegreso.value[1] + '&FechaIncio=' + arrRegreso.value[2] + "&operador1=" + Operador1 + "&LineaRem1=" + Linea1 + "&Rem1=" + Rem1;
                                     winVacio = window.open(strURL, 'winVacioX', 'width=850,height=500,status=no,scrollbars=no,left=50,top=50,resizable=no');
                                     winVacio.focus();
                                     document.getElementById("btnGuarda").disabled = false;
                                     return;
                                 }
                             }
                        }
                    }
                }
            }

            if (document.getElementById('diVentanaTiempos').style.display == 'block') {

                if (document.getElementById('txtIdventana').value == "") {
                    //alert("Es necesario capturar el numero de ventana de tiempos. Ya que el convenio de el pedido asignado lo requiere")
                    document.getElementById('lblErrorAjax').innerText = 'Es necesario capturar el numero de ventana de tiempos. Ya que el convenio de el pedido asignado lo requiere';
                    MuestraCargando(false);
                    document.getElementById("btnGuarda").disabled = false;
                    return;
                } else {
                    Array1[45] = document.getElementById('txtIdventana').value
                }

            } else {
                 Array1[45] = 'NULL'
            }
            //=================================================================================================================//
            //oerc 14/11/2016 - VacioAreaTracto Folio: 22592
            var strVacioAreaTracto = document.getElementById("hdnVacioXAreaTracto").value;
            if (strVacioAreaTracto == "S") {
                Array1[46] = 'S';
            } else {
                Array1[46] = 'N';
            }

            //rherrera GAAL
            if (document.getElementById('hdnObtenerGasolineras').value == 'S') {
                if (!document.getElementById("txtRutaRegreso").value == '') {
                    //document.getElementById("txtRutaRegreso").value = '0';
                    Array1[47] = document.getElementById("txtRutaRegreso").value; //Ruta de Regreso
                    Array1[48] = FormatoFecha(igedit_getById('wdcInicioViaje').getText(), document.getElementById('txtFechaDefault').value); //F_prog_ini_viaje
                    Array1[49] = FormatoFecha(igedit_getById('wdcFinViaje').getText(), document.getElementById('txtFechaDefault').value); //F_prog_fin_viaje
                    if (document.getElementById("txtHrsEstandar").value == '') {
                        document.getElementById("txtHrsEstandar").value = '0';
                    }
                    Array1[50] = document.getElementById("txtHrsEstandar").value; //HrsEstandar
                    Array1[51] = "1";  //viaje_regreso
                    wayPoints_regreso = pridesp01_pre_i_despasignarpedidos.getPuntosInteres(Array1[47]);
                }
                else {
                Array1[51] = "0";  //viaje_regreso
                }
                //Se trae los waypoints de las rutas
                wayPoints = pridesp01_pre_i_despasignarpedidos.getPuntosInteres(Array1[5]);

                //Obtiene el rendimiento de la Unidad, comparando si está capturado el remolque 2
                if (Array1[15] != '') { //si es full
                    rendimientoUnidad = 1.5
                }
                else{
                    rendimientoUnidad = 2.0
                }

            }

            if (document.getElementById('hdnValidaDocumentos').value == 'S') {
                Array1[52] = document.getElementById("ddlTipoServicio").value;
            }

            //FOLIO: 24328, GAFIGUEROA 28/04/2018
            if (document.getElementById("HdnCambiosOpeAlmex").value == "S") {
                Array1[53] = '0';
                Array1[54] = document.getElementById("HdnIdRecorrido").value;
                Array1[55] = document.getElementById("HdnIdRecorridoOrigen").value;
                Array1[56] = document.getElementById("HdnIdRecorridoDestino").value;
            }else{
                //jmartin folio: 24492  09/06/2018
                //Se asignan defaults para futuros cambios
                Array1[53] = '0';
                Array1[54] = '0';
                Array1[55] = '0';
                Array1[56] = '0';
                Array1[57] = '0';
            }

            // AGONZALEZ FOLIO:24443 30/05/2018
            if (document.getElementById('hdnLigaMonitoreo').value == 'S') {
                if (!document.getElementById("txtVelMax").value == '') {
                    Array1[58] = document.getElementById("txtVelMax").value;
                }
                else {
                    Array1[58] = "0";
                    document.getElementById('lblErrorAjax').innerText = 'Es necesario capturar la velocidad maxima';
                    MuestraCargando(false);
                    document.getElementById("btnGuarda").disabled = false;
                    return;
                }
            } else {
                Array1[58] = "0";
            }

            // GPEREZ F:24656 12/07/18 Destinatario en asignación, para viajes vacios - INNOVATIVOS
            if (document.getElementById("hdnUbicacionDiasRemolque").value == 'S') {
                if (!document.getElementById("txtDestinat").value == '') {
                    Array1[32] = document.getElementById("txtDestinat").value;
                }
                else {
                    Array1[32] = '0';
                }
            }
            //PHERNANDEZ    23/08/2018  FOLIO: 24834
            if (document.getElementById('hdnClasificaTipoRuta').value == 'S') {
                Array1[59] = document.getElementById('ddTipoRuta').value;
                Array1[60] = document.getElementById('ddModoCarga').value;
            }

            //NMANUEL 17/09/18 FOLIO:24496,.
            if (document.getElementById('ddlTipoArmado1')) {
                Array1[61] = document.getElementById('ddlTipoArmado1').value;
            }
//
            pridesp01_pre_i_despasignarpedidos.Guardar(Array1, Guardar_CallBack);
}

function initialize() {
    if ((lat == null) && (lon == null)) {
        lat = 24.842886;
        lon = -99.560470;
    }
    var mapOptions = {
        center: new google.maps.LatLng(lat, lon),
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: true,       // permite scroll dentro del mapa
        panControl: true,        // control de despalamiento del mapa
        zoomControl: true,       // barra lateral de zoom
        scaleControl: false,     // muesta la escala del mapa
        streetViewControl: true  // icono para habilitar el modo StreetView
    };
    map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
    var coord1 = astrOriLat + ',' + astrOriLon;
    var coord2 = astrDesLat + ',' + astrDesLon;
};

function calcRuta(oRutCoo1, oRutCoo2, dRutCoo1, dRutCoo2, tipo) {
    //cuando se carga una ruta la funcion es llamada con parametros
    if ((oRutCoo1 != undefined && oRutCoo1 != null) &&
        (oRutCoo2 != undefined && oRutCoo2 != null) &&
        (dRutCoo1 != undefined && dRutCoo1 != null) &&
        (dRutCoo2 != undefined && dRutCoo2 != null)) {
        oRut = oRutCoo1 + ',' + oRutCoo2;
        dRut = dRutCoo1 + ',' + dRutCoo2;
        var ori = new google.maps.LatLng(oRutCoo1, oRutCoo2)
        var des = new google.maps.LatLng(dRutCoo1, dRutCoo2)
    }

    if (tipo == 1) {
        wayp = wayPoints
    }
    else
    {
        wayp = wayPoints_regreso
    }
    var request = {
        origin: oRut, //ori,
        destination: dRut, //des
        waypoints: getWayP(wayp),
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING
    };

    directionServices.route(request, function(resp, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(resp);
            var bounds = resp.routes[0].bounds;
            showSteps(resp, tipo);
            var respGoogle = [];
            respGoogle = resp;
            //pridesp01_pre_i_despasignarpedidos.AjaxTest(JSON.stringify(respGoogle));
        }
        else if (status == google.maps.DirectionsStatus.NOT_FOUND) {
            alert("No se pudo trazar la ruta ya que contiene al menos un punto inválido. Verifique su ruta.");
            wayp.pop();
        }
        else if (status == google.maps.DirectionsStatus.MAX_WAYPOINTS_EXCEEDED) {
            alert("Solo se permite agregar 8 Puntos Intermedios");
        }
        else if (status == google.maps.DirectionsStatus.ZERO_RESULTS) {
            wayp.pop();
            alert("No se encontraron resultados, verifique los puntos origen, destino y puntos intermedios");
        }
        else {
            alert("error: " + status);
        }
    });
}

function getWayP(wp) {
    var wayPoint = [];

    for (i = 0; i < wp.value.length; i++) {
        //var waypointLatLng = new google.maps.LatLng(wp.value[i]);
        wayPoint.push({ location: wp.value[i]
        });
    }
    return wayPoint;

    //    for (i = 0; i < wp.length; i++) {
    //        wayPoint.push({ location: wp[i].location
    //        });
    //    }
    //    return wayPoint;

}

function showSteps(directionResult, tipo) {
    var markerArray = [];
    var contador = 0;
    var contadorverde = 0;
    var next_gas = false;
    var wayPoints = [];


    for (var g = 0; g < locations.length; g++) {

        next_gas = false;

        for (var l = 0; l < directionResult.routes[0].legs.length; l++) {
            var myRoute = directionResult.routes[0].legs[l];

            for (var i = 0; i < myRoute.steps.length; i++) {

                for (var j = 0; j < myRoute.steps[i].path.length; j++) {

                    var dif_lat = Math.abs(Math.abs(locations[g][5]) - Math.abs(myRoute.steps[i].path[j].lat())); //.lat

                    var dif_lng = Math.abs(Math.abs(locations[g][6]) - Math.abs(myRoute.steps[i].path[j].lng())); //.lng

                    if (dif_lat < 0.001 && dif_lng < 0.001) {

                        var cantPaths = myRoute.steps[i].path.length;
                        var result;

                        if ((j + 4) <= cantPaths - 1) {
                            var x1 = myRoute.steps[i].path[j].lng();
                            var y1 = myRoute.steps[i].path[j].lat();
                            var x2 = myRoute.steps[i].path[j + 4].lng();
                            var y2 = myRoute.steps[i].path[j + 4].lat();
                            var x0 = locations[g][6]; //lng
                            var y0 = locations[g][5]; //lat
                            result = (x0 * (y2 - y1)) + ((x1 - x2) * y0) + ((x2 * y1) - (y2 * x1));

                        }
                        else {
                            var x1 = myRoute.steps[i].path[j].lng();
                            var y1 = myRoute.steps[i].path[j].lat();
                            var x2 = myRoute.steps[i].path[cantPaths - 1].lng();
                            var y2 = myRoute.steps[i].path[cantPaths - 1].lat();
                            var x0 = locations[g][6]; //lng
                            var y0 = locations[g][5]; //lat
                            result = (x0 * (y2 - y1)) + ((x1 - x2) * y0) + ((x2 * y1) - (y2 * x1));
                        }

                        if (result >= 0) {

                            var coord = '' + locations[g][5] + ', ' + locations[g][6] + ''; //.lat , .lng
                            wayPoints.push({
                                location: coord
                            });
                            //rherrera GAAL: Estas son las coordenadas de las gasolineras que aplican para crear vale automáticamente
                            gaso.push(locations[g][0]);
                            gaso.push(locations[g][5]); //lat
                            gaso.push(locations[g][6]); //lon
                            gasolineras.push(gaso)

                            //Se mide las distancias
                            var locDist1 = astrOriLat + ',' + astrOriLon;
                            var locRegDist1 = astrOriRegLat + ',' + astrOriRegLon;
                            var locDist2 = locations[g][5] + ',' + locations[g][6];
                            //SetDistancia(locDist1, locDist2);
                            if (tipo == 1) {
                                ArrGaso[intContSteps] = locations[g][0] + ',' + document.getElementById('hdnIdAsignacion').value + ',' + document.getElementById('hdnIdAreaAsignacion').value + ',' + document.getElementById('hdnIdUnidad').value + ',' + locDist1 + ',' + locDist2 + ','
                                intContSteps = intContSteps + 1 //intCont
                            } else {
                            ArrGaso[intContSteps] = locations[g][0] + ',' + document.getElementById('hdnIdAsignacionRegreso').value + ',' + document.getElementById('hdnIdAreaAsignacion').value + ',' + document.getElementById('hdnIdUnidad').value + ',' + locRegDist1 + ',' + locDist2 + ','
                            intContSteps = intContSteps + 1 //intCont
                            }

                            SetDistancia2();

                        } else {
                            var strvar = 0;
                        }

                        contadorverde += 1;
                        next_gas = true;
                        if (next_gas) { break; }
                    }
                    contador = contador + 1;
                }
                if (next_gas) { break; }
            }
            if (next_gas) { break; }
        }
    }
    //gasolineras = JSON.stringify(gasolineras);
}

function SetDistancia(locDist1, locDist2) {
    var request = {
        origins: locDist1,
        destinations: locDist2,
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC
    };
    service.getDistanceMatrix(request, function (responseDist, status) {
        if (status = google.maps.DistanceMatrixStatus.OK) {
            var results = responseDist.rows[0].elements;
            var distancia = results[i].distance.text;
        }
      }
    )
  };

  function SetDistancia2() {
      var arryGas;
      var ErrNoSeguirGuardando = false
      arryGas = gasolineras;
      //Guardar la distancia
      for (i = (ArrGaso.length - 1); i < ArrGaso.length; i++) {
          strSubArray = ArrGaso[i].split(",")

          var locDist1 = strSubArray[4] + ',' + strSubArray[5]
          var locDist2 = strSubArray[6] + ',' + strSubArray[7]

          var request = {
              origins: [locDist1],
              destinations: [locDist2],
              travelMode: google.maps.TravelMode.DRIVING,
              unitSystem: google.maps.UnitSystem.METRIC
          };
          service.getDistanceMatrix(request, function(responseDist, status) {
              if (status = google.maps.DistanceMatrixStatus.OK) {
                  var distanciaRuta = responseDist.rows[0].elements[0].distance.value;
                  var distanciaRutaKms;
                  var lts_restantes;
                  distancia[icont] = responseDist.rows[0].elements[0].distance.value;

                  if (ArrGaso[icont] != undefined) {
                      ArrGaso[icont] = ArrGaso[icont] + '' + distanciaRuta + ','

                      icont = icont + 1;
                      if (icont == ArrGaso.length) {
                          var ArrGasoDist = new Array();
                          var resultDistancia;
                          var CombustibleMinimo;
                          for (y = 0; y < ArrGaso.length; y++) {
                              strSubArray = ArrGaso[y].split(",")
                              ArrGasoDist[y] = strSubArray[8];
                          }

                          CombustibleMinimo = document.getElementById('hdnCombustibleMinimo').value;
                          resultDistancia = Math.min.apply(Math, ArrGasoDist);
                          distanciaRutaKms = resultDistancia / 1000
                          lts_restantes = CombustibleMinimo - (distanciaRutaKms / rendimientoUnidad)

                          if (lts_restantes > 0) {
                              GuardaGasolineras(ArrGaso);
                          }
                          else {
                              window.close();
                          }
                      }
                  }
              }
          })
      }
  }

function SetGasolineras_CallBack(response) {
    var strS;
    strS = 1;
    //Pendiente*
}
//*******************************************

function FianzaNoRtornada(statusFianza) {
    if (statusFianza == "R" && document.getElementById("FianzaRetornada").value == "S") {
        return false;
    }
    else {
        return true;
    }
}


function Guardar_CallBack(response) {
    var bhayregreso = false;
    if (response.error != null) { return; }
    if (response.value[0] == 'Error') {
        document.getElementById("btnGuarda").disabled = false;
        lblErrorAjax.innerText = response.value[1]
        MuestraCargando(false)
    } else {
        var rowIndex; var oRow; var oGriduwgUnidades;
        oGriduwgUnidades = null
        if (document.getElementById('txtIdGrid').value == 'uwgUnidades') {
            oGriduwgUnidades = window.opener.igtbl_getGridById("uwgUnidades")
            rowIndex = window.opener.igtbl_getGridById('uwgUnidades').ActiveRow
            oRow = window.opener.igtbl_getRowById(rowIndex)
        } else {
            oGriduwgUnidades = window.opener.opener.igtbl_getGridById("uwgUnidades")
            rowIndex = window.opener.opener.igtbl_getGridById('uwgUnidades').ActiveRow
            oRow = window.opener.opener.igtbl_getRowById(rowIndex)
        }
        //rherrera GAAL ****************************************
        if (document.getElementById('hdnObtenerGasolineras').value == 'S') {
            var strSubArray;
            for (var i = 4; i < response.value.length; ++i) {
                strSubArray = response.value[i].split(",")
                switch (strSubArray[0]) {
                    //Viaje Normal
                    case "OriLon":
                        astrOriLon = strSubArray[1]
                        break;
                    case "OriLat":
                        astrOriLat = strSubArray[1]
                        break;
                    case "DesLon":
                        astrDesLon = strSubArray[1]
                        break;
                    case "DesLat":
                        astrDesLat = strSubArray[1]
                        break;
                    //Viaje Regreso
                    case "OriLonRegreso":
                        astrOriRegLon = strSubArray[1]
                        break;
                    case "OriLatRegreso":
                        astrOriRegLat = strSubArray[1]
                        break;
                    case "DesLonRegreso":
                        astrDesRegLon = strSubArray[1]
                        break;
                    case "DesLatRegreso":
                        astrDesRegLat = strSubArray[1]
                        break;
                    //Variables Regreso
                    case "id_asignacion_normal":
                        document.getElementById('hdnIdAsignacion').value = strSubArray[1]
                        break;
                    case "id_area_asignacion":
                        document.getElementById('hdnIdAreaAsignacion').value = strSubArray[1]
                        break;
                    case "id_unidad":
                        document.getElementById('hdnIdUnidad').value = strSubArray[1]
                        break;
                    case "id_asignacion_regreso":
                        document.getElementById('hdnIdAsignacionRegreso').value = strSubArray[1]
                        bhayregreso = true;
                        break;
                }
            }
            //Proceso para obtener las gasolineras y guardarlas en la tabla
            var coord1 = astrOriLat + ',' + astrOriLon;
            var coord2 = astrDesLat + ',' + astrDesLon;
            calcRuta(astrOriLat, astrOriLon, astrDesLat, astrDesLon, 1);
            //SI tiene ruta de regreso calcular
            if (bhayregreso == true) {
                calcRuta(astrOriRegLat, astrOriRegLon, astrDesRegLat, astrDesRegLon, 2);
            }
        }
        //******************************************************

        try {
            //Actualiza el registro, Agregando otro registro o agrupando, según sea el caso
            if (response.value[1] == 'NuevoRow') {
                //Nuevo Row
                if (response.value[2] == 'Actualiza') {
                    if (oGriduwgUnidades.GroupByBox.groups.length == 0) {
                        //Agrega Nuevo Registro
                        window.opener.igtbl_addNew('uwgUnidades', 0);
                        rowIndex = window.opener.igtbl_getGridById('uwgUnidades').ActiveRow
                        oRow = window.opener.igtbl_getRowById(rowIndex)
                        oGriduwgUnidades.setActiveRow(oRow);
                        oRow.setSelected(true);
                        ColumnasUnidadesDB(response, oRow)
                        if (window.opener.igtbl_getGridById('uwgPedidos') != null) {
                            //Elimina Pedidos Asignados
                            window.opener.igtbl_getGridById('uwgPedidos').AllowDelete = 1 //Si
                            window.opener.igtbl_deleteSelRows('uwgPedidos');
                            window.opener.igtbl_getGridById('uwgPedidos').AllowDelete = 2 //No
                        }
                    } else {
                        //Agrega Nuevo Registro
                        if (document.getElementById('txtIdGrid').value == 'uwgUnidades') {
                            var oRowId = window.opener.igtbl_getGridById('uwgUnidades').oActiveRow.Id
                            var oRow = window.opener.igtbl_getRowById(oRowId)
                        } else {
                            var oRowId = window.opener.opener.igtbl_getGridById('uwgUnidades').oActiveRow.Id
                            var oRow = window.opener.opener.igtbl_getRowById(oRowId)
                        }
                        ColumnasUnidadesDB(response, oRow)
                        //Si tiene pedidos
                        if (response.value[3] > 0) {
                            if (window.opener.igtbl_getGridById('uwgPedidos') != null) {
                                window.opener.igtbl_getGridById('uwgPedidos').AllowDelete = 1 //Si
                                window.opener.igtbl_deleteSelRows('uwgPedidos');
                                window.opener.igtbl_getGridById('uwgPedidos').AllowDelete = 2 //No
                            }
                        }
                    }
                } else {
                    if (oGriduwgUnidades.GroupByBox.groups.length == 0) {
                        ColumnasUnidadesDB(response, oRow)
                    } else {
                        if (document.getElementById('txtIdGrid').value == 'uwgUnidades') {
                            var oRowId = window.opener.igtbl_getGridById('uwgUnidades').oActiveRow.Id
                            var oRow = window.opener.igtbl_getRowById(oRowId)
                        } else {
                            var oRowId = window.opener.opener.igtbl_getGridById('uwgUnidades').oActiveRow.Id
                            var oRow = window.opener.opener.igtbl_getRowById(oRowId)
                        }
                        ColumnasUnidadesDB(response, oRow)
                    }
                    //Elimina Pedidos Asignados
                    //Si tiene pedidos
                    if (response.value[3] > 0) {
                        window.opener.igtbl_getGridById('uwgPedidos').AllowDelete = 1 //Si
                        window.opener.igtbl_deleteSelRows('uwgPedidos');
                        window.opener.igtbl_getGridById('uwgPedidos').AllowDelete = 2 //No
                    }
                }
            } else {
                var var1; var1 = false;
                if (oGriduwgUnidades.GroupByBox.groups.length == 0) {
                    for (var intcont = 0; intcont < oGriduwgUnidades.Rows.length; ++intcont) {
                        oGriduwgUnidades.Rows.rows[intcont].getNextRow();
                        if (document.getElementById("txtActualiza").value == oGriduwgUnidades.Rows.rows[intcont].getCellFromKey('id_asignacion').getValue()) {
                            //Obtiene el Row del registro seleccionado desde la Pantalla de VP
                            rowIndex = oGriduwgUnidades.Rows.rows[intcont].Id
                            if (document.getElementById('txtIdGrid').value == 'uwgUnidades') {
                                oRow = window.opener.igtbl_getRowById(rowIndex)
                            } else {
                                oRow = window.opener.opener.igtbl_getRowById(rowIndex)
                            }
                            oGriduwgUnidades.setActiveRow(oRow);
                            oRow.setSelected(true);
                            var1 = true
                            break;
                        }
                    }
                } else {
                    var oGriduwgUnidades;
                    if (document.getElementById('txtIdGrid').value == 'uwgUnidades') {
                        oGriduwgUnidades = window.opener.igtbl_getGridById("uwgUnidades")
                    } else {
                        oGriduwgUnidades = window.opener.opener.igtbl_getGridById("uwgUnidades")
                    }
                    for (var intcont = 0; intcont < oGriduwgUnidades.Rows.length; ++intcont) {
                        if (oGriduwgUnidades.Rows.rows[intcont].ChildRowsCount >> 0) {
                            var oRows = oGriduwgUnidades.Rows
                            oRow = oRows.getRow(intcont);
                            var oChildRows = oRow.Rows;
                            for (j = 0; j < oChildRows.length; j++) {
                                oChildRow = oChildRows.getRow(j);
                                for (c = 0; c < oChildRow.Band.Columns.length; c++) {
                                    var cell = oChildRow.getCell(c);
                                    if (cell != null) {
                                        switch (oChildRow.Band.Columns[c].Key) {
                                            case 'id_asignacion':
                                                if (document.getElementById("txtActualiza").value == cell.getValue()) {
                                                    var oRow;
                                                    if (document.getElementById('txtIdGrid').value == 'uwgUnidades') {
                                                        oRow = window.opener.igtbl_getRowById(cell.Row.Id)
                                                    } else {
                                                        oRow = window.opener.opener.igtbl_getRowById(cell.Row.Id)
                                                    }
                                                    oGriduwgUnidades.setActiveRow(oRow);
                                                    oRow.setSelected(true);
                                                    var1 = true
                                                } else {
                                                    break;
                                                }
                                                break;
                                        } // cierra switch
                                    }
                                } //cierra for columnas
                            } //cierra for childrows
                        }
                    }
                }
                //Si no esta dentro del For es una unidad con Viajes agrupados, Actualiza sobre el mismo registro
                if (var1 == false) {
                    rowIndex = oGriduwgUnidades.ActiveRow
                    if (document.getElementById('txtIdGrid').value == 'uwgUnidades') {
                        oRow = window.opener.igtbl_getRowById(rowIndex)
                    } else {
                        oRow = window.opener.opener.igtbl_getRowById(rowIndex)
                    }
                }
                //Actualiza Columnas
                if (oGriduwgUnidades.GroupByBox.groups.length == 0) {
                    ColumnasUnidadesDB(response, oRow)
                } else {
                    if (var1 == false) {
                        if (document.getElementById('txtIdGrid').value == 'uwgUnidades') {
                            var oRowId = window.opener.igtbl_getGridById('uwgUnidades').oActiveRow.Id
                            var oRow = window.opener.igtbl_getRowById(oRowId)
                        } else {
                            var oRowId = window.opener.opener.igtbl_getGridById('uwgUnidades').oActiveRow.Id
                            var oRow = window.opener.opener.igtbl_getRowById(oRowId)
                        }
                    }
                    ColumnasUnidadesDB(response, oRow)
                }
                oRow.setSelected(true);
            }

        } catch (er) {
            alert("Error Ajax: " + er.message)
            if (document.getElementById('hdnObtenerGasolineras').value == 'S') {
                document.getElementById("btnGuarda").disabled = true;
                //window.close()
            } else {
                window.close()
            }

        }
        lblErrorAjax.innerText = ""
        //document.getElementById("btnGuarda").disabled = false;
        MuestraCargando(false)
        //rherrera GAAL
        if (document.getElementById('hdnObtenerGasolineras').value == 'S') {
            document.getElementById("btnGuarda").disabled = true;
            //window.close()
        } else {
            window.close()
        }

        if (document.getElementById('txtIdGrid').value != 'uwgUnidades') {
            //rherrera GAAL
            if (document.getElementById('hdnObtenerGasolineras').value == 'S') {
                document.getElementById("btnGuarda").disabled = true;
                //window.opener.close()
            }
            else {
                window.opener.close()
            }

        }
    }
}

//rherrera GAAL
function GuardaGasolineras(arryGas) {
    pridesp01_pre_i_despasignarpedidos.SetGasolineras(arryGas);
    window.close();
}

function ColumnasUnidadesDB(response, oRow) {
    var strSubArray
    for (var i = 4; i < response.value.length; ++i) {
        strSubArray = response.value[i].split(",")
        if (strSubArray[2] == 'datetime') {
            oRow.getCellFromKey(strSubArray[0]).Value = strSubArray[1]
            oRow.getCellFromKey(strSubArray[0]).setValue(strSubArray[1])
        }
        //Inicializa Banderas
        else if (strSubArray[2] == 'bandera1') {
            if (strSubArray[3] > 0) {
                if (strSubArray[4] <= strSubArray[3] > 0) {
                    if (oRow.getCellFromKey("bandera1").Element != null) {
                        oRow.getCellFromKey("bandera1").Element.style.backgroundImage = "url(../Imagenes/controldeviajes/semaforo_verde.gif)"
                        oRow.getCellFromKey("bandera1").Column.Width = 30
                        oRow.getCellFromKey("bandera1").value = ""
                    }
                    else {
                        oRow.getCellFromKey("bandera1").Column.Style = 'backgroundImage:url(../Imagenes/controldeviajes/semaforo_verde.gif);Width:30px;';
                        oRow.getCellFromKey("bandera1").value = ""
                    }
                }
                else if (strSubArray[4] >= strSubArray[3] && strSubArray[6] <= strSubArray[5]) {
                    if (oRow.getCellFromKey("bandera1").Element != null) {
                        oRow.getCellFromKey("bandera1").Element.style.backgroundImage = "url(../imagenes/MttoAmarillo.gif)"
                        oRow.getCellFromKey("bandera1").Column.Width = 30
                        oRow.getCellFromKey("bandera1").value = ""
                    }
                    else {
                        oRow.getCellFromKey("bandera1").Column.Style = 'backgroundImage:url(../imagenes/MttoAmarillo.gif);Width:30px;';
                        oRow.getCellFromKey("bandera1").value = ""
                    }
                }
                else {
                    if (oRow.getCellFromKey("bandera1").Element != null) {
                        oRow.getCellFromKey("bandera1").Element.style.backgroundImage = "url(../imagenes/MttoRojo.gif)"
                        oRow.getCellFromKey("bandera1").Column.Width = 30
                        oRow.getCellFromKey("bandera1").value = ""
                    }
                    else {
                        oRow.getCellFromKey("bandera1").Column.Style = 'backgroundImage:url(../imagenes/MttoRojo.gif);Width:30px;';
                        oRow.getCellFromKey("bandera1").value = ""
                    }
                }
            }
        }
        else if (strSubArray[2] == 'bandera2') {
            if (strSubArray[3] > 0) {
                if (strSubArray[4] <= strSubArray[3] > 0) {
                    if (oRow.getCellFromKey("bandera2").Element != null) {
                        oRow.getCellFromKey("bandera2").Element.style.backgroundImage = "url(../Imagenes/controldeviajes/semaforo_verde.gif)"
                        oRow.getCellFromKey("bandera2").Column.Width = 30
                        oRow.getCellFromKey("bandera2").value = ""
                    }
                    else {
                        oRow.getCellFromKey("bandera2").Column.Style = 'backgroundImage:url(../Imagenes/controldeviajes/semaforo_verde.gif);Width:30px;';
                        oRow.getCellFromKey("bandera2").value = ""
                    }
                }
                else if (strSubArray[4] >= strSubArray[3] && strSubArray[6] <= strSubArray[5]) {
                    if (oRow.getCellFromKey("bandera2").Element != null) {
                        oRow.getCellFromKey("bandera2").Element.style.backgroundImage = "url(../imagenes/MttoAmarillo.gif)"
                        oRow.getCellFromKey("bandera2").Column.Width = 30
                        oRow.getCellFromKey("bandera2").value = ""
                    }
                    else {
                        oRow.getCellFromKey("bandera2").Column.Style = 'backgroundImage:url(../imagenes/MttoAmarillo.gif);Width:30px;';
                        oRow.getCellFromKey("bandera2").value = ""
                    }
                }
                else {
                    if (oRow.getCellFromKey("bandera2").Element != null) {
                        oRow.getCellFromKey("bandera2").Element.style.backgroundImage = "url(../imagenes/MttoRojo.gif)"
                        oRow.getCellFromKey("bandera2").Column.Width = 30
                        oRow.getCellFromKey("bandera2").value = ""
                    }
                    else {
                        oRow.getCellFromKey("bandera2").Column.Style = 'backgroundImage:url(../imagenes/MttoRojo.gif);Width:30px;';
                        oRow.getCellFromKey("bandera2").value = ""
                    }
                }
            }
        }
        else if (strSubArray[2] == 'bandera3') {
            if (strSubArray[3] > 0) {
                if (strSubArray[4] <= strSubArray[3] > 0) {
                    if (oRow.getCellFromKey("bandera3").Element != null) {
                        oRow.getCellFromKey("bandera3").Element.style.backgroundImage = "url(../Imagenes/controldeviajes/semaforo_verde.gif)"
                        oRow.getCellFromKey("bandera3").Column.Width = 30
                        oRow.getCellFromKey("bandera3").value = ""
                    }
                    else {
                        oRow.getCellFromKey("bandera3").Column.Style = 'backgroundImage:url(../Imagenes/controldeviajes/semaforo_verde.gif);Width:30px;';
                        oRow.getCellFromKey("bandera3").value = ""
                    }
                }
                else if (strSubArray[4] >= strSubArray[3] && strSubArray[6] <= strSubArray[5]) {
                    if (oRow.getCellFromKey("bandera3").Element != null) {
                        oRow.getCellFromKey("bandera3").Element.style.backgroundImage = "url(../imagenes/MttoAmarillo.gif)"
                        oRow.getCellFromKey("bandera3").Column.Width = 30
                        oRow.getCellFromKey("bandera3").value = ""
                    }
                    else {
                        oRow.getCellFromKey("bandera3").Column.Style = 'backgroundImage:url(../imagenes/MttoAmarillo.gif);Width:30px;';
                        oRow.getCellFromKey("bandera3").value = ""
                    }
                }
                else {
                    if (oRow.getCellFromKey("bandera3").Element != null) {
                        oRow.getCellFromKey("bandera3").Element.style.backgroundImage = "url(../imagenes/MttoRojo.gif)"
                        oRow.getCellFromKey("bandera3").Column.Width = 30
                        oRow.getCellFromKey("bandera3").value = ""
                    }
                    else {
                        oRow.getCellFromKey("bandera3").Column.Style = 'backgroundImage:url(../imagenes/MttoRojo.gif);Width:30px;';
                        oRow.getCellFromKey("bandera3").value = ""
                    }
                }
            }
        }
        else if (strSubArray[2] == 'bandera4') {
            if (strSubArray[3] > 0) {
                if (strSubArray[4] <= strSubArray[3] > 0) {
                    if (oRow.getCellFromKey("bandera4").Element != null) {
                        oRow.getCellFromKey("bandera4").Element.style.backgroundImage = "url(../Imagenes/controldeviajes/semaforo_verde.gif)"
                        oRow.getCellFromKey("bandera4").Column.Width = 30
                        oRow.getCellFromKey("bandera4").value = ""
                    }
                    else {
                        oRow.getCellFromKey("bandera4").Column.Style = 'backgroundImage:url(../Imagenes/controldeviajes/semaforo_verde.gif);Width:30px;';
                        oRow.getCellFromKey("bandera4").value = ""
                    }
                }
                else if (strSubArray[4] >= strSubArray[3] && strSubArray[6] <= strSubArray[5]) {
                    if (oRow.getCellFromKey("bandera4").Element != null) {
                        oRow.getCellFromKey("bandera4").Element.style.backgroundImage = "url(../imagenes/MttoAmarillo.gif)"
                        oRow.getCellFromKey("bandera4").Column.Width = 30
                        oRow.getCellFromKey("bandera4").value = ""
                    }
                    else {
                        oRow.getCellFromKey("bandera4").Column.Style = 'backgroundImage:url(../imagenes/MttoAmarillo.gif);Width:30px;';
                        oRow.getCellFromKey("bandera4").value = ""
                    }
                }
                else {
                    if (oRow.getCellFromKey("bandera4").Element != null) {
                        oRow.getCellFromKey("bandera4").Element.style.backgroundImage = "url(../imagenes/MttoRojo.gif)"
                        oRow.getCellFromKey("bandera4").Column.Width = 30
                        oRow.getCellFromKey("bandera4").value = ""
                    }
                    else {
                        oRow.getCellFromKey("bandera4").Column.Style = 'backgroundImage:url(../imagenes/MttoRojo.gif);Width:30px;';
                        oRow.getCellFromKey("bandera4").value = ""
                    }
                }
            }
        }
        else if (strSubArray[2] == 'indicador') {
            obj = oRow.getCellFromKey("indicador");
            if (obj != null) {
                if (oRow.getCellFromKey("indicador").Element != null) {

                    switch (strSubArray[1]) {
                        case "0":
                            oRow.getCellFromKey("indicador").Element.style.backgroundImage = "url(../imagenes/sinasiganar.bmp)"
                            oRow.getCellFromKey("indicador").Column.Width = 30
                            oRow.getCellFromKey("indicador").value = ""
                            oRow.getCellFromKey("indicador").Title = "No Aplica"
                            oRow.getCellFromKey("indicador").Element.title = "No Aplica"
                            break;
                        case "1":
                            oRow.getCellFromKey("indicador").Element.style.backgroundImage = "url(../imagenes/controldeviajes/semaforo_verde.gif)"
                            oRow.getCellFromKey("indicador").Column.Width = 30
                            oRow.getCellFromKey("indicador").value = ""
                            oRow.getCellFromKey("indicador").Title = "A tiempo"
                            oRow.getCellFromKey("indicador").Element.title = "A tiempo"
                            break;
                        case "2":
                            oRow.getCellFromKey("indicador").Element.style.backgroundImage = "url(../imagenes/MttoAmarillo.gif)"
                            oRow.getCellFromKey("indicador").Column.Width = 30
                            oRow.getCellFromKey("indicador").value = ""
                            oRow.getCellFromKey("indicador").Title = "Precaución"
                            oRow.getCellFromKey("indicador").Element.title = "Precaución"
                            break;
                        case "3":
                            oRow.getCellFromKey("indicador").Element.style.backgroundImage = "url(../imagenes/MttoRojo.gif)"
                            oRow.getCellFromKey("indicador").Column.Width = 30
                            oRow.getCellFromKey("indicador").value = ""
                            oRow.getCellFromKey("indicador").Title = "Retrasado"
                            oRow.getCellFromKey("indicador").Element.title = "Retrasado"
                            break;
                        default:
                            oRow.getCellFromKey("indicador").Element.style.backgroundImage = "url(../imagenes/sinasiganar.bmp)"
                            oRow.getCellFromKey("indicador").Column.Width = 30
                            oRow.getCellFromKey("indicador").value = ""
                            oRow.getCellFromKey("indicador").Title = "No Aplica"
                            oRow.getCellFromKey("indicador").Element.title = "No Aplica"
                    }
                }
            }
        }
        else if (strSubArray[2] == 'bandera5') {
            if (strSubArray[3] > 0) {
                if (strSubArray[4] <= strSubArray[3] > 0) {
                    if (oRow.getCellFromKey("bandera5").Element != null) {
                        oRow.getCellFromKey("bandera5").Element.style.backgroundImage = "url(../Imagenes/controldeviajes/semaforo_verde.gif)"
                        oRow.getCellFromKey("bandera5").Column.Width = 30
                        oRow.getCellFromKey("bandera5").value = ""
                    }
                    else {
                        oRow.getCellFromKey("bandera5").Column.Style = 'backgroundImage:url(../Imagenes/controldeviajes/semaforo_verde.gif);Width:30px;';
                        oRow.getCellFromKey("bandera5").value = ""
                    }
                }
                else if (strSubArray[4] >= strSubArray[3] && strSubArray[6] <= strSubArray[5]) {
                    if (oRow.getCellFromKey("bandera5").Element != null) {
                        oRow.getCellFromKey("bandera5").Element.style.backgroundImage = "url(../imagenes/MttoAmarillo.gif)"
                        oRow.getCellFromKey("bandera5").Column.Width = 30
                        oRow.getCellFromKey("bandera5").value = ""
                    }
                    else {
                        oRow.getCellFromKey("bandera5").Column.Style = 'backgroundImage:url(../imagenes/MttoAmarillo.gif);Width:30px;';
                        oRow.getCellFromKey("bandera5").value = ""
                    }
                }
                else {
                    if (oRow.getCellFromKey("bandera5").Element != null) {
                        oRow.getCellFromKey("bandera5").Element.style.backgroundImage = "url(../imagenes/MttoRojo.gif)"
                        oRow.getCellFromKey("bandera5").Column.Width = 30
                        oRow.getCellFromKey("bandera5").value = ""
                    }
                    else {
                        oRow.getCellFromKey("bandera5").Column.Style = 'backgroundImage:url(../imagenes/MttoRojo.gif);Width:30px;';
                        oRow.getCellFromKey("bandera5").value = ""
                    }
                }
            }
        }

        else if (strSubArray[2] == 'nombre_status_viaje') {
            if (strSubArray[4] != '') {
                if (strSubArray[4] != '' && strSubArray[5] != '') {
                    if (oRow.getCellFromKey("nombre_status_viaje").Element != null) {
                        if (strSubArray[5] > 0) {
                            oRow.getCellFromKey("nombre_status_viaje").Element.style.font.bold = true;
                        }
                        else if (strSubArray[4] > 0) {
                            oRow.getCellFromKey("nombre_status_viaje").Element.style.font.bold = true;
                        }
                        else {
                            oRow.getCellFromKey("nombre_status_viaje").Element.style.font.bold = false;
                        }
                        oRow.getCellFromKey("nombre_status_viaje").Element.style.font.italic = false;
                    }
                    else {
                        if (strSubArray[5] > 0) {
                            oRow.getCellFromKey("nombre_status_viaje").Column.Style = 'font-weight:bold;';
                        }
                        else if (strSubArray[4] > 0) {
                            oRow.getCellFromKey("nombre_status_viaje").Column.Style = 'font-weight:bold;';
                        }
                        else {
                            oRow.getCellFromKey("nombre_status_viaje").Column.Style = 'font-weight:bold;';
                        }
                        oRow.getCellFromKey("nombre_status_viaje").Column.Style = 'font-style:italic;';

                    }
                }
                else {
                    if (oRow.getCellFromKey("nombre_status_viaje").Element != null) {
                        oRow.getCellFromKey("nombre_status_viaje").Element.style.color = "red";
                        oRow.getCellFromKey("nombre_status_viaje").Element.style.font.italic = true;
                        oRow.getCellFromKey("nombre_status_viaje").Element.style.font.bold = true;
                    }
                    else {
                        oRow.getCellFromKey("nombre_status_viaje").Column.Style = 'font-weight:bold;color:red;font-style:italic;'
                    }
                }
            }
            oRow.getCellFromKey(strSubArray[0]).setValue(strSubArray[1])
        }

        else if (strSubArray[2] == 'no_viaje') {
            if (strSubArray[1] <= 0) {
                if (oRow.getCellFromKey("no_viaje").Element != null) {
                    oRow.getCellFromKey("no_viaje").Element.style.cursor = "auto";
                    oRow.getCellFromKey("no_viaje").Element.title = ""
                }
                else {
                    oRow.getCellFromKey("no_viaje").Column.Style = 'cursor:auto;';
                }
                oRow.getCellFromKey(strSubArray[0]).setValue(strSubArray[1])
            }
            else {
                oRow.getCellFromKey(strSubArray[0]).setValue(strSubArray[1])
            }
        }
        else if (strSubArray[2] == 'siguiente' && strSubArray[3] == 'Imagen') {
            if (oRow.getCellFromKey("siguiente").Element != null) {
                oRow.getCellFromKey("siguiente").Element.style.backgroundImage = "url(../Imagenes/controldeviajes/btnsiguientegrid.gif)"
                oRow.getCellFromKey("siguiente").Element.style.cursor = "hand";
            }
            else {
                oRow.getCellFromKey("siguiente").Column.Style = 'backgroundImage:url(../Imagenes/controldeviajes/btnsiguientegrid.gif);cursor:hand;';
            }
        }
        else if (strSubArray[2] == 'siguiente' && strSubArray[3] == 'SinImagen') {
            //No actualiza la imagen
            if (oRow.getCellFromKey("siguiente").Element != null) {
                oRow.getCellFromKey("siguiente").Element.style.backgroundImage = "url(../Imagenes/controldeviajes/spacer.GIF)"
                oRow.getCellFromKey("siguiente").Element.style.cursor = "auto";
            }
            else {
                oRow.getCellFromKey("siguiente").Column.Style = 'backgroundImage:url(../Imagenes/controldeviajes/spacer.GIF);cursor:auto;';
            }
        }
        else if (strSubArray[2] == 'OriLon') {
            //
        }
        else if (strSubArray[2] == 'OriLat') {
            //
        }
        else if (strSubArray[2] == 'DesLon') {
            //
        }
        else if (strSubArray[2] == 'DesLat') {
            //
        }
        else if (strSubArray[2] == 'OriLonRegreso') {
            //
        }
        else if (strSubArray[2] == 'OriLatRegreso') {
            //
        }
        else if (strSubArray[2] == 'DesLonRegreso') {
            //
        }
        else if (strSubArray[2] == 'DesLatRegreso') {
            //
        }
        else if (strSubArray[2] == 'id_asignacion_normal') {
                //
        }
        else if (strSubArray[2] == 'id_asignacion_regreso') {
            //
        }

        else {
            oRow.getCellFromKey(strSubArray[0]).setValue(strSubArray[1])
        }
    }
}

function BuscaSeguimiento() {
    var strURL; var winBuscStatus;
    strURL = '../busquedas/busqueda_despseguimiento.aspx?Campo=form1.txtSeguimiento&Descr=form1.txtSeguimientoDesc&SegTipoOp=1';
    winBuscStatus = window.open(strURL, 'BuscaSeguimiento', 'width=640,height=480,status=no,scrollbars=no');
    winBuscStatus.ventanaReferencia = window;
    winBuscStatus.Referencia = document.form1.txtSeguimiento;
    winBuscStatus.ReferenciaDesc = document.form1.txtSeguimientoDesc;
}

function AgregaTraslado() {
    var strURL; var winTraslado;
    strURL = '../pridesp01/pre_i_desptrasladoequipo.aspx?ParmAsignHdn=' + document.getElementById("txt_hdAsignacion").value + '&ParmStatus=' + document.getElementById("hdnStatus").value + '&ParmRem1=' + document.getElementById("txtRemolque1").value + '&ParmRem2=' + document.getElementById("txtRemolque2").value + '&ParmLinRem1=' + document.getElementById("txtLineaRem1").value + '&ParmLinRem2=' + document.getElementById("txtLineaRem2").value;
    winTraslado = window.open(strURL, 'TrasladodeEquipo', 'width=750,height=380,status=no,scrollbars=no');
    winTraslado.focus();
}

function wdcInicioViaje_ValueChange(oEdit, oldValue, oEvent) {
    //CambioValor('wdcInicioViaje');
}

function wdcFinViaje_ValueChange(oEdit, oldValue, oEvent) {
    //CambioValor('wdcFinViaje');
}

/*
CREA EL ELEMENTO DIV PARA OPACAR LA PANTALLA Y MOSTRAR MENSAJE DE CARGANDO
NO PERMITE MODIFICAR LA INFORMACIÓN HASTA QUE NO SE TERMINE DE REALIZAR EL PROCESO
*/

//var _backgroundElement = document.createElement("div");
// JGONZALEZ 07/12/2018 Folio 25414
var _backgroundElement = "";

function Load() {
    _backgroundElement = document.createElement("div"); // JGONZALEZ 07/12/2018
    $get("txtRuta").focus();
    $get("contenido").appendChild(_backgroundElement);
    if (document.layers) {
        document.captureEvents(Event.ONKEYPRESS);
    }
    document.onkeypress = CancelEnter;
}

function ActualizaTipoOperacion() {
    try {
            var strSeguimiento = document.getElementById('txtSeguimiento').value;
            pridesp01_pre_i_despasignarpedidos.GetTipoOperacionBySegNomCorto(strSeguimiento, ActualizaTipoOperacion_CallBack);
     } catch (e) {
        alert("Error Ajax:" + e.message);
   }

}

function ActualizaTipoOperacion_CallBack(response) {
    try {
        if (response.error != null) {
            alert("Error Ajax:" + response.error); return;
        }
        document.getElementById('ddlOperacion').value = response.value[0];
    } catch (e) {
        alert("Error Ajax:" + e.message);
    }
}

function CancelEnter() {
    if (event.keyCode == 13) {
        return false;
    }
}


//function Test() {
//    try {
//        if (document.getElementById('lblError') != null) {
//            document.getElementById('lblError').innerHTML = '';
//        }
//        var TUT = document.getElementById('txtIdTipoUTracto').value;
//        var TUR1 = document.getElementById('txtIdTipoUR1').value;
//        var TUD = document.getElementById('txtIdTipoUD').value;
//        var TUR2 = document.getElementById('txtIdTipoUR2').value;
//        var PesoCargar = document.getElementById('txtCargaKG').value;
//        var VolCargar = document.getElementById('txtCargarM3').value;
//        pridesp01_pre_i_despasignarpedidos.ObtieneCapacidadCarga(TUT, TUR1, TUR2, TUD, PesoCargar, VolCargar, ObtieneCapacidadCarga_CallBack);
//    } catch (e) {
//        alert("Error Ajax:" + e.message);
//    }
//}

//function ObtieneCapacidadCarga_CallBack(response) {
//    try {
//        if (response.error != null) {
//            document.getElementById('lblConfigMTC').innerHTML = "Configuración MTC: ";
//            alert("Error Ajax:" + response.error); return;
//        }
//        if (response.value[0] == 'Error') {
//            lblErrorAjax.innerText = ""
//            lblErrorAjax.innerText = response.value[1]
//            var oGuardar = document.getElementById('DivGuardar');
//            oGuardar.style.display = 'none';
//            document.getElementById('txtCapacidadKG').value = 0;
//            document.getElementById('txtCapacidadM3').value = 0;
//            document.getElementById('lblConfigMTC').innerHTML = "Configuración MTC: ";
//        } else {
//            lblErrorAjax.innerText = ""
//            var oGuardar = document.getElementById('DivGuardar');
//            oGuardar.style.display = 'block';
//            if (response.value[0] == "InfoCapacidad") {
//                document.getElementById('txtCapacidadKG').value = response.value[1];
//                document.getElementById('txtCapacidadM3').value = response.value[2];
//                document.getElementById('lblConfigMTC').innerHTML = response.value[3];
//            }
//        }
//    } catch (e) {
//        alert("Error Ajax:" + e.message);
//    }
//}

function ddlOperacionChange() {
}

function ddlTipoOpChange() {
}

/*
    Quethzel, F.23195
*/
var dataMensajes = [];

function validarDocumentos() {
    var param = $('#txtParamDocumentosPorUnidad').val();
    if (param == 1) {
        dataMensajes = [];

        // var unidad = $('#txtUnidad').val();
        // var remo1 = $('#txtRemolque1').val();
        // var remo2 = $('#txtRemolque2').val();
        // var dolly = $('#txtDolly').val();
        // var oper1 = $('#txtOperador1').val();
        // var oper2 = $('#txtOperador2').val();

        // if(unidad != "") checkDocumentosPorUnidad(unidad);
        // if(remo1 != "") checkDocumentosPorUnidad(remo1);
        // if(remo2 != "") checkDocumentosPorUnidad(remo2);
        // if(dolly != "") checkDocumentosPorUnidad(dolly);
        // if(oper1 != "") checkLicenciaOperador(oper1);
        // if(oper2 != "") checkLicenciaOperador(oper2);

        // setTimeout(function() {mostrarAvisos();},3000);
        checkDocumentosUnidad($('#txtUnidad').val());
    }
}

// ------------------------------------------------------------------------------------------------------
function checkDocumentosUnidad(id_unidad) {
        pridesp01_pre_i_despasignarpedidos.CheckDocumentosUnidadCF(id_unidad, checkDocumentosUnidad_CallBack)
}
function checkDocumentosUnidad_CallBack(data) {
    if (data.error) {
        console.log(data.error);
        $(document.createElement('p')).text(data.error).addClass('LabelError').appendTo('#ls-error-div');
    }
    else {
        data = JSON.parse(data.value);
        generarAvisos(data);
        var remo1 = $('#txtRemolque1').val();
        checkDocumentosRemo1(remo1);
    }
}
function checkDocumentosRemo1(id_unidad) {
    pridesp01_pre_i_despasignarpedidos.CheckDocumentosUnidadCF(id_unidad, checkDocumentosRemo1_CallBack)
}
function checkDocumentosRemo1_CallBack(data) {
    if (data.error) {
        console.log(data.error);
        $(document.createElement('p')).text(data.error).addClass('LabelError').appendTo('#ls-error-div');
    }
    else {
        data = JSON.parse(data.value);
        generarAvisos(data);
        var remo2 = $('#txtRemolque2').val();
        checkDocumentosRemo2(remo2);
    }
}
function checkDocumentosRemo2(id_unidad) {
        pridesp01_pre_i_despasignarpedidos.CheckDocumentosUnidadCF(id_unidad, checkDocumentosRemo2_CallBack)
}
function checkDocumentosRemo2_CallBack(data) {
    if (data.error) {
        console.log(data.error);
        $(document.createElement('p')).text(data.error).addClass('LabelError').appendTo('#ls-error-div');
    }
    else {
        data = JSON.parse(data.value);
        generarAvisos(data);
        var dolly = $('#txtDolly').val();
        checkDocumentosDolly(dolly);
    }
}
function checkDocumentosDolly(id_unidad) {
        pridesp01_pre_i_despasignarpedidos.CheckDocumentosUnidadCF(id_unidad, checkDocumentosDolly_CallBack)
}
function checkDocumentosDolly_CallBack(data) {
    if (data.error) {
        console.log(data.error);
        $(document.createElement('p')).text(data.error).addClass('LabelError').appendTo('#ls-error-div');
    }
    else {
        data = JSON.parse(data.value);
        generarAvisos(data);
        var oper1 = $('#txtOperador1').val();
        checkLicenciaOperador1(oper1);
    }
}
function checkLicenciaOperador1(id_operador, control) {
        pridesp01_pre_i_despasignarpedidos.CheckLicenciaOperador(id_operador, checkLicenciaOperador1_CallBack)
}
function checkLicenciaOperador1_CallBack(data) {
    if(data.error) {
        console.log(data.error);
        $(document.createElement('p')).text(data.error).addClass('LabelError').appendTo('#ls-error-div');
    }
    else {
        data = JSON.parse(data.value);
        generarAvisos(data);
        var oper2 = $('#txtOperador2').val();
        checkLicenciaOperador2(oper2);
    }
}
function checkLicenciaOperador2(id_operador, control) {
        pridesp01_pre_i_despasignarpedidos.CheckLicenciaOperador(id_operador, checkLicenciaOperador2_CallBack)
}
function checkLicenciaOperador2_CallBack(data) {
    if(data.error) {
        console.log(data.error);
        $(document.createElement('p')).text(data.error).addClass('LabelError').appendTo('#ls-error-div');
    }
    else {
        data = JSON.parse(data.value);
        generarAvisos(data);
        mostrarAvisos();
    }
}
// ------------------------------------------------------------------------------------------------------

function checkDocumentosPorUnidad(id_unidad) {
    pridesp01_pre_i_despasignarpedidos.CheckDocumentosUnidadCF(id_unidad, checkDocumentosPorUnidad_CallBack)
}
function checkDocumentosPorUnidad_CallBack(data) {
    if (data.error) {
        console.log(data.error);
        $(document.createElement('p')).text(data.error).addClass('LabelError').appendTo('#ls-error-div');
    }
    else {
        data = JSON.parse(data.value);
        generarAvisos(data);
    }
}

function checkLicenciaOperador(id_operador, control) {
    pridesp01_pre_i_despasignarpedidos.CheckLicenciaOperador(id_operador, checkLicenciaOperador_CallBack)
}
function checkLicenciaOperador_CallBack(data) {
    if(data.error) {
        console.log(data.error);
        $(document.createElement('p')).text(data.error).addClass('LabelError').appendTo('#ls-error-div');
    }
    else {
        data = JSON.parse(data.value);
        generarAvisos(data);
    }
}

function generarAvisos(data) {
    if(data.length > 0) {
        for (var i = 0; i < data.length; i++) {
            dataMensajes.push( { 'mensaje': data[i].mensaje, 'tipo_aviso': data[i].tipo_aviso } );
        };
    }
}

function mostrarAvisos() {
    var disableBtn = false;
    $('#ls-error-div').empty();
    for (var i = 0; i < dataMensajes.length; i++) {
        window.resizeTo(800, 600);
        if(dataMensajes[i].tipo_aviso == 1) {
            disableBtn = true;
        }
        $(document.createElement('p')).text(dataMensajes[i].mensaje).addClass('LabelError').appendTo('#ls-error-div');
    };
    $('#btnGuarda').prop('disabled', disableBtn);
}
