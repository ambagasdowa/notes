Imports System.Data
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Converters
Imports Newtonsoft.Json.Linq
Imports System.Collections.Generic
Imports Ajax.JSON
Imports System.Web.Script.Serialization
Imports System.Web.Services
Imports DataFramework

'Pantalla : Asignaci�n de Pedidos
Partial Class pridesp01_pre_i_despasignarpedidos
    Inherits systempageclass
    Protected key As String

#Region "Propiedades"

    Public strImgTractor, strImgRemolque1, strImgDolly, strImgRemolque2 As String
    Public bolCierraVentana As Boolean = False
    Public strOpcionGral As String = ""
    Public bolValidaKit As Boolean = False
    Public istrParamTresFronteras As String
    'Protected WithEvents txtHdnIndice As System.Web.UI.HtmlControls.HtmlInputHidden
    Private iBR As New net_b_despextended.pre_b_despasignarpedido
    Private iBROperador As New net_b_zamclases.net_b_clasepersonalcategorias
    Private iBRControlViajes As New net_b_despextended.pre_b_despcontroldeviajes 'megallegos 19/07/2016 // Folio:22197 MTC
    Private iBRValesComb As New net_b_despextended.pre_b_despvalescombustible 'megallegos 19/07/2016 // Folio:22197 MTC
    Private iBRPedidos As New net_b_despextended.pre_b_desppedidos
    Private lSegCtrl As New net_b_extended.net_b_login
    Private iuoBRUnidad As New net_b_metaclases.net_b_mclaseunidad
    Private luo_br_layout As New net_b_servicios.net_b_layouts
    Private iuoAsignacion As New net_b_zamclases.net_b_claseasignacionpedido
    Private iuoUnidad As New net_b_zamclases.net_b_claseunidad
    Private ddt_datos_vista As New Data.DataTable
    Private dtDatosREM As New Data.DataTable 'megallegos 30/01/2017 // Folio:22820 TRAREYSA
    Public lstrMultiCia As String = "", lstrValidaCapCarga As String = "", lstrValidaFianza As String = ""
    Public lstr_viajesvacios As String = "N", lstr_tiposegdetonador As String = ""
    Public lstr_bloquearemped As String = "N"
    Dim luoParm As New net_b_zamclasesextended.net_b_clasegeneralparametrosextended
    Public KGCargar As Decimal = 0, M3Cargar As Decimal = 0
    Public CapacidadM3 As Decimal = 0, CapacidadKG As Decimal = 0
    Dim strRemDest As String = "N"
    Public muestra_persmisionario As String
    Dim str_activatipooperacion As String = "N"
    Dim str_TipoOperacionPorUsuario As String = "N"
    Public ParmValidaFechVigLicOper As String = ""
    Public ParmValidaKmsUnidadSigPrev As String = ""
    Public MensajeAvisoKmsUnidad As String = ""
    Public strParmViajesPendientes As String
    Public intParmLimiteViajesPen As Decimal
    Private Shared strIdFlota As String
    Public strParsmVentanastiempos As String = ""
    'NMANUEL 19/07/2018 FOLIO:24655 PARAM BLOQUEO FECHA CHRYSLER
    Public strParamFechaCPO As String = ""
    Private luoFlotas As New net_b_zamclases.net_b_claseflotas
    Public strVal_CargasComb As String = ""  'megallegos 19/07/2016 // Folio:22197 MTC
    Public strTipoOp_AsigVV As String = "" 'megallegos -> Folio:22277 // SIMSA
    Public strParmValidaMaxAsign As String = ""
    Public intTipoOp As Integer = 0
    Public strtipoOpPed As String = "" 'megallegos 23/01/2017 // Folio:22791 MTC
    Public lstrMsgREM As String = "" 'megallegos 30/01/2017 // Folio:22820 TRAREYSA
    Public strValidaOrdenServREM As String = "" 'megallegos 30/01/2017 // Folio:22820 TRAREYSA
	Public strParmValesAutomaticos As String = "N" 'rherrera GAAL
    Private netVi As New net_b_zam.net_visor 'rherrera GAAL
    Public istrHabilitarFuncionalidadesAEO As String ' MSALVARADO AEO
    Public strFusionTlocalOPV8 As String 'oerc 24/08/ 17 
    Public lStrBloqueaRemolque As String = "N" 'gafigueroa 24/01/2018 // Folio: 23907, ALMEX
    Public lStrValidaOperPermEnUnidadPerm As String = "N" 'lehernandez 11/04/2018 folio: 24251 ALMEX
    Public strParmLigaMonitoreo As String = "N" 'AGONZALEZ 30/05/2018 Folio: 24443
    Private iuoBrClaseConsolidado As New net_b_clasesgwextended.net_b_claseconsolidadoextended

    Private istr_cambiaLongitudDescRuta As String = "" 'jmartin folio: 24453    31/05/2018
    Private istr_cambiaAnchoDescRuta As String = "" 'jmartin folio: 24453   07/09/2018
    Private bol_4contenedores As Boolean = False 'AGONZALEZ 02/08/2018 FOLIO:24709
    'PHERNANDEZ 23/08/2018  FOLIO: 24834
    Public lstr_TipoRuta As String = "N"

    'Folio 25019, 25055 oerc 11/10/18
    Public aplica_gposvision As String
    Private istr_ctrl_razon_social As String = "N" 'AMONTIEL FOLIO:25313
#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim arrListaPedidos As New ArrayList
        Dim arrListaPedidosAreas As New ArrayList
        Dim arrListaPedidosPK As New ArrayList
        Dim ldtFechas As New Data.DataTable
        Dim ldtRutaXMvto As New Data.DataTable
        Try
            Me.ValidaSesionUsuarios()
            Dim iuo_BRPedidos As New net_b_despextended.pre_b_desppedidos
            Ajax.Utility.RegisterTypeForAjax(GetType(pridesp01_pre_i_despasignarpedidos))
            key = "key=" & ConfigurationManager.AppSettings("APIKeyMap")
            Me.txtFechaDefault.Text = Application("DateIISTimeFormat")

            If Session("UserName") Is Nothing Or Session("vg_area") Is Nothing Then
                Response.Redirect(Application("myRoot") & "/relogin.aspx")
            End If
            Me.txtIdGrid.Text = Request("IdGrid")
            'Me.txtRutaDesc.Attributes.Add("Readonly", "Readonly")
            Me.lblError.Visible = False
            Me.lblErrorUP1.Visible = False
            Dim intConfig As Integer = 1
            Dim lbolFechSugerida As Boolean
            Dim lint_Tipo_Operacion As Integer
            Dim id_pedido() As String = Split(Request("Lista"), "[;]")
            Dim id_area() As String = Split(Request("Lista2"), "[;]")

            Try
                If id_pedido(0) <> "vacio" And id_pedido(0) <> "" Then
                    Dim idTipoOperacion As String = ValTipoOperacion(CInt(id_pedido(0)), CInt(id_area(0)))
                    Me.txtIDPedido.Text = idTipoOperacion
                End If
            Catch ex As Exception

            End Try
            'iBR.str_log = New net_objestandar.str_log(Session("conn").ToString)
            iBR.str_log = New net_objestandar.str_log(ConfigurationManager.AppSettings("strConn").ToString)
            iBR.AsignacionClass.str_log = iBR.str_log
            iBROperador.str_log = iBR.str_log
            Session("strConn") = iBR.str_log

            'GGUERRA FOLIO:21674
            If Not Request("PermisoViajeVacio") = "S" Then
                'Aplica Seguridad al proceso de Asignaci�n de Pedidos id_unico = 1354
                If lSegCtrl.ValidarPermisoAVentana(Session("UserName"), 1352, iBR.str_log) = False Then
                    Response.Redirect(Application("myRoot") & "/segctrl/faltanpermisos.htm")
                End If
            End If

            'Revisa si est� encendido el par�metro de cambios - rherrera ETF
            istrParamTresFronteras = luoParm.GetParametro("CAMBIOS_TRESFRONTERAS", iBR.str_log, False)
            If istrParamTresFronteras = Nothing Then istrParamTresFronteras = "N"
            If istrParamTresFronteras = "N" Then
                Me.chkPedidoUrgente.Visible = False
                Me.hdnParamTresFronteras.Value = "N"
            ElseIf istrParamTresFronteras = "S" Then
                Me.chkPedidoUrgente.Visible = True
                Me.txtHrsEstandar.ReadOnly = True
                Me.wdcFinViaje.ReadOnly = True
                Me.hdnParamTresFronteras.Value = "S"
            End If

            'JREGALADO Folio. Par�metro Trareysa
            Dim lstrParamTs As String = luoParm.GetParametro("validadocoperador", iBR.str_log, False)
            Dim tipoServ = If(lstrParamTs = "S", 1, 0)

            If tipoServ = 1 Then
                Me.divTiposervicio.Style.Add("display", "block")
                Me.hdnValidaDocumentos.Value = "S"
                With Me.ddlTipoServicio
                    .Visible = True
                    .DataSource = iuo_BRPedidos.GetTipoServicio(iBR.str_log)
                    .DataMember = "datos"
                    .DataValueField = "tipo_serv"
                    .DataTextField = "descripcion"
                    .DataBind()
                End With
            End If

            'NMANUEL 17/09/18 FOLIO:24496,
            If luoParm.GetParametro("HABCAMPOS_INTERFAZLANDSTAR", iBR.str_log, False).ToUpper = "S" Then
                Me.rowProspectoPedido.Style.Add("display", "inline-block")

                Dim id_tipoArmado_1 As String = ""
                With Me.ddlTipoArmado1
                    .DataSource = iBR.GetDTTipoArmado(iBR.str_log)
                    .DataMember = "datos"
                    .DataValueField = "cd"
                    .DataTextField = "descripcion"
                    .DataBind()
                End With
            End If

            'MSALVARADO AEO FOLIO: 23359 AEO
            istrHabilitarFuncionalidadesAEO = luoParm.GetParametro("habilitarFuncionalidadesAEO", iBR.str_log, False)

            'Quethzel, F.23195 Parametro Trareysa
            Dim lstrParax As String = luoParm.GetParametro("documentos_por_unidades", iBR.str_log, False)
            Me.txtParamDocumentosPorUnidad.Value = If(lstrParax = "S", 1, 0)

            'ereyes 07/01/2016 se agrega parametro para bloquear el remolque si es cargado desde el pedido no se podra modificar: Xpress Internacional
            lstr_bloquearemped = luoParm.GetParametro("bloqueoremolquepedidoasigna", iBR.str_log, False)

            'rherrera - Transpormex - Parametro Tipo Operaci�n por Usuario
            str_TipoOperacionPorUsuario = luoParm.GetParametro("TipoOperacionPorUsuario", iBR.str_log, False)

            'Folio:20957, Par�metro para validaci�n de la fecha de vigencia de la licencia del Operador
            'emartinez 08.May.2015, Cambios para ONTIME
            Me.ParmValidaFechVigLicOper = luoParm.GetParametro("despvalidavigencialicenciaoper", iBR.str_log, False)
            'Folio:20958, Par�metro para validaci�n de los Kil�metros Actuales de la Unidad + Kil�metros Ruta para comparar 
            'contra los Kil�metros del Siguiente Preventivo de la Unidad.
            Me.ParmValidaKmsUnidadSigPrev = UCase(luoParm.GetParametro("despvalidakmssigprevunidad", iBR.str_log, False))

            'ereyes se agrega parametro para mostrar la opcion de seleccion de vantanas de tiempo chrysler 05/07/2016
            strParsmVentanastiempos = UCase(luoParm.GetParametro("despventanaschrysler", iBR.str_log, False))
            'NMANUEL 19/07/18 FOLIO: 24655,BLOQUEO FECHA INICIO VIAJE CHRYSLER CON CPO
            strParamFechaCPO = UCase(luoParm.GetParametro("BLOQUEOFECHA_CHRYSLER_CPO", iBR.str_log, False))
            'PHERNANDEZ 23/08/2018 Parametro que habilita funcionalidad de tipo de ruta

            lstr_TipoRuta = luoParm.GetParametro("clasifica_tipo_ruta", iBR.str_log, False)
            If lstr_TipoRuta = "S" Then
                Me.hdnClasificaTipoRuta.Value = "S"
                Me.DivTipoRuta.Style.Add("display", "block")
                Me.DivTipoRutaModoCarga.Style.Add("display", "block")
            End If

            'megallegos -> Parametro para mostrar el tipo de operacion en un viaje vacio :: Folio:22277 // SIMSA
            strTipoOp_AsigVV = luoParm.GetParametro("TipoOp_AsigVV", iBR.str_log, False)
            If strTipoOp_AsigVV = "S" Then
                Me.hdnTipoOp_AsigVV.Value = "S"
                Me.tbTipoOp.Visible = True
            Else
                Me.tbTipoOp.Visible = False
            End If

            'GAFIGUEROA 24/01/2018, FOLIO: 23907, ALMEX: Se agrega el Parametro para bloquear los campos
            'de Remolque 1, Dolly y Remolque 2 cuando la asignasi�n fue creada desde el m�dulo de ConsolidadoNet.
            lStrBloqueaRemolque = luoParm.GetParametro("CAMBIOS_OPE_ALMEX", iBR.str_log, False)
            'FOLIO: 24266, GAFIGUEROA 18/04/2018
            Me.HdnCambiosOpeAlmex.Value = luoParm.GetParametro("CAMBIOS_OPE_ALMEX", iBR.str_log, False)

            'lehernandez fecha: 11/01/2018 folio: 24251
            lStrValidaOperPermEnUnidadPerm = UCase(luoParm.GetParametro("CAMBIOS_OPE_ALMEX", iBR.str_log, False))
            If lStrValidaOperPermEnUnidadPerm = "S" Then
                Me.hdnValidaOperadorPermEnUnidadPerm.Value = "S"
            Else
                Me.hdnValidaOperadorPermEnUnidadPerm.Value = "N"
            End If

            'AGONZALEZ 30/05/2018 FOLIO:24443
            strParmLigaMonitoreo = UCase(luoParm.GetParametro("ligamonitoreo", iBR.str_log, False))
            If strParmLigaMonitoreo = "S" Then
                Me.divVelMax.Visible = True
                Me.hdnLigaMonitoreo.Value = "S"
            Else
                Me.hdnLigaMonitoreo.Value = "N"
            End If

            'jmartin folio: 24453   31/05/2018
            istr_cambiaLongitudDescRuta = luoParm.GetParametro("cambia_longitud_desc_ruta", iBR.str_log, False).ToString.ToUpper
            If istr_cambiaLongitudDescRuta = "S" Then
                istr_cambiaAnchoDescRuta = luoParm.GetParametro("cambia_ancho_desc_ruta", iBR.str_log, False).ToString
                Me.txtRutaDesc.MaxLength = 120
                Me.txtRutaDesc.Width = If(istr_cambiaAnchoDescRuta <> "", CInt(istr_cambiaAnchoDescRuta), 200)
            End If
            'AMONTIEL FOLIO:25313  11/12/2018
            istr_ctrl_razon_social = luoParm.GetParametro("ctrl_razon_social", iBR.str_log, False).ToString.ToUpper
            If istr_ctrl_razon_social = "S" Then
                Me.DivConfigViaje.Attributes.Clear()
                Me.DivConfigViaje.Attributes.Add("display", "block")
                Me.hdn_razon_social.Value = "S"
            End If

            If Page.IsPostBack = False Then
                'MSALVARADO AEO
                'Obtiene la informaci�n del origen de la pantalla, si se abre desde la pantalla
                'del proceso de Agenda de Viajes se muestra la informaci�n de bot�n de Guardar2
                If Request("Opc") = "AGV" Then
                    Me.txthdnOpcion.Value = "AGV"
                    Me.btnGuarda2.Style.Add("display", "block")
                    Me.DivGuardar.Style.Add("display", "none")
                    'Me.btnGuarda2.Visible = True
                    'Me.DivGuardar.Visible = False
                    Me.txt_hdPasoAgenda.Value = 1
                Else
                    Me.btnGuarda2.Style.Add("display", "none")
                    Me.DivGuardar.Style.Add("display", "block")
                    'Me.DivGuardar.Visible = True
                    'Me.btnGuarda2.Visible = False
                End If

                'Carga el DDL del Tipo Operacion
                With Me.ddlOperacion
                    .DataSource = iuo_BRPedidos.GetDTTipoOperacion(iBR.str_log)
                    .DataMember = "datos"
                    .DataValueField = "id_tipo_operacion"
                    .DataTextField = "tipo_operacion"
                    .DataBind()
                End With

                With Me.ddlTipoOp
                    .DataSource = iuo_BRPedidos.GetDTTipoOperacion(iBR.str_log)
                    .DataMember = "datos"
                    .DataValueField = "id_tipo_operacion"
                    .DataTextField = "tipo_operacion"
                    .DataBind()
                End With

                Dim luoVG As net_b_zam.net_b_appvariables = Session("AppVariables")
                Me.ddlTipoOp.SelectedValue = luoVG.Id_Tipo_Operacion

                lstrValidaFianza = luoParm.GetParametro("validafianza", iBR.str_log, False)
                If UCase(lstrValidaFianza) = "S" Then
                    Me.DivFianza1.Style.Item(System.Web.UI.HtmlTextWriterStyle.Visibility) = "visible"
                    Me.DivFianza2.Style.Item(System.Web.UI.HtmlTextWriterStyle.Visibility) = "visible"
                End If

                'Valida si es DINET para cambio de Etiquetas
                luoParm = New net_b_zamclasesextended.net_b_clasegeneralparametrosextended
                Dim strDINET As String = ""
                strDINET = luoParm.GetParametro("dinet", iBR.str_log, False)
                If strDINET = "S" Then
                    Me.lblOperador1.Text = "Conductor 1"
                    Me.lblOperador2.Text = "Conductor 2"
                Else
                    Me.lblOperador1.Text = "Operador 1"
                    Me.lblOperador2.Text = "Operador 2"
                End If

                'ereyes 09/09/2014 tpx
                str_activatipooperacion = luoParm.GetParametro("tipooperasigtpx", iBR.str_log, False)
                Me.hdn_tipoopertpx.Value = If(str_activatipooperacion = "S", "S", "N")

                'Eramirez 13/09/2016
                Me.hdn_valida_tipoOperacion.Value = luoParm.GetParametro("valida_tipoOperacion", iBR.str_log, False)
                Me.hdn_valida_tipoOperacion.Value = If(Me.hdn_valida_tipoOperacion.Value = "S", "S", "N")

                strImgTractor = "../Imagenes/spacer.GIF"
                strImgRemolque1 = "../Imagenes/spacer.GIF"
                strImgDolly = "../Imagenes/spacer.GIF"
                strImgRemolque2 = "../Imagenes/spacer.GIF"

                'Put user code to initialize the page here
                iBR.AsignacionClass.Area = Session("vg_area")
                iBR.AsignacionClass.Ingreso = Session("UserName")
                iBR.AsignacionClass.Fecha_ingreso = Date.Now

                Me.wdcInicioViaje.DisplayModeFormat = Application("DateTimeFormat")
                Me.wdcInicioViaje.EditModeFormat = Application("DateTimeMask")
                Me.wdcFinViaje.DisplayModeFormat = Application("DateTimeFormat")
                Me.wdcFinViaje.EditModeFormat = Application("DateTimeMask")

                'Llama a la funci�n que obtiene el par�metro de captura de Kit
                iBR.ValidaCapturarKit()

                'Fusion Tlocal-OPV8 no permitimos modificar la ruta y cargamos la ruta del mvto oerc 24/08/17 folio 23121
                strFusionTlocalOPV8 = UCase(luoParm.GetParametro("Funcionalidad_Pedido_X_Mvtos", iBR.str_log, False))

                'Si no envia el id_asignacion, 
                If Request("ParmAsignHdn") Is Nothing Then

                    'Pasa la lista de pedidos a un arreglo
                    iBR.PasarAArreglo(Request("Lista"), "[;]", arrListaPedidos, iBR.str_log)
                    iBR.PasarAArreglo(Request("Lista2"), "[;]", arrListaPedidosAreas, iBR.str_log)
                    iBR.PasarAArreglo(Request("Lista3"), "[;]", arrListaPedidosPK, iBR.str_log)

                    If strParsmVentanastiempos = "S" Then

                        Dim dtDatos As Data.DataTable
                        Dim bolMuetraventan As Boolean = False
                        If arrListaPedidosPK(0) <> "vacio" Then
                            For aux = 0 To arrListaPedidosPK.Count - 1
                                dtDatos = iBR.GetVentanasChrysler(arrListaPedidosPK(aux), iBR.str_log)
                                If dtDatos.Rows.Count > 0 Then
                                    Me.MuetraDiVentana(1)
                                    Me.hdn_pedidopk.Value = arrListaPedidosPK(aux)
                                    bolMuetraventan = True
                                    Exit For
                                End If
                            Next
                        End If
                        If bolMuetraventan = False Then
                            Me.MuetraDiVentana(0)
                            Me.hdn_pedidopk.Value = 0
                        End If
                    End If

                    'ereyes  09/09/2014 transpormex
                    If str_activatipooperacion = "S" Then
                        ''Obtiene el tipo de operaciones del primer pedido encontrado
                        If arrListaPedidos(0) <> "vacio" Then
                            Me.ddlOperacion.SelectedValue = iuo_BRPedidos.GetTipoOperacionByPedido(arrListaPedidos(0), arrListaPedidosAreas(0), iBR.str_log)
                            Me.ddlOperacion.Enabled = False
                        Else
                            'rherrera - Transpormex - Valida si el usuario tiene relacionado un tipo_operacion 
                            If str_TipoOperacionPorUsuario <> "S" Then
                                'As� era como se encontraba el Else anteriormente
                                Me.ddlOperacion.SelectedValue = iBR.AsignacionClass.Id_Tipo_Operacion
                                Me.ddlOperacion.Enabled = True
                            Else
                                'Obtiene el id_tipo_operacion del usuario
                                lint_Tipo_Operacion = iBR.AsignacionClass.GetTipoOperacionPorUsuario(CStr(Session("UserName")), iBR.str_log)
                                'Si tiene lo asigna y no lo permite modificar / Si no tiene se permite asignar un valor
                                If lint_Tipo_Operacion > 0 Then
                                    Me.ddlOperacion.SelectedValue = lint_Tipo_Operacion
                                    Me.ddlOperacion.Enabled = False
                                Else
                                    Me.ddlOperacion.SelectedValue = lint_Tipo_Operacion
                                    Me.ddlOperacion.Enabled = True
                                End If
                            End If

                        End If
                    ElseIf luoParm.GetParametro("valida_tipoOperacion", iBR.str_log, False) = "S" Then
                        Me.ddlOperacion.Enabled = True
                    ElseIf strTipoOp_AsigVV = "S" Then
                        If arrListaPedidos(0) <> "vacio" Then
                            Me.ddlTipoOp.SelectedValue = iuo_BRPedidos.GetTipoOperacionByPedido(arrListaPedidos(0), arrListaPedidosAreas(0), iBR.str_log)
                            Me.ddlTipoOp.Enabled = False
                        End If
                    End If


                    'Se guarda la informacion del Listado de Areas y Listado de Pedidos.
                    Me.txtListaPedidos.Text = Request("Lista")
                    Me.txtListaPedidosAreas.Text = Request("Lista2")

                    'Obtiene la informaci�n de las Fechas del o los Pedidos para sugerirlos en las
                    'Fechas de Inicio y Fin de Viaje para la Asignaci�n

                    'Se envian como argumentos la lista de los Pedidos de los cuales va a obtener
                    'las fechas para la asignaci�n y la estructura del LOG como referencia.
                    ldtFechas = iBR.GetFechaIniFinViaje(Request("Lista2"), Request("Lista"), iBR.str_log)
                    If ldtFechas.Rows.Count > 0 Then
                        lbolFechSugerida = True
                        'Asigna la Fecha Inicio de Viaje de la Asignaci�n
                        If Not ldtFechas.Rows(0).Item("f_carfin_prog") Is DBNull.Value Then
                            Me.wdcInicioViaje.Value = ldtFechas.Rows(0).Item("f_carfin_prog")
                            Me.wdcInicioViaje.Text = ldtFechas.Rows(0).Item("f_carfin_prog")
                        End If
                        'Asigna la Fecha de Fin de Viaje de la Asignaci�n
                        If Not ldtFechas.Rows(0).Item("f_desfin_prog") Is DBNull.Value Then
                            Me.wdcFinViaje.Value = ldtFechas.Rows(0).Item("f_desfin_prog")
                            Me.wdcFinViaje.Text = ldtFechas.Rows(0).Item("f_desfin_prog")
                        End If
                    End If

                    'Obtiene las propiedades del nuevo viaje
                    iBR.AbrirNuevo(iBR.AsignacionClass.Area, arrListaPedidos, arrListaPedidosAreas, Request("Unidad"))

                    'FusionTlocal-OPV8 Traemos la ruta del mvto actual folio 23121 oerc 24/08/17
                    If strFusionTlocalOPV8 = "S" Then
                        If arrListaPedidosPK(0) <> "vacio" Then
                            ldtRutaXMvto = iBR.GetRutaMvtoActual(arrListaPedidosAreas(0), arrListaPedidos(0), iBR.str_log)
                            Me.txtRuta.Text = ldtRutaXMvto.Rows(0).Item("id_ruta")
                            Me.txtRutaDesc.Text = ldtRutaXMvto.Rows(0).Item("desc_ruta")
                            iBR.AsignacionClass.Ruta = ldtRutaXMvto.Rows(0).Item("id_ruta")

                            Me.txtRuta.ReadOnly = True
                            Me.txtRutaDesc.ReadOnly = True
                            Me.btnBuscarJefeFlota.Visible = False
                        Else
                            'Habilitamos la captura de la clasficiacion del mvto Fusion Tlocal-OPV8
                            'Folio 23121 para poder calcular el sueldo de los viajes vacios oerc 10/10/17
                            Me.dvClasfMvto.Style.Add("display", "block")

                            With Me.ddlClasf
                                .DataSource = iBR.GetClasfMvtoVacio(iBR.str_log)
                                .DataMember = "datos"
                                .DataValueField = "id_clasificacionmov"
                                .DataTextField = "descripcion"
                                .DataBind()
                            End With
                        End If
                    End If

                    If lstr_bloquearemped = "S" Then
                        'valida que no se pueda modifcar el remolque 1
                        If iBR.AsignacionClass.PrevioRemPedido1 = True Then
                            Me.txtRemolque1.Enabled = False
                            Me.txtLineaRem1.Enabled = False
                            Me.divImgLinea.Style.Add("display", "none")
                            Me.divImgRem1.Style.Add("display", "none")
                        End If
                        'valida que no se pueda modifcar el remolque 2
                        If iBR.AsignacionClass.PrevioRemPedido2 = True Then
                            Me.txtRemolque2.Enabled = False
                            Me.txtLineaRem2.Enabled = False
                            Me.divImgLinea2.Style.Add("display", "none")
                            Me.divImgRem2.Style.Add("display", "none")
                        End If
                    End If

                    'Obtiene configuracion default
                    If iBR.AsignacionClass.Remolque1.Length > 0 Then
                        intConfig = intConfig + 1
                    End If
                    If iBR.AsignacionClass.Remolque2.Length > 0 Then
                        intConfig = intConfig + 1
                    End If

                    'Busca el parametro de Multicompania MILAC, Folio : 15491
                    lstrMultiCia = luoParm.GetParametro("despmulticompania", iBR.str_log, False)
                    If UCase(lstrMultiCia) = "S" Then
                        Try
                            'Valida la informacion de los Pedidos y la Unidad, se envian como argumentos el Listado 
                            'de Pedidos, el Listado de Areas, la Unidad y la estructura del LOG como referencia.
                            iBR.ValidaCompaniaPedidosUnidad(arrListaPedidos, arrListaPedidosAreas, iBR.AsignacionClass.Tractor, iBR.str_log)
                        Catch ex As Exception
                            Me.DivGuardar.Style.Item(System.Web.UI.HtmlTextWriterStyle.Display) = "none"
                            Throw ex
                        End Try
                    End If

                    'Validamos funcionalidad de asignacion por area del tracto oerc 14/11/16  FOLIO:22592
                    If Request("AreaXTracto") = "S" Then
                        Me.hdnVacioXAreaTracto.Value = "S"
                    End If

                    'Funcionalidad para validar que solo se asignen pedidos con unidades de la misma area oerc 17/11/16 folio 22601
                    Dim lstrParamValAreas As String
                    lstrParamValAreas = UCase(luoParm.GetParametro("Val_area_pedido_vs_area_unidad", iBR.str_log, False))
                    If lstrParamValAreas = "S" And arrListaPedidos.Count > 0 AndAlso arrListaPedidos(0) <> "vacio" Then
                        Dim strPaso, strPaso2, strBandera, strareapaso, strpedidopaso As String
                        Dim areaTracto As Integer

                        areaTracto = iBR.AreaXTracto(iBR.AsignacionClass.Tractor, iBR.str_log)
                        For intCont As Integer = 0 To arrListaPedidos.Count - 1
                            strPaso = arrListaPedidos(intCont)
                            strPaso2 = arrListaPedidosAreas(intCont)

                            If strPaso2 <> CStr(areaTracto) Then
                                strBandera = "S"
                                strareapaso = strPaso2
                                strpedidopaso = strPaso
                                Exit For
                            End If
                        Next
                        If strBandera = "S" Then
                            Me.DivGuardar.Style.Item(System.Web.UI.HtmlTextWriterStyle.Display) = "none"
                            Throw New Exception("El area " & strareapaso & " del pedido " & strpedidopaso & " es diferente al area " & CStr(areaTracto) & " de la unidad " & iBR.AsignacionClass.Tractor & ", Favor de Verificar.")
                        End If
                    End If



                Else 'En caso de recibir el id_asignacion, abre la asignacion solicitada
                    Me.txt_hdAsignacion.Text = Request("ParmAsignHdn")
                    iBR.AsignacionClass.GetAsignacion(Request("ParmAsignHdn"), iBR.AsignacionClass, iBR.str_log)
                    intConfig = iBR.AsignacionClass.Id_configuracionviaje
                    Me.txtNumAsignacion.Text = iBR.AsignacionClass.Num_Asignacion
                    Me.hdnStatus.Value = iBR.AsignacionClass.Status_Asignacion
                    'Valida que si la Asignaci�n ya tiene un Viaje, NO se pueda
                    'editar la informaci�n de los datos de la asignaci�n.
                    If Request("ParmEdit") = "0" Then
                        If istrHabilitarFuncionalidadesAEO = "S" Then
                            ' MSALVARADO AEO
                            Me.btnGuarda2.Visible = False
                        Else
                            Me.DivGuardar.Style.Item(System.Web.UI.HtmlTextWriterStyle.Display) = "none"
                            'Me.btnGuardar.Visible = False
                        End If


                    Else
                        If istrHabilitarFuncionalidadesAEO = "S" Then
                            'MSALVARADO AEO
                            If Request("Opc") = "AGV" Then
                                Me.btnGuarda2.Visible = True
                            End If
                        Else
                            Me.DivGuardar.Style.Item(System.Web.UI.HtmlTextWriterStyle.Display) = "block"
                            'Me.btnGuardar.Visible = True
                        End If
                    End If
                    'PHERNANDEZ 23/08/2018 Validamos la funcionalidad tipo ruta, cambios TGARCIA
                    If lstr_TipoRuta = "S" Then
                        Me.ddTipoRuta.SelectedValue = iBR.AsignacionClass.clasificacion_ruta
                        Me.ddModoCarga.SelectedValue = iBR.AsignacionClass.clasificacion_carga_diesel
                    End If

                    'NMANUEL 20/07/18 FOLIO:24655 BLOQUEO DE FECHA CUANDO TIENE CPO EL VIAJE
                    If strParamFechaCPO = "S" Then
                        Dim CPOGuia As Data.DataTable
                        CPOGuia = iBR.GetCPOAsignacionGuia(Request("ParmAsignHdn"), iBR.str_log)
                        If CPOGuia.Rows.Count > 0 Then
                            If CPOGuia.Rows(0).Item("id_cpo") <> "" Then
                                Me.wdcInicioViaje.ReadOnly = True
                            End If
                        End If
                    End If

                    If luoParm.GetParametro("HABCAMPOS_INTERFAZLANDSTAR", iBR.str_log, False).ToUpper = "S" Then
                        Me.ddlTipoArmado1.SelectedValue = iBR.AsignacionClass.tipo_armado
                    End If

                    'ereyes 05/07/2016 : Ventanas Chrysler (indicador de tiempos)
                    If strParsmVentanastiempos = "S" Then

                        Dim dtDatos As Data.DataTable
                        Dim bolMuetraventan As Boolean = False
                        Dim dtPedidosAsig As New Data.DataTable
                        Dim row As Data.DataRow

                        If iBR.AsignacionClass.IdVentanaChrysler <> "" Then
                            Dim dtVentana As Data.DataTable
                            dtVentana = iBR.GetDescVentanaUnico(iBR.AsignacionClass.IdVentanaChrysler, iBR.str_log)

                            If dtVentana.Rows.Count > 0 Then
                                Me.txtIdventana.Text = dtVentana.Rows(0).Item("id_unico")
                                Me.txtDescventana.Text = dtVentana.Rows(0).Item("cadena")
                            End If
                        Else
                            Me.txtIdventana.Text = ""
                            Me.txtDescventana.Text = ""
                        End If

                        'obtiene lo pedido de la asignacion
                        dtPedidosAsig = iBR.GetPedidosDeUnaAsignacion(Request("ParmAsignHdn"), iBR.str_log)

                        For Each row In dtPedidosAsig.Rows
                            dtDatos = iBR.GetVentanasChrysler(row.Item("id_pedidopk"), iBR.str_log)
                            If dtDatos.Rows.Count > 0 Then
                                Me.MuetraDiVentana(1)
                                Me.hdn_pedidopk.Value = row.Item("id_pedidopk")
                                bolMuetraventan = True
                                Exit For
                            End If
                        Next

                        If bolMuetraventan = False Then
                            Me.MuetraDiVentana(0)
                            Me.hdn_pedidopk.Value = 0
                        End If
                    End If

                    'Cambios STI 23/12/2011
                    Dim luoParam As New net_b_zamclasesextended.net_b_clasegeneralparametrosextended
                    Dim lstrParam2 As String
                    lstrParam2 = UCase(luoParam.GetParametro("remolquesempalmados", iBR.str_log, False))
                    If iBR.AsignacionClass.Remolque1 <> Nothing And lstrParam2 = "S" Then
                        Me.Img50.Visible = True
                    End If

                    'Funcionalidad para validar que solo se asignen pedidos con unidades de la misma area oerc 17/11/16 folio 22601
                    Dim lstrParamValAreas As String
                    lstrParamValAreas = UCase(luoParm.GetParametro("Val_area_pedido_vs_area_unidad", iBR.str_log, False))

                    If lstrParamValAreas = "S" Then

                        Dim dtPedidosAsigVal As New Data.DataTable
                        dtPedidosAsigVal = iBR.GetPedidosDeUnaAsignacion(Request("ParmAsignHdn"), iBR.str_log)

                        If dtPedidosAsigVal.Rows.Count > 0 Then
                            Dim strPaso, strPaso2, strBandera, strareapaso, strpedidopaso As String
                            Dim areaTracto As Integer

                            areaTracto = iBR.AreaXTracto(iBR.AsignacionClass.Tractor, iBR.str_log)
                            For intCont As Integer = 0 To dtPedidosAsigVal.Rows.Count - 1
                                strPaso = dtPedidosAsigVal.Rows(intCont).Item("num_pedido")
                                strPaso2 = dtPedidosAsigVal.Rows(intCont).Item("id_area")

                                If strPaso2 <> CStr(areaTracto) Then
                                    strBandera = "S"
                                    strareapaso = strPaso2
                                    strpedidopaso = strPaso
                                    Exit For
                                End If
                            Next
                            If strBandera = "S" Then
                                Me.DivGuardar.Style.Item(System.Web.UI.HtmlTextWriterStyle.Display) = "none"
                                Throw New Exception("El area " & strareapaso & " del pedido " & strpedidopaso & " es diferente al area " & CStr(areaTracto) & " de la unidad " & iBR.AsignacionClass.Tractor & ", Favor de Verificar.")
                            End If
                        End If

                    End If

                    'megallegos -> Parametro para mostrar el tipo de operacion en un viaje vacio :: Folio:22277 // SIMSA
                    If strTipoOp_AsigVV = "S" Then
                        lint_Tipo_Operacion = iBR.AsignacionClass.GetTipoOperacionByAsignacion(iBR.str_log)
                        If lint_Tipo_Operacion > 0 Then
                            Me.ddlTipoOp.SelectedValue = lint_Tipo_Operacion
                            Dim dtPedidosAsigVal As New Data.DataTable
                            dtPedidosAsigVal = iBR.GetPedidosDeUnaAsignacion(Request("ParmAsignHdn"), iBR.str_log)
                            If dtPedidosAsigVal.Rows.Count > 0 Then
                                Me.ddlTipoOp.Enabled = False
                            End If
                        End If
                    End If


                    'FusionTlocal-OPV8 Traemos la ruta del mvto actual folio 23121 oerc 28/08/17
                    If strFusionTlocalOPV8 = "S" Then
                        Dim dtPedidosAsigVal As New Data.DataTable
                        dtPedidosAsigVal = iBR.GetPedidosDeUnaAsignacion(Request("ParmAsignHdn"), iBR.str_log)
                        If dtPedidosAsigVal.Rows.Count > 0 Then
                            Me.txtRuta.ReadOnly = True
                            Me.txtRutaDesc.ReadOnly = True
                            Me.btnBuscarJefeFlota.Visible = False
                        Else
                            'jmartin folio: 23121   18/10/2017
                            'oerc ajustamos cambios para solo mostrar cuando es vacio folio: 23121  oerc 30/10/17
                            Me.dvClasfMvto.Style.Add("display", "block")
                            With Me.ddlClasf
                                .DataSource = iBR.GetClasfMvtoVacio(iBR.str_log)
                                .DataMember = "datos"
                                .DataValueField = "id_clasificacionmov"
                                .DataTextField = "descripcion"
                                .DataBind()
                                .SelectedValue = iBR.AsignacionClass.ClasificacionMovimiento
                            End With
                        End If
                    End If

                    'AGONZALEZ FOLIO:24443 30/05/2018
                    If strParmLigaMonitoreo = "S" Then
                        Me.txtVelMax.Text = iBR.AsignacionClass.VelMax
                    End If

                End If
                'Se llena la pantalla con la informaci�n de la Asignaci�n 
                Me.txtRuta.Text = iBR.AsignacionClass.Ruta
                If iBR.AsignacionClass.Ruta <> 0 Then
                    'Se llama a la funci�n que valida la Ruta, se le envia como argumento
                    'el campo de la Descripci�n de la Ruta como referencia 
                    iBR.ValidaRuta(iBR.AsignacionClass.Ruta, iBR.str_log, Me.txtRutaDesc.Text)

                    'rherrera ETF 22/11/14
                    If istrParamTresFronteras = "S" Then
                        'Obtiene si es urgente
                        'lintUrgente = iBR.GetUrgente(CInt(Me.txtRuta.Text), 1, iBR.AsignacionClass.Area, 1, iBR.str_log, False)

                        'Obtiene el tiempo_urgente
                        Me.hdnTiempoUrgente.Value = iBR.GetTiempoUrgente(CInt(Me.txtRuta.Text), 1, 0, 0, iBR.str_log, True)

                        'Obtiene el tiempo_ruta
                        Me.hdnTiempoRuta.Value = iBR.GetTiempoUrgente(CInt(Me.txtRuta.Text), 0, 0, 0, iBR.str_log, True)

                        'Obtiene si es urgente dependiendo el tiempo
                        If iBR.AsignacionClass.tiempo_ruta = Me.hdnTiempoRuta.Value Then
                            Me.chkPedidoUrgente.Checked = False
                        ElseIf iBR.AsignacionClass.tiempo_ruta = Me.hdnTiempoUrgente.Value Then
                            Me.chkPedidoUrgente.Checked = True
                        End If

                    End If

                End If
                If Request("ParmAsignHdn") Is Nothing And lbolFechSugerida = True Then
                    If Not ldtFechas.Rows(0).Item("f_carfin_prog") Is DBNull.Value Then
                        iBR.AsignacionClass.F_prog_ini_viaje = ldtFechas.Rows(0).Item("f_carfin_prog")
                        Me.wdcInicioViaje.Value = ldtFechas.Rows(0).Item("f_carfin_prog")
                    Else
                        iBR.AsignacionClass.F_prog_ini_viaje = Today
                        Me.wdcInicioViaje.Value = Today
                    End If
                    If Not ldtFechas.Rows(0).Item("f_desfin_prog") Is DBNull.Value Then
                        iBR.AsignacionClass.F_prog_fin_viaje = ldtFechas.Rows(0).Item("f_desfin_prog")
                        Me.wdcFinViaje.Value = ldtFechas.Rows(0).Item("f_desfin_prog")
                    Else
                        iBR.AsignacionClass.F_prog_fin_viaje = Today
                        Me.wdcFinViaje.Value = Today
                    End If

                Else
                    Me.wdcInicioViaje.Value = iBR.AsignacionClass.F_prog_ini_viaje
                    'Me.wdcInicioViaje.Text = iBR.AsignacionClass.F_prog_ini_viaje
                    Me.wdcFinViaje.Value = iBR.AsignacionClass.F_prog_fin_viaje
                    'Me.wdcFinViaje.Text = iBR.AsignacionClass.F_prog_fin_viaje
                End If

                'Se agrega la validaci�n del par�metro de SU TRANSPORTE para la validaci�n del Armado
                'emartinez 02-ENERO-2009
                luoParm = New net_b_zamclasesextended.net_b_clasegeneralparametrosextended
                Dim lstrParam As String = ""
                'Se llama a la funci�n GetParametro, se envian como argumentos el nombre del 
                'par�metro, la estructura del LOG como referencia y una variable que indica 
                'si se valida o no que existe el par�metro en la base de datos. emartinez 02-ENE-2009
                lstrParam = luoParm.GetParametro("CAMBIOS_SUTRANSPORTE", iBR.str_log, False)
                If lstrParam = "S" Then Me.txtHdnValidaArmado.Value = 0
                If lstrParam = "N" Then Me.txtHdnValidaArmado.Value = 1

                'En caso de que el valor sea TRUE muestra la informaci�n 
                'para capturar el kit
                If iBR.CapturarKit = True Then
                    Me.DivKit.Style.Item(System.Web.UI.HtmlTextWriterStyle.Display) = "block"
                    'Me.tblkit.Visible = True
                Else
                    Me.DivKit.Style.Item(System.Web.UI.HtmlTextWriterStyle.Display) = "none"
                    'Me.tblkit.Visible = False
                End If
                Me.txtKit.Text = iBR.AsignacionClass.Kit
                Me.bolValidaKit = iBR.CapturarKit
                Me.txtIngreso.Text = iBR.AsignacionClass.Ingreso
                Me.txtHrsEstandar.Text = iBR.AsignacionClass.HrsEstandar
                Me.txtUnidad.Text = iBR.AsignacionClass.Tractor
                Me.txtUnidadOriginal.Text = iBR.AsignacionClass.Tractor
                Me.txtLineaRem1.Text = iBR.AsignacionClass.LineaRem1
                Me.txtLineaRem1Original.Text = iBR.AsignacionClass.LineaRem1
                Me.txtRemolque1.Text = iBR.AsignacionClass.Remolque1
                Me.txtRemolque1Original.Text = iBR.AsignacionClass.Remolque1
                Me.txtDolly.Text = iBR.AsignacionClass.Dolly
                Me.txtLineaRem2.Text = iBR.AsignacionClass.LineaRem2
                Me.txtLineaRem2Original.Text = iBR.AsignacionClass.LineaRem2
                Me.txtRemolque2.Text = iBR.AsignacionClass.Remolque2
                Me.txtRemolque2Original.Text = iBR.AsignacionClass.Remolque2
                Me.txtObservaciones.Text = iBR.AsignacionClass.Observaciones
                Me.txtOperador1.Text = iBR.AsignacionClass.Operador1
                Me.txtOperador1Original.Text = iBR.AsignacionClass.Operador1

                'Establece las imagenes
                If Trim(Me.txtUnidad.Text) <> "" Then
                    Me.strImgTractor = "../Imagenes/tractor.JPG"
                Else
                    Me.strImgTractor = "../Imagenes/spacer.GIF"
                End If
                If Trim(Me.txtRemolque1.Text) <> "" Then
                    strImgRemolque1 = "../Imagenes/remolque.JPG"
                Else
                    strImgRemolque1 = "../Imagenes/spacer.GIF"
                End If
                If Trim(Me.txtDolly.Text) <> "" Then
                    strImgDolly = "../Imagenes/dolly.JPG"
                Else
                    strImgDolly = "../Imagenes/spacer.GIF"
                End If
                If Trim(Me.txtRemolque2.Text) <> "" Then
                    strImgRemolque2 = "../Imagenes/remolque.JPG"
                Else
                    strImgRemolque2 = "../Imagenes/spacer.GIF"
                End If

                'GAFIGUEROA 24/01/2018, FOLIO: 23907, ALMEX
                If UCase(lStrBloqueaRemolque) = "S" Then
                    If UCase(iBR.AsignacionClass.sist_origen) = "CONS" Then
                        'valida que no se pueda modifcar el remolque 1
                        Me.txtRemolque1.Enabled = False
                        Me.txtLineaRem1.Enabled = False
                        Me.divImgLinea.Style.Add("display", "none")
                        Me.divImgRem1.Style.Add("display", "none")
                        'valida que no se pueda modifcar el Dolly
                        'Me.txtDolly.Enabled = False
                        'Me.imgBDolly.Style.Add("display", "none")
                        'valida que no se pueda modifcar el remolque 2
                        Me.txtRemolque2.Enabled = False
                        Me.txtLineaRem2.Enabled = False
                        Me.divImgLinea2.Style.Add("display", "none")
                        Me.divImgRem2.Style.Add("display", "none")
                        Me.wdcInicioViaje.Enabled = False
                        Me.wdcFinViaje.Enabled = False
                        'valida que no se pueda modifcar la unidad Siempre y cuando este 
                        'sea del tipo de unidad Thorton, Rabon o Camioneta
                        Dim ldtConsEmbarque = iuoBrClaseConsolidado.GetConsEmbarque(Request("ParmAsignHdn"), iBR.str_log)
                        If ldtConsEmbarque.Rows(0).Item("cantSolicitud") >= 1 And ldtConsEmbarque.Rows(0).Item("TipoUnidad") = 1 Then
                            Me.txtUnidad.Enabled = False
                            Me.Img3.Visible = False
                        End If
                    End If
                End If

                'Folio:20957, Valida Fecha Vigencia de Licencia del Operador
                'Cambios ONTIME, emartinez 08.May.2015
                If ParmValidaFechVigLicOper = "S" And iBR.AsignacionClass.Operador1 <> 0 Then
                    'Se llama a la funci�n que valida la vigencia de la licencia del Operador, se envian como 
                    'argumentos el Identificador del Operador, la Fecha de Inicio de Viaje Programado y 
                    'la estructura del LOG como referencia.
                    iBR.ValidaVigenciaLicenciaOperador(iBR.AsignacionClass.Operador1, iBR.AsignacionClass.F_prog_ini_viaje, iBR.str_log)
                End If

                'Me.ddlOperacion.SelectedValue = iBR.AsignacionClass.Id_Tipo_Operacion

                'Muestra los datos del Operador 2
                Me.txtOperador2.Text = iBR.AsignacionClass.Operador2
                Me.txtOperador2Original.Text = iBR.AsignacionClass.Operador2
                iBR.ValidaOperador(2, Me.txtOperador2Nombre.Text)
                'Folio:20957, Valida Fecha Vigencia de Licencia del Operador2
                'Cambios ONTIME, emartinez 08.May.2015
                If ParmValidaFechVigLicOper = "S" And iBR.AsignacionClass.Operador2 <> 0 Then
                    'Se llama a la funci�n que valida la vigencia de la licencia del Operador, se envian como 
                    'argumentos el Identificador del Operador, la Fecha de Inicio de Viaje Programado y 
                    'la estructura del LOG como referencia.
                    iBR.ValidaVigenciaLicenciaOperador(iBR.AsignacionClass.Operador2, iBR.AsignacionClass.F_prog_ini_viaje, iBR.str_log)
                End If

                'Folio:20958, Par�metro para validaci�n de los Kil�metros Actuales de la Unidad + Kil�metros Ruta para comparar 
                'contra los Kil�metros del Siguiente Preventivo de la Unidad.
                Me.ParmValidaKmsUnidadSigPrev = UCase(luoParm.GetParametro("despvalidakmssigprevunidad", iBR.str_log, False))
                If Me.ParmValidaKmsUnidadSigPrev = "S" Then
                    Me.MensajeAvisoKmsUnidad = ""
                    'Folio:21014, Se llama a la funci�n ValidaKmsSiguientePreventivoUnidad, se envian como argumentos la Unidad, la Ruta
                    'y la estructura del LOG como referencia.Se corrige detalle para que solamente muestre el aviso de la validaci�n y
                    'que no detenga el proceso de Asignaci�n.
                    If iBR.AsignacionClass.Tractor <> "0" And iBR.AsignacionClass.Ruta <> 0 Then
                        Me.MensajeAvisoKmsUnidad = iBR.ValidaKmsSiguientePreventivoUnidad(iBR.AsignacionClass.Tractor, iBR.AsignacionClass.Ruta, iBR.str_log)
                    End If
                    If Me.MensajeAvisoKmsUnidad.Length > 0 Then
                        'Me.lblError.Visible = True
                        Me.lblErrorAjax.Text = Me.MensajeAvisoKmsUnidad
                    End If
                End If

                'Me.txtOperador2Nombre.Text = "SIN OPERADOR"
                Me.txtSeguimiento.Text = iBR.AsignacionClass.SeguimientoNombreCorto
                Me.txtSeguimientoDesc.Text = iBR.AsignacionClass.SeguimientoDesc

                If lstr_bloquearemped = "S" Then
                    'valida que no se pueda modifcar el remolque 1
                    If iBR.AsignacionClass.SeguimientoDesc <> "VACIO" Then
                        If Me.txtRemolque1.Text <> "" Then
                            Me.txtRemolque1.Enabled = False
                            Me.txtLineaRem1.Enabled = False
                            Me.divImgLinea.Style.Add("display", "none")
                            Me.divImgRem1.Style.Add("display", "none")
                        End If
                        'valida que no se pueda modifcar el remolque 2
                        If Me.txtRemolque2.Text <> "" Then
                            Me.txtRemolque2.Enabled = False
                            Me.txtLineaRem2.Enabled = False
                            Me.divImgLinea2.Style.Add("display", "none")
                            Me.divImgRem2.Style.Add("display", "none")
                        End If
                    End If
                End If

                'rherrera - Transpormex - 26/03/15 Debido a ser el �ltimo pedido la validaci�n del Tipo de Operaci�n por Usuario es la que manda
                If str_TipoOperacionPorUsuario <> "S" Then
                    'ereyes  09/09/2014 transpormex
                    If str_activatipooperacion <> "S" Then
                        Me.ddlOperacion.SelectedValue = iBR.AsignacionClass.GetTipoOperacionBySeg(iBR.str_log)
                    End If
                End If

                Me.ddlConfigViaje.SelectedValue = intConfig
                Session("ActualizaAsignacion") = True


                'Obtener el parametro nomodificaremolque que deshabilita los campos de remolque1 y 2 as� como sus busquedas.
                'Cambios STI 15/11/11
                Dim strParam As String
                luoParm = New net_b_zamclasesextended.net_b_clasegeneralparametrosextended
                strParam = luoParm.GetParametro("nomodificaremolque", iBR.str_log, False)
                'Si se encuentra el parametro de STI
                If UCase(strParam) = "S" Then
                    If arrListaPedidos.Count = 0 Then
                        Me.txtRemolque1.Enabled = False
                        Me.txtRemolque2.Enabled = False
                        Me.ImgSearchRem1.Disabled = True
                        Me.ImgSearchRem2.Disabled = True
                    ElseIf arrListaPedidos(0) <> "vacio" Then
                        Me.txtRemolque1.Enabled = False
                        Me.txtRemolque2.Enabled = False
                        Me.ImgSearchRem1.Disabled = True
                        Me.ImgSearchRem2.Disabled = True
                    End If
                End If

                'rherrera ETF
                If istrParamTresFronteras = "S" Then
                    If arrListaPedidos.Count > 0 Then
                        If UCase(arrListaPedidos(0)) = "VACIO" Then
                            Me.chkPedidoUrgente.Enabled = False
                            Me.chkPedidoUrgente.Enabled = False
                        End If
                    End If
                End If

                'Se valida la informaci�n del Operador, se envian como argumentos
                'un valor para identificar si se valida el Operador 1 o el 
                'Operador 2 y de referencia en campo del Nombre del Operador 
                iBR.ValidaOperador(1, Me.txtOperador1Nombre.Text)

                'Validamos grupos de vision MultiEmpresa Folio 25055,25019 oerc 11/10/18
                Me.aplica_gposvision = luoParm.GetParametro("aplica_gposvision", iBR.str_log, False)

            End If

            ''Establece las imagenes
            'If Trim(Me.txtUnidad.Text) <> "" Then
            '    Me.strImgTractor = "../Imagenes/tractor.JPG"
            'Else
            '    Me.strImgTractor = "../Imagenes/spacer.GIF"
            'End If
            'If Trim(Me.txtRemolque1.Text) <> "" Then
            '    strImgRemolque1 = "../Imagenes/remolque.JPG"
            'Else
            '    strImgRemolque1 = "../Imagenes/spacer.GIF"
            'End If
            'If Trim(Me.txtDolly.Text) <> "" Then
            '    strImgDolly = "../Imagenes/dolly.JPG"
            'Else
            '    strImgDolly = "../Imagenes/spacer.GIF"
            'End If
            'If Trim(Me.txtRemolque2.Text) <> "" Then
            '    strImgRemolque2 = "../Imagenes/remolque.JPG"
            'Else
            '    strImgRemolque2 = "../Imagenes/spacer.GIF"
            'End If

            'megallegos 30/01/17 -> PARAM: Valida en la Asignaci�n que el Remolque no tenga abiertas Ordenes de Servicio || Folio:22820 // TRAREYSA
            strValidaOrdenServREM = luoParm.GetParametro("ValidaOrdenServREM", iBR.str_log, False)

            'Se Valida que la Unidad no tenga una Orden de Servicio Abierta, se envian como 
            'argumentos la Unidad y la estructura del LOG como referencia,emartinez 11-AGO-2009
            luoParm = New net_b_zamclasesextended.net_b_clasegeneralparametrosextended
            Dim lstrMsgUnidad As String = ""
            Dim lintValorParam As Integer = 0
            Dim luoUnidad As New net_b_zamclasesextended.net_b_claseunidadextended
            luoUnidad.ValidaOrdenServicioUnidad(Me.txtUnidad.Text, iBR.str_log, lstrMsgUnidad, False)
            'Se llama a la funci�n GetParametro, se envian como argumentos el nombre del 
            'par�metro, la estructura del LOG como referencia y una variable que indica 
            'si se valida o no que existe el par�metro en la base de datos. emartinez 02-ENE-2009
            lintValorParam = luoParm.GetParametroNumerico("despvalidaordenserviciounidad", iBR.str_log, False)
            Select Case lintValorParam
                Case 0  'No valida
                Case 1  'Valida y muestra mensaje, permite continuar
                    If lstrMsgUnidad.Length > 0 Then
                        Me.lblError.Text = "ADVERTENCIA : " & lstrMsgUnidad
                        Me.lblError.Visible = True
                    End If
                Case 2  'Valida y NO permite continuar
                    If lstrMsgUnidad.Length > 0 Then
                        Throw New Exception(lstrMsgUnidad)
                    End If

                    'megallegos 30/01/17 -> Valida en la Asignaci�n que el Remolque no tenga abiertas Ordenes de Servicio || Folio:22820 // TRAREYSA
                    If strValidaOrdenServREM = "S" Then
                        'Valida sobre el Remolque1 y LineaRemolque1, tanto propias y americanas.
                        If Me.txtRemolque1.Text <> "" And Me.txtLineaRem1.Text = "" Then
                            luoUnidad.ValidaOrdenServicioUnidad(Me.txtRemolque1.Text, iBR.str_log, lstrMsgREM, False)
                            If lstrMsgREM.Length > 0 Then
                                lstrMsgREM = lstrMsgREM.Replace("La Unidad", "El Remolque")
                                Throw New Exception(lstrMsgREM)
                            End If
                        ElseIf Me.txtRemolque1.Text <> "" And Me.txtLineaRem1.Text <> "" Then
                            luoUnidad.ValidaOrdenServicioUnidadLinea(Me.txtRemolque1.Text, Me.txtLineaRem1.Text, iBR.str_log, lstrMsgREM, False)
                            If lstrMsgREM.Length > 0 Then
                                Throw New Exception(lstrMsgREM)
                            End If
                        End If
                        'Valida sobre el Remolque2 y LineaRemolque2, tanto propias y americanas.
                        If Me.txtRemolque2.Text <> "" And Me.txtLineaRem2.Text = "" Then
                            luoUnidad.ValidaOrdenServicioUnidad(Me.txtRemolque2.Text, iBR.str_log, lstrMsgREM, False)
                            If lstrMsgREM.Length > 0 Then
                                lstrMsgREM = lstrMsgREM.Replace("La Unidad", "El Remolque")
                                Throw New Exception(lstrMsgREM)
                            End If
                        ElseIf Me.txtRemolque2.Text <> "" And Me.txtLineaRem2.Text <> "" Then
                            luoUnidad.ValidaOrdenServicioUnidadLinea(Me.txtRemolque2.Text, Me.txtLineaRem2.Text, iBR.str_log, lstrMsgREM, False)
                            If lstrMsgREM.Length > 0 Then
                                Throw New Exception(lstrMsgREM)
                            End If
                        End If
                    End If
            End Select

            'Se Valida que la Unidad no tenga una Orden de Servicio Abierta, se envian como argumentos la Unidad
            '   y la estructura del LOG como referencia, si el parametro esta activo, muestra el mensaje, permite continuar.
            '   Jdlcruz, GASA 28-Nov-2012, Folio: 17747.
            Dim lstrValidaOrden As String = ""
            lstrValidaOrden = luoParm.GetParametro("ValidaOrden_Mantenimiento", iBR.str_log, False)
            If lstrValidaOrden = "S" Then
                Dim lstrMsgUnidad2 As String = ""
                'Valida sobre el Remolque1 y LineaRemolque1, tanto propias americanas y americanas.
                If Me.txtRemolque1.Text <> "" And Me.txtLineaRem1.Text = "" Then
                    luoUnidad.ValidaOrdenServicioUnidad(Me.txtRemolque1.Text, iBR.str_log, lstrMsgUnidad2, False)

                    If Me.lblError.Text <> "" And lstrMsgUnidad2.Length > 0 Then
                        lstrMsgUnidad2 = lstrMsgUnidad2.Replace("La Unidad", "El Remolque")
                        Me.lblError.Text = Me.lblError.Text & " y " & lstrMsgUnidad2
                    ElseIf lstrMsgUnidad2.Length > 0 Then
                        Me.lblError.Text = "ADVERTENCIA : " & lstrMsgUnidad2
                        Me.lblError.Visible = True
                    End If
                ElseIf Me.txtRemolque1.Text <> "" And Me.txtLineaRem1.Text <> "" Then
                    luoUnidad.ValidaOrdenServicioUnidadLinea(Me.txtRemolque1.Text, Me.txtLineaRem1.Text, iBR.str_log, lstrMsgUnidad2, False)

                    If Me.lblError.Text <> "" And lstrMsgUnidad2.Length > 0 Then
                        Me.lblError.Text = Me.lblError.Text & " y " & lstrMsgUnidad2
                    ElseIf lstrMsgUnidad2.Length > 0 Then
                        Me.lblError.Text = "ADVERTENCIA : " & lstrMsgUnidad2
                        Me.lblError.Visible = True
                    End If
                End If
                'Valida sobre el Remolque2 y LineaRemolque2, tanto propias americanas y americanas.
                If Me.txtRemolque2.Text <> "" And Me.txtLineaRem2.Text = "" Then
                    luoUnidad.ValidaOrdenServicioUnidad(Me.txtRemolque2.Text, iBR.str_log, lstrMsgUnidad2, False)

                    If Me.lblError.Text <> "" And lstrMsgUnidad2.Length > 0 Then
                        lstrMsgUnidad2 = lstrMsgUnidad2.Replace("La Unidad", "El Remolque")
                        Me.lblError.Text = Me.lblError.Text & " y " & lstrMsgUnidad2
                    ElseIf lstrMsgUnidad2.Length > 0 Then
                        Me.lblError.Text = "ADVERTENCIA : " & lstrMsgUnidad2
                        Me.lblError.Visible = True
                    End If
                ElseIf Me.txtRemolque2.Text <> "" And Me.txtLineaRem2.Text <> "" Then
                    luoUnidad.ValidaOrdenServicioUnidadLinea(Me.txtRemolque2.Text, Me.txtLineaRem2.Text, iBR.str_log, lstrMsgUnidad2, False)

                    If Me.lblError.Text <> "" And lstrMsgUnidad2.Length > 0 Then
                        Me.lblError.Text = Me.lblError.Text & " y " & lstrMsgUnidad2
                    ElseIf lstrMsgUnidad2.Length > 0 Then
                        Me.lblError.Text = "ADVERTENCIA : " & lstrMsgUnidad2
                        Me.lblError.Visible = True
                    End If
                End If

            End If

            'HRFF 13/11/2012 Se valida si es Viaje Vacio y que admita los controles de Remitente y Destinatario en esta pantalla
            strRemDest = luoParm.GetParametro("desp_ViajeV_RemDest", iBR.str_log, False)
            Dim strtipoopera = luoParm.GetParametro("areas_transpormex", iBR.str_log, False)
            Dim bol_RemYDest As Boolean = False
            Dim bol_tipoopera As Boolean = False

            If strtipoopera = "S" Then
                If arrListaPedidos.Count > 0 Then
                    If UCase(arrListaPedidos(0)) = "VACIO" Then
                        tblRemDestOper.Style.Add("display", "block")
                        bol_tipoopera = True
                    End If
                Else
                    tblRemDestOper.Style.Add("display", "block")
                    bol_tipoopera = True
                End If
            End If
            If strRemDest = "S" Then

                Dim EnabledRemolque As Boolean = True
                Dim EnabledRemolque2 As Boolean = True

                If Request("ParmAsignHdn") Is Nothing Then
                    If Request("Lista") = "vacio[;]" Then
                        tblRemDestOper.Style.Add("display", "block")
                        tblRemDest.Style.Add("display", "block")
                        bol_RemYDest = True
                        lblRemDestHabil.Text = "1"
                    Else
                        If iBR.AsignacionClass.Remolque1.Length > 0 Then
                            EnabledRemolque = False
                        End If
                        If iBR.AsignacionClass.Remolque2.Length > 0 Then
                            EnabledRemolque2 = False
                        End If
                    End If
                Else
                    Dim dtPedidosAsig As New Data.DataTable
                    dtPedidosAsig = iBR.GetPedidosDeUnaAsignacion(Request("ParmAsignHdn"), iBR.str_log)

                    If dtPedidosAsig.Rows.Count = 0 Then
                        tblRemDest.Style.Add("display", "block")
                        bol_RemYDest = True
                        lblRemDestHabil.Text = "1"
                    ElseIf dtPedidosAsig.Rows.Count = 1 Then
                        If dtPedidosAsig.Rows(0)("status_pedido").ToString() = "4" And dtPedidosAsig.Rows(0)("observaciones").ToString().Contains("Repo/Colocaci�n") = True Then
                            tblRemDest.Style.Add("display", "block")
                            bol_RemYDest = True
                            lblRemDestHabil.Text = "1"
                        Else
                            If iBR.AsignacionClass.Remolque1.Length > 0 Then
                                EnabledRemolque = False
                            End If
                            If iBR.AsignacionClass.Remolque2.Length > 0 Then
                                EnabledRemolque2 = False
                            End If
                        End If
                    Else
                        If iBR.AsignacionClass.Remolque1.Length > 0 Then
                            EnabledRemolque = False
                        End If
                        If iBR.AsignacionClass.Remolque2.Length > 0 Then
                            EnabledRemolque2 = False
                        End If
                    End If
                End If
                FianzaRetornada.Value = "S"

                If IsPostBack = False Then
                    txtLineaRem1.Enabled = EnabledRemolque
                    txtRemolque1.Enabled = EnabledRemolque
                    ImgSearchRem1.Visible = EnabledRemolque
                    Img5.Visible = EnabledRemolque

                    txtLineaRem2.Enabled = EnabledRemolque2
                    txtRemolque2.Enabled = EnabledRemolque2
                    ImgSearchRem2.Visible = EnabledRemolque2
                    Img7.Visible = EnabledRemolque2
                End If
            End If

            If bol_RemYDest = True Then
                'Agregar valores a los campos de Remitente y Destinatario
                If iBR.AsignacionClass.Id_remitente <> "0" Then
                    Me.txtRemitente.Text = iBR.AsignacionClass.Id_remitente
                    Me.txtRemitenteNom.Text = iBR.GetNombreById(iBR.AsignacionClass.Id_remitente, iBR.str_log)
                End If
                If iBR.AsignacionClass.Id_destinatario <> "0" Then
                    Me.txtDestinatario.Text = iBR.AsignacionClass.Id_destinatario
                    Me.txtDestinatarioNom.Text = iBR.GetNombreById(iBR.AsignacionClass.Id_destinatario, iBR.str_log)
                End If
            End If

            'rherrera - Transpormex - 26/03/15
            If str_TipoOperacionPorUsuario <> "S" Then
                If bol_RemYDest = True Or bol_tipoopera Then
                    Me.ddlOperacion.SelectedValue = iBR.AsignacionClass.GetTipoOperacionByAsignacion(iBR.str_log)
                    'If str_activatipooperacion = "S" Then
                    '    Me.ddlOperacion.Enabled = True
                    'End If
                End If
            Else
                If Request("ParmAsignHdn") Is Nothing Then
                    Me.ddlOperacion.SelectedValue = iBR.AsignacionClass.GetTipoOperacionPorUsuario(CStr(Session("UserName")), iBR.str_log)
                    'Si tiene lo asigna y no lo permite modificar / Si no tiene se permite asignar un valor
                    If lint_Tipo_Operacion > 0 Then
                        Me.ddlOperacion.SelectedValue = lint_Tipo_Operacion
                        Me.ddlOperacion.Enabled = False
                    Else
                        Me.ddlOperacion.SelectedValue = lint_Tipo_Operacion
                        Me.ddlOperacion.Enabled = True
                    End If
                Else
                    Me.ddlOperacion.SelectedValue = iBR.AsignacionClass.GetTipoOperacionByAsignacion(iBR.str_log)
                End If
            End If

            Dim luoTracto As New net_b_zamclasesextended.net_b_claseunidadextended
            luoTracto.GetUnidad("T", Me.txtUnidad.Text, iBR.str_log)
            Me.idFlota.Text = luoTracto.IdFlota

            'Se obtiene la informaci�n del par�metro "Validacion de Carga Asignada vs. Capacidad del Vehiculo"
            'Folio : 16715, emartinez 07/Jun/2012, Proyecto : TMS DINET Fase 1
            luoParm = New net_b_zamclasesextended.net_b_clasegeneralparametrosextended
            Me.lstrValidaCapCarga = luoParm.GetParametro("despvalidacargavscapacidad", iBR.str_log, False)
            If Me.lstrValidaCapCarga = "S" Then
                Me.hfDINET.Text = "1"
                Me.txtHdnParCapCarga.Value = "S"
                'Dim luoTracto As New net_b_zamclasesextended.net_b_claseunidadextended
                Dim luoRem1 As New net_b_zamclasesextended.net_b_claseunidadextended
                Dim luoRem2 As New net_b_zamclasesextended.net_b_claseunidadextended
                Dim luoDolly As New net_b_zamclasesextended.net_b_claseunidadextended
                Me.trInfoCapacidadCarga.Visible = True
                Me.trConfigMTC.Visible = True
                'Se envian los datos del Listado de Areas y Pedidos, la Unidad, Remolque 1, Dolly 
                'y Remolque 2 para obtener la configuracion.
                Dim decTotalM3 As Decimal = 0
                If Trim(Me.txtUnidad.Text) <> "" Then
                    'luoTracto.GetUnidad("T", Me.txtUnidad.Text, iBR.str_log)
                    Me.txtTipoUTracto.Text = luoTracto.TipoUnidadDesc
                    Me.txtIdTipoUTracto.Text = luoTracto.TipoUnidad

                    'Obtiene la capacidad de Volumen del tipo de unidad
                    iBR.GetCapacidadUnidadVolumen(luoTracto.TipoUnidad, iBR.str_log, decTotalM3)
                End If
                If Trim(Me.txtRemolque1.Text) <> "" Then
                    luoRem1.GetUnidad("R", Me.txtRemolque1.Text, iBR.str_log)
                    Me.txtTipoURem1.Text = luoRem1.TipoUnidadDesc
                    Me.txtIdTipoUR1.Text = luoRem1.TipoUnidad
                    'Obtiene la capacidad de Volumen del tipo de unidad
                    iBR.GetCapacidadUnidadVolumen(luoRem1.TipoUnidad, iBR.str_log, decTotalM3)
                End If
                If Trim(Me.txtDolly.Text) <> "" Then
                    luoDolly.GetUnidad("D", Me.txtDolly.Text, iBR.str_log)
                    Me.txtTipoUDolly.Text = luoDolly.TipoUnidadDesc
                    Me.txtIdTipoUD.Text = luoDolly.TipoUnidad
                    'Obtiene la capacidad de Volumen del tipo de unidad
                    iBR.GetCapacidadUnidadVolumen(luoDolly.TipoUnidad, iBR.str_log, decTotalM3)
                End If
                If Trim(Me.txtRemolque2.Text) <> "" Then
                    luoRem1.GetUnidad("R", Me.txtRemolque2.Text, iBR.str_log)
                    Me.txtTipoURem1.Text = luoRem1.TipoUnidadDesc
                    Me.txtIdTipoUR1.Text = luoRem1.TipoUnidad
                    'Obtiene la capacidad de Volumen del tipo de unidad
                    iBR.GetCapacidadUnidadVolumen(luoRem1.TipoUnidad, iBR.str_log, decTotalM3)
                End If
                'Se llama a la funcion que obtiene la informacion del Volumen y Peso de los Productos
                'de los Pedidos para validar la Capacidad de Carga del armado , emartinez 19/Jun/2012
                'Folio : , DINET
                If Request("ParmAsignHdn") Is Nothing Then
                    iBR.ObtienePesoVolumenPedidos(arrListaPedidosAreas, arrListaPedidos, Me.KGCargar, Me.M3Cargar, iBR.str_log)
                Else
                    iBR.ObtienePesoVolumenPedidos(iBR.AsignacionClass.ListaPedidosAreas, iBR.AsignacionClass.ListaPedidos, Me.KGCargar, Me.M3Cargar, iBR.str_log)
                End If
                Me.txtCargaKG.Text = Format(Me.KGCargar, "###,##0.00")
                Me.txtCargarM3.Text = Format(Me.M3Cargar, "###,##0.00")
                Try
                    'Se llama a la funcion que Obtiene la Capacidad de Carga del Armado, se envian como argumentos
                    'el Tipo de Unidad del Tractor, el Tipo de Unidad del Remolque1, el Tipo de Unidad del Dolly, 
                    'el Tipo de Unidad del Remolque2, 
                    'Dim luoCC As New net_b_operacionesV8.ed_b_despcapacidadcarga
                    Dim strDescMTC As String = ""
                    'luoCC.InicializaEstructuraDelLog(1, iBR.str_log)
                    'iBR.CapacidadCarga.id_unidad = Me.txtIdTipoUTracto.Text
                    'iBR.CapacidadCarga.id_remolque_1 = Me.txtIdTipoUR1.Text
                    'iBR.CapacidadCarga.id_dolly = Me.txtIdTipoUD.Text
                    'iBR.CapacidadCarga.id_remolque_2 = Me.txtIdTipoUR2.Text
                    iBR.GetCapacidadCargaArmado(CInt(Me.txtIdTipoUTracto.Text), CInt(Me.txtIdTipoUR1.Text), CInt(Me.txtIdTipoUR2.Text), CInt(Me.txtIdTipoUD.Text), Me.CapacidadKG, Me.CapacidadM3, strDescMTC, iBR.str_log)
                    If strDescMTC <> "" Then
                        Me.lblConfigMTC.Text = Me.lblConfigMTC.Text & strDescMTC
                    Else
                        Me.lblConfigMTC.Text = Me.lblConfigMTC.Text & "No Existe"
                    End If
                    Me.CapacidadM3 = decTotalM3
                    Me.txtCapacidadKG.Text = Format(Me.CapacidadKG, "###,##0.00")
                    Me.txtCapacidadM3.Text = Format(Me.CapacidadM3, "###,##0.00")
                Catch ex As Exception
                    Me.DivGuardar.Style.Item(System.Web.UI.HtmlTextWriterStyle.Display) = "none"
                    Throw ex
                End Try
                'Hace la validacion de la Capacidad de Carga vs. Capacidad de Vehiculo
                If (Me.KGCargar > Me.CapacidadKG) Or (Me.M3Cargar > Me.CapacidadM3) Then
                    Me.DivGuardar.Style.Item(System.Web.UI.HtmlTextWriterStyle.Display) = "none"
                    Throw New Exception("La Capacidad de Carga o de Volumen es menor a la Capacidad de Carga registrada en el Cat�logo de Configuraci�n de MTC o el Volumen configurado en el Tipo de Unidad.")
                Else
                    Me.DivGuardar.Style.Item(System.Web.UI.HtmlTextWriterStyle.Display) = "block"
                End If
            Else
                Me.txtHdnParCapCarga.Value = ""
                Me.trInfoCapacidadCarga.Visible = False
                Me.trConfigMTC.Visible = False
            End If

            'Obtiene la fianza de los Remolques Americanos y Propios Americanos
            Dim iuo_br As New net_b_despextended.pre_b_despfianza
            Dim intNacRem1 As Integer = 0
            Dim intNacRem2 As Integer = 0
            If UCase(lstrValidaFianza) = "S" Then
                'Obtengo la nacionalidad del remolque1, cuando es propio y no tiene linea.
                'Jdlcruz, GASA 29-Nov-2012, Folio: 17748
                If Me.txtLineaRem1.Text = "" And Me.txtRemolque1.Text <> "" Then
                    intNacRem1 = luoUnidad.GetRemolqueNacionalidad(Me.txtRemolque1.Text, iBR.str_log)
                End If

                If ((Me.txtLineaRem1.Text <> "" And Me.txtRemolque1.Text <> "") Or intNacRem1 = 2) Then
                    Dim intControl As Integer = 0
                    intControl = luoUnidad.GetRemolqueControlActivo(Me.txtLineaRem1.Text, Me.txtRemolque1.Text, iBR.str_log)
                    iuo_br.AbreVentanaFianza(intControl, Me.txtFianza1.Text, Nothing, Nothing, "", iBR.str_log)
                End If
            End If
            If UCase(lstrValidaFianza) = "S" Then
                'Obtengo la nacionalidad del remolque2, cuando es propio y no tiene linea.
                'Jdlcruz, GASA 29-Nov-2012, Folio: 17748.
                If Me.txtLineaRem2.Text = "" And Me.txtRemolque2.Text <> "" Then
                    intNacRem2 = luoUnidad.GetRemolqueNacionalidad(Me.txtRemolque2.Text, iBR.str_log)
                End If

                If ((Me.txtLineaRem2.Text <> "" And Me.txtRemolque2.Text <> "") Or intNacRem2 = 2) Then
                    Dim intControl As Integer = 0
                    intControl = luoUnidad.GetRemolqueControlActivo(Me.txtLineaRem2.Text, Me.txtRemolque2.Text, iBR.str_log)
                    iuo_br.AbreVentanaFianza(intControl, Me.txtFianza2.Text, Nothing, Nothing, "", iBR.str_log)
                End If
            End If

            'AGONZALEZ FOLIO:24709 02/06/2018
            hdn4Contenedores.Value = luoParm.GetParametro("4Contenedores", iBR.str_log, False)
            bol_4contenedores = IIf(hdn4Contenedores.Value = "S", True, False)
            If (bol_4contenedores) Then
                trInfoContenedores.Visible = True
                trInfoContenedor1.Visible = True
                trInfoContenedor2.Visible = True
                If IsPostBack = False Then
                    Dim dtPedidos As New Data.DataTable
                    If Request("ParmAsignHdn") Is Nothing Then
                        For i As Integer = 0 To arrListaPedidos.Count - 1
                            If arrListaPedidos(i).ToString().Contains("vacio") = False Then
                                LlenarContenedores(arrListaPedidos(i), arrListaPedidosAreas(i))
                            Else
                                Exit For
                            End If
                        Next
                    Else
                        dtPedidos = iBR.GetPedidosDeUnaAsignacion(Request("ParmAsignHdn").ToString(), iBR.str_log)
                        For i As Integer = 0 To dtPedidos.Rows.Count - 1
                            LlenarContenedores(dtPedidos.Rows(i)("id_pedido"), dtPedidos.Rows(i)("id_area"))
                        Next
                    End If
                End If
            End If

            Dim strContenedores_AsiViaje As String = luoParm.GetParametro("Contenedores_AsiViaje", iBR.str_log, False)
            txtParamContenedores.Text = strContenedores_AsiViaje
            If strContenedores_AsiViaje = "S" Then
                If IsPostBack = False Then
                    Dim dtPedidos As New Data.DataTable
                    Dim dtPedido As New Data.DataTable
                    Dim bandera As Integer = 0
                    If Request("ParmAsignHdn") Is Nothing Then
                        For i As Integer = 0 To arrListaPedidos.Count - 1
                            If arrListaPedidos(i).ToString().Contains("vacio") = False Then
                                dtPedido = iBR.GetNombresContenedoresDeUnPedido(arrListaPedidos(i), arrListaPedidosAreas(i), iBR.str_log)
                                trInfoContenedores.Visible = True
                                If dtPedido.Rows(0)("SerieContenedor1").ToString() <> "" Then
                                    If bandera = 0 Then
                                        trInfoContenedor1.Visible = True
                                        txtCont1Ped1.Text = dtPedido.Rows(0)("SerieContenedor1").ToString()
                                        txtCont2Ped1.Text = dtPedido.Rows(0)("SerieContenedor2").ToString()

                                        txtIdContenedor1.Text = dtPedido.Rows(0)("IdContenedor1").ToString()
                                        txtIdContenedor1b.Text = dtPedido.Rows(0)("IdContenedor2").ToString()

                                        txtIdPedido_Contenedores1.Text = arrListaPedidos(i).ToString()
                                        txtareaPedido_Contenedores1.Text = arrListaPedidosAreas(i).ToString()

                                        bandera = bandera + 1
                                    Else
                                        trInfoContenedor2.Visible = True
                                        txtCont1Ped2.Text = dtPedido.Rows(0)("SerieContenedor1").ToString()
                                        txtCont2Ped2.Text = dtPedido.Rows(0)("SerieContenedor2").ToString()

                                        txtIdContenedor2.Text = dtPedido.Rows(0)("IdContenedor1").ToString()
                                        txtIdContenedor2b.Text = dtPedido.Rows(0)("IdContenedor2").ToString()

                                        txtIdPedido_Contenedores2.Text = arrListaPedidos(i).ToString()
                                        txtareaPedido_Contenedores2.Text = arrListaPedidosAreas(i).ToString()

                                        bandera = bandera + 1
                                    End If
                                Else
                                    If bandera = 0 Then
                                        trInfoContenedor1.Visible = True

                                        txtIdPedido_Contenedores1.Text = arrListaPedidos(i).ToString()
                                        txtareaPedido_Contenedores1.Text = arrListaPedidosAreas(i).ToString()

                                        bandera = bandera + 1
                                    Else
                                        trInfoContenedor2.Visible = True

                                        txtIdPedido_Contenedores2.Text = arrListaPedidos(i).ToString()
                                        txtareaPedido_Contenedores2.Text = arrListaPedidosAreas(i).ToString()

                                        bandera = bandera + 1
                                    End If
                                End If
                            Else
                                Exit For
                            End If
                            If bandera = 2 Then
                                Exit For
                            End If
                        Next
                    Else
                        dtPedidos = iBR.GetPedidosDeUnaAsignacion(Request("ParmAsignHdn").ToString(), iBR.str_log)
                        trInfoContenedores.Visible = True
                        For i As Integer = 0 To dtPedidos.Rows.Count - 1
                            dtPedido = iBR.GetNombresContenedoresDeUnPedido(dtPedidos.Rows(i)("id_pedido"), dtPedidos.Rows(i)("id_area"), iBR.str_log)
                            trInfoContenedores.Visible = True
                            If dtPedido.Rows(0)("SerieContenedor1").ToString() <> "" Then
                                If bandera = 0 Then
                                    trInfoContenedor1.Visible = True
                                    txtCont1Ped1.Text = dtPedido.Rows(0)("SerieContenedor1").ToString()
                                    txtCont2Ped1.Text = dtPedido.Rows(0)("SerieContenedor2").ToString()

                                    txtIdContenedor1.Text = dtPedido.Rows(0)("IdContenedor1").ToString()
                                    txtIdContenedor1b.Text = dtPedido.Rows(0)("IdContenedor2").ToString()

                                    txtIdPedido_Contenedores1.Text = dtPedidos.Rows(i)("id_pedido")
                                    txtareaPedido_Contenedores1.Text = dtPedidos.Rows(i)("id_area")

                                    bandera = bandera + 1
                                Else
                                    trInfoContenedor2.Visible = True
                                    txtCont1Ped2.Text = dtPedido.Rows(0)("SerieContenedor1").ToString()
                                    txtCont2Ped2.Text = dtPedido.Rows(0)("SerieContenedor2").ToString()

                                    txtIdContenedor2.Text = dtPedido.Rows(0)("IdContenedor1").ToString()
                                    txtIdContenedor2b.Text = dtPedido.Rows(0)("IdContenedor2").ToString()

                                    txtIdPedido_Contenedores2.Text = dtPedidos.Rows(i)("id_pedido")
                                    txtareaPedido_Contenedores2.Text = dtPedidos.Rows(i)("id_area")

                                    bandera = bandera + 1
                                End If
                            Else
                                If bandera = 0 Then
                                    trInfoContenedor1.Visible = True

                                    txtIdPedido_Contenedores1.Text = dtPedidos.Rows(i)("id_pedido")
                                    txtareaPedido_Contenedores1.Text = dtPedidos.Rows(i)("id_area")

                                    bandera = bandera + 1
                                Else
                                    trInfoContenedor2.Visible = True

                                    txtIdPedido_Contenedores2.Text = dtPedidos.Rows(i)("id_pedido")
                                    txtareaPedido_Contenedores2.Text = dtPedidos.Rows(i)("id_area")

                                    bandera = bandera + 1
                                End If
                            End If
                            If bandera = 2 Then
                                Exit For
                            End If
                        Next
                    End If
                End If
            End If

            'Valida si se encuentra activo el parametro para mostrar el nombre del permisionario de la unidad en caso de serlo.
            muestra_persmisionario = luoParm.GetParametro("muestra_permisionario", iBR.str_log, False)
            If muestra_persmisionario = "S" Then
                Me.DivDatosPermUnidad.Style.Item(System.Web.UI.HtmlTextWriterStyle.Display) = "block"
                iBR.GetNomPermisionario(Me.txtPermisionario.Text, iBR.AsignacionClass.Tractor)
            End If

            'ereyes OS Xpress Internacional  02/12/2015
            lstr_viajesvacios = luoParm.GetParametro("op_viajesvacios_auto", iBR.str_log, False)
            lstr_tiposegdetonador = luoParm.GetParametro("tiposegdetonaviajevacio", iBR.str_log, False)
            If lstr_viajesvacios = "S" Then
                Me.hdn_viajevacioauto.Value = lstr_viajesvacios
                Me.hdn_tiposegdetonador.Value = lstr_tiposegdetonador
            Else
                Me.hdn_viajevacioauto.Value = "N"
                Me.hdn_tiposegdetonador.Value = ""
            End If

            'GGUERRA FOLIO:21597
            strIdFlota = Request("Flota")

            'rherrera GAAL Creaci�n autom�tica de Vales; Par�metro para obtener las gasolineras cercanas a la ruta
            Me.hdnObtenerGasolineras.Value = luoParm.GetParametro("CalculoValesCombustibleAuto", iBR.str_log, False)
            Me.hdnObtenerGasolineras.Value = If(Me.hdnObtenerGasolineras.Value = "S", "S", "N")
            If Me.hdnObtenerGasolineras.Value = "S" Then
                If Request("ParmAsignHdn") <> Nothing Then
                    Me.txtRutaRegreso.Enabled = False
                    'Revisa si es una asignaci�n de Viaje de Regreso, si es as�, deshabilita el bot�n de Guardar para no permitir modificarla
                    If iBR.AsignacionClass.viaje_regreso > 1 Then
                        'Me.DivGuardar.Style.Item(System.Web.UI.HtmlTextWriterStyle.Display) = "none"
                    End If
                End If
                Me.hdnCombustibleMinimo.Value = luoParm.GetParametro("CantidadCombMinimoGasolineras", iBR.str_log, False)
            Else
                Me.tablaRutaSiguiente.Visible = False
                Me.txtRutaRegreso.Visible = False
                Me.txtRutaRegresoDesc.Visible = False
                Me.lblRutaSiguiente.Visible = False
                Me.Img4.Visible = False
            End If

            'ERAMIREZ F:24499 05/06/2018
            Me.hdnShowOrigenDestinoVacio.Value = luoParm.GetParametro("showOrigenDestinoVacio", iBR.str_log, False)
            If Me.hdnShowOrigenDestinoVacio.Value = "S" Then
                Me.MostrarOrigenDestino(iBR.str_log)
            End If

            'GPEREZ F:24656 13/07/18 -- Innovativos
            Me.hdnUbicacionDiasRemolque.Value = luoParm.GetParametro("ubicacion_dias_remolque", iBR.str_log, False)
            If Me.hdnUbicacionDiasRemolque.Value = "S" And Request("Lista") = "vacio[;]" Then
                tblUbicacionDiasRemolque.Style.Add("display", "block")
                If iBR.AsignacionClass.Id_destinatario <> "0" Then
                    Me.txtDestinat.Text = iBR.AsignacionClass.Id_destinatario
                    Me.txtDestinatNom.Text = iBR.GetNombreById(iBR.AsignacionClass.Id_destinatario, iBR.str_log)
                End If
            End If


        Catch ex As Exception
            Me.trConfigMTC.Visible = False
            Me.trInfoCapacidadCarga.Visible = False
            Me.MuestraRemDest(arrListaPedidos)
            ClientScript.RegisterStartupScript(Page.GetType(), "test", "<script>lblErrorAjax.innerText = '" & ex.Message & "';</script>")
        End Try

    End Sub

    Protected Sub MuestraRemDest(ByRef arrListaPedidos As ArrayList)
        Try
            'HRFF 13/11/2012 Se valida si es Viaje Vacio y que admita los controles de Remitente y Destinatario en esta pantalla
            strRemDest = luoParm.GetParametro("desp_ViajeV_RemDest", iBR.str_log, False)
            Dim strtipoopera = luoParm.GetParametro("areas_transpormex", iBR.str_log, False)
            Dim bol_RemYDest As Boolean = False
            Dim bol_tipoopera As Boolean = False

            If strtipoopera = "S" Then
                If arrListaPedidos.Count > 0 Then
                    If UCase(arrListaPedidos(0)) = "VACIO" Then
                        tblRemDestOper.Style.Add("display", "block")
                        bol_tipoopera = True
                    End If
                Else
                    tblRemDestOper.Style.Add("display", "block")
                    bol_tipoopera = True
                End If
            End If
            If strRemDest = "S" Then

                Dim EnabledRemolque As Boolean = True
                Dim EnabledRemolque2 As Boolean = True

                If Request("ParmAsignHdn") Is Nothing Then
                    If Request("Lista") = "vacio[;]" Then
                        tblRemDestOper.Style.Add("display", "block")
                        tblRemDest.Style.Add("display", "block")
                        bol_RemYDest = True
                        lblRemDestHabil.Text = "1"
                    Else
                        If iBR.AsignacionClass.Remolque1.Length > 0 Then
                            EnabledRemolque = False
                        End If
                        If iBR.AsignacionClass.Remolque2.Length > 0 Then
                            EnabledRemolque2 = False
                        End If
                    End If
                Else
                    Dim dtPedidosAsig As New Data.DataTable
                    dtPedidosAsig = iBR.GetPedidosDeUnaAsignacion(Request("ParmAsignHdn"), iBR.str_log)

                    If dtPedidosAsig.Rows.Count = 0 Then
                        tblRemDest.Style.Add("display", "block")
                        bol_RemYDest = True
                        lblRemDestHabil.Text = "1"
                    ElseIf dtPedidosAsig.Rows.Count = 1 Then
                        If dtPedidosAsig.Rows(0)("status_pedido").ToString() = "4" And dtPedidosAsig.Rows(0)("observaciones").ToString().Contains("Repo/Colocaci�n") = True Then
                            tblRemDest.Style.Add("display", "block")
                            bol_RemYDest = True
                            lblRemDestHabil.Text = "1"
                        Else
                            If iBR.AsignacionClass.Remolque1.Length > 0 Then
                                EnabledRemolque = False
                            End If
                            If iBR.AsignacionClass.Remolque2.Length > 0 Then
                                EnabledRemolque2 = False
                            End If
                        End If
                    Else
                        If iBR.AsignacionClass.Remolque1.Length > 0 Then
                            EnabledRemolque = False
                        End If
                        If iBR.AsignacionClass.Remolque2.Length > 0 Then
                            EnabledRemolque2 = False
                        End If
                    End If
                End If
                FianzaRetornada.Value = "S"

                If IsPostBack = False Then
                    txtLineaRem1.Enabled = EnabledRemolque
                    txtRemolque1.Enabled = EnabledRemolque
                    ImgSearchRem1.Visible = EnabledRemolque
                    Img5.Visible = EnabledRemolque

                    txtLineaRem2.Enabled = EnabledRemolque2
                    txtRemolque2.Enabled = EnabledRemolque2
                    ImgSearchRem2.Visible = EnabledRemolque2
                    Img7.Visible = EnabledRemolque2
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function ValTipoOperacion(ByRef idPedido As Integer, ByRef idArea As Integer) As String
        iBR.str_log = New net_objestandar.str_log(ConfigurationManager.AppSettings("strConn").ToString)
        Dim ipedido As New net_b_zamclases.net_b_clasepedido
        ipedido.GetDatosPedido(idArea, idPedido, iBR.str_log)
        Return ipedido.id_tipo_operacion
    End Function
    'Llama a la funci�n que Guarda la informaci�n
    'Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGuardar.Click
    '    If Session("UserName") Is Nothing Or Session("vg_area") Is Nothing Then
    '        Response.Redirect(Application("myRoot") & "/relogin.aspx")
    '    End If
    '    Salvar()
    '    'Me.bolCierraVentana = True
    'End Sub

    Public Sub MuetraDiVentana(ByVal Opcion As Integer)
        Try
            Select Case Opcion

                Case 0
                    Me.diVentanaTiempos.Style.Add("display", "none")

                Case 1
                    Me.diVentanaTiempos.Style.Add("display", "block")

                Case 2
                    Me.diVentanaTiempos.Style.Add("display", "none")
            End Select

        Catch ex As Exception
            Throw
        End Try
    End Sub

    'rherrera ETF 20/11/14
    Public Sub GetTiempoUrgente()
        Dim ldtFechaFin As DateTime
        Dim arrListaPedidos As ArrayList
        Dim arrListaPedidosAreas As ArrayList
        Dim intUrgente As Integer

        If Me.txtListaPedidosAreas.Text = Nothing Then
            Me.txtListaPedidosAreas.Text = iBR.AsignacionClass.ListaPedidosAreas(0)
        Else
            iBR.PasarAArreglo(Request("Lista2"), "[;]", arrListaPedidosAreas, iBR.str_log)
            Me.txtListaPedidosAreas.Text = arrListaPedidosAreas(0)
        End If

        If Me.txtListaPedidos.Text = Nothing Then
            Me.txtListaPedidos.Text = iBR.AsignacionClass.ListaPedidos(0)
        Else
            iBR.PasarAArreglo(Request("Lista"), "[;]", arrListaPedidos, iBR.str_log)
            Me.txtListaPedidos.Text = arrListaPedidos(0)
        End If

        Me.txtHrsEstandar.Text = iBR.GetTiempoUrgente(CInt(Me.txtRuta.Text), intUrgente, Me.txtListaPedidosAreas.Text, Me.txtListaPedidos.Text, iBR.str_log)
        iBR.AsignacionClass.tiempo_ruta = Me.txtHrsEstandar.Text
        iBR.AsignacionClass.HrsEstandar = Me.txtHrsEstandar.Text

        If intUrgente = 0 Then
            Me.chkPedidoUrgente.Checked = False
        Else
            Me.chkPedidoUrgente.Checked = True
        End If

        ldtFechaFin = Me.wdcInicioViaje.Value
        Me.wdcFinViaje.Value = ldtFechaFin.AddHours(CDec(Me.txtHrsEstandar.Text))

    End Sub

    Public Sub Salvar()
        Try
            If Not Page.IsValid Then Return

            'Se asigna la Unidad 0 cuando no se ha capturado alg�n dato en el campo
            'de Tractor, fecha de modificaci�n del c�digo 15-Dic-2004 emartinez
            If Trim(Me.txtUnidad.Text) = "" Then Me.txtUnidad.Text = "0"
            'fin de la modificaci�n

            If Me.txtUnidad.Text <> "0" Then
                Dim luoParm As New net_b_zamclasesextended.net_b_clasegeneralparametrosextended
                Dim lstrMsgUnidad As String = ""
                Dim lintValorParam As Integer = 0
                Dim luoUnidad As New net_b_zamclasesextended.net_b_claseunidadextended
                luoUnidad.ValidaOrdenServicioUnidad(Me.txtUnidad.Text, iBR.str_log, lstrMsgUnidad, False)
                'Se llama a la funci�n GetParametro, se envian como argumentos el nombre del 
                'par�metro, la estructura del LOG como referencia y una variable que indica 
                'si se valida o no que existe el par�metro en la base de datos. emartinez 02-ENE-2009
                lintValorParam = luoParm.GetParametroNumerico("despvalidaordenserviciounidad", iBR.str_log, False)
                Select Case lintValorParam
                    Case 0  'No valida
                    Case 1  'Valida y muestra mensaje, permite continuar
                        If lstrMsgUnidad.Length > 0 Then
                            Me.lblError.Text = "ADVERTENCIA : " & lstrMsgUnidad
                            Me.lblError.Visible = True
                        End If
                    Case 2  'Valida y NO permite continuar
                        If lstrMsgUnidad.Length > 0 Then
                            Throw New Exception(lstrMsgUnidad)
                        End If
                End Select
            End If

            'Si ya ejecuto el proceso de actualizar sale del proceso e inicializa la variable 
            'para cerrar la pantalla a True
            If Session("ActualizaAsignacion") = False Then
                bolCierraVentana = True
                Return
            End If

            'Si no envia el id_asignacion, 
            If Request("ParmAsignHdn") Is Nothing Then
                'Se ingresa la informaci�n de la Nueva Asignaci�n
                Call Ingresar()

                'Se inicializa la variable para que no ejecute la misma accion al 
                'dar dos veces click al bot�n de guardar
                Session("ActualizaAsignacion") = False

                iBR.SetSeguimientoActual(Me.txtUnidad.Text, iBR.str_log, True)

            Else
                'Actualiza la informaci�n de la Asignaci�n
                Call Update()
                'Se inicializa la variable para que ejecute la misma accion al 
                'dar dos veces click al bot�n de guardar
                Session("ActualizaAsignacion") = False

                'Se actualiza el Seguimiento
                iBR.SetSeguimientoActual(Me.txtUnidad.Text, iBR.str_log, True)
                'En caso de que haya cambiado de unidad
                If Me.txtUnidad.Text <> Me.txtUnidadOriginal.Text Then
                    'Se actualiza la informaci�n de la Unidad Original
                    iBR.SetSeguimientoActual(Me.txtUnidadOriginal.Text, iBR.str_log, True)
                End If
            End If

            Me.txtLineaRem1Original.Text = iBR.AsignacionClass.LineaRem1
            Me.txtRemolque1Original.Text = iBR.AsignacionClass.Remolque1
            Me.txtLineaRem2Original.Text = iBR.AsignacionClass.LineaRem2
            Me.txtRemolque2Original.Text = iBR.AsignacionClass.Remolque2

            bolCierraVentana = True



        Catch ex As Exception
            Me.lblError.Text = ex.Message
            Me.lblError.Visible = True
        End Try

    End Sub

    Public Sub Ingresar()
        Dim arrListaPedidos As New ArrayList
        Dim arrListaPedidosAreas As New ArrayList
        Dim arrListaPedidosPK As New ArrayList

        iBR.AsignacionClass.Area = Session("vg_area")
        iBR.AsignacionClass.Ingreso = Session("UserName")
        iBR.AsignacionClass.Fecha_ingreso = Date.Now

        'Pasa la lista de pedidos a un arreglo
        iBR.PasarAArreglo(Request("Lista"), "[;]", arrListaPedidos, iBR.str_log)
        iBR.PasarAArreglo(Request("Lista2"), "[;]", arrListaPedidosAreas, iBR.str_log)
        iBR.PasarAArreglo(Request("Lista3"), "[;]", arrListaPedidosPK, iBR.str_log)

        iBR.AsignacionClass.Ruta = Me.txtRuta.Text
        iBR.AsignacionClass.F_prog_ini_viaje = Me.wdcInicioViaje.Value
        iBR.AsignacionClass.F_prog_fin_viaje = Me.wdcFinViaje.Value
        iBR.AsignacionClass.Ingreso = Me.txtIngreso.Text
        iBR.AsignacionClass.HrsEstandar = Me.txtHrsEstandar.Text
        iBR.AsignacionClass.Kit = Me.txtKit.Text
        If Trim(Me.txtUnidad.Text) = "" Then
            iBR.AsignacionClass.Tractor = "0"
        Else
            iBR.AsignacionClass.Tractor = Me.txtUnidad.Text
        End If
        iBR.AsignacionClass.LineaRem1 = Me.txtLineaRem1.Text
        iBR.AsignacionClass.Remolque1 = Me.txtRemolque1.Text
        iBR.AsignacionClass.Dolly = Me.txtDolly.Text
        iBR.AsignacionClass.LineaRem2 = Me.txtLineaRem2.Text
        iBR.AsignacionClass.Remolque2 = Me.txtRemolque2.Text
        If Not Request("Flota") Is Nothing Then
            If IsNumeric(Request("Flota")) Then
                iBR.AsignacionClass.Id_Flota = Request("Flota")
            End If
        End If
        iBR.AsignacionClass.Id_configuracionviaje = Me.ddlConfigViaje.SelectedValue

        'inicializa el valor del  operador 1 en caso de que sea vacio lo pone como cero 
        If Trim(Me.txtOperador1.Text) = "" Then
            iBR.AsignacionClass.Operador1 = 0
        Else
            iBR.AsignacionClass.Operador1 = Me.txtOperador1.Text
        End If

        'inicializa el valor del  operador 2 en caso de que sea vacio lo pone como cero 
        If Trim(Me.txtOperador2.Text) = "" Then
            iBR.AsignacionClass.Operador2 = 0
        Else
            iBR.AsignacionClass.Operador2 = Me.txtOperador2.Text
        End If

        If Trim(Me.txtObservaciones.Text) = "" Then
            iBR.AsignacionClass.Observaciones = ""
        Else
            iBR.AsignacionClass.Observaciones = Me.txtObservaciones.Text
        End If

        iBR.ListaPedidos = arrListaPedidos
        iBR.ListaPedidosAreas = arrListaPedidosAreas
        iBR.ListaPedidosPK = arrListaPedidosPK
        Me.txtNumAsignacion.Text = iBR.AsignacionClass.Viaje

        iBR.AsignacionClass.SeguimientoNombreCorto = Me.txtSeguimiento.Text

        'Se agrega la validaci�n del par�metro de SU TRANSPORTE para actualizar la Fecha Recibido en 
        'los Pedidos de la Asignaci�n, emartinez 30-MARZO-2009
        Dim luoParam As New net_b_zamclasesextended.net_b_clasegeneralparametrosextended
        Dim lstrParam As String = ""
        'Se llama a la funci�n GetParametro, se envian como argumentos el nombre del 
        'par�metro, la estructura del LOG como referencia y una variable que indica 
        'si se valida o no que existe el par�metro en la base de datos.
        lstrParam = luoParam.GetParametro("CAMBIOS_SUTRANSPORTE", iBR.str_log, False)
        If lstrParam = "S" Then iBR.AsignacionClass.ActualizaFechaRecibido = True

        iBR.CrearAsignacion(Me.txtLineaRem1Original.Text, Me.txtRemolque1Original.Text, Me.txtLineaRem2Original.Text, Me.txtRemolque2Original.Text)

    End Sub

    Public Sub Update()

        iBR.AsignacionClass.GetAsignacion(Request("ParmAsignHdn"), iBR.AsignacionClass, iBR.str_log)

        iBR.AsignacionClass.Ruta = Me.txtRuta.Text
        iBR.AsignacionClass.F_prog_ini_viaje = Me.wdcInicioViaje.Value
        iBR.AsignacionClass.F_prog_fin_viaje = Me.wdcFinViaje.Value
        If Trim(Me.txtUnidad.Text) = "" Then
            iBR.AsignacionClass.Tractor = "0"
        Else
            iBR.AsignacionClass.Tractor = Me.txtUnidad.Text
        End If
        iBR.AsignacionClass.LineaRem1 = Me.txtLineaRem1.Text
        iBR.AsignacionClass.Remolque1 = Me.txtRemolque1.Text
        iBR.AsignacionClass.Dolly = Me.txtDolly.Text
        iBR.AsignacionClass.LineaRem2 = Me.txtLineaRem2.Text
        iBR.AsignacionClass.Remolque2 = Me.txtRemolque2.Text
        iBR.AsignacionClass.Id_configuracionviaje = Me.ddlConfigViaje.SelectedValue
        iBR.AsignacionClass.Kit = Me.txtKit.Text

        'inicializa el valor del  operador 1 en caso de que sea vacio lo pone como cero 
        If Trim(Me.txtOperador1.Text) = "" Then
            iBR.AsignacionClass.Operador1 = 0
        Else
            iBR.AsignacionClass.Operador1 = Me.txtOperador1.Text
        End If

        'inicializa el valor del  operador 2 en caso de que sea vacio lo pone como cero 
        If Trim(Me.txtOperador2.Text) = "" Then
            iBR.AsignacionClass.Operador2 = 0
        Else
            iBR.AsignacionClass.Operador2 = Me.txtOperador2.Text
        End If

        If Trim(Me.txtObservaciones.Text) = "" Then
            iBR.AsignacionClass.Observaciones = ""
        Else
            iBR.AsignacionClass.Observaciones = Me.txtObservaciones.Text
        End If

        Me.txtNumAsignacion.Text = iBR.AsignacionClass.Num_Asignacion

        iBR.AsignacionClass.SeguimientoNombreCorto = Me.txtSeguimiento.Text
        iBR.SalvarEdicion(Me.txtLineaRem1Original.Text, Me.txtRemolque1Original.Text, Me.txtLineaRem2Original.Text, Me.txtRemolque2Original.Text, iBR.str_log)
    End Sub

    Private Sub cusvInicioViaje_ServerValidate(ByVal source As System.Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)
        Try
            If Not IsDate(Me.wdcInicioViaje.Value) Then
                Throw New Exception("El dato capturado en Fecha Inicio Viaje debe ser una fecha v�lida")
            End If

            args.IsValid = True
        Catch ex As Exception
            lblError.Text = ex.Message
            lblError.Visible = True
            args.IsValid = False
        End Try
    End Sub

    Private Sub cusvFinViaje_ServerValidate(ByVal source As System.Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)
        Try
            If Not IsDate(Me.wdcFinViaje.Value) Then
                Throw New Exception("El dato capturado en Fecha Fin Viaje debe ser una fecha v�lida")
            End If

            args.IsValid = True
        Catch ex As Exception
            lblError.Text = ex.Message
            lblError.Visible = True
            args.IsValid = False
        End Try
    End Sub

    Private Sub cusvRuta_ServerValidate(ByVal source As System.Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cusvRuta.ServerValidate
        Try

            If Trim(Me.txtRuta.Text) = "" Then Me.txtRuta.Text = "0"
            ''iBR.ValidaRutaAsignacion(Me.txtRutaDesc.Text)
            iBR.ValidaOrigenRutaAsignacion(Me.txtRuta.Text, Me.txtUnidad.Text, Me.wdcInicioViaje.Value, iBR.AsignacionClass.Asignacion, Me.txtRespValOrigCont.Text, Me.txtRutaDesc.Text, iBR.str_log)
            iBR.AsignacionClass.Ruta = Me.txtRuta.Text
            args.IsValid = True
        Catch ex As Exception
            Me.txtRutaDesc.Text = ""
            lblError.Text = ex.Message
            Me.cusvRuta.ErrorMessage = ex.Message
            args.IsValid = False
        End Try
    End Sub

    'Funci�n que valida la informaci�n del Kit
    Protected Sub cusvKit_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cusvKit.ServerValidate
        Try
            iBR.ValidaKit(Trim(Me.txtKit.Text), Trim(Me.txtUnidad.Text), Trim(Me.txtRemolque1.Text), Trim(Me.txtDolly.Text), Trim(Me.txtRemolque2.Text), iBR.str_log)
            args.IsValid = True
        Catch ex As Exception
            Me.txtKit.Text = ""
            lblError.Text = ex.Message
            Me.cusvUnidad.ErrorMessage = ex.Message
            args.IsValid = False
        End Try
    End Sub

    'Funci�n que valida la informaci�n del Tractor
    Private Sub cusvUnidad_ServerValidate(ByVal source As System.Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cusvUnidad.ServerValidate
        Try
            If Trim(Me.txtUnidad.Text) = "" Then Me.txtUnidad.Text = "0"
            iBR.AsignacionClass.Tractor = Me.txtUnidad.Text
            iBR.ValidaTractor()
            If Me.txtUnidad.Text <> "0" Then
                Dim luoParm As New net_b_zamclasesextended.net_b_clasegeneralparametrosextended
                Dim lstrMsgUnidad As String = ""
                Dim lintValorParam As Integer = 0
                Dim luoUnidad As New net_b_zamclasesextended.net_b_claseunidadextended
                luoUnidad.ValidaOrdenServicioUnidad(Me.txtUnidad.Text, iBR.str_log, lstrMsgUnidad, False)
                'Se llama a la funci�n GetParametro, se envian como argumentos el nombre del 
                'par�metro, la estructura del LOG como referencia y una variable que indica 
                'si se valida o no que existe el par�metro en la base de datos. emartinez 02-ENE-2009
                lintValorParam = luoParm.GetParametroNumerico("despvalidaordenserviciounidad", iBR.str_log, False)
                Select Case lintValorParam
                    Case 0  'No valida
                    Case 1  'Valida y muestra mensaje, permite continuar
                        If lstrMsgUnidad.Length > 0 Then
                            Me.lblError.Text = "ADVERTENCIA : " & lstrMsgUnidad
                            Me.lblError.Visible = True
                        End If
                    Case 2  'Valida y NO permite continuar
                        If lstrMsgUnidad.Length > 0 Then
                            Throw New Exception(lstrMsgUnidad)
                        End If
                End Select
            End If
			'Si se encuentra activo el parametro muestra el nombre del permisionario.
            If muestra_persmisionario = "S" Then
                Me.txtPermisionario.Text = iBR.strPermisionario
            End If
            args.IsValid = True
        Catch ex As Exception
            Me.cusvUnidad.ErrorMessage = ex.Message
            args.IsValid = False
        End Try
    End Sub

    'Valida la informaci�n del Remolque 1
    Private Sub cusvRemolque1_ServerValidate(ByVal source As System.Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cusvRemolque1.ServerValidate
        Try
            'Seg�n la opci�n no valida el Armado para el caso de madrinas
            If Me.txtHdnValidaArmado.Value = 0 Then Exit Sub

            iBR.AsignacionClass.str_log = iBR.str_log
            iBR.AsignacionClass.LineaRem1 = Me.txtLineaRem1.Text
            iBR.AsignacionClass.Remolque1 = Me.txtRemolque1.Text
            iBR.ValidaRemolque1()
            args.IsValid = True
        Catch ex As Exception
            Me.cusvRemolque1.ErrorMessage = ex.Message
            args.IsValid = False
        End Try
    End Sub

    'Valida la informaci�n del Remolque 2
    Private Sub cusvRemolque2_ServerValidate(ByVal source As System.Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cusvRemolque2.ServerValidate
        Try
            'Seg�n la opci�n no valida el Armado para el caso de madrinas
            If Me.txtHdnValidaArmado.Value = 0 Then Exit Sub

            iBR.AsignacionClass.Remolque1 = Me.txtRemolque1.Text
            iBR.AsignacionClass.LineaRem1 = Me.txtLineaRem1.Text
            iBR.AsignacionClass.Dolly = Me.txtDolly.Text

            iBR.AsignacionClass.LineaRem2 = Me.txtLineaRem2.Text
            iBR.AsignacionClass.Remolque2 = Me.txtRemolque2.Text
            iBR.ValidaRemolque2()
            args.IsValid = True
        Catch ex As Exception
            Me.cusvRemolque2.ErrorMessage = ex.Message
            args.IsValid = False
        End Try
    End Sub

    'Funci�n que Valida los datos del Dolly
    Private Sub cusvDolly_ServerValidate(ByVal source As System.Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cusvDolly.ServerValidate
        Try
            'Seg�n la opci�n no valida el Armado para el caso de madrinas
            If Me.txtHdnValidaArmado.Value = 0 Then Exit Sub

            'Valida la informaci�n del Dolly
            iBR.AsignacionClass.Dolly = Me.txtDolly.Text
            iBR.ValidaDolly()
            args.IsValid = True
        Catch ex As Exception
            Me.cusvDolly.ErrorMessage = ex.Message
            args.IsValid = False
        End Try
    End Sub

    'Valida la informaci�n de la L�nea del Remolque 
    Private Sub cusvLineaRem1_ServerValidate(ByVal source As System.Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cusvLineaRem1.ServerValidate
        Try
            'Seg�n la opci�n no valida el Armado para el caso de madrinas
            If Me.txtHdnValidaArmado.Value = 0 Then Exit Sub

            iBR.AsignacionClass.str_log = iBR.str_log

            iBR.AsignacionClass.LineaRem1 = Me.txtLineaRem1.Text
            iBR.AsignacionClass.Remolque1 = Me.txtRemolque1.Text
            iBR.ValidaRemolque1()
            args.IsValid = True
        Catch ex As Exception
            Me.cusvLineaRem1.ErrorMessage = ex.Message
            args.IsValid = False
        End Try
    End Sub

    Private Sub cusvLineaRem2_ServerValidate(ByVal source As System.Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cusvLineaRem2.ServerValidate
        Try
            'Seg�n la opci�n no valida el Armado para el caso de madrinas
            If Me.txtHdnValidaArmado.Value = 0 Then Exit Sub

            iBR.AsignacionClass.Remolque1 = Me.txtRemolque1.Text
            iBR.AsignacionClass.LineaRem1 = Me.txtLineaRem1.Text
            iBR.AsignacionClass.Dolly = Me.txtDolly.Text

            iBR.AsignacionClass.LineaRem2 = Me.txtLineaRem2.Text
            iBR.AsignacionClass.Remolque2 = Me.txtRemolque2.Text
            iBR.ValidaRemolque2()
            args.IsValid = True
        Catch ex As Exception
            Me.cusvLineaRem2.ErrorMessage = ex.Message
            args.IsValid = False
        End Try
    End Sub

    'Se le aplica formato a las columnas del WebGrid de paso para refrescar la informaci�n en la 
    'pantalla de Asignaci�n de Viajes y Control de Viajes
    Private Sub uwgPaso_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles uwgPaso.InitializeLayout
        e.Layout.Bands(0).Columns.FromKey("posdate").Format = Application("DateTimeFormat")
        e.Layout.Bands(0).Columns.FromKey("f_prog_ini_viaje").Format = Application("DateTimeFormat")
        e.Layout.Bands(0).Columns.FromKey("fecha_real_viaje").Format = Application("DateTimeFormat")
        e.Layout.Bands(0).Columns.FromKey("f_prog_fin_viaje").Format = Application("DateTimeFormat")
        e.Layout.Bands(0).Columns.FromKey("fecha_disponible").Format = Application("DateTimeFormat")
        e.Layout.Bands(0).Columns.FromKey("f_ini_status").Format = Application("DateTimeFormat")
    End Sub

    Private Sub uwgPaso_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwgPaso.InitializeRow
        Dim intDiasMtto, intDiasVac, intDiaVenceLic As Integer
        Dim intFIniProg, intFIniReal1, intFFinProg As Integer

        'Columna de Mensajes Nuevos
        If e.Row.Cells.FromKey("MensajesNuevos").Value <= 0 Then
            e.Row.Cells.FromKey("MensajesNuevos").Style.BackgroundImage = "../Imagenes/mensajewritegrid.gif"
            e.Row.Cells.FromKey("MensajesNuevos").Value = DBNull.Value
        End If

        'Columna de AlarmasNuevas
        If e.Row.Cells.FromKey("AlarmasNuevas").Value <= 0 Then
            e.Row.Cells.FromKey("AlarmasNuevas").Style.BackgroundImage = "../Imagenes/spacer.GIF"
            e.Row.Cells.FromKey("AlarmasNuevas").Style.Cursor = Infragistics.WebUI.[Shared].Cursors.NotSet
        Else
            e.Row.Cells.FromKey("AlarmasNuevas").Title = "Mostrar Alarmas de la Unidad"
        End If

        'Columna del Viajes Pendientes
        If e.Row.Cells.FromKey("Pend").Value > 0 Then
            e.Row.Cells.FromKey("Pend").Style.ForeColor = Drawing.Color.MediumBlue
            e.Row.Cells.FromKey("Pend").Title = "Mostrar Viajes Pendientes"
        Else
            e.Row.Cells.FromKey("Pend").Style.Cursor = Infragistics.WebUI.[Shared].Cursors.NotSet
        End If

        If e.Row.Cells.FromKey("status_asignacion").Value = 1 Then
            e.Row.Cells.FromKey("num_asignacion").Style.ForeColor = Drawing.Color.Red
        End If

        If e.Row.Cells.FromKey("no_viaje").Value > 0 Then
            e.Row.Cells.FromKey("operadorunidad").Value = e.Row.Cells.FromKey("operadorviaje").Value
            e.Row.Cells.FromKey("no_viaje").Title = "Editar Viaje"
        Else
            e.Row.Cells.FromKey("tiempos").Style.BackgroundImage = "../Imagenes/spacer.GIF"
            e.Row.Cells.FromKey("siguiente").Style.BackgroundImage = "../Imagenes/spacer.GIF"
            e.Row.Cells.FromKey("no_viaje").Style.Cursor = Infragistics.WebUI.[Shared].Cursors.NotSet
        End If

        'Calcula los Dias para Mantenimiento de la Unidad
        If Not e.Row.Cells.FromKey("fecha_servicio_sig").Value = Nothing Then
            'Obtiene la diferencia de dias contra la fecha actual
            intDiasMtto = DateDiff(DateInterval.Day, Date.Today, e.Row.Cells.FromKey("fecha_servicio_sig").Value)
            e.Row.Cells.FromKey("fecha_servicio_sig").Text = intDiasMtto
            If intDiasMtto <= 0 Then
                e.Row.Cells.FromKey("fecha_servicio_sig").Style.ForeColor = Drawing.Color.Red
            End If
        End If

        'Calcula los Dias para Vacaciones del Operador
        If Not e.Row.Cells.FromKey("fecha_sale_vac").Value = Nothing Then
            'Obtiene la diferencia de dias contra la fecha actual
            intDiasVac = DateDiff(DateInterval.Day, Date.Today, e.Row.Cells.FromKey("fecha_sale_vac").Value)
            e.Row.Cells.FromKey("fecha_sale_vac").Text = intDiasVac
            If intDiasVac <= 0 Then
                e.Row.Cells.FromKey("fecha_sale_vac").Style.ForeColor = Drawing.Color.Red
            End If
        End If

        'Calcula los Dias para el Vencimiento de la Licencia
        If Not e.Row.Cells.FromKey("fecha_venclicencia").Value = Nothing Then
            'Obtiene la diferencia de dias contra la fecha actual
            intDiaVenceLic = DateDiff(DateInterval.Day, Date.Today, e.Row.Cells.FromKey("fecha_venclicencia").Value)
            e.Row.Cells.FromKey("fecha_venclicencia").Text = intDiaVenceLic
            If intDiaVenceLic <= 0 Then
                e.Row.Cells.FromKey("fecha_venclicencia").Style.ForeColor = Drawing.Color.Red
            End If
        End If

        'Obtiene la informaci�n del Operador 2, si existe, entonces muestra un 
        'icono a un lado del Nombre del Operador para se�alar que el Viaje 
        'cuenta con 2 operadores
        If Not e.Row.Cells.FromKey("operadorunidad2").Value = Nothing Then
            e.Row.Cells.FromKey("mostrar_operador2").Style.BackgroundImage = "../Imagenes/ico11.gif"
        End If

        ''Muestra la informaci�n de las Fechas e indica si hay atraso
        If Not e.Row.Cells.FromKey("f_prog_ini_viaje").Value = Nothing Then
            'Compara la Fecha Inicio Programada con la Fecha Actual
            intFIniProg = DateDiff(DateInterval.Day, Date.Today, e.Row.Cells.FromKey("f_prog_ini_viaje").Value)
            If intFIniProg <= 0 Then
                'Muestra la Fecha Ini. Prog.
                e.Row.Cells.FromKey("f_prog_ini_viaje").Style.ForeColor = Drawing.Color.Red
            End If
        End If

        'Compara la Fecha Ini.Real 
        If Not e.Row.Cells.FromKey("fecha_real_viaje").Value = Nothing Then
            'Compara la Fecha Ini. Real contra la Fecha Ini. Prog
            If Not e.Row.Cells.FromKey("f_prog_ini_viaje").Value = Nothing Then
                intFIniReal1 = DateDiff(DateInterval.Day, e.Row.Cells.FromKey("f_prog_ini_viaje").Value, e.Row.Cells.FromKey("fecha_real_viaje").Value)
                If intFIniReal1 <= 0 Then
                    e.Row.Cells.FromKey("fecha_real_viaje").Style.ForeColor = Drawing.Color.Red
                End If
            End If

            'Compara la Fecha Ini Real contra la Fecha Actual
            intFIniReal1 = DateDiff(DateInterval.Day, Date.Today, e.Row.Cells.FromKey("fecha_real_viaje").Value)
            If intFIniReal1 <= 0 Then
                e.Row.Cells.FromKey("fecha_real_viaje").Style.ForeColor = Drawing.Color.Red
            End If
        End If

        'Compara la Fecha Fin Prog. contra la Fecha Actual
        If Not e.Row.Cells.FromKey("f_prog_fin_viaje").Value = Nothing Then
            intFFinProg = DateDiff(DateInterval.Day, Date.Today, e.Row.Cells.FromKey("f_prog_fin_viaje").Value)
            If intFFinProg <= 0 Then
                e.Row.Cells.FromKey("f_prog_fin_viaje").Style.ForeColor = Drawing.Color.Red
            End If
        End If

        ''Asigna el color al estatus del viaje
        ''Detecta el caso de que el estatus este atrasado o en aviso
        If e.Row.Cells.FromKey("id_statusviaje").Value <> Nothing Then
            'e.Row.Cells.FromKey("nombre_status_viaje").Style.Cursor = Infragistics.WebUI.[Shared].Cursors.Hand
            'e.Row.Cells.FromKey("nombre_status_viaje").Title = "Mostrar Bit�cora de Status"
            If e.Row.Cells.FromKey("duracion_aviso").Value <> Nothing Then
                Dim intMinutosAviso As Integer = iBR.ConvertirAMinutos(e.Row.Cells.FromKey("duracion_aviso").Value)
                Dim intMinutosMaxima As Integer = iBR.ConvertirAMinutos(e.Row.Cells.FromKey("duracion_maxima").Value)
                If DateDiff(DateInterval.Minute, e.Row.Cells.FromKey("f_ini_status").Value, Date.Now) >= intMinutosMaxima Then
                    e.Row.Cells.FromKey("nombre_status_viaje").Style.ForeColor = Drawing.Color.FromArgb(CInt("&H" & e.Row.Cells.FromKey("color_maxima").Value))
                    e.Row.Cells.FromKey("nombre_status_viaje").Style.Font.Bold = True
                ElseIf DateDiff(DateInterval.Minute, e.Row.Cells.FromKey("f_ini_status").Value, Date.Now) >= intMinutosAviso Then
                    e.Row.Cells.FromKey("nombre_status_viaje").Style.ForeColor = Drawing.Color.FromArgb(CInt("&H" & e.Row.Cells.FromKey("color_aviso").Value))
                    e.Row.Cells.FromKey("nombre_status_viaje").Style.Font.Bold = True
                Else
                    e.Row.Cells.FromKey("nombre_status_viaje").Style.ForeColor = Drawing.Color.FromArgb(CInt("&H" & e.Row.Cells.FromKey("color").Value))
                    e.Row.Cells.FromKey("nombre_status_viaje").Style.Font.Bold = False
                End If
                e.Row.Cells.FromKey("nombre_status_viaje").Style.Font.Italic = False
            Else
                e.Row.Cells.FromKey("nombre_status_viaje").Style.ForeColor = Drawing.Color.Red
                e.Row.Cells.FromKey("nombre_status_viaje").Style.Font.Italic = True
                e.Row.Cells.FromKey("nombre_status_viaje").Style.Font.Bold = True
            End If
        Else
            e.Row.Cells.FromKey("nombre_status_viaje").Value = e.Row.Cells.FromKey("desp_status_nombre").Value
            e.Row.Cells.FromKey("nombre_status_viaje").Style.ForeColor = Drawing.Color.Red
            e.Row.Cells.FromKey("nombre_status_viaje").Style.Font.Italic = True
            e.Row.Cells.FromKey("nombre_status_viaje").Style.Font.Bold = False
            e.Row.Cells.FromKey("f_ini_status").Value = e.Row.Cells.FromKey("f_status_unidad").Value
        End If

        'Muestra la informaci�n del la Gu�a 
        With e.Row.Cells
            If Not .FromKey("num_guia").Value = Nothing Then
                If .FromKey("CantGuias").Value = 1 Then
                    .FromKey("num_guia").Title = "Mostrar Gu�a del Viaje"
                ElseIf .FromKey("CantGuias").Value > 1 Then
                    .FromKey("num_guia").Title = "Mostrar Gu�as del Viaje"
                    .FromKey("mostrar_guias").Style.BackgroundImage = "../Imagenes/variasguias.gif"
                End If
                .FromKey("num_guia").Style.Cursor = Infragistics.WebUI.[Shared].Cursors.Hand
            End If

            'Muestra la informaci�n del Pedido
            If Not .FromKey("id_pedido").Value = Nothing Then
                If .FromKey("CantPedidos").Value = 1 Then
                    .FromKey("id_pedido").Title = "Mostrar Pedido del Viaje"

                ElseIf .FromKey("CantPedidos").Value > 1 Then
                    .FromKey("id_pedido").Title = "Mostrar Pedidos del Viaje"
                    .FromKey("mostrar_pedidos").Style.BackgroundImage = "../Imagenes/variospedidos.gif"
                End If
                .FromKey("id_pedido").Style.Cursor = Infragistics.WebUI.[Shared].Cursors.Hand
            End If
        End With

        'Inicializa el tipo de Servicio del pedido
        Select Case e.Row.Cells.FromKey("tipo_serv").Value
            Case "E"
                e.Row.Cells.FromKey("tipo_serv").Text = "Expo."
            Case "I"
                e.Row.Cells.FromKey("tipo_serv").Text = "Impo."
            Case "D"
                e.Row.Cells.FromKey("tipo_serv").Text = "Dom."
            Case Else
                e.Row.Cells.FromKey("tipo_serv").Text = ""
        End Select
    End Sub

    'Valida la informaci�n del Operador 1
    Private Sub cusvOperador1_ServerValidate(ByVal source As System.Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cusvOperador1.ServerValidate
        Try
            If Session("UserName") Is Nothing Or Session("vg_area") Is Nothing Then
                Response.Redirect(Application("myRoot") & "/relogin.aspx")
            End If
            Dim lds As New Data.DataSet
            If Trim(Me.txtOperador1.Text) = "" Then Me.txtOperador1.Text = "0"
            iBR.AsignacionClass.Operador1 = Me.txtOperador1.Text
            'Se llama a la funci�n que valida los datos del Operador, se envian como 
            'argumentos un identificador para validar el operador 1 o el operador 2
            'y como referencia el campo del Nombre del operador 
            iBR.ValidaOperador(1, Me.txtOperador1Nombre.Text)
            iBROperador.GetDatosOperador(Trim(Me.txtOperador1.Text), lds, iBROperador.str_log)
            args.IsValid = True
        Catch ex As Exception
            Me.txtOperador1Nombre.Text = ""
            lblError.Text = ex.Message
            Me.cusvOperador1.ErrorMessage = ex.Message
            args.IsValid = False
        End Try
    End Sub

    'Valida la informaci�n del Operador 2
    Private Sub cusvOperador2_ServerValidate(ByVal source As System.Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cusvOperador2.ServerValidate
        Try
            If Session("UserName") Is Nothing Or Session("vg_area") Is Nothing Then
                Response.Redirect(Application("myRoot") & "/relogin.aspx")
            End If
            Dim lds As New Data.DataSet
            'No se requiere capturar el Operador 2
            If Trim(Me.txtOperador2Nombre.Text) = "" Then Me.txtOperador2Nombre.Text = "0"
            iBR.AsignacionClass.Operador2 = Me.txtOperador2.Text
            'Se llama a la funci�n que valida la informaci�n del Operador 2, se envian 
            'como argumentos el operador a validar 1 o 2 y el campo del Nombre del Operador
            'como referencia para regresar la informaci�n.
            iBR.ValidaOperador(2, Me.txtOperador2Nombre.Text)
            iBROperador.GetDatosOperador(Trim(Me.txtOperador2.Text), lds, iBROperador.str_log)
            args.IsValid = True
        Catch ex As Exception
            Me.txtOperador2Nombre.Text = ""
            lblError.Text = ex.Message
            Me.cusvOperador2.ErrorMessage = ex.Message
            args.IsValid = False
        End Try
    End Sub

    Private Sub txtRemolque2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRemolque2.TextChanged

    End Sub

    'Valida el Tipo de Seguimiento seleccionado, emartinez 04-Jul-2007
    Private Sub cusvSeguimiento_ServerValidate(ByVal source As System.Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cusvSeguimiento.ServerValidate
        Try
            If Session("UserName") Is Nothing Or Session("vg_area") Is Nothing Then
                Response.Redirect(Application("myRoot") & "/relogin.aspx")
            End If
            'Se valida el Tipo de Seguimiento, se llama a la funci�n ValidaSeguimiento
            'se envian como argumentos el Nombre Corto del Seguimiento, la estructura
            'del LOG como referencia y el Campo de la Descripci�n del Seguimiento como 
            'referencia, emartinez 04-Jul-2007
            iBR.ValidaSeguimiento(Me.txtSeguimiento.Text, iBR.str_log, Me.txtSeguimientoDesc.Text)
            args.IsValid = True
        Catch ex As Exception
            Me.cusvSeguimiento.ErrorMessage = ex.Message
            args.IsValid = False
        End Try
    End Sub

    Protected Sub txtHrsEstandar_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ldtHoras As New DateTime
            ldtHoras = Me.wdcInicioViaje.Value
            Me.wdcFinViaje.Value = ldtHoras.AddHours(Me.txtHrsEstandar.Text)
            ScriptManager.GetCurrent(Page).SetFocus(Me.txtKit)
        Catch ex As Exception
            Me.lblErrorUP1.Visible = True
            Me.lblErrorUP1.Text = ex.Message
        End Try
    End Sub

    Protected Sub txtUnidad_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUnidad.TextChanged
        Try
            If Session("UserName") Is Nothing Or Session("vg_area") Is Nothing Then
                Response.Redirect(Application("myRoot") & "/relogin.aspx")
            End If
            If Trim(Me.txtUnidad.Text) = "" Then Me.txtUnidad.Text = "0"
            iBR.AsignacionClass.Tractor = Me.txtUnidad.Text
            iBR.ValidaTractor()
            If Me.txtUnidad.Text <> "0" Then
                Dim luoParm As New net_b_zamclasesextended.net_b_clasegeneralparametrosextended
                Dim lstrMsgUnidad As String = ""
                Dim lintValorParam As Integer = 0
                Dim luoUnidad As New net_b_zamclasesextended.net_b_claseunidadextended
                luoUnidad.ValidaOrdenServicioUnidad(Me.txtUnidad.Text, iBR.str_log, lstrMsgUnidad, False)
                'Se llama a la funci�n GetParametro, se envian como argumentos el nombre del 
                'par�metro, la estructura del LOG como referencia y una variable que indica 
                'si se valida o no que existe el par�metro en la base de datos. emartinez 02-ENE-2009
                lintValorParam = luoParm.GetParametroNumerico("despvalidaordenserviciounidad", iBR.str_log, False)
                Select Case lintValorParam
                    Case 0  'No valida
                    Case 1  'Valida y muestra mensaje, permite continuar
                        If lstrMsgUnidad.Length > 0 Then
                            Me.lblError.Text = "ADVERTENCIA : " & lstrMsgUnidad
                            Me.lblError.Visible = True
                        End If
                    Case 2  'Valida y NO permite continuar
                        If lstrMsgUnidad.Length > 0 Then
                            Throw New Exception(lstrMsgUnidad)
                        End If
                End Select
            End If
            'Valida y obtiene la informaci�n del Operador
            Me.txtOperador1.Text = iBR.AsignacionClass.Operador1
            'Se llama a la funci�n que valida los datos del Operador, se envian como 
            'argumentos un identificador para validar el operador 1 o el operador 2
            'y como referencia el campo del Nombre del operador 
            iBR.ValidaOperador(1, Me.txtOperador1Nombre.Text)
            ScriptManager.GetCurrent(Page).SetFocus(Me.txtLineaRem1)
        Catch ex As Exception
            Me.lblError.Visible = True
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub txtKit_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtKit.TextChanged
        Try
            If Session("UserName") Is Nothing Or Session("vg_area") Is Nothing Then
                Response.Redirect(Application("myRoot") & "/relogin.aspx")
            End If
            Dim strKit(4) As String
            strKit(0) = Trim(Me.txtKit.Text)
            iBR.GetUnidadesKit(strKit, iBR.str_log)
            Me.txtRemolque1.Text = strKit(2)
            Me.txtDolly.Text = strKit(3)
            Me.txtRemolque2.Text = strKit(4)

        Catch ex As Exception
            Me.txtRemolque1.Text = ""
            Me.txtDolly.Text = ""
            Me.txtRemolque2.Text = ""
            Me.lblError.Visible = True
            Me.lblError.Text = ex.Message
        End Try
    End Sub

    'Se valida y obtiene la informaci�n de la Ruta, emartinez 15-JULIO-2009
    Protected Sub txtRuta_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If Session("UserName") Is Nothing Or Session("vg_area") Is Nothing Then
                Response.Redirect(Application("myRoot") & "/relogin.aspx")
            End If
            If Trim(Me.txtRuta.Text) = "" Then Me.txtRuta.Text = "0"
            ''iBR.ValidaRutaAsignacion(Me.txtRutaDesc.Text)
            iBR.ValidaOrigenRutaAsignacion(Me.txtRuta.Text, Me.txtUnidad.Text, Me.wdcInicioViaje.Value, iBR.AsignacionClass.Asignacion, Me.txtRespValOrigCont.Text, Me.txtRutaDesc.Text, iBR.str_log)
            iBR.AsignacionClass.Ruta = Me.txtRuta.Text
            ScriptManager.GetCurrent(Page).SetFocus(Me.wdcInicioViaje)
        Catch ex As Exception
            Me.txtRutaDesc.Text = ""
            Me.lblErrorUP1.Visible = True
            Me.lblErrorUP1.Text = ex.Message
        End Try
    End Sub


#Region "Valida Campos"

    'Valida Kit
    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
        Public Function ValidaKit(ByVal strKit As String) As ArrayList
        Try
            If Session("UserName") Is Nothing Or Session("vg_area") Is Nothing Then
                Throw New Exception("Su Sesi�n a terminado, favor de iniciar nuevamente en el modulo.")
            End If
            Dim arraylist As New ArrayList
            iBR.str_log = Session("strConn")
            Dim strArrKit(4) As String
            Dim strUnidad, strRem1, strRem2, strDolly, strarea As String
            strUnidad = ""
            strRem1 = ""
            strRem2 = ""
            strDolly = ""
            strarea = ""
            'strArrKit(0) = Trim(strKit)
            'iBR.GetUnidadesKit(strArrKit, iBR.str_log)

            'Consideramos el Area del Usuario para los Filtros MultiEmpresa 2018 oerc 12/10/18 Folio 25019, 25055
            Me.aplica_gposvision = luoParm.GetParametro("aplica_gposvision", iBR.str_log, False)
            If aplica_gposvision = "S" Then
                strarea = Session("vg_area")
            End If

            iBR.ValidaKit(Trim(strKit), strUnidad, strRem1, strDolly, strRem2, iBR.str_log, strarea)
            arraylist.Add(strRem1) ' txtRemolque1
            arraylist.Add(strDolly) 'txtDolly
            arraylist.Add(strRem2) 'txtRemolque2
            arraylist.Add(strUnidad) 'txtUnidad


            Return arraylist

        Catch ex As Exception
            Dim arraylist As New ArrayList
            arraylist.Add("Error")
            arraylist.Add(ex.Message)
            Return arraylist
            Throw ex
        End Try
    End Function

    'Valida Ruta
    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
        Public Function ValidaRuta(ByVal intRuta As Integer, ByVal strUnidad As String, ByVal dteFecha As String) As ArrayList
        Try
            If Session("UserName") Is Nothing Or Session("vg_area") Is Nothing Then
                Throw New Exception("Su Sesi�n a terminado, favor de iniciar nuevamente en el modulo.")
            End If
            Dim arraylist As New ArrayList
            iBR.str_log = Session("strConn")

            If Trim(intRuta) = "" Then intRuta = 0
            Dim strRuta As String = ""
            iBR.ValidaOrigenRutaAsignacion(intRuta, strUnidad, CDate(dteFecha), iBR.AsignacionClass.Asignacion, "0", strRuta, iBR.str_log)
            arraylist.Add(strRuta)
            iBR.AsignacionClass.Ruta = intRuta

            'Folio:20958, Par�metro para validaci�n de los Kil�metros Actuales de la Unidad + Kil�metros Ruta para comparar 
            'contra los Kil�metros del Siguiente Preventivo de la Unidad.
            Me.ParmValidaKmsUnidadSigPrev = UCase(luoParm.GetParametro("despvalidakmssigprevunidad", iBR.str_log, False))
            If Me.ParmValidaKmsUnidadSigPrev = "S" Then
                Me.MensajeAvisoKmsUnidad = ""
                'Se llama a la funci�n ValidaKmsSiguientePreventivoUnidad, se envian como argumentos la Unidad, la Ruta
                'y la estructura del LOG como referencia.
                If strUnidad <> "" And intRuta <> 0 Then Me.MensajeAvisoKmsUnidad = iBR.ValidaKmsSiguientePreventivoUnidad(strUnidad, intRuta, iBR.str_log)
                If Me.MensajeAvisoKmsUnidad.Length > 0 Then
                    arraylist.Add("Aviso")
                    arraylist.Add(Me.MensajeAvisoKmsUnidad)
                End If
            End If

            Return arraylist

        Catch ex As Exception
            Dim arraylist As New ArrayList
            arraylist.Add("Error")
            arraylist.Add(ex.Message)
            Return arraylist
            Throw ex
        End Try
    End Function

    'Valida Operador
    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
       Public Function ValidaOperador(ByVal intOperador As Integer, ByVal intNoOperador As Integer) As ArrayList

        Try
            If Session("UserName") Is Nothing Or Session("vg_area") Is Nothing Then
                Throw New Exception("Su Sesi�n a terminado, favor de iniciar nuevamente en el modulo.")
            End If
            Dim arraylist As New ArrayList
            iBR.str_log = Session("strConn")
            iBROperador.str_log = Session("strConn")
            Dim strOperador As String = ""
            Dim lds As New Data.DataSet
            If Trim(intOperador) = "" Then intOperador = 0
            If intNoOperador = 1 Then
                iBR.AsignacionClass.Operador1 = intOperador
                iBR.ValidaOperador(1, strOperador)
                arraylist.Add(1) '0
            ElseIf intNoOperador = 2 Then
                iBR.AsignacionClass.Operador2 = intOperador
                iBR.ValidaOperador(2, strOperador)
                arraylist.Add(2) '0
            End If

            'GGUERRA FOLIO:21597
            Dim strParmFlota As String
            strParmFlota = luoParm.GetParametro("filtrapersonal_x_areaflota", iBR.str_log, False).ToUpper()
            If strParmFlota = "S" Then
                iBROperador.FiltroAreaFlota = True
                luoFlotas.GetFlota(strIdFlota, iBROperador.str_log)
                iBROperador.AreaFlota = luoFlotas.Id_Area_Flota
            End If

            'Funcionalidad MultiEmpresa 2018 oerc 12/10/18 Folio 25019, 25055
            aplica_gposvision = luoParm.GetParametro("aplica_gposvision", iBR.str_log, False).ToUpper()
            If aplica_gposvision = "S" Then
                iBROperador.FiltroArea = True
                iBROperador.Area = Session("vg_area")
            End If


            iBROperador.GetDatosOperador(Trim(intOperador), lds, iBROperador.str_log)

            arraylist.Add(strOperador) '1

            Return arraylist

        Catch ex As Exception
            Dim arraylist As New ArrayList
            arraylist.Add("Error")
            arraylist.Add(ex.Message)
            arraylist.Add(intNoOperador)
            Return arraylist
            Throw ex
        End Try
    End Function

    'Valida Seguimiento
    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
        Public Function ValidaSeguimiento(ByVal strSeguimiento As String) As ArrayList
        Try
            If Session("UserName") Is Nothing Or Session("vg_area") Is Nothing Then
                Throw New Exception("Su Sesi�n a terminado, favor de iniciar nuevamente en el modulo.")
            End If
            Dim arraylist As New ArrayList
            iBR.str_log = Session("strConn")
            Dim strSegDesc As String = ""

            iBR.ValidaSeguimiento(strSeguimiento, iBR.str_log, strSegDesc)

            arraylist.Add(strSegDesc)

            Return arraylist

        Catch ex As Exception
            Dim arraylist As New ArrayList
            arraylist.Add("Error")
            arraylist.Add(ex.Message)
            Return arraylist
            Throw ex
        End Try
    End Function

    'Valida Hora
    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
        Public Function ValidaHora(ByVal strHoraEst As String, ByVal strHoraInicio As String) As ArrayList
        Try
            If Session("UserName") Is Nothing Or Session("vg_area") Is Nothing Then
                Throw New Exception("Su Sesi�n a terminado, favor de iniciar nuevamente en el modulo.")
            End If
            Dim arraylist As New ArrayList
            iBR.str_log = Session("strConn")

            Dim ldtHoras As New DateTime
            If IsDate(strHoraInicio) = False Then Throw New Exception("Favor de capturar una fecha valida en el campo de fecha inicio viaje.")
            ldtHoras = CDate(strHoraInicio)
            arraylist.Add(ldtHoras.AddHours(strHoraEst)) 'Me.wdcFinViaje.Value

            Return arraylist

        Catch ex As Exception
            Dim arraylist As New ArrayList
            arraylist.Add("Error")
            arraylist.Add(ex.Message)
            Return arraylist
            Throw ex
        End Try
    End Function

    'Valida Unidad
    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
       Public Function ValidaUnidad(ByVal intRuta As Integer, ByVal strUnidad As String, ByVal aintIdAsignacion As Integer, ByVal astrListaPedidos As String, ByVal astrListaPedidosAreas As String) As ArrayList
        Try


            Dim lstrHabilitarFuncionalidadesAEO As String = ""
            Dim arraylist As New ArrayList
            Dim luo_BR As New net_b_despextended.pre_b_despcrearviaje
            Dim luoParm As New net_b_zamclasesextended.net_b_clasegeneralparametrosextended
            Dim lstrMsgUnidad As String = ""
            Dim lintValorParam As Integer = 0
            Dim luoUnidad As New net_b_zamclasesextended.net_b_claseunidadextended
            Dim str_param_areaflota As String
            luo_BR.str_log = Session("strConn")
            luo_BR.ViajeClass.Tractor = strUnidad.ToUpper
            luo_BR.ValidaTractor()

            str_param_areaflota = luoParm.GetParametro("busq_remxarea", luo_BR.str_log, False)
            aplica_gposvision = luoParm.GetParametro("aplica_gposvision", luo_BR.str_log, False)

            'Validamos el Area de la Unidad MultiEmpresa 2018 oerc 11/10/18 Folio 25019
            If str_param_areaflota = "S" Or aplica_gposvision = "S" Then
                luo_BR.ValidaArea(strUnidad, "T", Session("vg_area"), luo_BR.str_log)
            End If

            luoUnidad.ValidaOrdenServicioUnidad(luo_BR.ViajeClass.Tractor, luo_BR.str_log, lstrMsgUnidad, True)
            lintValorParam = luoParm.GetParametroNumerico("despvalidaordenserviciounidad", luo_BR.str_log, False)
            Select Case lintValorParam
                Case 0  'No valida
                Case 1  'Valida y muestra mensaje, permite continuar
                    If lstrMsgUnidad.Length > 0 Then
                        Me.lblError.Text = "ADVERTENCIA : " & lstrMsgUnidad
                        Me.lblError.Visible = True
                    End If
                Case 2  'Valida y NO permite continuar
                    If lstrMsgUnidad.Length > 0 Then
                        Throw New Exception(lstrMsgUnidad)
                    End If
            End Select

            'Valida la informacion de la Asignacion y la Unidad.
            lstrMultiCia = luoParm.GetParametro("despmulticompania", luo_BR.str_log, False)
            If UCase(lstrMultiCia) = "S" Then
                If aintIdAsignacion = 0 Then
                    'Obtiene la validacion con la informacion de las Areas y los Pedidos.
                    Dim larrListaP As New ArrayList
                    Dim larrListaPA As New ArrayList
                    'Pasa la lista de pedidos a un arreglo
                    luo_BR.PasarAArreglo(astrListaPedidos, "[;]", larrListaP, luo_BR.str_log)
                    luo_BR.PasarAArreglo(astrListaPedidosAreas, "[;]", larrListaPA, luo_BR.str_log)
                    'Valida la informacion de los Pedidos y la Unidad, se envian como argumentos el Listado 
                    'de Pedidos, el Listado de Areas, la Unidad y la estructura del LOG como referencia.
                    iBR.ValidaCompaniaPedidosUnidad(larrListaP, larrListaPA, strUnidad, luo_BR.str_log)
                Else
                    'Obtiene la validacion directamente con la informacion de la Asignacion
                    luo_BR.ValidaCompaniaAsignacionUnidad(aintIdAsignacion, strUnidad, luo_BR.str_log)
                End If
            End If

            'jcrodr�guez folio 23451 18/09/2017 Valida que sea funcionalidades de AEO 

            lstrHabilitarFuncionalidadesAEO = luoParm.GetParametro("habilitarFuncionalidadesAEO", luo_BR.str_log, False)


            If lstrHabilitarFuncionalidadesAEO = "S" Then
                'Obtiene la informaci�n del Operador de la Unidad
                luoUnidad.GetUnidad("T", strUnidad, luo_BR.str_log)
                Dim strOperador As String = ""
                iBR.str_log = luo_BR.str_log
                iBR.AsignacionClass.Operador1 = luoUnidad.IdOperador
                iBR.ValidaOperador(1, strOperador)
                arraylist.Add("DatosOper") '0'
                arraylist.Add(luoUnidad.IdOperador) '1'
                arraylist.Add(strOperador) '2'
                arraylist.Add("") '3'
            Else

                'Obtiene la informacion del parametro de Validacion de Capacidad de Carga vs. Capacidad del Vehiculo
                'Folio : 16715, Proyecto : TMS DINET Fase 1, emartinez 11/Jun/2012
                luoParm = New net_b_zamclasesextended.net_b_clasegeneralparametrosextended
                Me.lstrValidaCapCarga = luoParm.GetParametro("despvalidacargavscapacidad", luo_BR.str_log, False)
                If Me.lstrValidaCapCarga = "S" Then
                    luoUnidad.GetUnidad("T", strUnidad, luo_BR.str_log)
                    'Obtiene la informacion del Tipo de Unidad, Descripcion del Tipo de Unidad del Tractor
                    arraylist.Add("InfoTipoUnidad") '0'
                    arraylist.Add(luoUnidad.TipoUnidad) '1'
                    arraylist.Add(luoUnidad.TipoUnidadDesc) '2'
                    arraylist.Add(luo_BR.Nom_Permisionario) '3'
                Else
                    arraylist.Add("") '0'
                    arraylist.Add("") '1'
                    arraylist.Add("") '2'
                    arraylist.Add(luo_BR.Nom_Permisionario) '3'
                End If

            End If



            'Folio:20958, Par�metro para validaci�n de los Kil�metros Actuales de la Unidad + Kil�metros Ruta para comparar 
            'contra los Kil�metros del Siguiente Preventivo de la Unidad.
            Me.ParmValidaKmsUnidadSigPrev = UCase(luoParm.GetParametro("despvalidakmssigprevunidad", luo_BR.str_log, False))
            If Me.ParmValidaKmsUnidadSigPrev = "S" Then
                Me.MensajeAvisoKmsUnidad = ""
                'Se llama a la funci�n ValidaKmsSiguientePreventivoUnidad, se envian como argumentos la Unidad, la Ruta
                'y la estructura del LOG como referencia.
                If strUnidad <> "" And intRuta <> 0 Then Me.MensajeAvisoKmsUnidad = iBR.ValidaKmsSiguientePreventivoUnidad(strUnidad, intRuta, luo_BR.str_log)
                If Me.MensajeAvisoKmsUnidad.Length > 0 Then
                    arraylist.Add("Aviso") '4'
                    arraylist.Add(Me.MensajeAvisoKmsUnidad) '5'
                End If
            End If

            'Regresa un Array con los resultados de la validaci�n
            Return arraylist

        Catch ex As Exception
            Dim arraylist As New ArrayList
            arraylist.Add("Error")
            arraylist.Add(ex.Message)
            Return arraylist
            Throw ex
        End Try
    End Function

    'Valida Remolques y Lineas
    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
    Public Function ValidaLineasyRemolques(ByVal strLinea As String, ByVal strRemolque As String, ByVal intRemNum As Integer, ByVal strArguments As String()) As ArrayList
        'remNum = indica el numero de remolque a validar.
        Try
            Dim adtRemolque As New Data.DataTable
            Dim arraylist As New ArrayList
            Dim luo_BR As New net_b_despextended.pre_b_despcrearviaje
            Dim luoUnidad As New net_b_zamclasesextended.net_b_claseunidadextended
            Dim str_param_areaflota As String
            luo_BR.str_log = Session("strConn")
            luo_BR.ViajeClass.str_log = luo_BR.str_log
            'Se agregan los parametros que se usaran en el array que regresa al javascript
            'Esto para que siempre coincidan con su respectivo valor
            arraylist.Add("") '0 intRemNum
            arraylist.Add("") '1 InfoRem
            arraylist.Add("") '2 TipoUnidad
            arraylist.Add("") '3 TipoUnidadDesc
            arraylist.Add("") '4 fianza

            'strArguments(0) = LineaRem1
            'strArguments(1) = Remolque1
            'strArguments(2) = Dolly
            'strArguments(3) = Remolque2

            str_param_areaflota = luoParm.GetParametro("busq_remxarea", luo_BR.str_log, False)

            'Validamos el Area de la Unidad MultiEmpresa 2018 oerc 12/10/18 Folio 25019, 25055
            Me.aplica_gposvision = luoParm.GetParametro("aplica_gposvision", luo_BR.str_log, False)

            If strLinea = "" Then
                If str_param_areaflota = "S" Or aplica_gposvision = "S" Then
                    luo_BR.ValidaArea(strRemolque, "R", Session("vg_area"), luo_BR.str_log)
                End If
            End If

            arraylist(0) = intRemNum '0
            If intRemNum = 1 Then
                luo_BR.ViajeClass.LineaRem1 = strLinea
                luo_BR.ViajeClass.Remolque1 = strRemolque

                'MSALVARADO 10/10/2017 FOLIO: 23507
                If luoParm.GetParametro("habilitarFuncionalidadesAEO", luo_BR.str_log, False) = "S" Then
                    'Se modifica la funci�n para solamente validar Remolques Propios, los Remolques Americanos no se validan
                    'si el Remolque Americano no existe, lo da de alta en el Cat�logo de Remolques Americanos
                    'Folio:21152, emartinez 16.Jul.2015
                    If strRemolque.Length > 0 And Trim(strLinea).Length = 0 Then
                        luo_BR.ValidaRemolque1(adtRemolque)
                    End If
                Else
                    luo_BR.ValidaRemolque1(adtRemolque)
                End If


                'Obtiene la informacion del parametro de Validacion de Capacidad de Carga vs. Capacidad del Vehiculo
                'Folio : 16715, Proyecto : TMS DINET Fase 1, emartinez 11/Jun/2012
                luoParm = New net_b_zamclasesextended.net_b_clasegeneralparametrosextended
                Me.lstrValidaCapCarga = luoParm.GetParametro("despvalidacargavscapacidad", luo_BR.str_log, False)
                If Me.lstrValidaCapCarga = "S" Then
                    luoUnidad.GetUnidad("R", strLinea, strRemolque, luo_BR.str_log)
                    'Obtiene la informacion del Tipo de Unidad, Descripcion del Tipo de Unidad del Tractor
                    arraylist(1) = "InfoRem1"               '1
                    arraylist(2) = luoUnidad.TipoUnidad     '2
                    arraylist(3) = luoUnidad.TipoUnidadDesc '3
                End If
            Else    'entra cuando va a validar el remolque 2 
                luo_BR.ViajeClass.LineaRem1 = strArguments(0)
                luo_BR.ViajeClass.Remolque1 = strArguments(1)
                luo_BR.ViajeClass.Dolly = strArguments(2)
                luo_BR.ViajeClass.Remolque2 = strArguments(3)
                luo_BR.ViajeClass.LineaRem2 = strLinea
                luo_BR.ViajeClass.Remolque2 = strRemolque
                luo_BR.ValidaRemolque2(adtRemolque)
                'Obtiene la informacion del parametro de Validacion de Capacidad de Carga vs. Capacidad del Vehiculo
                'Folio : 16715, Proyecto : TMS DINET Fase 1, emartinez 11/Jun/2012
                luoParm = New net_b_zamclasesextended.net_b_clasegeneralparametrosextended
                Me.lstrValidaCapCarga = luoParm.GetParametro("despvalidacargavscapacidad", luo_BR.str_log, False)
                If Me.lstrValidaCapCarga = "S" Then
                    luoUnidad.GetUnidad("R", strLinea, strRemolque, luo_BR.str_log)
                    'Obtiene la informacion del Tipo de Unidad, Descripcion del Tipo de Unidad del Tractor
                    arraylist(1) = "InfoRem2"                   '1
                    arraylist(2) = luoUnidad.TipoUnidad         '2
                    arraylist(3) = luoUnidad.TipoUnidadDesc     '3
                End If
            End If

            'Obtiene la fianza del remolque
            If Not adtRemolque Is Nothing Then
                If adtRemolque.Rows.Count > 0 Then
                    'la fianza solo aplica para remolques americanos
                    If strLinea.Length > 0 Then
                        arraylist(4) = adtRemolque.Rows(0).Item("num_fianzas")
                    End If
                End If
            End If

            Return arraylist
        Catch ex As Exception
            Dim arraylist As New ArrayList
            arraylist.Add("Error")
            arraylist.Add(ex.Message)
            arraylist.Add(intRemNum)
            Return arraylist
            Throw ex
        End Try
    End Function

    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
    Public Function GetControlRemolque(ByVal strLinea As String, ByVal strRemolque As String, ByVal strAsignacion As String, ByVal astrPedido As String) As ArrayList
        Try
            Dim arraylist As New ArrayList
            Dim luoUnidad As New net_b_zamclasesextended.net_b_claseunidadextended
            luoUnidad.str_log = Session("strConn")
            Dim intControl As Integer = 0
            Dim strTipoServ As String = ""
            Dim strStatusFianza As String = ""
            intControl = luoUnidad.GetRemolqueControlActivo(strLinea, strRemolque, luoUnidad.str_log)

            strStatusFianza = luoUnidad.GetStatusFianzaRemolqueControl(intControl, luoUnidad.str_log)

            Dim ldtPedidos As New Data.DataTable
            'Obtiene el tipo de servicio del pedido de la asignacion
            ldtPedidos = iuoAsignacion.GetInfoPedidoAsignacion(strAsignacion, luoUnidad.str_log)

            'Si no esta activo el remolque, si es impo no continua

            If Not ldtPedidos Is Nothing Then
                If ldtPedidos.Rows.Count > 0 Then
                    strTipoServ = UCase(ldtPedidos.Rows(0).Item("tipo_serv"))
                    If intControl = 0 Then
                        '   Se valida que el tipo de servicio tambien sea "E", Jdlcruz, GASA 29-Nov-2012, Folio: 17748.
                        If UCase(ldtPedidos.Rows(0).Item("tipo_serv")) = "I" Or UCase(ldtPedidos.Rows(0).Item("tipo_serv")) = "E" Then
                            Throw New Exception("El Remolque " & strRemolque & " no se encuentra activo.")
                        End If
                    End If
                Else
                    'Obtiene el pedido que se esta asignando
                    If IsNumeric(astrPedido) Then
                        Dim ipedido As New net_b_zamclases.net_b_clasepedido
                        ipedido.GetDatosPedido(Session("vg_area"), CInt(astrPedido), luoUnidad.str_log)
                        strTipoServ = UCase(ipedido.tipo_serv)
                        If intControl = 0 Then
                            '   Se valida que el tipo de servicio tambien sea "E", Jdlcruz, GASA 29-Nov-2012, Folio: 17748.
                            If UCase(ipedido.tipo_serv) = "I" Or UCase(ipedido.tipo_serv) = "E" Then
                                Throw New Exception("El Remolque " & strRemolque & " no se encuentra activo.")
                            End If
                        End If
                    End If
                End If
            End If

            arraylist.Add(intControl)
            arraylist.Add(strTipoServ)

            arraylist.Add(strStatusFianza)

            Return arraylist

        Catch ex As Exception
            Dim arraylist As New ArrayList
            arraylist.Add("Error")
            arraylist.Add(ex.Message)
            Return arraylist
            Throw ex
        End Try
    End Function
    'Funcion para obtener la nacionalidad del Remolque cuando es propio y no cuenta con id_linea.
    'Jdlcruz, GASA 29-Nov-2012, Folio: 17748
    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
    Public Function GetNacionalidadRemolque(ByVal strRemolque As String) As ArrayList
        Try
            Dim arraylist As New ArrayList
            Dim luoUnidad As New net_b_zamclasesextended.net_b_claseunidadextended
            luoUnidad.str_log = Session("strConn")
            Dim intNacionalidad As Integer = 0
            Dim strTipoServ As String = ""
            intNacionalidad = luoUnidad.GetRemolqueNacionalidad(strRemolque, luoUnidad.str_log)

            arraylist.Add(intNacionalidad)

            Return arraylist

        Catch ex As Exception
            Dim arraylist As New ArrayList
            arraylist.Add("Error")
            arraylist.Add(ex.Message)
            Return arraylist
            Throw ex
        End Try
    End Function

    'rherrera GAAL
    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
    Public Function getPuntosInteres(ByVal idRuta As String) As ArrayList
        Try
            Dim arraylist As New ArrayList
            Dim strCell As String
            netVi.str_log = New net_objestandar.str_log(ConfigurationManager.AppSettings("strConn"))
            Dim dtGeoref As New DataTable
            dtGeoref = Me.netVi.GetGeoref(CInt(idRuta), netVi.str_log)
            For Each registro As Data.DataRow In dtGeoref.Rows
                If registro("tipo_punto") = "M" Then
                    strCell = registro("posx") & "," & registro("posy") & ""
                    arraylist.Add(strCell)
                End If
            Next
            Return arraylist
        Catch ex As Exception
            Dim arraylist As New ArrayList
            arraylist.Add("Error")
            arraylist.Add(ex.Message)
            Return arraylist
            Throw ex
        End Try

    End Function


    'Valida Dolly
    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
       Public Function ValidaDolly(ByVal strDolly As String) As ArrayList
        Try
            Dim arraylist As New ArrayList
            Dim luo_BR As New net_b_despextended.pre_b_despcrearviaje
            Dim luoUnidad As New net_b_zamclasesextended.net_b_claseunidadextended
            Dim str_param_areaflota As String
            luo_BR.str_log = Session("strConn")
            luo_BR.ViajeClass.Dolly = strDolly
            luo_BR.ValidaDolly()

            str_param_areaflota = luoParm.GetParametro("busq_remxarea", luo_BR.str_log, False)

            'Validamos el Area de la Unidad MultiEmpresa 2018 oerc 12/10/18 Folio 25019, 25055
            Me.aplica_gposvision = luoParm.GetParametro("aplica_gposvision", luo_BR.str_log, False)

            If str_param_areaflota = "S" Or aplica_gposvision = "S" Then
                luo_BR.ValidaArea(strDolly, "D", Session("vg_area"), luo_BR.str_log)
            End If

            'Obtiene la informacion del parametro de Validacion de Capacidad de Carga vs. Capacidad del Vehiculo
            'Folio : 16715, Proyecto : TMS DINET Fase 1, emartinez 11/Jun/2012
            luoParm = New net_b_zamclasesextended.net_b_clasegeneralparametrosextended
            Me.lstrValidaCapCarga = luoParm.GetParametro("despvalidacargavscapacidad", luo_BR.str_log, False)
            If Me.lstrValidaCapCarga = "S" Then
                luoUnidad.GetUnidad("D", strDolly, luo_BR.str_log)
                'Obtiene la informacion del Tipo de Unidad, Descripcion del Tipo de Unidad del Tractor
                arraylist.Add("InfoTipoUnidad")
                arraylist.Add(luoUnidad.TipoUnidad)
                arraylist.Add(luoUnidad.TipoUnidadDesc)
            End If
            Return arraylist
        Catch ex As Exception
            Dim arraylist As New ArrayList
            arraylist.Add("Error")
            arraylist.Add(ex.Message)
            Return arraylist
            Throw ex
        End Try
    End Function

    'ValidaFechas
    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
        Public Function ValidaFechas(ByVal strHoraIni As String, ByVal strHoraFin As String) As ArrayList
        Try
            If Session("UserName") Is Nothing Or Session("vg_area") Is Nothing Then
                Throw New Exception("Su Sesi�n a terminado, favor de iniciar nuevamente en el modulo.")
            End If
            Dim arraylist As New ArrayList
            iBR.str_log = Session("strConn")

            Dim ldecHoras As Decimal = 0
            Dim ldecMin As Decimal = 0
            If IsDate(strHoraIni) = False Then Throw New Exception("Favor de capturar una fecha valida en el campo de fecha inicio viaje.")
            If IsDate(strHoraFin) = False Then Throw New Exception("Favor de capturar una fecha valida en el campo de fecha fin viaje.")
            ldecHoras = DateDiff(DateInterval.Hour, CDate(strHoraIni), CDate(strHoraFin))
            ldecMin = DateDiff(DateInterval.Minute, CDate(strHoraIni), CDate(strHoraFin))
            ldecMin = ldecMin - (ldecHoras * 60)
            arraylist.Add(ldecHoras + ldecMin / 100)

            Return arraylist

        Catch ex As Exception
            Dim arraylist As New ArrayList
            arraylist.Add("Error")
            arraylist.Add(ex.Message)
            Return arraylist
            Throw ex
        End Try
    End Function

    'Obtiene la Capacidad de Carga del Armado de Vehiculos
    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
    Public Function ObtieneCapacidadCarga(ByVal intTipoUTracto As String, ByVal intTipoUR1 As String, ByVal intTipoUR2 As String, ByVal intTipoUDolly As String, ByVal PesoACargar As String, ByVal VolACargar As String, ByVal Array() As String) As ArrayList
        Try
            Dim arraylist As New ArrayList
            Dim strDescripcion As String = "Configuraci�n MTC: "
            Dim strUnidad As String = ""
            Dim strIdTipoUnidad As String = "0"
            Dim strRem1 As String = ""
            Dim strIdTipoRem1 As String = "0"
            Dim strRem2 As String = ""
            Dim strIdTipoRem2 As String = "0"
            Dim strDolly As String = ""
            Dim strIdTipoDolly As String = "0"
            Dim luoTracto As New net_b_zamclasesextended.net_b_claseunidadextended
            Dim luoRem1 As New net_b_zamclasesextended.net_b_claseunidadextended
            Dim luoRem2 As New net_b_zamclasesextended.net_b_claseunidadextended
            Dim luoDolly As New net_b_zamclasesextended.net_b_claseunidadextended
            iBR.str_log = Session("strConn")
            Try
                'Se llama a la funcion que Obtiene la Capacidad de Carga del Armado, se envian como argumentos
                'el Tipo de Unidad del Tractor, el Tipo de Unidad del Remolque1, el Tipo de Unidad del Dolly, 
                'el Tipo de Unidad del Remolque2, 
                Dim decTotalM3 As Decimal = 0
                If Array(0) <> "" And Array(0) <> "0" Then
                    'Obtiene el nombre del Tipo de Unidad
                    luoTracto.GetUnidad("T", Array(0), iBR.str_log)
                    'Obtiene la capacidad de Volumen del tipo de unidad
                    iBR.GetCapacidadUnidadVolumen(luoTracto.TipoUnidad, iBR.str_log, decTotalM3)
                    strUnidad = luoTracto.TipoUnidadDesc
                    strIdTipoUnidad = CStr(luoTracto.TipoUnidad)
                End If
                If Array(1) <> "" And Array(1) <> "0" Then
                    'Obtiene el nombre del Tipo de Unidad
                    luoRem1.GetUnidad("R", Array(1), iBR.str_log)
                    'Obtiene la capacidad de Volumen del tipo de unidad
                    iBR.GetCapacidadUnidadVolumen(luoRem1.TipoUnidad, iBR.str_log, decTotalM3)
                    strRem1 = luoRem1.TipoUnidadDesc
                    strIdTipoRem1 = CStr(luoRem1.TipoUnidad)
                End If
                If Array(2) <> "" And Array(2) <> "0" Then
                    'Obtiene el nombre del Tipo de Unidad
                    luoDolly.GetUnidad("D", Array(2), iBR.str_log)
                    'Obtiene la capacidad de Volumen del tipo de unidad
                    iBR.GetCapacidadUnidadVolumen(luoDolly.TipoUnidad, iBR.str_log, decTotalM3)
                    strDolly = luoDolly.TipoUnidadDesc
                    strIdTipoDolly = CStr(luoDolly.TipoUnidad)
                End If
                If Array(3) <> "" And Array(3) <> "0" Then
                    'Obtiene el nombre del Tipo de Unidad
                    luoRem2.GetUnidad("R", Array(3), iBR.str_log)
                    'Obtiene la capacidad de Volumen del tipo de unidad
                    iBR.GetCapacidadUnidadVolumen(luoRem2.TipoUnidad, iBR.str_log, decTotalM3)
                    strRem2 = luoRem2.TipoUnidadDesc
                    strIdTipoRem2 = CStr(luoRem2.TipoUnidad)
                End If

                Dim strDescMTC As String = ""
                iBR.GetCapacidadCargaArmado(strIdTipoUnidad, strIdTipoRem1, strIdTipoRem2, strIdTipoDolly, Me.CapacidadKG, Me.CapacidadM3, strDescMTC, iBR.str_log)
                If strDescMTC <> "" Then
                    strDescripcion = strDescripcion & strDescMTC
                Else
                    strDescripcion = "Configuraci�n MTC: No Existe"
                End If

                Me.CapacidadM3 = decTotalM3
                Me.KGCargar = CDec(PesoACargar)
                Me.M3Cargar = CDec(VolACargar)

                'Hace la validacion de la Capacidad de Carga vs. Capacidad de Vehiculo
                If (Me.KGCargar > Me.CapacidadKG) Or (Me.M3Cargar > Me.CapacidadM3) Then
                    arraylist.Add("Error")                              '0
                    'Throw New Exception("La Capacidad de Carga es menor a la Capacidad registrada en el Cat�logo de Configuraci�n de MTC.")
                Else
                    arraylist.Add("InfoCapacidad")                      '0
                End If
                'Regresa la informacion de la Capacidad de Carga del Vehiculo
                arraylist.Add(Format(Me.CapacidadKG, "###,##0.00"))     '1
                arraylist.Add(Format(Me.CapacidadM3, "###,##0.00"))     '2
                arraylist.Add(strDescripcion)                           '3
                'Env�a la descripcion de los remolques
                arraylist.Add(strIdTipoUnidad)                          '4
                arraylist.Add(strUnidad)                                '5
                arraylist.Add(strIdTipoRem1)                            '6
                arraylist.Add(strRem1)                                  '7
                arraylist.Add(strIdTipoRem2)                            '8
                arraylist.Add(strRem2)                                  '9
                arraylist.Add(strIdTipoDolly)                           '10
                arraylist.Add(strDolly)                                 '11
                Return arraylist
            Catch ex As Exception
                Throw ex
            End Try
        Catch ex As Exception
            Dim arraylist As New ArrayList
            arraylist.Add("Error")
            arraylist.Add(ex.Message)
            Return arraylist
            Throw ex
        End Try
    End Function

    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
    Public Function GetTipoOperacionBySegNomCorto(ByVal strNombreCortoSeguimiento As String) As ArrayList
        Try
            Dim arraylist As New ArrayList
            Dim strTipoOperacion As String = "0"
            iBR.str_log = Session("strConn")
            Try
                'iBR.str_log = New net_objestandar.str_log(ConfigurationManager.AppSettings("strConn").ToString)
                'strTipoOperacion = iBR.AsignacionClass.GetTipoOperacionBySeg(iBR.str_log)
                arraylist.Add(CStr(iBR.AsignacionClass.GetTipoOperacionBySegNombreCorto(strNombreCortoSeguimiento, iBR.str_log)))     '0
                Return arraylist
            Catch ex As Exception
                Throw ex
            End Try
        Catch ex As Exception
            Dim arraylist As New ArrayList
            arraylist.Add("Error")
            arraylist.Add(ex.Message)
            Return arraylist
            Throw ex
        End Try
    End Function

    'Valida Remitente
    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
    Public Function ValidaRemitente(ByVal intRemitente As Integer) As ArrayList
        Try
            If Session("strConn") Is Nothing Then
                Throw New Exception("su sesi�n a terminado, favor de iniciar sesi�n nuevamente en el modulo.")
            End If
            Dim arraylist As New ArrayList
            iBR.str_log = Session("strConn")
            iBR.AsignacionClass.Id_remitente = intRemitente
            arraylist.Add(iBR.AsignacionClass.Id_remitente)           '0
            arraylist.Add(iBR.GetNombreById(iBR.AsignacionClass.Id_remitente, iBR.str_log))           '1
            Return arraylist

        Catch ex As Exception
            Dim arraylist As New ArrayList
            arraylist.Add("Error")
            arraylist.Add(ex.Message)
            Return arraylist
            Throw ex
        End Try
    End Function
    ' MSALVARADO AEO FOLIO: 23507
    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
    Public Function ValidadEvidenciaOperador(ByVal Operador As Integer, ByVal num_operador As Integer) As ArrayList
        Try
            Dim tablaEvidenciasPend As Data.DataTable
            If Session("strConn") Is Nothing Then
                Throw New Exception("su sesi�n a terminado, favor de iniciar sesi�n nuevamente en el modulo.")
            End If
            Dim arraylist As New ArrayList
            iBR.str_log = Session("strConn")
            tablaEvidenciasPend = iBR.GetEvidenciasPendOperador(Operador, iBR.str_log)
            arraylist.Add(num_operador)           '0
            arraylist.Add(tablaEvidenciasPend.Rows.Count)           '1
            arraylist.Add(Operador) ' 2
            Return arraylist

        Catch ex As Exception
            Dim arraylist As New ArrayList
            arraylist.Add("Error")
            arraylist.Add(ex.Message)
            Return arraylist
            Throw ex
        End Try
    End Function

    'Valida Destinatario
    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
    Public Function ValidaDestinatario(ByVal intDestinatario As Integer) As ArrayList
        Try
            If Session("strConn") Is Nothing Then
                Throw New Exception("su sesi�n a terminado, favor de iniciar sesi�n nuevamente en el modulo.")
            End If
            Dim arraylist As New ArrayList
            iBR.str_log = Session("strConn")
            iBR.AsignacionClass.Id_destinatario = intDestinatario
            arraylist.Add(iBR.AsignacionClass.Id_destinatario)           '0
            arraylist.Add(iBR.GetNombreById(iBR.AsignacionClass.Id_destinatario, iBR.str_log))           '1
            Return arraylist

        Catch ex As Exception
            Dim arraylist As New ArrayList
            arraylist.Add("Error")
            arraylist.Add(ex.Message)
            Return arraylist
            Throw ex
        End Try
    End Function

#End Region

    'Funcion que valida los datos antes de guardar la asignaci�n
    Public Sub ValidaDatos(ByVal Array() As String)
        Try
            iBR.ValidaOrigenRutaAsignacion(Array(5), Array(1), CDate(Array(6)), iBR.AsignacionClass.Asignacion, "0", "", iBR.str_log)
            If Array(10) <> "" Then 'verificamos la informaci�n del kit
                iBR.ValidaKit(Trim(Array(10)), Trim(Array(1)), Trim(Array(12)), Trim(Array(13)), Trim(Array(14)), iBR.str_log)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Guardar
    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
    Public Function Guardar(ByVal Array() As String) As ArrayList
        Try
            Dim luoMSG As New net_b_despextended.pre_b_despmensajes
            Dim luoParm As New net_b_zamclasesextended.net_b_clasegeneralparametrosextended
            Dim luoUnidad As New net_b_zamclasesextended.net_b_claseunidadextended
            Dim iuo_bandera As New net_b_servicios.net_b_bandera
            Dim bolCommit As Boolean = False
            Dim ldt_unidades, ldtUnidades, ldt_datos_det, ddt_datos_bandera As New Data.DataTable
            Dim intVista, intNoViaje As Integer
            Dim lstrMsgUnidad As String = ""
            Dim strFormat As String = ""
            Dim lintValorParam As Integer = 0
            Dim ArrayReturn As New ArrayList
            Dim ArrayReturnRegreso As New ArrayList
            Dim intViajeNormal As Integer
            Dim intAsignacionNormal As Integer
            'GAFIGUEROA 24/01/2018, FOLIO: 23907, ALMEX
            Dim luoOperador As New net_b_clasesprincipalesextended.ede_b_personalpersonal
            Dim lStrDiasDescanso As String = "N"
            'LEHERNANDEZ 11/04/2018 FOLIO: 24251
            Dim luoPersonal_Personal As New net_b_zam.net_b_personal
            Dim lstrValidaOperPermAUnidadPerm As String = "N"
            'GAFIGUEROA 27/04/2018, FOLIO: 24328, ALMEX
            Dim lStrCambiosOpeAlmex As String = ""

            'Conexi�n
            iBR.str_log = Session("strConn")

            luo_br_layout.str_log = Session("strConn")
            iuoAsignacion.str_log = Session("strConn")
            iuo_bandera.str_log = Session("strConn")
            'Se manda llamar el metodo que valida la informaci�n
            ValidaDatos(Array)

            'GAFIGUEROA 24/01/2018, FOLIO: 23907, ALMEX
            luoOperador.str_log = Session("strConn")

            'lehernandez fecha: 11/04/2018 folio : 24251
            lstrValidaOperPermAUnidadPerm = luoParm.GetParametro("CAMBIOS_OPE_ALMEX", iBR.str_log, False)
            If lstrValidaOperPermAUnidadPerm = "S" Then
                Dim id_unidad As String = Trim(Array(1))
                Dim id_operador1 As String = Trim(Array(18))
                Dim id_operador2 As String = Trim(Array(19))

                Dim ds_datosOper1 As New Data.DataSet
                Dim ds_datosOper2 As New Data.DataSet
                Dim ds_datosUnidad As New Data.DataSet

                Dim strUnidadTercero = ""
                Dim strUnidadIdPermisionario = ""
                Dim strOper1Tercero = ""
                Dim strOper1IdPermisionario = ""
                Dim strOper2Tercero = ""
                Dim strOper2IdPermisionario = ""


                If id_unidad <> 0 Then
                    luoUnidad.GetIdTerceroyTerceroUnidad(id_unidad, ds_datosUnidad, iBR.str_log)
                    Dim ldr_Row As DataRow
                    ldr_Row = ds_datosUnidad.Tables("datos").Rows(0)
                    strUnidadTercero = ldr_Row.Item("tercero")
                    strUnidadIdPermisionario = ldr_Row.Item("id_tercero")
                End If

                If id_operador1 <> 0 Then
                    luoPersonal_Personal.GetTerceroOperador(id_operador1, ds_datosOper1, iBR.str_log)
                    Dim ldr_Row As DataRow
                    ldr_Row = ds_datosOper1.Tables("datos").Rows(0)
                    strOper1Tercero = ldr_Row.Item("tercero")
                    strOper1IdPermisionario = ldr_Row.Item("id_tercero")
                End If

                If id_operador2 <> 0 Then
                    luoPersonal_Personal.GetTerceroOperador(id_operador2, ds_datosOper2, iBR.str_log)
                    Dim ldr_Row As DataRow
                    ldr_Row = ds_datosOper1.Tables("datos").Rows(0)
                    strOper2Tercero = ldr_Row.Item("tercero")
                    strOper2IdPermisionario = ldr_Row.Item("id_tercero")
                End If

                If Not strUnidadTercero = "" And Not IsDBNull("strUnidadTercero") Then
                    If Not strOper1Tercero = "" And Not IsDBNull("strOper1Tercero") Then
                        If strUnidadTercero = "N" Then 'Unidad que NO es permisionaria
                            If strOper1Tercero = "S" Then 'Operador Permisionario
                                Throw New Exception("El operador 1 es permisionario. No es posible asignar el operador a una unidad propia.")
                            Else
                                If strOper1IdPermisionario <> strUnidadIdPermisionario Then
                                    Throw New Exception("La Unidad y el Operador 1 pertenecen a un permisionario diferente")
                                End If
                            End If
                        ElseIf strUnidadTercero = "S" Then ' Unidad que ES permisionaria
                            If strOper1Tercero = "N" Then 'Operador que NO es permisionario
                                Throw New Exception("La unidad es una unidad permisionaria. No es posible asignar el operador 1 que no es permisionario.")
                            Else
                                If strOper1IdPermisionario <> strUnidadIdPermisionario Then
                                    Throw New Exception("La unidad y el operador 1 pertenecen a un permisionario diferente")
                                End If
                            End If
                        End If
                    End If
                    If Not strOper2Tercero = "" And Not IsDBNull("strOper2Tercero") Then
                        If strUnidadTercero = "N" Then 'Unidad que NO es permisionaria
                            If strOper2Tercero = "S" Then 'Operador Permisionario
                                Throw New Exception("El operador 2 es permisionario. No es posible asignar el operador a una unidad propia.")
                            Else
                                If strOper2IdPermisionario <> strUnidadIdPermisionario Then
                                    Throw New Exception("La Unidad y el Operador pertenecen a un permisionario diferente")
                                End If
                            End If
                        ElseIf strUnidadTercero = "S" Then ' Unidad que ES permisionaria
                            If strOper2Tercero = "N" Then 'Operador que NO es permisionario
                                Throw New Exception("La unidad es una unidad permisionaria. No es posible asignar el operador 2 que no es permisionario.")
                            Else
                                If strOper2IdPermisionario <> strUnidadIdPermisionario Then
                                    Throw New Exception("La unidad y el operador 2 pertenecen a un permisionario diferente")
                                End If
                            End If
                        End If
                    End If
                End If



            End If

            'Quethzel, F.23195
            Dim docsParam As String = luoParm.GetParametro("documentos_por_unidades", iBR.str_log, False)
            If docsParam = "S" Then
                Dim id_unidad As String = Trim(Array(1))
                Dim id_remolque1 As String = Trim(Array(12))
                Dim id_remolque2 As String = Trim(Array(15))
                Dim id_dolly As String = Trim(Array(13))
                Dim id_operador1 As String = Trim(Array(18))
                Dim id_operador2 As String = Trim(Array(19))

                Dim mensajes As New List(Of Object)()
                mensajes.AddRange(GetListDocumentosUnidadCF(id_unidad))
                mensajes.AddRange(GetListDocumentosUnidadCF(id_remolque1))
                mensajes.AddRange(GetListDocumentosUnidadCF(id_remolque2))
                mensajes.AddRange(GetListDocumentosUnidadCF(id_dolly))
                mensajes.AddRange(GetListDocumentosOperador(id_operador1))
                mensajes.AddRange(GetListDocumentosOperador(id_operador2))

                If mensajes.Contains(mensajes.Find(Function(o) o.tipo_aviso = tipo_aviso.bloqueada)) Then
                    Dim listMensajes As New List(Of String)()
                    For Each o As Object In mensajes
                        listMensajes.Add(o.mensaje.ToString())
                    Next o
                    Throw New Exception(String.Join(", ", listMensajes.ToArray()))
                End If

            End If

            'GAFIGUEROA 24/01/2018, FOLIO: 23907, ALMEX: Se agrega el Parametro hacer el llamado al procedimiento 
            'que valida los dias de descanso del Operador
            lStrDiasDescanso = luoParm.GetParametro("OPE_DIASDESCANSO_ALMEX", iBR.str_log, False)
            If UCase(lStrDiasDescanso) = "S" Then
                Dim lIntIdOperador As Integer = Trim(Array(18))
                If lIntIdOperador = 0 Then
                    Throw New Exception("No se ha seleccionado un operador. Favor de validar.")
                End If
                'Hace el llamado a la f�ncion que reaiza la validaci�nde los d�as de descanso del Operador.
                luoOperador.GetValidaDiasDescansoOperador(lIntIdOperador, iBR.str_log)
            End If


            'Valida que el personal no tenga documentos vencidos
            'JREGALADO Folio. Par�metro Trareysa
            'CSCABRALES, FOLIO: 24965, 26/09/2018
            Dim lstrParamTs As String = luoParm.GetParametro("validadocoperador", iBR.str_log, False)
            Dim tipoServ = If(lstrParamTs = "S", 1, 0)
            If tipoServ = 1 Then
                Dim idPersonal As Integer = Trim(Array(18))
                If idPersonal = 0 Then
                    Throw New Exception("No se ha seleccionado un operador")
                End If
                Dim tipoServicio As String = Trim(Array(52))
                ValidaVencimientoDocumento(idPersonal, tipoServicio)
            End If

            'rherrera GAAL
            strParmValesAutomaticos = luoParm.GetParametro("CalculoValesCombustibleAuto", iBR.str_log, False)

            If Array(33) = "1" Then
                If Array(31) = "" Then
                    Throw New Exception("Favor de Capturar el remitente")
                ElseIf Array(32) = "" Then
                    Throw New Exception("Favor de Capturar el destinatario")
                ElseIf Array.Length > 43 AndAlso (Array(43) = "" Or Array(43) = "0" Or Array(43) = "1") Then
                    Throw New Exception("Favor de Capturar el Tipo de Operacion")
                End If
            End If

            'megallegos 19/07/2016 // Folio:22197 MTC
            'Revisa �ltimo viaje de la Unidad y validar� que para dicho Viaje exista al menos un vale de combustible en estatus de Aceptado 
            strVal_CargasComb = luoParm.GetParametro("Val_CargasComb", iBR.str_log, False)
            If strVal_CargasComb = "S" Then
                Dim intMaxViajeUnid, intMaxComb, intValidaComb As Integer
                Dim idUnidad As String = Trim(Array(1))
                intValidaComb = iBRControlViajes.GetValidaCombustible(idUnidad, iBR.str_log)
                intMaxViajeUnid = iBRControlViajes.GetMaxViaje_Unidad(idUnidad, iBR.str_log)
                If intValidaComb = 1 Then
                    If intMaxViajeUnid <> 0 Then
                        intMaxComb = iBRValesComb.GetUltimoViaje_StatusDoc(intMaxViajeUnid, iBR.str_log)
                        If intMaxComb <= 0 Then
                            Throw New Exception("No se han registrado las cargas de combustible del Viaje " & intMaxViajeUnid & " de la Unidad " & idUnidad)
                        End If
                    End If
                End If
            End If

            'megallegos -> Valida que se ingrese el tipo de operacion // Folio:22277 - SIMSA
            strTipoOp_AsigVV = luoParm.GetParametro("TipoOp_AsigVV", iBR.str_log, False)
            If strTipoOp_AsigVV = "S" Then
                If Array(43) = "" Or Array(43) = "0" Then
                    Throw New Exception("Favor de Capturar el Tipo de Operacion")
                End If
            End If

            'megallegos 20/01/17 -> Folio:22791 // MTC
            strtipoOpPed = luoParm.GetParametro("tipoOpPed", iBR.str_log, False)
            If strtipoOpPed = "S" Then
                If Array(43) = "0" Or Array(43) = "1" Then
                    Dim arrPedidosPK As New ArrayList
                    Dim arrIdArea As New ArrayList
                    iBR.PasarAArreglo(Array(3), "[;]", arrIdArea, iBR.str_log)
                    iBR.PasarAArreglo(Array(4), "[;]", arrPedidosPK, iBR.str_log)
                    If arrPedidosPK(0) <> "vacio" And arrIdArea(0) <> "vacio" Then
                        For pedido = 0 To arrPedidosPK.Count - 1
                            For area = 0 To arrIdArea.Count - 1
                                intTipoOp = iBR.GetTipoOperacionXPedido(arrPedidosPK(pedido), arrIdArea(area), iBR.str_log)
                                Array(43) = intTipoOp
                                'iuoAsignacion.TipoOp = intTipoOp
                            Next
                        Next
                    End If
                End If
            End If

            'GGUERRA FOLIO:21254
            '========Carmex parametros para restringir la asignacion si el operador tiene viajes pendiente por liquidar
            strParmViajesPendientes = luoParm.GetParametro("trafdespvaliviajespendxliq", iBR.str_log, False)
            If strParmViajesPendientes = Nothing Then strParmViajesPendientes = "N"
            intParmLimiteViajesPen = luoParm.GetParametroNumerico("trafCantidadViajesPend", iBR.str_log, False)
            If intParmLimiteViajesPen = Nothing Then intParmLimiteViajesPen = 0
            '========================
            'GGUERRA FOLIO:22765
            strParmValidaMaxAsign = luoParm.GetParametro("validamaxasigporunidad", iBR.str_log, False)

            'Validaci�n de estado de operador - Quethzel 03/12/2014
            Dim opr1 As Integer
            Dim lds As New Data.DataSet
            opr1 = Array(18)

            'GGUERRA FOLIO:21597
            Dim strParmFlota As String
            strParmFlota = luoParm.GetParametro("filtrapersonal_x_areaflota", iBR.str_log, False).ToUpper()
            If strParmFlota = "S" Then
                iBROperador.FiltroAreaFlota = True
                luoFlotas.GetFlota(strIdFlota, iBR.str_log)
                iBROperador.AreaFlota = luoFlotas.Id_Area_Flota
            End If

            iBROperador.GetDatosOperador(opr1, lds, iBR.str_log)

            If Trim(Array(1)) = "" Then Array(0) = "0" 'txtUnidad
            If Trim(Array(1)) <> "0" Then
                'Cambios Almex, gafigueroa 21/02/2018, FOLIO: 24047
                Dim lstrLigaCONSOPV6_Almex As String = luoParm.GetParametro("LigaCONSOPV6_Almex", iBR.str_log, False)
                If UCase(lstrLigaCONSOPV6_Almex) = "S" Then
                    'Pregunta si  tiene liga con operaciones
                    Dim lstrLigaCONSOPV6 As String = luoParm.GetParametro("consligaoperaciones", iBR.str_log, False)
                    'valida si el parametro de liga con operaciones esta prendido.
                    If UCase(lstrLigaCONSOPV6) = "S" Then
                        'Declaraci�n de Variables Locales.
                        Dim lbol_ValidaRem As Boolean
                        Dim lstrUnidad As String
                        Dim lStrRemolque As String
                        Dim lStrRemolque2 As String

                        'Se asignan los valores de la Unidad y el Remolque1 y Remolque2
                        lstrUnidad = Trim(Array(1))
                        lStrRemolque = Trim(Array(12)) 'txtRemolque1
                        lStrRemolque2 = Trim(Array(15)) 'txtRemolque2

                        luoUnidad.GetUnidad("T", lstrUnidad, True, iBR.str_log)

                        'Valida si el tipo de unidad es THORTON, RABON O CAMIONETA entonces NO valida que se captura un Remolque
                        lbol_ValidaRem = luoUnidad.GetTipoUnidad(luoUnidad.TipoUnidad, iBR.str_log)
                        If lbol_ValidaRem = False Then
                            'Valida la clasificacion de la unidad
                            If Not luoUnidad.Tipo_Unidad = 7 Then
                                Throw New Exception("La Clasificaci�n de la unidad ingresada no es UNIDAD CON CAPACIDAD DE CARGA. favor de validar.")
                            End If

                            'Valida si tiene dato en el campo Remolque 1 y 
                            If lStrRemolque.Length > 0 Then
                                Throw New Exception("No se puede ingresar el Remolque cuando el tipo de unidad es THORTON, RABON � CAMIONETA. Favor de validar.")
                            End If

                            If lStrRemolque2.Length > 0 Then
                                Throw New Exception("No se puede ingresar el Remolque 2 cuando el tipo de unidad es THORTON, RABON � CAMIONETA. Favor de validar.")
                            End If
                        End If
                    End If
                End If

                luoUnidad.ValidaOrdenServicioUnidad(Trim(Array(0)), iBR.str_log, lstrMsgUnidad, False)
                'Se llama a la funci�n GetParametro, se envian como argumentos el nombre del 
                'par�metro, la estructura del LOG como referencia y una variable que indica 
                'si se valida o no que existe el par�metro en la base de datos. emartinez 02-ENE-2009
                lintValorParam = luoParm.GetParametroNumerico("despvalidaordenserviciounidad", iBR.str_log, False)
                Select Case lintValorParam
                    Case 0  'No valida
                    Case 1  'Valida y muestra mensaje, permite continuar
                        If lstrMsgUnidad.Length > 0 Then
                            Me.lblError.Text = "ADVERTENCIA : " & lstrMsgUnidad
                            Me.lblError.Visible = True
                        End If
                    Case 2  'Valida y NO permite continuar
                        If lstrMsgUnidad.Length > 0 Then
                            Throw New Exception(lstrMsgUnidad)
                        End If

                        'megallegos 30/01/17 -> Valida en la Asignaci�n que el Remolque no tenga abiertas Ordenes de Servicio || Folio:22820 // TRAREYSA
                        strValidaOrdenServREM = luoParm.GetParametro("ValidaOrdenServREM", iBR.str_log, False)
                        If strValidaOrdenServREM = "S" Then
                            'Valida sobre el Remolque1 y LineaRemolque1, tanto propias y americanas.
                            If Trim(Array(12)) <> "" And Trim(Array(11)) = "" Then
                                luoUnidad.ValidaOrdenServicioUnidad(Trim(Array(12)), iBR.str_log, lstrMsgREM, False)
                                If lstrMsgREM.Length > 0 Then
                                    lstrMsgREM = lstrMsgREM.Replace("La Unidad", "El Remolque")
                                    Throw New Exception(lstrMsgREM)
                                End If
                            ElseIf Trim(Array(12)) <> "" And Trim(Array(11)) <> "" Then
                                luoUnidad.ValidaOrdenServicioUnidadLinea(Trim(Array(12)), Trim(Array(11)), iBR.str_log, lstrMsgREM, False)
                                If lstrMsgREM.Length > 0 Then
                                    Throw New Exception(lstrMsgREM)
                                End If
                            End If
                            'Valida sobre el Remolque2 y LineaRemolque2, tanto propias y americanas.
                            If Trim(Array(15)) <> "" And Trim(Array(14)) = "" Then
                                luoUnidad.ValidaOrdenServicioUnidad(Trim(Array(15)), iBR.str_log, lstrMsgREM, False)
                                If lstrMsgREM.Length > 0 Then
                                    lstrMsgREM = lstrMsgREM.Replace("La Unidad", "El Remolque")
                                    Throw New Exception(lstrMsgREM)
                                End If
                            ElseIf Trim(Array(15)) <> "" And Trim(Array(14)) <> "" Then
                                luoUnidad.ValidaOrdenServicioUnidadLinea(Trim(Array(15)), Trim(Array(14)), iBR.str_log, lstrMsgREM, False)
                                If lstrMsgREM.Length > 0 Then
                                    Throw New Exception(lstrMsgREM)
                                End If
                            End If
                        End If
                End Select
            End If

            'Se Valida que la Unidad no tenga una Orden de Servicio Abierta, se envian como argumentos la Unidad
            '   y la estructura del LOG como referencia, si el parametro esta activo, muestra el mensaje, permite continuar.
            '   Jdlcruz, GASA 28-Nov-2012, Folio: 17747.
            Dim lstrValidaOrden As String = ""
            Dim lstrDetenerProceso As String = ""
            lstrValidaOrden = luoParm.GetParametro("ValidaOrden_Mantenimiento", iBR.str_log, False)
            If lstrValidaOrden = "S" Then
                lstrDetenerProceso = luoParm.GetParametro("ValidaOrden_DetenerProceso", iBR.str_log, False)
                Dim boolDetener As Boolean = False
                If lstrDetenerProceso = "S" Then
                    boolDetener = True
                End If

                Dim lstrMsgUnidad2 As String = ""
                'Valida sobre el Remolque1 y LineaRemolque1.
                If Trim(Array(12)) <> "" And Trim(Array(11)) = "" Then
                    luoUnidad.ValidaOrdenServicioUnidad(Trim(Array(12)), iBR.str_log, lstrMsgUnidad2, False)

                    If boolDetener = True Then
                        If lstrMsgUnidad2.Length > 0 Then
                            lstrMsgUnidad2 = lstrMsgUnidad2.Replace("La Unidad", "El Remolque")
                            Throw New Exception(lstrMsgUnidad2)
                        End If
                    End If
                ElseIf Trim(Array(12)) <> "" And Trim(Array(11)) <> "" Then
                    luoUnidad.ValidaOrdenServicioUnidadLinea(Trim(Array(12)), Trim(Array(11)), iBR.str_log, lstrMsgUnidad2, False)

                    If boolDetener = True Then
                        If lstrMsgUnidad2.Length > 0 Then
                            Throw New Exception(lstrMsgUnidad2)
                        End If
                    End If
                End If
                'Valida sobre el Remolque2 y LineaRemolque2.
                If Trim(Array(15)) <> "" And Trim(Array(14)) = "" Then
                    luoUnidad.ValidaOrdenServicioUnidad(Trim(Array(15)), iBR.str_log, lstrMsgUnidad2, False)

                    If boolDetener = True Then
                        If lstrMsgUnidad2.Length > 0 Then
                            lstrMsgUnidad2 = lstrMsgUnidad2.Replace("La Unidad", "El Remolque")
                            Throw New Exception(lstrMsgUnidad2)
                        End If
                    End If
                ElseIf Trim(Array(15)) <> "" And Trim(Array(14)) <> "" Then
                    luoUnidad.ValidaOrdenServicioUnidadLinea(Trim(Array(15)), Trim(Array(14)), iBR.str_log, lstrMsgUnidad2, False)

                    If boolDetener = True Then
                        If lstrMsgUnidad2.Length > 0 Then
                            Throw New Exception(lstrMsgUnidad2)
                        End If
                    End If
                End If

            End If

            'Funcionalidad para validar que solo se asignen pedidos con unidades de la misma area oerc 17/11/16 folio 22601
            Dim lstrParamValAreas As String
            lstrParamValAreas = UCase(luoParm.GetParametro("Val_area_pedido_vs_area_unidad", iBR.str_log, False))

            If lstrParamValAreas = "S" And Array(0) <> "" Then

                Dim dtPedidosAsigVal As New Data.DataTable
                dtPedidosAsigVal = iBR.GetPedidosDeUnaAsignacion(Array(0), iBR.str_log)

                If dtPedidosAsigVal.Rows.Count > 0 Then
                    Dim strPaso, strPaso2, strBandera, strareapaso, strpedidopaso As String
                    Dim areaTracto As Integer

                    areaTracto = iBR.AreaXTracto(Array(1), iBR.str_log)
                    For intCont As Integer = 0 To dtPedidosAsigVal.Rows.Count - 1
                        strPaso = dtPedidosAsigVal.Rows(intCont).Item("num_pedido")
                        strPaso2 = dtPedidosAsigVal.Rows(intCont).Item("id_area")

                        If strPaso2 <> CStr(areaTracto) Then
                            strBandera = "S"
                            strareapaso = strPaso2
                            strpedidopaso = strPaso
                            Exit For
                        End If
                    Next
                    If strBandera = "S" Then
                        Throw New Exception("El area " & strareapaso & " del pedido " & strpedidopaso & " es diferente al area " & CStr(areaTracto) & " de la unidad " & iBR.AsignacionClass.Tractor & ", Favor de Verificar.")
                    End If
                End If
            End If

            'Si no envia el id_asignacion, 
            If Trim(Array(0)) = "" Then  'Request("ParmAsignHdn") Is Nothing Then
                'Se ingresa la informaci�n de la Nueva Asignaci�n
                Call Ingresar2(Array, ArrayReturn)
                iBR.SetSeguimientoActual(Trim(Array(1)), iBR.str_log, True, luoParm.GetParametro("ActSegTercero", iBR.AsignacionClass.str_log, False))

                If strParmValesAutomaticos = "S" Then
                    intViajeNormal = Array(5)
                    intAsignacionNormal = iBR.AsignacionClass.Asignacion
                    If Array(51) <> 0 Then
                        If Array(47) <> "" Then
                            Array(2) = "vacio[;]"
                            Array(3) = "vacio[;]"
                            Array(4) = "0[;]"
                            Array(5) = Array(47) 'Me.txtRutaRegreso.Text
                            Array(6) = Array(48) 'Me.wdcInicioViaje.Value
                            Array(7) = Array(49) 'Me.wdcFinViaje.Value
                            Array(8) = Array(50) 'Me.txtHrsEstandar.Text
                            Array(51) = iBR.AsignacionClass.Asignacion 'Viaje_regreso con la asignaci�n anterior
                        End If
                        Call Ingresar2(Array, ArrayReturnRegreso)
                    End If
                End If
            Else
                'Actualiza la informaci�n de la Asignaci�n
                Call Actualizar(Array, ArrayReturn)
                'rherrera GAAL
                If strParmValesAutomaticos = "S" Then
                    intViajeNormal = Array(5)
                    intAsignacionNormal = iBR.AsignacionClass.Asignacion
                    '    'Borra los vales siempre y cuando est�n en edici�n
                    '    iBR.CancelarValesPorCambioDestino(iBR.AsignacionClass.Area, iBR.AsignacionClass.Num_Asignacion, iBR.str_log)
                End If

                'Se actualiza el Seguimiento
                iBR.SetSeguimientoActual(Trim(Array(1)), iBR.str_log, True, luoParm.GetParametro("ActSegTercero", iBR.AsignacionClass.str_log, False))
                'En caso de que haya cambiado de unidad
                If Trim(Array(1)) <> Trim(Array(26)) Then
                    'Se actualiza la informaci�n de la Unidad Original
                    iBR.SetSeguimientoActual(Trim(Array(26)), iBR.str_log, True, luoParm.GetParametro("ActSegTercero", iBR.AsignacionClass.str_log, False))
                End If
            End If

            'GAFIGUEROA 27/04/2018, FOLIO: 24328,Se agrega el Parametro hacer el llamado al procedimiento 
            'que valida los datos de la asignacion.
            lStrCambiosOpeAlmex = luoParm.GetParametro("CAMBIOS_OPE_ALMEX", iBR.str_log, False)
            If UCase(lStrCambiosOpeAlmex) = "S" Then
                'Asigna el numero de asignaci�n
                Dim lInt_IdAsignacion As Integer = IIf(Array(0) = "", iBR.AsignacionClass.Asignacion, Array(0)) 'id_asignacion
                Dim lInt_IdOperador As Integer = iBR.AsignacionClass.Operador1
                Dim lInt_IdOperador2 As Integer = iBR.AsignacionClass.Operador2
                Dim lInt_IdUnidad As String = iBR.AsignacionClass.Tractor
                Dim lInt_IdConfiguracionViaje As Integer = iBR.AsignacionClass.Id_configuracionviaje
                'Hace el llamado al metodo que obtiene los datos de la asignaci�n
                iBR.AsignacionClass.GetAsignacion(lInt_IdAsignacion, iBR.AsignacionClass, iBR.str_log)
                'Valida si la asignaci�n es de Operaciones.
                If iBR.AsignacionClass.sist_origen = "ZAM" Then
                    'Hace el llamado a la f�ncion que actualiza los datos de origen y destino de la ruta
                    Dim lInt_IdRecorrido As Integer = Array(54) 'IdRecorrido
                    Dim lInt_IdRecorridoOrigen As Integer = Array(55) 'IdRecorridoOrigen
                    Dim lInt_IdRecorridoDestino As Integer = Array(56) 'IdRecorridoDestino
                    'Hace el llamado al m�todo que ejecuta la actualizaci�n del origen 
                    iuoAsignacion.SetIdRecorridoOrigenDestino(lInt_IdAsignacion, lInt_IdRecorrido, lInt_IdRecorridoOrigen, lInt_IdRecorridoDestino, iBR.str_log)
                End If

                'Hace el llamado a la funci�n que realiza las validaciones
                iuoAsignacion.GetValidaDatosDespacho(lInt_IdAsignacion, iBR.str_log)
            End If

            '//================================hrflores 20/06/2013 Cambio Contenedores de Pedidos================================//
            If Array(34) = "S" Then
                ActualizarContenedores(Array, ArrayReturn)
            End If
            '//=================================================================================================================//

            'Si no tiene Asignaci�n, entonces se crea una nueva
            If Array(0) = "" Then
                'ArrayReturn.Add("NuevoRow") '1
                ArrayReturn.Item(1) = "NuevoRow"
                'Obtiene el nuevo id_asignacion
                Array(0) = iBR.AsignacionClass.Asignacion
            Else
                'ArrayReturn.Add("ActualizaRow") '1
                ArrayReturn.Item(1) = "ActualizaRow"
            End If

            'Se cruzan las fechas
            Select Case Array(27)
                Case "dd/MM/yyyy HH:mm"
                    strFormat = "MM/dd/yyyy HH:mm"
                Case "MM/dd/yyyy HH:mm"
                    strFormat = "dd/MM/yyyy HH:mm"
            End Select

            'Obtiene la vista en base al usuario
            luo_br_layout.id_ventana = 11
            luo_br_layout.idusuario = Session("UserName")
            ldt_unidades = luo_br_layout.ObtenerVistaUsuario()

            If ldt_unidades.Rows.Count = 1 Then
                Me.ColumnasUnidades(ldt_unidades.Rows(0).Item("id_vista"), Array(16), Array(0), Array(1), False, ldtUnidades)
                intVista = ldt_unidades.Rows(0).Item("id_vista")
                'vista del usuario
            ElseIf ldt_unidades.Rows.Count > 1 Then
                Me.ColumnasUnidades(ldt_unidades.Rows(1).Item("id_vista"), Array(16), Array(0), Array(1), False, ldtUnidades)
                intVista = ldt_unidades.Rows(1).Item("id_vista")
            ElseIf ldt_unidades.Rows.Count = 0 Then
                luo_br_layout.id_ventana = 11
                luo_br_layout.id_vista = 92
                ddt_datos_vista = luo_br_layout.ObtenerVista()
                luo_br_layout.id_vista = ddt_datos_vista.Rows(0).Item("id_vista")
                intVista = ddt_datos_vista.Rows(0).Item("id_vista")
                luo_br_layout.idusuario = Session("UserName")
                Me.ColumnasUnidades(ddt_datos_vista.Rows(0).Item("id_vista"), Array(16), Array(0), Array(1), False, ldtUnidades)
            End If

            If ldtUnidades.Rows.Count > 0 Then
                If ldtUnidades.Rows.Count = 1 Then
                    'ArrayReturn.Add("Agrupa") '2
                    ArrayReturn.Item(2) = "Agrupa" '2
                    'Si es la primer asignaci�n deja la nueva que acaba de crear.
                    If Array(28) <> "null" Then
                        'Se asigna la Asignaci�n Seleccionada, para que consulte por esta y no por la nueva Asignaci�n.
                        Array(0) = Array(28)
                    End If
                ElseIf ldtUnidades.Rows.Count > 1 Then
                    'ArrayReturn.Add("Actualiza") '2 
                    ArrayReturn.Item(2) = "Actualiza" '2 
                End If

                'Consulta la Asignaci�n del row seleccionado
                iuoAsignacion.GetAsignacionMaster(1, Array(0), Nothing, Nothing, iuoAsignacion, iuoAsignacion.str_log)
                ldtUnidades.Clear()

                'rherrera GAAL
                If strParmValesAutomaticos = "S" Then
                    If (Array(51) <> 0) Then 'Si trae viaje de regreso, toma la asignaci�n padre
                        Me.ColumnasUnidades(intVista, Array(16), Array(51), Array(1), True, ldtUnidades)
                    Else
                        Me.ColumnasUnidades(intVista, Array(16), Array(0), Array(1), True, ldtUnidades)
                    End If
                Else
                    'Consulta el registro seleccionado Estandar
                    Me.ColumnasUnidades(intVista, Array(16), Array(0), Array(1), True, ldtUnidades)
                End If

                luo_br_layout.id_vista = intVista
                ldt_datos_det = luo_br_layout.ObtenerVistaDetalle()
                iuo_bandera.id_bandera = 1
                ddt_datos_bandera = iuo_bandera.ObtenerBanderasDetalleCompleto()

                If ldtUnidades.Rows.Count > 0 Then
                    'Valida posibles valores Null
                    If ldtUnidades.Rows(0).Item("duracion_aviso") Is DBNull.Value Then ldtUnidades.Rows(0).Item("duracion_aviso") = 0
                    If ldtUnidades.Rows(0).Item("duracion_maxima") Is DBNull.Value Then ldtUnidades.Rows(0).Item("duracion_maxima") = 0
                    If ldtUnidades.Rows(0).Item("id_statusviaje") Is DBNull.Value Then ldtUnidades.Rows(0).Item("id_statusviaje") = 0
                    If ldtUnidades.Rows(0).Item("no_viaje") Is DBNull.Value Then
                        intNoViaje = 0
                    Else
                        intNoViaje = ldtUnidades.Rows(0).Item("no_viaje")
                    End If

                    'Recorre las columnas del grid de Unidades
                    For Each registro As Data.DataRow In ldt_datos_det.Rows
                        Dim uwgColumn As New Infragistics.WebUI.UltraWebGrid.UltraGridColumn
                        Dim strCell As String
                        If Not ldtUnidades.Rows(0).Item(registro("alias")) Is DBNull.Value Then
                            Dim strFecha As String
                            If registro("tipo_dato") = "datetime" Then
                                strFecha = Format(ldtUnidades.Rows(0).Item(registro("alias")), strFormat)
                                strCell = registro("alias") & "," & strFecha & "," & registro("tipo_dato")
                            Else
                                Select Case registro("alias")
                                    Case "nombre_status_viaje"
                                        'Si no tiene viaje pone el estatus de la unidad
                                        If ldtUnidades.Rows(0).Item(registro("alias")) = "" Then
                                            ldtUnidades.Rows(0).Item(registro("alias")) = ldtUnidades.Rows(0).Item("desp_status_nombre")
                                        End If
                                        If ldtUnidades.Columns.Contains("id_statusviaje") = True And ldtUnidades.Columns.Contains("duracion_aviso") = True And ldtUnidades.Columns.Contains("duracion_maxima") = True Then
                                            strCell = registro("alias") & "," & CStr(ldtUnidades.Rows(0).Item(registro("alias"))) & "," & "nombre_status_viaje, " & CStr(ldtUnidades.Rows(0).Item("id_statusviaje")) & "," & CStr(ldtUnidades.Rows(0).Item("duracion_aviso")) & "," & CStr(ldtUnidades.Rows(0).Item("duracion_maxima"))
                                        Else
                                            strCell = registro("alias") & "," & CStr(ldtUnidades.Rows(0).Item(registro("alias"))) & "," & registro("alias")
                                        End If
                                    Case "bandera1"
                                        strCell = registro("alias") & "," & CStr(ldtUnidades.Rows(0).Item(registro("alias"))) & "," & "bandera1" & "," & ddt_datos_bandera.Rows.Count & "," & ddt_datos_bandera.Rows(0).Item("de") & "," & ldtUnidades.Rows(0).Item(registro("alias")) & "," & ddt_datos_bandera.Rows(1).Item("a") & "," & ddt_datos_bandera.Rows(1).Item("de")
                                    Case "bandera2"
                                        strCell = registro("alias") & "," & CStr(ldtUnidades.Rows(0).Item(registro("alias"))) & "," & "bandera2" & "," & ddt_datos_bandera.Rows.Count & "," & ddt_datos_bandera.Rows(3).Item("de") & "," & ldtUnidades.Rows(0).Item(registro("alias")) & "," & ddt_datos_bandera.Rows(4).Item("a") & "," & ddt_datos_bandera.Rows(4).Item("de")
                                    Case "bandera3"
                                        strCell = registro("alias") & "," & CStr(ldtUnidades.Rows(0).Item(registro("alias"))) & "," & "bandera3" & "," & ddt_datos_bandera.Rows.Count & "," & ddt_datos_bandera.Rows(6).Item("de") & "," & ldtUnidades.Rows(0).Item(registro("alias")) & "," & ddt_datos_bandera.Rows(7).Item("a") & "," & ddt_datos_bandera.Rows(7).Item("de")
                                    Case "bandera4"
                                        strCell = registro("alias") & "," & CStr(ldtUnidades.Rows(0).Item(registro("alias"))) & "," & "bandera4" & "," & ddt_datos_bandera.Rows.Count & "," & ddt_datos_bandera.Rows(9).Item("de") & "," & ldtUnidades.Rows(0).Item(registro("alias")) & "," & ddt_datos_bandera.Rows(10).Item("a") & "," & ddt_datos_bandera.Rows(10).Item("de")
                                    Case "bandera5"
                                        strCell = registro("alias") & "," & CStr(ldtUnidades.Rows(0).Item(registro("alias"))) & "," & "bandera5" & "," & ddt_datos_bandera.Rows.Count & "," & ddt_datos_bandera.Rows(12).Item("de") & "," & ldtUnidades.Rows(0).Item(registro("alias")) & "," & ddt_datos_bandera.Rows(13).Item("a") & "," & ddt_datos_bandera.Rows(13).Item("de")
                                    Case "siguiente"
                                        'Si tiene viaje asigna imagen de seguimiento sino no.
                                        If intNoViaje > 0 Then
                                            strCell = registro("alias") & "," & ldtUnidades.Rows(0).Item(registro("alias")) & "," & registro("alias") & "," & "Imagen"
                                        Else
                                            strCell = registro("alias") & "," & ldtUnidades.Rows(0).Item(registro("alias")) & "," & registro("alias") & "," & "SinImagen"
                                        End If
                                    Case Else
                                        strCell = registro("alias") & "," & CStr(ldtUnidades.Rows(0).Item(registro("alias"))) & "," & registro("alias")
                                End Select
                            End If
                        Else
                            If registro("alias") = "no_viaje" Then
                                strCell = registro("alias") & "," & ldtUnidades.Rows(0).Item(registro("alias")) & "," & registro("alias") & "," & ldtUnidades.Rows(0).Item("operadorviaje")
                            ElseIf registro("alias") = "siguiente" Then
                                strCell = registro("alias") & "," & ldtUnidades.Rows(0).Item(registro("alias")) & "," & registro("alias") & "," & "SinImagen"
                            ElseIf registro("alias") = "nombre_status_viaje" Then
                                'Si no tiene viaje pone el estatus de la unidad
                                If ldtUnidades.Rows(0).Item(registro("alias")) Is DBNull.Value Then
                                    ldtUnidades.Rows(0).Item(registro("alias")) = ldtUnidades.Rows(0).Item("desp_status_nombre")
                                End If
                                If ldtUnidades.Rows(0).Item(registro("alias")) Is DBNull.Value Then
                                    ldtUnidades.Rows(0).Item(registro("alias")) = ""
                                End If
                                If ldtUnidades.Columns.Contains("id_statusviaje") = True And ldtUnidades.Columns.Contains("duracion_aviso") = True And ldtUnidades.Columns.Contains("duracion_maxima") = True Then
                                    strCell = registro("alias") & "," & CStr(ldtUnidades.Rows(0).Item(registro("alias"))) & "," & "nombre_status_viaje, " & CStr(ldtUnidades.Rows(0).Item("id_statusviaje")) & "," & CStr(ldtUnidades.Rows(0).Item("duracion_aviso")) & "," & CStr(ldtUnidades.Rows(0).Item("duracion_maxima"))
                                Else
                                    strCell = registro("alias") & "," & CStr(ldtUnidades.Rows(0).Item(registro("alias"))) & "," & registro("alias")
                                End If
                            Else
                                strCell = registro("alias") & "," & "" & "," & registro("tipo_dato") & "," & ""
                            End If
                        End If
                        ArrayReturn.Add(strCell)
                    Next
                    'rherrera Antes Llenado de coordenadas de origen y destino
                End If
            End If
            'rherrera GAAL
            'strParmValesAutomaticos = luoParm.GetParametro("CalculoValesCombustibleAuto", iBR.str_log, False)
            If strParmValesAutomaticos = "S" Then
                Dim strDato As String
                'Enviar la ruta a la funci�n
                Dim ArrayGeo As ArrayList
                Dim ArrayGeoRegreso As ArrayList

                'Viaje Normal
                ArrayGeo = GetGeoOrigenDestinoCodeBehind(intViajeNormal)
                'Regresa Origen Longitud
                strDato = "OriLon" & "," & ArrayGeo.Item(0) & "," & "OriLon"
                ArrayReturn.Add(strDato)
                'RegresaOrigen Latitud
                strDato = "OriLat" & "," & ArrayGeo.Item(1) & "," & "OriLat"
                ArrayReturn.Add(strDato)
                'Regresa Destino Longitud
                strDato = "DesLon" & "," & ArrayGeo.Item(2) & "," & "DesLon"
                ArrayReturn.Add(strDato)
                'Regresa Destino Latitud
                strDato = "DesLat" & "," & ArrayGeo.Item(3) & "," & "DesLat"
                ArrayReturn.Add(strDato)
                'id_asignacion
                strDato = "id_asignacion_normal" & "," & intAsignacionNormal & "," & "id_asignacion_normal"
                ArrayReturn.Add(strDato)

                'Viaje Regreso
                If (Array(51) <> 0) Then 'If (Array(47) <> "") Then
                    ArrayGeoRegreso = GetGeoOrigenDestinoCodeBehind(Array(47))
                    'Regresa Origen Longitud
                    strDato = "OriLonRegreso" & "," & ArrayGeoRegreso.Item(0) & "," & "OriLonRegreso"
                    ArrayReturn.Add(strDato)
                    'RegresaOrigen Latitud
                    strDato = "OriLatRegreso" & "," & ArrayGeoRegreso.Item(1) & "," & "OriLatRegreso"
                    ArrayReturn.Add(strDato)
                    'Regresa Destino Longitud
                    strDato = "DesLonRegreso" & "," & ArrayGeoRegreso.Item(2) & "," & "DesLonRegreso"
                    ArrayReturn.Add(strDato)
                    'Regresa Destino Latitud
                    strDato = "DesLatRegreso" & "," & ArrayGeoRegreso.Item(3) & "," & "DesLatRegreso"
                    ArrayReturn.Add(strDato)
                    'id_asigancion
                    strDato = "id_asignacion_regreso" & "," & iBR.AsignacionClass.Asignacion & "," & "id_asignacion_regreso"
                    ArrayReturn.Add(strDato)
                End If
            End If

            'ArrayReturn(0) = Num_asignacion
            'ArrayReturn(1) = Indica si es un nuevo registro o si solo actualiza
            'ArrayReturn(2) = Indica si agrupa o agrega nuevo row
            'ArrayReturn(>2)= Parametros a Actualizar en Grid de Unidades
            Return ArrayReturn

        Catch ex As Exception
            Dim arraylist As New ArrayList
            arraylist.Add("Error")
            'Alicia 
            If ex.Message <> "" Then
                arraylist.Add("" & ex.Message.ToString)
            Else
                arraylist.Add("" & ex.InnerException.Message.ToString)
            End If
            arraylist.Add(Array)
            Return arraylist
            Throw ex
        End Try
    End Function

    'rherrera GAAL Guardar Gasolineras con Unidad y Distancia
    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
    Public Sub SetGasolineras(ByVal dataArray() As String)
        Try

            Dim Values(4) As String
            Dim lintCont As Integer
            Dim arrayCampos As New ArrayList
            iBR.str_log = Session("strConn")

            For lintCont = 0 To (dataArray.Length - 1)
                'Funci�n que convierte el string a un Array
                ArregloArray(dataArray(lintCont), ",", arrayCampos, iBR.str_log)
                'Asigna los valores al DataSet
                Values(0) = arrayCampos.Item(0) 'id_gas
                Values(1) = arrayCampos.Item(1) 'id_asignacion
                Values(2) = arrayCampos.Item(2) 'id_area_asignacion
                Values(3) = arrayCampos.Item(3) 'id_unidad
                Values(4) = arrayCampos.Item(8) 'id_unidad
                iBR.SetGasolineras(Values, iBR.str_log)
            Next
        Catch ex As Exception

        End Try
    End Sub

    'rherrera GAAL Obtiene las coordenadas de la ruta
    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
        Public Function GetGeoOrigenDestino(ByVal intIdRuta As Integer) As ArrayList
        Try
            If Session("UserName") Is Nothing Or Session("vg_area") Is Nothing Then
                Throw New Exception("Su Sesi�n termin�, favor de iniciar de nuevo el m�dulo.")
            End If
            Dim arraylist As New ArrayList
            iBR.str_log = Session("strConn")
            Dim strArrRuta(4) As String
            Dim strOriLon, strOriLat, strDesLon, strDesLat As String
            strOriLon = ""
            strOriLat = ""
            strDesLon = ""
            strDesLat = ""
            iBR.ObtieneGeoOrigenDestino(intIdRuta, strOriLon, strOriLat, strDesLon, strDesLat, iBR.str_log)
            arraylist.Add(strOriLon)
            arraylist.Add(strOriLat)
            arraylist.Add(strDesLon)
            arraylist.Add(strDesLat)

            Return arraylist

        Catch ex As Exception
            Dim arraylist As New ArrayList
            arraylist.Add("Error")
            arraylist.Add(ex.Message)
            Return arraylist
            Throw ex
        End Try
    End Function

    'rherrera GAAL
    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
    Public Function getListaGasolineras() As String
        netVi.str_log = New net_objestandar.str_log(ConfigurationManager.AppSettings("strConn"))
        Dim dt As New DataTable
        dt = netVi.getListaGasolineras(netVi.str_log)
        Return dtJson(dt)
    End Function
    'rherrera GAAL
    Public Function getListaGasolinerasDT() As DataTable
        netVi.str_log = New net_objestandar.str_log(ConfigurationManager.AppSettings("strConn"))
        Dim dt As New DataTable
        dt = netVi.getListaGasolineras(netVi.str_log)
        Return dt
    End Function
    'GPEREZ F:24656 16/07/18 -- plaza destino de ruta.
    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
    Public Function GetDestinoRuta(ByVal ruta As String) As String
        netVi.str_log = New net_objestandar.str_log(ConfigurationManager.AppSettings("strConn"))
        Dim dt As New DataTable
        dt = netVi.GetDestinoRuta(netVi.str_log, ruta)
        Return dtJson(dt)
    End Function

    Public Class JsonResGoogle
        Public Class Route
            Public Class Leg
                Public Class Stepp
                    Public Class Path
                        Dim length As Object
                    End Class
                    Public paths As Path()
                End Class
                Public steps As Stepp()
            End Class
            Public legs As Leg()
        End Class
        Public routes As Route()

        Public Sub New()

        End Sub
    End Class

    '    <WebMethod()> _
    'rherrera GAAL
    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
    Public Function AjaxTest(ByVal directionResultA As String) As String
        Try
            Dim directionResult As JsonResGoogle = JsonConvert.DeserializeObject(Of JsonResGoogle)(directionResultA)
            'Dim json As JToken = JsonConvert.DeserializeObject(Of JToken)(directionResult)
            'Dim markerArray() As Stringkt
            Dim markerArrayDT As DataTable
            Dim locations(,) As String
            Dim contador As Integer = 0
            Dim contadorverde As Integer = 0
            Dim next_gas As Boolean = False
            Dim intCont As Integer = 0
            Dim ArrGaso() As String
            Dim rows As Integer

            markerArrayDT = Me.getListaGasolinerasDT()
            rows = markerArrayDT.Rows.Count
            ReDim locations(8, 0 To rows)
            For i = 0 To rows
                locations(1, i) = markerArrayDT.Rows(i)(0)
                locations(2, i) = markerArrayDT.Rows(i)(1)
                locations(3, i) = markerArrayDT.Rows(i)(2)
                locations(4, i) = markerArrayDT.Rows(i)(3)
                locations(5, i) = markerArrayDT.Rows(i)(4)
                locations(6, i) = markerArrayDT.Rows(i)(5)
                locations(7, i) = markerArrayDT.Rows(i)(6)
                locations(8, i) = markerArrayDT.Rows(i)(7)
            Next

            ''Recorrido de todas las gasolineras
            'For g As Integer = 0 To (g < locations.Length)
            '    next_gas = False
            '    'Recorrido por la ruta (1ruta)
            '    For l As Integer = 0 To (l < directionResult.routes(0).legs.Length)
            '        Dim myRoute = directionResult.routes(0).legs(l)

            '        For i As Integer = 0 To (i < myRoute.steps.Length)

            '            For j As Integer = 0 To (j < myRoute.steps(i).paths.Length)

            '                Dim loclat = locations(g, 5)
            '                Dim loclon = locations(g, 6)
            '                Dim loclat2 = 0 'Math.Abs(loclat)
            '                Dim loclon2 = 0 'Math.Abs(loclon)
            '                Dim dif_lat = Math.Abs(loclat2 - Math.Abs(myRoute.steps(i).Path(j).lat())) ' //.lat
            '                Dim dif_lng = Math.Abs(loclon2 - Math.Abs(myRoute.steps(i).Path(j).lng())) ' //.lng

            '                If (dif_lat < 0.001 And dif_lng < 0.001) Then

            '                    Dim cantPaths = myRoute.steps(i).Path.length
            '                    Dim result As String

            '                    If ((j + 4) <= cantPaths - 1) Then
            '                        Dim x1 As Decimal = myRoute.steps(i).Path(j).lng()
            '                        Dim y1 As Decimal = myRoute.steps(i).Path(j).lat()
            '                        Dim x2 As Decimal = myRoute.steps(i).Path(j + 4).lng()
            '                        Dim y2 As Decimal = myRoute.steps(i).Path(j + 4).lat()
            '                        Dim x0 As Decimal = locations(g, 6) '//lng
            '                        Dim y0 As Decimal = locations(g, 5) '//lat
            '                        result = (x0 * (y2 - y1)) + ((x1 - x2) * y0) + ((x2 * y1) - (y2 * x1))
            '                    Else
            '                        Dim x1 As Decimal = myRoute.steps(i).Path(j).lng()
            '                        Dim y1 As Decimal = myRoute.steps(i).Path(j).lat()
            '                        Dim x2 As Decimal = myRoute.steps(i).Path(cantPaths - 1).lng()
            '                        Dim y2 As Decimal = myRoute.steps(i).Path(cantPaths - 1).lat()
            '                        Dim x0 As Decimal = locations(g, 6) '//lng
            '                        Dim y0 As Decimal = locations(g, 5) '//lat
            '                        result = (x0 * (y2 - y1)) + ((x1 - x2) * y0) + ((x2 * y1) - (y2 * x1))
            '                    End If

            '                    If (result >= 0) Then

            '                        Dim coord = "" + locations(g, 5) + ", " + locations(g, 6) + "" '//.lat , .lng

            '                        ArrGaso(intCont) = locations(g, 0) + "," + "2" + "," + locations(g, 6) + ","
            '                        intCont = intCont + 1

            '                    Else

            '                    End If

            '                    contadorverde = contadorverde + 1
            '                    next_gas = True
            '                    If (next_gas) Then
            '                        'break }
            '                    End If

            '                End If
            '                contador = contador + 1
            '            Next
            '            If (next_gas) Then
            '                '{ break }
            '            End If
            '        Next
            '        If (next_gas) Then
            '            'break }
            '        End If
            '    Next
            'Next

            Return "S"
        Catch ex As Exception
            Return "N"
        End Try
    End Function

    'rherrera GAAL
    Function dtJson(ByVal dt As DataTable) As String
        Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer()
        Dim rows As New List(Of Dictionary(Of String, Object))()
        Dim row As Dictionary(Of String, Object)
        For Each dr As DataRow In dt.Rows
            row = New Dictionary(Of String, Object)()
            For Each col As DataColumn In dt.Columns
                row.Add(col.ColumnName, dr(col))
            Next
            rows.Add(row)
        Next
        Return serializer.Serialize(rows)
    End Function

    Public Function GetGeoOrigenDestinoCodeBehind(ByVal intIdRuta As Integer) As ArrayList
        Try
            If Session("UserName") Is Nothing Or Session("vg_area") Is Nothing Then
                Throw New Exception("Su Sesi�n termin�, favor de iniciar de nuevo el m�dulo.")
            End If
            Dim arraylist As New ArrayList
            iBR.str_log = Session("strConn")
            Dim strArrRuta(4) As String
            Dim strOriLon, strOriLat, strDesLon, strDesLat As String
            strOriLon = ""
            strOriLat = ""
            strDesLon = ""
            strDesLat = ""
            iBR.ObtieneGeoOrigenDestino(intIdRuta, strOriLon, strOriLat, strDesLon, strDesLat, iBR.str_log)
            arraylist.Add(strOriLon)
            arraylist.Add(strOriLat)
            arraylist.Add(strDesLon)
            arraylist.Add(strDesLat)

            Return arraylist

        Catch ex As Exception
            Dim arraylist As New ArrayList
            arraylist.Add("Error")
            arraylist.Add(ex.Message)
            Return arraylist
            Throw ex
        End Try
    End Function

    '*******************************************
    'Public Sub ValidaTipoOperacionPedido(ByVal id_pedido As Integer, ByVal id_area As Integer, ByVal id_unidad As String, ByVal id_remolque1 As String, ByVal id_remolque2 As String, ByRef astr_log As net_objestandar.str_log)
    '    Try
    '        Dim tipoOpPedido, tipoOpunidad, tipoOpRemolque1, TipoOpRemolque2 As Integer

    '        Dim lstrParms(2) As String
    '        Dim ldtPedido, ldUnidades, ldRemolque1, ldRemolque2 As New Data.DataTable

    '        lstrParms(0) = id_area
    '        lstrParms(1) = id_pedido
    '        lstrParms(2) = " id_tipo_operacion "
    '        Me.GetData(net_d_dam.net_d_constantes.qt_desp_pedido, net_d_dam.net_d_constantes.q_desp_pedido, lstrParms, ldtPedido, astr_log)


    '        If ldtPedido.Rows.Count > 0 Then
    '            tipoOpPedido = ldtPedido.Rows(0).Item("id_tipo_operacion")
    '        Else
    '            tipoOpPedido = 0
    '        End If

    '        lstrParms(0) = id_unidad
    '        lstrParms(1) = " id_tipo_operacion "
    '        Me.GetData(net_d_dam.net_d_constantes.qt_mtto_unidades, net_d_dam.net_d_constantes.q_mtto_unidades, lstrParms, ldUnidades, astr_log)

    '        lstrParms(0) = id_remolque1
    '        lstrParms(1) = " id_tipo_operacion "
    '        Me.GetData(net_d_dam.net_d_constantes.qt_mtto_unidades, net_d_dam.net_d_constantes.q_mtto_unidades, lstrParms, ldRemolque1, astr_log)

    '        lstrParms(0) = id_remolque2
    '        lstrParms(1) = " id_tipo_operacion "
    '        Me.GetData(net_d_dam.net_d_constantes.qt_mtto_unidades, net_d_dam.net_d_constantes.q_mtto_unidades, lstrParms, ldRemolque2, astr_log)


    '        If ldUnidades.Rows.Count > 0 Then
    '            tipoOpunidad = ldUnidades.Rows(0).Item("id_tipo_operacion")
    '        Else
    '            tipoOpunidad = 0
    '        End If

    '        If ldRemolque1.Rows.Count > 0 Then
    '            tipoOpRemolque1 = ldRemolque1.Rows(0).Item("id_tipo_operacion")
    '        Else
    '            tipoOpunidad = 0
    '        End If

    '        If ldRemolque2.Rows.Count > 0 Then
    '            TipoOpRemolque2 = ldRemolque2.Rows(0).Item("id_tipo_operacion")
    '        Else
    '            tipoOpunidad = 0
    '        End If


    '        If tipoOpPedido <> tipoOpunidad Then
    '            Throw New Exception("El tipo de operaci�n de la Unidad: " & id_unidad & ", no corresponde con el Tipo de operaci�n del pedido")
    '        End If

    '        If tipoOpPedido <> tipoOpRemolque1 Then
    '            Throw New Exception("El tipo de operaci�n del Remolque1: " & id_remolque1 & ", no corresponde con el Tipo de operaci�n del pedido")
    '        End If

    '        If tipoOpPedido <> TipoOpRemolque2 Then
    '            Throw New Exception("El tipo de operaci�n del Remolque2: " & id_remolque2 & ", no corresponde con el Tipo de operaci�n del pedido")
    '        End If

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Guardar
    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
    Public Function Guardar2(ByVal Array() As String) As ArrayList
        Try
            Dim luoMSG As New net_b_despextended.pre_b_despmensajes
            Dim luoParm As New net_b_zamclasesextended.net_b_clasegeneralparametrosextended
            Dim luoUnidad As New net_b_zamclasesextended.net_b_claseunidadextended
            Dim iuo_bandera As New net_b_servicios.net_b_bandera
            Dim bolCommit As Boolean = False
            Dim ldt_unidades, ldtUnidades, ldt_datos_det, ddt_datos_bandera As New Data.DataTable
            Dim intVista As Integer
            Dim lstrMsgUnidad As String = ""
            Dim strFormat As String = ""
            Dim lintValorParam As Integer = 0
            Dim ArrayReturn As New ArrayList
            Dim ArrayReturn2 As New ArrayList
            'Conexi�n
            iBR.str_log = Session("strConn")

            luo_br_layout.str_log = Session("strConn")
            iuoAsignacion.str_log = Session("strConn")
            iuo_bandera.str_log = Session("strConn")
            'Se manda llamar el metodo que valida la informaci�n
            ValidaDatos(Array)

            If Array(33) = "1" Then
                If Array(31) = "" Then
                    Throw New Exception("Favor de Capturar el remitente")
                ElseIf Array(32) = "" Then
                    Throw New Exception("Favor de Capturar el destinatario")
                End If
            End If

            If Trim(Array(1)) = "" Then Array(0) = "0" 'txtUnidad
            If Trim(Array(1)) <> "0" Then
                luoUnidad.ValidaOrdenServicioUnidad(Trim(Array(0)), iBR.str_log, lstrMsgUnidad, False)
                'Se llama a la funci�n GetParametro, se envian como argumentos el nombre del 
                'par�metro, la estructura del LOG como referencia y una variable que indica 
                'si se valida o no que existe el par�metro en la base de datos. emartinez 02-ENE-2009
                lintValorParam = luoParm.GetParametroNumerico("despvalidaordenserviciounidad", iBR.str_log, False)
                Select Case lintValorParam
                    Case 0  'No valida
                    Case 1  'Valida y muestra mensaje, permite continuar
                        If lstrMsgUnidad.Length > 0 Then
                            Me.lblError.Text = "ADVERTENCIA : " & lstrMsgUnidad
                            Me.lblError.Visible = True
                        End If
                    Case 2  'Valida y NO permite continuar
                        If lstrMsgUnidad.Length > 0 Then
                            Throw New Exception(lstrMsgUnidad)
                        End If
                End Select
            End If


            'Se Valida que la Unidad no tenga una Orden de Servicio Abierta, se envian como argumentos la Unidad
            '   y la estructura del LOG como referencia, si el parametro esta activo, muestra el mensaje, permite continuar.
            '   Jdlcruz, GASA 28-Nov-2012, Folio: 17747.
            Dim lstrValidaOrden As String = ""
            Dim lstrDetenerProceso As String = ""
            lstrValidaOrden = luoParm.GetParametro("ValidaOrden_Mantenimiento", iBR.str_log, False)
            If lstrValidaOrden = "S" Then
                lstrDetenerProceso = luoParm.GetParametro("ValidaOrden_DetenerProceso", iBR.str_log, False)
                Dim boolDetener As Boolean = False
                If lstrDetenerProceso = "S" Then
                    boolDetener = True
                End If

                Dim lstrMsgUnidad2 As String = ""
                'Valida sobre el Remolque1 y LineaRemolque1.
                If Trim(Array(12)) <> "" And Trim(Array(11)) = "" Then
                    luoUnidad.ValidaOrdenServicioUnidad(Trim(Array(12)), iBR.str_log, lstrMsgUnidad2, False)

                    If boolDetener = True Then
                        If lstrMsgUnidad2.Length > 0 Then
                            lstrMsgUnidad2 = lstrMsgUnidad2.Replace("La Unidad", "El Remolque")
                            Throw New Exception(lstrMsgUnidad2)
                        End If
                    End If
                ElseIf Trim(Array(12)) <> "" And Trim(Array(11)) <> "" Then
                    luoUnidad.ValidaOrdenServicioUnidadLinea(Trim(Array(12)), Trim(Array(11)), iBR.str_log, lstrMsgUnidad2, False)

                    If boolDetener = True Then
                        If lstrMsgUnidad2.Length > 0 Then
                            Throw New Exception(lstrMsgUnidad2)
                        End If
                    End If
                End If
                'Valida sobre el Remolque2 y LineaRemolque2.
                If Trim(Array(15)) <> "" And Trim(Array(14)) = "" Then
                    luoUnidad.ValidaOrdenServicioUnidad(Trim(Array(15)), iBR.str_log, lstrMsgUnidad2, False)

                    If boolDetener = True Then
                        If lstrMsgUnidad2.Length > 0 Then
                            lstrMsgUnidad2 = lstrMsgUnidad2.Replace("La Unidad", "El Remolque")
                            Throw New Exception(lstrMsgUnidad2)
                        End If
                    End If
                ElseIf Trim(Array(15)) <> "" And Trim(Array(14)) <> "" Then
                    luoUnidad.ValidaOrdenServicioUnidadLinea(Trim(Array(15)), Trim(Array(14)), iBR.str_log, lstrMsgUnidad2, False)

                    If boolDetener = True Then
                        If lstrMsgUnidad2.Length > 0 Then
                            Throw New Exception(lstrMsgUnidad2)
                        End If
                    End If
                End If

            End If

            'Si no envia el id_asignacion, 
            If Trim(Array(0)) = "" Then  'Request("ParmAsignHdn") Is Nothing Then
                'Se ingresa la informaci�n de la Nueva Asignaci�n
                Call Ingresar2(Array, ArrayReturn)
                iBR.SetSeguimientoActual(Trim(Array(1)), iBR.str_log, True, luoParm.GetParametro("ActSegTercero", iBR.AsignacionClass.str_log, False))
            Else
                'Actualiza la informaci�n de la Asignaci�n
                Call Actualizar(Array, ArrayReturn)
                'Se actualiza el Seguimiento
                iBR.SetSeguimientoActual(Trim(Array(1)), iBR.str_log, True, luoParm.GetParametro("ActSegTercero", iBR.AsignacionClass.str_log, False))
                'En caso de que haya cambiado de unidad
                If Trim(Array(1)) <> Trim(Array(26)) Then
                    'Se actualiza la informaci�n de la Unidad Original
                    iBR.SetSeguimientoActual(Trim(Array(26)), iBR.str_log, True, luoParm.GetParametro("ActSegTercero", iBR.AsignacionClass.str_log, False))
                End If
            End If

            'Si no tiene Asignaci�n, entonces se crea una nueva
            If Array(0) = "" Then
                'Obtiene el nuevo id_asignacion
                Array(0) = iBR.AsignacionClass.Asignacion
                ArrayReturn.Item(0) = iBR.AsignacionClass.Asignacion
                ArrayReturn.Item(1) = "NuevoRow"
            Else
                ArrayReturn.Item(0) = iBR.AsignacionClass.Asignacion
                ArrayReturn.Item(1) = "ActualizaRow"
            End If

            'Se cruzan las fechas
            Select Case Array(27)
                Case "dd/MM/yyyy HH:mm"
                    strFormat = "MM/dd/yyyy HH:mm"
                Case "MM/dd/yyyy HH:mm"
                    strFormat = "dd/MM/yyyy HH:mm"
            End Select

            Dim ldt_pedidos, ldtPedidos As New Data.DataTable
            luo_br_layout.str_log = Session("StrConn")
            luo_br_layout.id_ventana = 62
            luo_br_layout.idusuario = Session("UserName")
            ldt_pedidos = luo_br_layout.ObtenerVistaUsuario()

            If ldt_pedidos.Rows.Count = 1 Then
                Me.ColumnasPedidos(ldt_pedidos.Rows(0).Item("id_vista"), ldtPedidos, Array(0))
                intVista = ldt_pedidos.Rows(0).Item("id_vista")

            ElseIf ldt_pedidos.Rows.Count > 1 Then
                Me.ColumnasPedidos(ldt_pedidos.Rows(ldt_pedidos.Rows.Count - 1).Item("id_vista"), ldtPedidos, Array(0))
                intVista = ldt_pedidos.Rows(ldt_pedidos.Rows.Count - 1).Item("id_vista")

            ElseIf ldt_pedidos.Rows.Count = 0 Then
                luo_br_layout.id_vista = 62     'Agenda de Viajes
                luo_br_layout.id_ventana = 62   'Agenda de Viajes
                ddt_datos_vista = luo_br_layout.ObtenerVista()
                luo_br_layout.id_vista = ddt_datos_vista.Rows(0).Item("id_vista")
                intVista = ddt_datos_vista.Rows(0).Item("id_vista")
                luo_br_layout.idusuario = Session("UserName")
                Me.ColumnasPedidos(ddt_datos_vista.Rows(0).Item("id_vista"), ldtPedidos, Array(0))
            End If

            luo_br_layout.id_vista = intVista
            ldt_datos_det = luo_br_layout.ObtenerVistaDetalle()

            'ArrayReturn.Add(ldtPedidos.Rows.Count)    'ArrayReturn[0]
            iBR.AsignacionClass.GetAsignacion(ArrayReturn(0), iBR.AsignacionClass, iBR.str_log)
            If iBR.AsignacionClass.ListaPedidosPK.Count = 1 Then
                ArrayReturn2.Add("UNO")
            ElseIf iBR.AsignacionClass.ListaPedidosPK.Count > 1 Then
                ArrayReturn2.Add("MAS")
            End If

            If ldtPedidos.Rows.Count > 0 Then
                For Each lrow As Data.DataRow In ldtPedidos.Rows
                    For Each registro As Data.DataRow In ldt_datos_det.Rows
                        Dim uwgColumn As New Infragistics.WebUI.UltraWebGrid.UltraGridColumn
                        Dim strCell As String = ""
                        If Not lrow.Item(registro("alias")) Is DBNull.Value Then
                            Dim strFecha As String
                            If registro("tipo_dato") = "datetime" Then
                                strFecha = Format(lrow.Item(registro("alias")), strFormat)
                                strCell = registro("alias") & "," & strFecha & "," & registro("tipo_dato")
                            Else
                                Select Case registro("alias")
                                    Case "tipo_serv"
                                        If registro("alias") = "tipo_serv" Then
                                            Select Case CStr(lrow.Item(registro("alias")))
                                                Case "E"
                                                    lrow.Item(registro("alias")) = "Exportaci�n"
                                                Case "I"
                                                    lrow.Item(registro("alias")) = "Importaci�n"
                                                Case "D"
                                                    lrow.Item(registro("alias")) = "Dom�stico"
                                                Case "C"
                                                    lrow.Item(registro("alias")) = "Cruce"
                                                Case Else
                                                    lrow.Item(registro("alias")) = ""
                                            End Select
                                            strCell = registro("alias") & "," & CStr(lrow.Item(registro("alias"))) & "," & registro("alias")
                                        End If
                                    Case Else
                                        strCell = registro("alias") & "," & CStr(lrow.Item(registro("alias"))) & "," & registro("alias")
                                End Select
                            End If
                        Else
                            strCell = registro("alias") & "," & "" & "," & registro("tipo_dato") & "," & ""
                        End If
                        ArrayReturn2.Add(strCell)
                    Next
                    ArrayReturn2.Add("NuevoRow")
                Next
            End If

            'ArrayReturn(0) = Num_asignacion
            'ArrayReturn(1) = Indica si es un nuevo registro o si solo actualiza
            'ArrayReturn(2) = Indica si agrupa o agrega nuevo row
            'ArrayReturn(>2)= Parametros a Actualizar en Grid de Unidades
            Return ArrayReturn2

        Catch ex As Exception
            Dim arraylist As New ArrayList
            arraylist.Add("Error")
            arraylist.Add(ex.Message)
            arraylist.Add(Array)
            Return arraylist
            Throw ex
        End Try
    End Function

    'Asigna la informaci�n de las columnas de los Pedidos para la pantalla de Agenda de Viajes, recibe como argumentos
    'el identificador de la Vista, un DataTable de referencia y el N�mero de Viaje.
    Private Sub ColumnasPedidos(ByVal idvista As Integer, ByRef ldt_pedidos As Data.DataTable, ByVal aintNoViaje As Integer)
        Dim ldt_datos_det As New Data.DataTable
        Dim ldt_datos_query As New Data.DataTable
        Dim lCount As Integer = 0
        'If Application("strConn") = Nothing Then Response.Redirect(Application("myRoot") & "/relogin.aspx")
        luo_br_layout.id_vista = idvista 'Me.wcVistas.SelectedRow.Cells.FromKey("id_vista").Value
        luo_br_layout.str_log = Session("StrConn")
        ldt_datos_det = luo_br_layout.ObtenerVistaDetalle()
        ldt_datos_query = luo_br_layout.ObtenerQuery()

        If ldt_datos_query.Rows.Count > 0 Then
            If ldt_datos_query.Rows(0).Item("dselect") = "" Then Throw New Exception("Falta definir una vista para los Pedidos")
            ldt_pedidos = luo_br_layout.ObtenerQuery("SELECT " & ldt_datos_query.Rows(0).Item("dselect"), ldt_datos_query.Rows(0).Item("dfrom"), ldt_datos_query.Rows(0).Item("dwhere") & " AND DA.id_asignacion IN (" & aintNoViaje & ")")
        End If
    End Sub

    Public Sub Ingresar2(ByVal str() As String, ByRef strReturn As ArrayList)
        Dim arrListaPedidos As New ArrayList
        Dim arrListaPedidosAreas As New ArrayList
        Dim arrListaPedidosPK As New ArrayList
        Dim arraylistPedidos As New ArrayList
        Dim arraylistPedidosArea As New ArrayList
        Dim luo_BR As New net_b_despextended.pre_b_despcrearviaje
        Dim luoTipoOperacion As New net_b_despextended.ede_b_desptipooperacion

        Dim liAreaTracto As Integer
        If str(46) = "S" Then
            liAreaTracto = iBR.AreaXTracto(str(1), iBR.str_log)
        Else
            liAreaTracto = Session("vg_area")
        End If
        iBR.AsignacionClass.Area = liAreaTracto
        iBR.AsignacionClass.Ingreso = Session("UserName")
        iBR.AsignacionClass.Fecha_ingreso = Date.Now
        iBR.AsignacionClass.str_log = Session("strConn")
        luo_BR.str_log = iBR.AsignacionClass.str_log
        'Pasa la lista de pedidos a un arreglo
        iBR.PasarAArreglo(str(2), "[;]", arrListaPedidos, iBR.str_log) 'Lista 1
        iBR.PasarAArreglo(str(3), "[;]", arrListaPedidosAreas, iBR.str_log) 'Lista 2
        iBR.PasarAArreglo(str(4), "[;]", arrListaPedidosPK, iBR.str_log) 'Lista 3
        Dim luoParmtaAux = New net_b_zamclasesextended.net_b_clasegeneralparametrosextended

        Dim lstrAreasTPX As String = ""
        lstrAreasTPX = luoParm.GetParametro("areas_transpormex", iBR.str_log, False)
        If lstrAreasTPX = "S" Then
            If arrListaPedidosAreas.Count > 0 Then
                Dim intAreaPedidoTPX As Integer = Val(arrListaPedidosAreas(0))
                If arrListaPedidosAreas.Count > 1 Then
                    Dim intAreaComp As Integer = 0
                    For i As Integer = 1 To arrListaPedidosAreas.Count - 1
                        intAreaComp = Val(arrListaPedidosAreas(i))
                        If intAreaComp <> intAreaPedidoTPX Then
                            Throw New Exception("Todos los pedidos deben de pertenecer a la misma �rea")
                        End If
                    Next
                End If
                iBR.AsignacionClass.Area = intAreaPedidoTPX
            End If
        End If
        iBR.AbrirNuevo(iBR.AsignacionClass.Area, arrListaPedidos, arrListaPedidosAreas, str(1))

        If iBR.id_tipo_operacion_pedidos <> 0 Then
            iBR.AsignacionClass.Id_Tipo_Operacion = iBR.id_tipo_operacion_pedidos
        End If
        iBR.AsignacionClass.Ruta = str(5)                                   'Me.txtRuta.Text
        iBR.AsignacionClass.F_prog_ini_viaje = CDate(str(6))                'Me.wdcInicioViaje.Value
        iBR.AsignacionClass.F_prog_fin_viaje = CDate(str(7))                'Me.wdcFinViaje.Value
        iBR.AsignacionClass.Ingreso = str(8)                                'Me.txtIngreso.Text
        iBR.AsignacionClass.HrsEstandar = str(9)                            'Me.txtHrsEstandar.Text
        iBR.AsignacionClass.Kit = str(10)                                   'Me.txtKit.Text
        If Trim(str(1)) = "" Then
            iBR.AsignacionClass.Tractor = "0"
        Else
            iBR.AsignacionClass.Tractor = str(1)                            'Me.txtUnidad.Text
        End If
        If iBR.AsignacionClass.Tractor <> "0" Then
            'Busca el parametro de Multicompania MILAC, Folio : 15491
            luoParm = New net_b_zamclasesextended.net_b_clasegeneralparametrosextended
            lstrMultiCia = luoParm.GetParametro("despmulticompania", iBR.str_log, False)
            If UCase(lstrMultiCia) = "S" Then
                'Valida la informacion de los Pedidos y la Unidad, se envian como argumentos el Listado 
                'de Pedidos, el Listado de Areas, la Unidad y la estructura del LOG como referencia.
                iBR.ValidaCompaniaPedidosUnidad(arrListaPedidos, arrListaPedidosAreas, iBR.AsignacionClass.Tractor, iBR.str_log)
            End If
        End If

        iBR.AsignacionClass.LineaRem1 = str(11)                             'Me.txtLineaRem1.Text
        iBR.AsignacionClass.Remolque1 = str(12)                             'Me.txtRemolque1.Text
        iBR.AsignacionClass.Dolly = str(13)                                 'Me.txtDolly.Text
        iBR.AsignacionClass.LineaRem2 = str(14)                             'Me.txtLineaRem2.Text
        iBR.AsignacionClass.Remolque2 = str(15)                             'Me.txtRemolque2.Text

        If iBR.AsignacionClass.Dolly.Length > 0 And iBR.AsignacionClass.Remolque1.Length = 0 Then
            Throw New Exception("Debe capturar el Remolque1 o limpiar el campo de Dolly")
        End If

        If iBR.AsignacionClass.Remolque2.Length > 0 Then
            If iBR.AsignacionClass.Remolque1.Length = 0 Then
                Throw New Exception("No es posible capturar el Remolque 2 cuando no se ha capturado el Remolque 1")
            Else
                If iBR.AsignacionClass.Remolque2 = iBR.AsignacionClass.Remolque1 And iBR.AsignacionClass.LineaRem2 = iBR.AsignacionClass.LineaRem1 Then
                    Throw New Exception("El Remolque 2 debe ser diferente al Remolque 1.")
                End If
            End If
        End If

        If Trim(iBR.AsignacionClass.Remolque1) <> "" Or iBR.AsignacionClass.LineaRem1 <> "" Then
            iBR.ValidaRemolque(iBR.AsignacionClass.LineaRem1, iBR.AsignacionClass.Remolque1, iBR.str_log) 'valida si existe
        End If
        If Trim(iBR.AsignacionClass.Remolque2) <> "" Or iBR.AsignacionClass.LineaRem2 <> "" Then
            iBR.ValidaRemolque(iBR.AsignacionClass.LineaRem2, iBR.AsignacionClass.Remolque2, iBR.str_log) 'valida si existe
        End If
        If IsNumeric(str(16)) Then
            iBR.AsignacionClass.Id_Flota = str(16) 'txtFlota
        Else
            str(16) = 0
            iBR.AsignacionClass.Id_Flota = str(16)
        End If

        iBR.AsignacionClass.Id_configuracionviaje = str(17) 'Me.ddlConfigViaje.SelectedValue

        'inicializa el valor del  operador 1 en caso de que sea vacio lo pone como cero 
        If Trim(str(18)) = "" Then
            iBR.AsignacionClass.Operador1 = 0
        Else
            iBR.AsignacionClass.Operador1 = Trim(str(18))
        End If

        'Folio:20957, Par�metro para validaci�n de la fecha de vigencia de la licencia del Operador
        'emartinez 08.May.2015, Cambios para ONTIME
        Me.ParmValidaFechVigLicOper = UCase(luoParm.GetParametro("despvalidavigencialicenciaoper", iBR.str_log, False))
        If Me.ParmValidaFechVigLicOper = "S" And iBR.AsignacionClass.Operador1 <> 0 Then
            'Se llama a la funci�n que valida la vigencia de la licencia del Operador, se envian como 
            'argumentos el Identificador del Operador, la Fecha de Inicio de Viaje Programado y 
            'la estructura del LOG como referencia.
            iBR.ValidaVigenciaLicenciaOperador(iBR.AsignacionClass.Operador1, iBR.AsignacionClass.F_prog_ini_viaje, iBR.str_log)
        End If

        'inicializa el valor del  operador 2 en caso de que sea vacio lo pone como cero 
        If Trim(str(19)) = "" Then
            iBR.AsignacionClass.Operador2 = 0
        Else
            iBR.AsignacionClass.Operador2 = Trim(str(19))
        End If
        If iBR.AsignacionClass.Operador2 <> 0 Then
            If iBR.AsignacionClass.Operador2 = iBR.AsignacionClass.Operador1 Then
                Throw New Exception("No es posible capturar dos veces el mismo operador.")
            End If
        End If

        'GGUERRA FOLIO:21254
        If strParmViajesPendientes = "S" Then
            Dim CantidadViajes As Integer
            'OPERADO0R 1

            If Trim(str(18)) = "" Then

                Throw New Exception("Favor de capturar un operador valido")
            Else
                If iBR.AsignacionClass.Operador1 > 0 Then
                    CantidadViajes = iBR.ValidarViajesPendientesXLiquidar(iBR.AsignacionClass.Operador1, iBR.str_log)
                    If CantidadViajes >= intParmLimiteViajesPen Then
                        Throw New Exception("No es posible guardar la asignaci�n, existen viajes pendientes por liquidar del operador: " & iBR.AsignacionClass.OperadorNombre)
                    End If
                End If
            End If

            If Trim(str(19)) = "" Then

                Throw New Exception("Favor de capturar un operador valido")
            Else
                If iBR.AsignacionClass.Operador2 > 0 Then
                    CantidadViajes = iBR.ValidarViajesPendientesXLiquidar(iBR.AsignacionClass.Operador2, iBR.str_log)
                    If CantidadViajes >= intParmLimiteViajesPen Then
                        Throw New Exception("No es posible guardar la asignaci�n, existen viajes pendientes por liquidar del operador: " & iBR.AsignacionClass.Operador2Nombre)
                    End If
                End If

            End If
        End If

        'GGUERRA FOLIO:22765 Valida el l�mite de Asignaciones Pendientes por Unidad
        If strParmValidaMaxAsign = "S" Then
            Dim CantidadAsignaciones As Integer
            'Unidad
            If Trim(str(1)) = "" Then
                Throw New Exception("Favor de capturar una Unidad v�lida")
            Else
                CantidadAsignaciones = iBR.ValidarAsignacionesPendientesXUnidad(iBR.AsignacionClass.Tractor, iBR.str_log)
                If CantidadAsignaciones >= intParmLimiteViajesPen Then
                    Throw New Exception("No es posible guardar la Asignaci�n, existen Asignaciones pendientes de la Unidad: " & iBR.AsignacionClass.Tractor)
                End If
            End If
        End If

        'Folio:20957, Par�metro para validaci�n de la fecha de vigencia de la licencia del Operador, emartinez 08.May.2015, Cambios para ONTIME
        If Me.ParmValidaFechVigLicOper = "S" And iBR.AsignacionClass.Operador2 <> 0 Then
            'Se llama a la funci�n que valida la vigencia de la licencia del Operador, se envian como 
            'argumentos el Identificador del Operador, la Fecha de Inicio de Viaje Programado y 
            'la estructura del LOG como referencia.
            iBR.ValidaVigenciaLicenciaOperador(iBR.AsignacionClass.Operador2, iBR.AsignacionClass.F_prog_ini_viaje, iBR.str_log)
        End If

        '************************************************************************
        '********************************************************************************
        If Me.ParmValidaFechVigLicOper = "S" Then
            iBR.ValidaVigenciaLicenciaOperador(iBR.AsignacionClass.Operador2, iBR.AsignacionClass.F_prog_ini_viaje, iBR.str_log)
        End If

        If Trim(str(20)) = "" Then
            iBR.AsignacionClass.Observaciones = ""
        Else
            iBR.AsignacionClass.Observaciones = Trim(str(20))
        End If

        iBR.ListaPedidos = arrListaPedidos
        iBR.ListaPedidosAreas = arrListaPedidosAreas
        iBR.ListaPedidosPK = arrListaPedidosPK

        If arrListaPedidos.Count = 0 Then
            arrListaPedidos.Add("vacio")
            iBR.ListaPedidos = arrListaPedidos
        End If

        strReturn.Add(iBR.AsignacionClass.Viaje) '0  'Me.txtNumAsignacion.Text =
        strReturn.Add("") '1 
        strReturn.Add("") '2
        strReturn.Add(arrListaPedidosPK.Count) '3
        If Trim(str(21)) = "" Or Trim(str(21)) = "0" Then
            Throw New Exception("Favor de seleccionar un tipo de seguimiento valido.")
        End If
        iBR.AsignacionClass.SeguimientoNombreCorto = Trim(str(21)) '= Me.txtSeguimiento.Text

        'Se agrega la validaci�n del par�metro de SU TRANSPORTE para actualizar la Fecha Recibido en 
        'los Pedidos de la Asignaci�n, emartinez 30-MARZO-2009
        Dim luoParam As New net_b_zamclasesextended.net_b_clasegeneralparametrosextended
        Dim lstrParam As String = ""
        'Se llama a la funci�n GetParametro, se envian como argumentos el nombre del 
        'par�metro, la estructura del LOG como referencia y una variable que indica 
        'si se valida o no que existe el par�metro en la base de datos.
        lstrParam = luoParam.GetParametro("CAMBIOS_SUTRANSPORTE", iBR.str_log, False)
        If lstrParam = "S" Then iBR.AsignacionClass.ActualizaFechaRecibido = True

        'Se agregan los datos de CapacidadKG y CapacidadM3, emartinez , 20/Jun/2012
        iBR.AsignacionClass.Capacidad_CargaM3 = str(29)
        iBR.AsignacionClass.Capacidad_CargaKG = str(30)

        If str(31) <> "" Then
            iBR.AsignacionClass.Id_remitente = str(31)
        Else
            iBR.AsignacionClass.Id_remitente = "0"
        End If

        If str(32) <> "" Then
            iBR.AsignacionClass.Id_destinatario = str(32)
        Else
            iBR.AsignacionClass.Id_destinatario = "0"
        End If
        str_activatipooperacion = luoParmtaAux.GetParametro("tipooperasigtpx", iBR.str_log, False)

        'Ingresa el Tipo de Operaci�n en caso de existir
        If str_activatipooperacion = "S" Then
            If str.Length > 34 Then
                If str(43) <> "" And str(43) <> "0" And str(43) <> "1" Then
                    iBR.AsignacionClass.Id_Tipo_Operacion = str(43)
                Else
                    iBR.AsignacionClass.Id_Tipo_Operacion = "1"
                End If
            End If
        End If
        'megallegos -> Ingresa el Tipo de Operaci�n en caso de existir // Folio: 22277 - SIMSA
        If strTipoOp_AsigVV = "S" Then
            If str(43) <> "" And str(43) <> "0" Then
                iBR.AsignacionClass.Id_Tipo_Operacion = str(43)
            Else
                iBR.AsignacionClass.Id_Tipo_Operacion = "0"
            End If
        End If
        If arrListaPedidos(0).ToString.ToUpper.Trim = "VACIO" Then
            'Se hace cambio de TRANSPORMEX para tomar el �rea del Tipo de operaci�n
            'Valida si obtiene el �rea del Usuario o del Tipo de operacion
            If lstrAreasTPX = "S" Then
                If str(43) <> "" And str(43) <> "0" Then
                    iBR.AsignacionClass.Area = luoTipoOperacion.GetAreaTipoOperacion(str(43), iBR.str_log)
                End If
            End If

        End If

        If luoParmtaAux.GetParametro("filtra_unidades_x_t_operacion", iBR.str_log, False) = "S" And iBR.AsignacionClass.ListaPedidos.Item(0).ToString.Trim.ToUpper <> "VACIO" Then
            If arrListaPedidos.Count > 0 Then
                Dim tOpPedido As Integer
                Dim validaOperacion = iBR.ValidaTipoOperacionPedido(iBR.AsignacionClass.ListaPedidos.Item(0), iBR.AsignacionClass.ListaPedidosAreas.Item(0), iBR.AsignacionClass.Tractor, iBR.AsignacionClass.Remolque1, iBR.AsignacionClass.Remolque2, iBR.str_log, tOpPedido)
                Dim descOpPedido = iBR.getDescTipoOperacion(tOpPedido, iBR.str_log)
                Select Case validaOperacion
                    Case 1
                        Throw New Exception("El tipo de Operaci�n del Pedido es: " & descOpPedido & " ,y no coincide con el tipo de operaci�n de la Unidad: " & iBR.AsignacionClass.Tractor)

                    Case 2
                        Throw New Exception("El tipo de Operaci�n del Pedido es: " & descOpPedido & " ,y no coincide con el tipo de operaci�n de la Unidad: " & iBR.AsignacionClass.Remolque1)

                    Case 3
                        Throw New Exception("El tipo de Operaci�n del Pedido es: " & descOpPedido & " ,y no coincide con el tipo de operaci�n de la Unidad: " & iBR.AsignacionClass.Remolque2)
                End Select
            End If
        End If

        'megallegos 20/01/17 -> Folio:22791 // MTC
        strtipoOpPed = luoParm.GetParametro("tipoOpPed", iBR.str_log, False)
        If strtipoOpPed = "S" Then
            If str(43) <> "" And str(43) <> "0" And str(43) <> "1" Then
                iBR.AsignacionClass.Id_Tipo_Operacion = str(43)
            Else
                iBR.AsignacionClass.Id_Tipo_Operacion = "1"
            End If
        End If        

        'jmartin folio: 23121   17/10/2017
        'fusion tlocal OperacionesV8
        If str.Length > 52 Then
            If (str(53).ToLower <> "ul") Then
                iBR.AsignacionClass.ClasificacionMovimiento = str(53)
            Else
                iBR.AsignacionClass.ClasificacionMovimiento = 0
            End If
        End If

        'rherrera 
        Dim lstrViajeRegreso As String = ""
        lstrViajeRegreso = luoParm.GetParametro("CalculoValesCombustibleAuto", iBR.str_log, False)
        If lstrViajeRegreso = "S" Then
            If str(51) <> 0 Then
                iBR.AsignacionClass.viaje_regreso = str(51)
            End If
        End If

        iBR.AsignacionClass.IdVentanaChrysler = str(45)

        'AGONZALEZ FOLIO:24443 30/05/2018
        If str.Length > 57 Then
            If (str(57).ToLower <> "ul") Then
                iBR.AsignacionClass.IdVentanaChrysler = str(45)
            Else
                iBR.AsignacionClass.IdVentanaChrysler = "NULL"
            End If

            iBR.AsignacionClass.VelMax = str(58) 'Me.txtVelMax.Text
        End If
        'PHERNANDEZ 23/08/2018 Validamos clasificacion de ruta, cambios TGARCIA
        lstr_TipoRuta = luoParm.GetParametro("clasifica_tipo_ruta", iBR.str_log, False)
        If lstr_TipoRuta = "S" Then
            iBR.AsignacionClass.clasificacion_ruta = str(59)
            iBR.AsignacionClass.clasificacion_carga_diesel = str(60)
            iBR.AsignacionClass.ParamTipoRuta = "S"
        End If

        'NMANUEL 20/09/18 FOLIO: 24496
        Dim lstr_TipoArmado As String = luoParm.GetParametro("HABCAMPOS_INTERFAZLANDSTAR", iBR.str_log, False)
        If lstr_TipoArmado = "S" Then
            iBR.AsignacionClass.tipo_armado = str(61)
        End If

        iBR.CrearAsignacion(Trim(str(22)), Trim(str(23)), Trim(str(24)), Trim(str(25)))

    End Sub

    'Actualiza la informaci�n de la Asignaci�n, recibe como argumentos un arreglo de string con la informaci�n de 
    'la pantalla de asignaci�n y un arraylist como referencia para regresar valores
    Public Sub Actualizar(ByVal str() As String, ByRef strReturn As ArrayList)
        Dim luo_BR As New net_b_despextended.pre_b_despcrearviaje
        iBR.AsignacionClass.GetAsignacion(str(0), iBR.AsignacionClass, iBR.str_log)
        luo_BR.str_log = iBR.str_log
        iBR.AsignacionClass.Ruta = str(5) 'Me.txtRuta.Text
        iBR.AsignacionClass.F_prog_ini_viaje = str(6) 'Me.wdcInicioViaje.Value
        iBR.AsignacionClass.F_prog_fin_viaje = str(7) 'Me.wdcFinViaje.Value
        Dim luoParmtaAux = New net_b_zamclasesextended.net_b_clasegeneralparametrosextended
        Dim lstrParamTresFronteras As String
        If Trim(str(1)) = "" Then
            iBR.AsignacionClass.Tractor = "0"
        Else
            iBR.AsignacionClass.Tractor = str(1) 'Me.txtUnidad.Text
        End If

        If Trim(str(21)) = "" Then
            Throw New Exception("Favor de seleccionar un tipo de seguimiento valido.")
        End If
        'Se agrega la validacion de la Multicompania, valida la informacion de la Asignacion y la Unidad.
        If iBR.AsignacionClass.Tractor <> "0" Then
            luoParm = New net_b_zamclasesextended.net_b_clasegeneralparametrosextended
            lstrMultiCia = luoParm.GetParametro("despmulticompania", luo_BR.str_log, False)
            If UCase(lstrMultiCia) = "S" Then
                'Obtiene la validacion directamente con la informacion de la Asignacion, se envian como 
                'argumentos la Asignacion, la Unidad y la estructura del LOG como referencia.
                luo_BR.ValidaCompaniaAsignacionUnidad(str(0), iBR.AsignacionClass.Tractor, luo_BR.str_log)
            End If
        End If

        iBR.AsignacionClass.LineaRem1 = str(11) 'Me.txtLineaRem1.Text
        iBR.AsignacionClass.Remolque1 = str(12) 'Me.txtRemolque1.Text
        iBR.AsignacionClass.Dolly = str(13) 'Me.txtDolly.Text
        iBR.AsignacionClass.LineaRem2 = str(14) 'Me.txtLineaRem2.Text
        iBR.AsignacionClass.Remolque2 = str(15) 'Me.txtRemolque2.Text
        iBR.AsignacionClass.Id_configuracionviaje = str(17) 'Me.ddlConfigViaje.SelectedValue
        iBR.AsignacionClass.Kit = str(10) 'Me.txtKit.Text

        If iBR.AsignacionClass.Dolly.Length > 0 And iBR.AsignacionClass.Remolque1.Length = 0 Then
            Throw New Exception("Debe capturar el remolque1 � limpiar el campo de Dolly")
        End If

        If iBR.AsignacionClass.Remolque2.Length > 0 Then
            If iBR.AsignacionClass.Remolque1.Length = 0 Then
                Throw New Exception("No es posible capturar el Remolque 2 cuando no se ha capturado el Remolque 1")
            Else
                If iBR.AsignacionClass.Remolque2 = iBR.AsignacionClass.Remolque1 And iBR.AsignacionClass.LineaRem2 = iBR.AsignacionClass.LineaRem1 Then
                    Throw New Exception("El Remolque 2 debe ser diferente al Remolque 1.")
                End If
            End If
        End If

        'MSALVARADO 10/10/2017 FOLIO: 23507
        If luoParm.GetParametro("habilitarFuncionalidadesAEO", luo_BR.str_log, False) = "S" Then
            'Se modifica la funci�n para que solamente se valide la informaci�n del Remolque Propio, los Remolques Americanos
            'no se validan ya que se dan de alta cuando no existe la informaci�n en el Cat�logo de Remolques Americanos
            'emartinez 16.Jul.2015
            If Trim(iBR.AsignacionClass.Remolque1) <> "" And Trim(iBR.AsignacionClass.LineaRem1).Length = 0 Then
                iBR.ValidaRemolque(iBR.AsignacionClass.LineaRem1, iBR.AsignacionClass.Remolque1, iBR.str_log) 'valida si existe
            End If
            If Trim(iBR.AsignacionClass.Remolque2) <> "" And Trim(iBR.AsignacionClass.LineaRem2).Length = 0 Then
                iBR.ValidaRemolque(iBR.AsignacionClass.LineaRem2, iBR.AsignacionClass.Remolque2, iBR.str_log) 'valida si existe
            End If
        Else
            If Trim(iBR.AsignacionClass.Remolque1) <> "" Or iBR.AsignacionClass.LineaRem1 <> "" Then
                iBR.ValidaRemolque(iBR.AsignacionClass.LineaRem1, iBR.AsignacionClass.Remolque1, iBR.str_log) 'valida si existe
            End If
            If Trim(iBR.AsignacionClass.Remolque2) <> "" Or iBR.AsignacionClass.LineaRem2 <> "" Then
                iBR.ValidaRemolque(iBR.AsignacionClass.LineaRem2, iBR.AsignacionClass.Remolque2, iBR.str_log) 'valida si existe)
            End If
        End If

        If IsNumeric(str(16)) Then
            iBR.AsignacionClass.Id_Flota = str(16) 'txtFlota
        End If
        'inicializa el valor del  operador 1 en caso de que sea vacio lo pone como cero 
        If Trim(str(18)) = "" Then
            iBR.AsignacionClass.Operador1 = 0
        Else
            iBR.AsignacionClass.Operador1 = str(18) 'Me.txtOperador1.Text
        End If
        'Folio:20957, Par�metro para validaci�n de la fecha de vigencia de la licencia del Operador
        'emartinez 08.May.2015, Cambios para ONTIME
        Me.ParmValidaFechVigLicOper = UCase(luoParm.GetParametro("despvalidavigencialicenciaoper", iBR.str_log, False))
        If Me.ParmValidaFechVigLicOper = "S" And iBR.AsignacionClass.Operador1 <> 0 Then
            'Se llama a la funci�n que valida la vigencia de la licencia del Operador, se envian como 
            'argumentos el Identificador del Operador, la Fecha de Inicio de Viaje Programado y 
            'la estructura del LOG como referencia.
            iBR.ValidaVigenciaLicenciaOperador(iBR.AsignacionClass.Operador1, iBR.AsignacionClass.F_prog_ini_viaje, iBR.str_log)
        End If

        'inicializa el valor del  operador 2 en caso de que sea vacio lo pone como cero 
        If Trim(str(19)) = "" Then
            iBR.AsignacionClass.Operador2 = 0
        Else
            iBR.AsignacionClass.Operador2 = str(19) 'Me.txtOperador2.Text
        End If
        If iBR.AsignacionClass.Operador2 <> 0 Then
            If iBR.AsignacionClass.Operador2 = iBR.AsignacionClass.Operador1 Then
                Throw New Exception("No es posible capturar dos veces el mismo operador.")
            End If
        End If

        'GGUERRA FOLIO:21254
        If strParmViajesPendientes = "S" Then
            Dim CantidadViajes As Integer
            'OPERADO0R 1

            If Trim(str(18)) = "" Then

                Throw New Exception("Favor de capturar un operador valido")
            Else
                If iBR.AsignacionClass.Operador1 > 0 Then
                    CantidadViajes = iBR.ValidarViajesPendientesXLiquidar(iBR.AsignacionClass.Operador1, iBR.str_log)
                    If CantidadViajes >= intParmLimiteViajesPen Then
                        Throw New Exception("No es posible guardar la asignaci�n, existen viajes pendientes por liquidar del operador: " & iBR.AsignacionClass.OperadorNombre)
                    End If
                End If
            End If

            If Trim(str(19)) = "" Then

                Throw New Exception("Favor de capturar un operador valido")
            Else
                If iBR.AsignacionClass.Operador2 > 0 Then
                    CantidadViajes = iBR.ValidarViajesPendientesXLiquidar(iBR.AsignacionClass.Operador2, iBR.str_log)
                    If CantidadViajes >= intParmLimiteViajesPen Then
                        Throw New Exception("No es posible guardar la asignaci�n, existen viajes pendientes por liquidar del operador: " & iBR.AsignacionClass.Operador2Nombre)
                    End If
                End If

            End If
        End If

        'GGUERRA FOLIO:22765
        strParmValidaMaxAsign = luoParm.GetParametro("validamaxasigporunidad", iBR.str_log, False)

        intParmLimiteViajesPen = luoParm.GetParametroNumerico("trafCantidadViajesPend", iBR.str_log, False)
        If intParmLimiteViajesPen = Nothing Then intParmLimiteViajesPen = 0

        'GGUERRA FOLIO:22765 Valida el l�mite de Asignaciones Pendientes por Unidad
        If strParmValidaMaxAsign = "S" Then
            Dim CantidadAsignaciones As Integer
            'Unidad
            If Trim(str(1)) = "" Then
                Throw New Exception("Favor de capturar una Unidad v�lida")
            Else
                CantidadAsignaciones = iBR.ValidarAsignacionesPendientesXUnidad(iBR.AsignacionClass.Tractor, iBR.str_log)
                If CantidadAsignaciones >= intParmLimiteViajesPen Then
                    Throw New Exception("No es posible guardar la Asignaci�n, existen Asignaciones pendientes de la Unidad: " & iBR.AsignacionClass.Tractor)
                End If
            End If
        End If

        'Folio:20957, Par�metro para validaci�n de la fecha de vigencia de la licencia del Operador
        'emartinez 08.May.2015, Cambios para ONTIME
        If Me.ParmValidaFechVigLicOper = "S" And iBR.AsignacionClass.Operador2 <> 0 Then
            'Se llama a la funci�n que valida la vigencia de la licencia del Operador, se envian como 
            'argumentos el Identificador del Operador, la Fecha de Inicio de Viaje Programado y 
            'la estructura del LOG como referencia.
            iBR.ValidaVigenciaLicenciaOperador(iBR.AsignacionClass.Operador2, iBR.AsignacionClass.F_prog_ini_viaje, iBR.str_log)
        End If

        If Trim(str(20)) = "" Then
            iBR.AsignacionClass.Observaciones = ""
        Else
            iBR.AsignacionClass.Observaciones = str(20) 'Me.txtObservaciones.Text
        End If

        If str(31) <> "" Then
            iBR.AsignacionClass.Id_remitente = str(31)
        Else
            iBR.AsignacionClass.Id_remitente = "0"
        End If

        If str(32) <> "" Then
            iBR.AsignacionClass.Id_destinatario = str(32)
        Else
            iBR.AsignacionClass.Id_destinatario = "0"
        End If
        str_activatipooperacion = luoParmtaAux.GetParametro("tipooperasigtpx", luo_BR.str_log, False)
        If str_activatipooperacion = "S" Then
            'Ingresa el Tipo de Operaci�n en caso de existir
            If str.Length > 34 Then
                If str(43) <> "" And str(43) <> "0" And str(43) <> "1" Then
                    iBR.AsignacionClass.Id_Tipo_Operacion = str(43)
                Else
                    iBR.AsignacionClass.Id_Tipo_Operacion = "1"
                End If
            End If
        End If
        'megallegos -> Ingresa el Tipo de Operaci�n en caso de existir // Folio: 22277 - SIMSA
        If strTipoOp_AsigVV = "S" Then
            If str.Length > 34 Then
                If str(43) <> "" And str(43) <> "0" Then
                    iBR.AsignacionClass.Id_Tipo_Operacion = str(43)
                Else
                    iBR.AsignacionClass.Id_Tipo_Operacion = "0"
                End If
            End If
        End If

        'rherrera 24/11/14 ETF
        lstrParamTresFronteras = luoParm.GetParametro("CAMBIOS_TRESFRONTERAS", luo_BR.str_log, False)
        If lstrParamTresFronteras Is Nothing Then lstrParamTresFronteras = "N"
        'Se actualiza el campo Urgente solo cuando el parametros de Cambios_TresFronteras este activo
        If lstrParamTresFronteras = "S" Then
            Dim intUrgente, intPedido, intArea As Integer
            Dim strListaPedidos As String
            intPedido = 0
            intArea = 0

            If iBR.AsignacionClass.ListaPedidos.Count = 0 Then
                strListaPedidos = ""
            ElseIf Trim(iBR.AsignacionClass.ListaPedidos(0)) = "vacio" Then
                strListaPedidos = ""
            Else
                strListaPedidos = iBR.AsignacionClass.ListaPedidos(0)
            End If

            If strListaPedidos <> "" Then
                intPedido = iBR.AsignacionClass.ListaPedidos(0)
                intArea = iBR.AsignacionClass.ListaPedidosAreas(0)
            End If

            If str(44) = "1" Then
                intUrgente = 1
                iBR.AsignacionClass.Urgente = 1
            Else
                intUrgente = 0
                iBR.AsignacionClass.Urgente = 0
            End If

            iBR.ActualizaUrgente(intArea, intPedido, intUrgente, iBR.str_log, False)
        End If

        '19/12/2016 megallegos -> En caso de que haya cambiado de unidad, actualizar la unidad en el tb desp_pedido desde la asigancion // Folio: 22707 || TIN
        If Trim(str(1)) <> Trim(str(26)) Then
            Dim dtPedido As New Data.DataTable
            Dim npedido = "", idArea As String = ""
            dtPedido = iBR.GetPedidosDeUnaAsignacion(str(0), iBR.str_log)
            If dtPedido.Rows.Count > 0 Then
                For x As Integer = 0 To dtPedido.Rows.Count - 1
                    npedido = dtPedido.Rows(x).Item("num_pedido")
                    idArea = dtPedido.Rows(x).Item("id_area")
                Next
                iBR.ActualizarUnidadPedido(str(1), idArea, npedido, iBR.str_log, False)
            End If
        End If

        If luoParmtaAux.GetParametro("filtra_unidades_x_t_operacion", iBR.str_log, False) = "S" And iBR.AsignacionClass.ListaPedidos.Count <> 0 Then
            If iBR.AsignacionClass.ListaPedidos.Count > 0 Then
                Dim tOpPedido As Integer
                Dim validaOperacion = iBR.ValidaTipoOperacionPedido(iBR.AsignacionClass.ListaPedidos.Item(0), iBR.AsignacionClass.ListaPedidosAreas.Item(0), iBR.AsignacionClass.Tractor, iBR.AsignacionClass.Remolque1, iBR.AsignacionClass.Remolque2, iBR.str_log, tOpPedido)
                Dim descOpPedido = iBR.getDescTipoOperacion(tOpPedido, iBR.str_log)
                Select Case validaOperacion
                    Case 1
                        Throw New Exception("El tipo de Operaci�n del Pedido es: " & descOpPedido & " ,y no coincide con el tipo de operaci�n de la Unidad: " & iBR.AsignacionClass.Tractor)

                    Case 2
                        Throw New Exception("El tipo de Operaci�n del Pedido es: " & descOpPedido & " ,y no coincide con el tipo de operaci�n de la Unidad: " & iBR.AsignacionClass.Remolque1)

                    Case 3
                        Throw New Exception("El tipo de Operaci�n del Pedido es: " & descOpPedido & " ,y no coincide con el tipo de operaci�n de la Unidad: " & iBR.AsignacionClass.Remolque2)
                End Select
            End If
        End If

        strReturn.Add(iBR.AsignacionClass.Num_Asignacion) '0
        strReturn.Add("") '1 
        strReturn.Add("") '2
        strReturn.Add(0) '3

        iBR.AsignacionClass.SeguimientoNombreCorto = str(21) 'Me.txtSeguimiento.Text

        iBR.AsignacionClass.IdVentanaChrysler = IIf(str(45) = "NULL", "NULL", str(45))

        'jmartin folio: 23121   18/10/2017
        'If str.Length > str(53) Then
        If str(53) <> "ul" Then
            iBR.AsignacionClass.ClasificacionMovimiento = str(53)
        End If

        'AGONZALEZ FOLIO:24443 30/05/2018
        If (Not str(58) Is Nothing And str(58) <> "0") Then
            iBR.AsignacionClass.VelMax = str(58) 'Me.txtVelMax.Text
        End If
        'PHERNANDEZ 23/08/2018 Funcionalidad clasificacion de casetas, cambios TGARCIA.
        lstr_TipoRuta = UCase(luoParm.GetParametro("clasifica_tipo_ruta", iBR.str_log, False))
        If lstr_TipoRuta = "S" Then
            iBR.AsignacionClass.ParamTipoRuta = "S"
            iBR.AsignacionClass.clasificacion_ruta = str(59)
            iBR.AsignacionClass.clasificacion_carga_diesel = str(60)
        End If

        'NMANUEL 21/09/18 FOLIO: 24496, LANDSTAR.
        Dim lstr_TipoArmado As String
        lstr_TipoArmado = UCase(luoParm.GetParametro("HABCAMPOS_INTERFAZLANDSTAR", iBR.str_log, False))
        If lstr_TipoArmado = "S" Then
            iBR.AsignacionClass.tipo_armado = str(61)
            iBR.AsignacionClass.ParamTipoArmado = "S"
        End If

        'iBR.SalvarEdicion(Me.txtLineaRem1Original.Text, Me.txtRemolque1Original.Text, Me.txtLineaRem2Original.Text, Me.txtRemolque2Original.Text, iBR.str_log)
        iBR.SalvarEdicion(Trim(str(22)), Trim(str(23)), Trim(str(24)), Trim(str(25)), iBR.str_log)

        'rherrera GAAL  
        Dim lstrParamValesAutomaticos As String = luoParm.GetParametro("CalculoValesCombustibleAuto", luo_BR.str_log, False)
        If lstrParamValesAutomaticos = "S" Then
            Dim intAsignacion, intAsignacionOriginal, intPlazaOrigen, intPlazaDestino As Integer 'Revisa el campo viaje_regreso
            Dim strNumAsignacion As String
            Dim dtDatos As DataTable

            dtDatos = iBR.getAsignacionPadreHijo(1, iBR.AsignacionClass.Asignacion, iBR.str_log)
            If dtDatos.Rows.Count > 0 Then
                If Not dtDatos.Rows(0).Item("id_asignacion") Is DBNull.Value Then intAsignacionOriginal = dtDatos.Rows(0).Item("id_asignacion")
                If Not dtDatos.Rows(0).Item("num_asignacion") Is DBNull.Value Then strNumAsignacion = dtDatos.Rows(0).Item("num_asignacion")
                If Not dtDatos.Rows(0).Item("viaje_regreso") Is DBNull.Value Then intAsignacion = dtDatos.Rows(0).Item("viaje_regreso")
                If Not dtDatos.Rows(0).Item("id_plaza_origen") Is DBNull.Value Then intPlazaOrigen = dtDatos.Rows(0).Item("id_plaza_origen")
                If Not dtDatos.Rows(0).Item("id_plaza_destino") Is DBNull.Value Then intPlazaDestino = dtDatos.Rows(0).Item("id_plaza_destino")
            End If

            If intAsignacion = 1 Then 'Las asignaciones de ida tienen el n�mero 1 
                'Desligamos
                iBR.DesligaAsignacionHija(iBR.AsignacionClass.Asignacion, iBR.str_log)
                'Cancelamos los pre vales
                iBR.setBorraPreVales(iBR.AsignacionClass.Asignacion, iBR.str_log)
                'Cancelamos vales en edici�n
                iBR.CancelarValesPorCambioDestino(iBR.AsignacionClass.Area, iBR.AsignacionClass.Num_Asignacion, iBR.str_log)

            ElseIf intAsignacion > 1 Then
                'Vuelve a tomar los datos de la asignaci�n de regreso
                Dim intAsignacionOriginalRegreso, intAsignacionRegreso, intPlazaOrigenRegreso, intPlazaDestinoRegreso As Integer
                Dim strNumAsignacionRegreso As String = ""
                dtDatos.Clear()
                dtDatos = iBR.getAsignacionPadreHijo(1, intAsignacion, iBR.str_log)
                If dtDatos.Rows.Count > 0 Then
                    If Not dtDatos.Rows(0).Item("id_asignacion") Is DBNull.Value Then intAsignacionOriginalRegreso = dtDatos.Rows(0).Item("id_asignacion")
                    If Not dtDatos.Rows(0).Item("num_asignacion") Is DBNull.Value Then strNumAsignacionRegreso = dtDatos.Rows(0).Item("num_asignacion")
                    If Not dtDatos.Rows(0).Item("viaje_regreso") Is DBNull.Value Then intAsignacionRegreso = dtDatos.Rows(0).Item("viaje_regreso")
                    If Not dtDatos.Rows(0).Item("id_plaza_origen") Is DBNull.Value Then intPlazaOrigenRegreso = dtDatos.Rows(0).Item("id_plaza_origen")
                    If Not dtDatos.Rows(0).Item("id_plaza_destino") Is DBNull.Value Then intPlazaDestinoRegreso = dtDatos.Rows(0).Item("id_plaza_destino")
                End If
                'Desligamos
                iBR.DesligaAsignacionHija(intAsignacion, iBR.str_log)
                'Cancelamos los pre vales
                iBR.setBorraPreVales(intAsignacion, iBR.str_log)
                'Cancelamos vales en edici�n
                iBR.CancelarValesPorCambioDestino(iBR.AsignacionClass.Area, strNumAsignacionRegreso, iBR.str_log)
            Else
                'Cancelamos los pre vales
                iBR.setBorraPreVales(iBR.AsignacionClass.Asignacion, iBR.str_log)
                'Cancelamos vales en edici�n
                iBR.CancelarValesPorCambioDestino(iBR.AsignacionClass.Area, iBR.AsignacionClass.Num_Asignacion, iBR.str_log)
            End If
        End If

    End Sub

    Public Sub ActualizarContenedores(ByVal str() As String, ByRef strReturn As ArrayList)
        iBR.str_log = Session("strConn")

        If (str(35) <> "" And str(36) <> "") And (str(39) <> "" And str(40) <> "") Then
            iBR.ActualizaContenedoresPedido(str(35), str(36), str(39), str(40))
        End If
        If (str(37) <> "" And str(38) <> "") And (str(41) <> "" And str(42) <> "") Then
            iBR.ActualizaContenedoresPedido(str(37), str(38), str(41), str(42))
        End If

    End Sub

    Private Sub ColumnasUnidades(ByVal idvista As Integer, ByVal atxtFlota As Integer, ByVal aidAsignacion As Integer, ByVal stridUnidad As String, ByVal bolViajeActual As Boolean, ByRef ldtDatos As Data.DataTable)
        Dim ldt_datos_det As New Data.DataTable
        Dim ldt_datos_query As New Data.DataTable
        Dim lCount As Integer = 0

        luo_br_layout.id_vista = idvista
        luo_br_layout.str_log = Session("strConn")

        ldt_datos_det = luo_br_layout.ObtenerVistaDetalle()
        ldt_datos_query = luo_br_layout.ObtenerQuery()

        If ldt_datos_query.Rows.Count > 0 Then
            Dim luoParam As New net_b_zamclasesextended.net_b_clasegeneralparametrosextended
            Dim strParametro, strWhere, strFiltroArea As String
            Dim ldtFiltros As New Data.DataTable
            Dim luoVG As net_b_zam.net_b_appvariables = Session("AppVariables")
            strParametro = luoParam.GetParametro("CAMBIOS_SUTRANSPORTE", luo_br_layout.str_log, False)
            If ldt_datos_query.Rows(0).Item("dwhere") = "" Then
                ldt_datos_query.Rows(0).Item("dwhere") = " WHERE "
            End If

            If strParametro = "S" Then
                strFiltroArea = " ( ASI.id_area in ( " & CStr(luoVG.AreasUnidades) & " ) OR ASI.id_area IS NULL )"
            Else
                strFiltroArea = " ( mtto_unidades.id_area in ( " & CStr(luoVG.AreasUnidades) & " ) )"
            End If

            If bolViajeActual = False Then
                strWhere = ldt_datos_query.Rows(0).Item("dwhere") + " ( mtto_unidades.estatus = 'A' ) AND " & strFiltroArea & _
                                        " AND ( mtto_unidades.tipo = 'T' ) AND (trafico_viaje.viajeactual in ( 'S' )  OR trafico_viaje.viajeactual IS NULL )  "
                'Else
                '    strWhere = ldt_datos_query.Rows(0).Item("dwhere") + " ( mtto_unidades.estatus = 'A' ) AND " & strFiltroArea & _
                '                                          " AND ( mtto_unidades.tipo = 'T' ) AND trafico_viaje.viajeactual in ( 'S' ) "
            Else
                strWhere = ldt_datos_query.Rows(0).Item("dwhere") + " ASI.id_asignacion = " & aidAsignacion
            End If

            'Hace el select en base al id_asignacion
            'strWhere = strWhere & "AND  ASI.id_asignacion = " & aidAsignacion
            strWhere = strWhere & " AND  mtto_unidades.id_unidad = '" & stridUnidad & "'"
            ldtDatos = luo_br_layout.ObtenerQuery("SELECT " & ldt_datos_query.Rows(0).Item("dselect"), ldt_datos_query.Rows(0).Item("dfrom"), strWhere)

        End If

    End Sub

    Public Sub ArregloArray(ByVal strCampos As String, ByVal strIdentifica As String, ByRef Array As ArrayList, ByRef astr_log As net_objestandar.str_log)
        Dim intPosicion As Integer, intIndex As Integer
        Dim strValor As String
        Array = New ArrayList
        'Obtiene la primer Posici�n
        intPosicion = InStr(strCampos.Substring(intIndex), strIdentifica)
        Do
            If intPosicion = 0 Then Exit Do
            intPosicion = intPosicion - 1
            strValor = strCampos.Substring(intIndex, intPosicion)
            Array.Add(strValor)
            intIndex = intIndex + intPosicion + 1
            intPosicion = InStr(strCampos.Substring(intIndex), strIdentifica)
        Loop
    End Sub
    'Metodo que verifica si el usuario no es uno de los pendientes por sacar de la aplicaci�n o si no 
    'a perdido la sesi�n en la aplicaci�n. VHORTEG�N 16/DICIEMBRE/2009
    Public Sub ValidaSesionUsuarios()
        Try
            If Not ConfigurationManager.AppSettings("ControlarConWS") Is Nothing Then
                If Session("UserName") Is Nothing Then
                    Response.Redirect(Application("myRoot") & "/sesion_terminada.aspx?SesionEnd=1") 'REDIRECCIONAMOS A LA PANTALLA DEL LOGIN.
                End If
                'VERIFICAMOS SI NO ES UNO DE LOS USUARIOS PENDIENTES POR SACAR DE LA APLICACI�N
                If Application("UserOPV6_Eliminar").rows.count > 0 Then
                    For Each lrow As Data.DataRow In Application("UserOPV6_Eliminar").rows
                        If UCase(lrow.Item("usuario_lis")) = UCase(Session("UserName")) And UCase(lrow.Item("equipo_nombre")) = UCase(Session("UserEquipo")) Then
                            lrow.Delete()     'LO ELIMINAMOS DEL LISATADO DE PENDIENTES DE ELIMINAR
                            Session("SesionEndBySystem") = "1"
                            Exit For
                        End If
                    Next
                End If
                'SI VIENE CON VALOR A 1 SIGNIFICA QUE ESTA PENDIENTE POR SACAR DEL SISTEMA.
                If Not Session("SesionEndBySystem") Is Nothing Then
                    If Session("SesionEndBySystem") = "1" Then
                        Session("SesionEndBySystem") = Nothing
                        'LO REDIRECCIONA A LA PAGINA DE AVISO QUE FUE TERMINADA SU SESI�N POR EL ADMINISTRADOR DEL
                        'SISTEMA Y ABANDONA LA SESI�N.
                        Response.Redirect(Application("myRoot") & "/sesion_terminada.aspx")
                    End If
                End If
            Else 'ENTRA CUANO NO LO VA A VALIDAR DESDE EL WS DEL CONTROL DE LAS LICENCIAS.
                If Session("UserName") Is Nothing Then
                    Response.Redirect(Application("myRoot") & "/sesion_terminada.aspx?SesionEnd=1") 'REDIRECCIONAMOS A LA PANTALLA DEL LOGIN.
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Valida Contenedor
    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
    Public Function ValidaContenedor(ByVal strContenedor As String, ByVal strId As String) As ArrayList
        Try
            If Session("strConn") Is Nothing Then
                Throw New Exception("su sesi�n a terminado, favor de iniciar sesi�n nuevamente en el modulo.")
            End If
            Dim ArrayContenedor As New ArrayList
            iBR.str_log = Session("strConn")
            Dim ldtContA As New Data.DataTable
            ldtContA = iBR.GetDTContenedor(strContenedor, iBR.str_log)
            If ldtContA.Rows.Count <= 0 Then
                Throw New Exception("El " & strId & ":  " & strContenedor & " no existe en la base de datos.")
            Else
                ArrayContenedor.Add(strId) '0 id del contenedor
                ArrayContenedor.Add(ldtContA.Rows(0).Item("no_serie")) '1 txtContenedor A o B
                ArrayContenedor.Add(ldtContA.Rows(0).Item("id_contenedor")) '2 txtIdContenedor1 - 1b
            End If
            Return ArrayContenedor
        Catch ex As Exception
            Dim arraylist As New ArrayList
            arraylist.Add("Error")
            arraylist.Add(ex.Message)
            arraylist.Add(strId)
            Return arraylist
            Throw ex
        End Try
    End Function

    Protected Sub chkPedidoUrgente_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkPedidoUrgente.CheckedChanged
        'Si la ruta tiene un valor hacer el calculo utilizando los campos F.Inicio,F.Final y actualizar Hrs Standar
        Try
            Dim ldtFechaFin As DateTime
            Dim intUrgente As Integer

            'Valida si se tiene un valor en la ruta
            If CInt(Me.txtRuta.Text) <> 0 Then

                If Me.chkPedidoUrgente.Checked = True Then intUrgente = 1
                If Me.chkPedidoUrgente.Checked = False Then intUrgente = 0

                'Consulta tiempo_ruta y tiempo_urgente de la ruta
                Me.txtHrsEstandar.Text = iBR.GetTiempoUrgente(CInt(Me.txtRuta.Text), intUrgente, 0, 0, iBR.str_log, True)

                ldtFechaFin = Me.wdcInicioViaje.Value
                'Hace el calculo en la pantalla
                Me.wdcFinViaje.Value = ldtFechaFin.AddHours(CDec(Me.txtHrsEstandar.Text))

            End If
        Catch ex As Exception
            Me.lblErrorUP1.Visible = True
            Me.lblErrorUP1.Text = ex.Message
        End Try
    End Sub


    'Valida Contenedor
    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
    Public Function UltimoDestinoNuevoOrigen(ByVal Unidad As String, ByVal RutaNueva As String) As ArrayList
        Try
            If Session("strConn") Is Nothing Then
                Throw New Exception("su sesi�n a terminado, favor de iniciar sesi�n nuevamente en el modulo.")
            End If
            Dim ArrayRegreso As New ArrayList
            iBR.str_log = Session("strConn")
            Dim ldtDatos As New Data.DataTable
            Dim ldtDatosRuta As New Data.DataTable

            'obtiene la informacion la ruta del ultimo viaje con informacion sus plazas
            ldtDatos = iBR.ObtenerUltimaRutaUnidad(Unidad, iBR.str_log)
            ' obtiene la informacion de la ruta con sus plazas
            ldtDatosRuta = iBR.InfoPlazasRuta(RutaNueva, iBR.str_log)

            If ldtDatos.Rows.Count > 0 Then
                Select Case ldtDatos.Rows(0).Item("status_viaje")
                    Case "A", "T" 'pendiente o en transito
                        Dim Area As Integer = ldtDatos.Rows(0).Item("id_area")
                        Dim NoViaje As Integer = ldtDatos.Rows(0).Item("no_viaje")
                        If ldtDatos.Rows(0).Item("viajeactual") = "S" Then
                            Throw New Exception("No es posible continuar. La unidad " & Unidad & " se encuentra en viaje activo. Area: " & CStr(Area) & " numero de viaje: " & CStr(NoViaje) & ".")
                        End If

                    Case Else

                End Select
                'obtenemos el ultimo destino
                ArrayRegreso.Add(ldtDatos.Rows(0).Item("destino")) 'Ultimo destino

                If ldtDatosRuta.Rows.Count > 0 Then
                    'obtenermos el origen de la nueva ruta
                    ArrayRegreso.Add(ldtDatosRuta.Rows(0).Item("origen")) 'origen nueva ruta
                End If
                'obtenemos la fecha
                ArrayRegreso.Add(CStr(ldtDatos.Rows(0).Item("fecha_real_fin_viaje")))
            Else
                'quiere decir que la unidad no tiene un viaje anterior lo deja pasar
                ArrayRegreso.Add(0)
            End If


            Return ArrayRegreso
        Catch ex As Exception
            Dim arraylist As New ArrayList
            arraylist.Add("Error")
            arraylist.Add(ex.Message)
            Return arraylist
            Throw ex
        End Try
    End Function

    Enum tipo_aviso
        aviso = 0
        bloqueada = 1
        omitir = 2
    End Enum

    Private Function ValidaVencimientoDocumento(ByVal idPersonal As Integer, ByVal TipoServicio As String) As Boolean
        Dim conn As String = ConfigurationManager.AppSettings("strConn")
        Dim dm As New DataManager(conn)
        Dim nVencidos As Integer = 0
        Dim docVencidos As String = ""

        Dim q As Query = dm.NewQuery() _
        .Sel("id_tipoexamen") _
        .From("rho_tipoexamenmedico_tpserv") _
        .WhereVal("tipo_serv", TipoServicio)
        Dim dt As DataTable = dm.ExecTable(q)


        'CSCABRALES,26/09/2018, FOLIO: 24965
        Dim qparam As Query = dm.NewQuery() _
        .Sel("valor") _
        .From("general_parametros") _
        .WhereVal("nombre", "validadocoperador_inovativos")

        Dim dtparam As DataTable = dm.ExecTable(qparam)

        If dtparam.Rows.Count > 0 Then
            If dtparam.Rows(0).Item("valor") = "S" Then
                If dt.Rows.Count > 0 Then
                    For Each row As DataRow In dt.Rows
                        Dim q1 As Query = dm.NewQuery() _
                        .Sel("em.vencimiento") _
                        .Sel("te.nombre").Top(1) _
                        .Sel("cem.dias_x_bloqueo").Top(1) _
                        .FromAs("em", "rho_examenmedico") _
                        .Join("te", "rho_tipoexamenmedico") _
                        .JoinOn("em.id_tipoexamen", "te.id_tipoexamen") _
                        .Join("cem", "rho_catalogoexamenmedico") _
                        .JoinOn("em.id_tipoexamen", "cem.id_catalogoexamen") _
                        .WhereVal("em.id_tipoexamen", row("id_tipoexamen")) _
                        .Where("em.id_personal", idPersonal) _
                        .OrderBy("em.vencimiento", Constructor.dbOrd.Desc)

                        Dim dt2 As DataTable = dm.ExecTable(q1)
                        If dt2.Rows.Count > 0 Then
                            Dim fecha As Date = Date.Parse(dt2.Rows(0).Item("vencimiento").ToString)
                            Dim dias_x_bloqueo = Convert.ToInt32(dt2.Rows(0).Item("dias_x_bloqueo").ToString)
                            fecha = fecha.AddDays(-dias_x_bloqueo)
                            If fecha < System.DateTime.Now Then
                                nVencidos = nVencidos + 1
                                docVencidos = docVencidos & dt2.Rows(0).Item("nombre") & ", "
                            End If
                        End If
                    Next
                Else
                    Throw New Exception("El tipo de servicio " & TipoServicio & " no tiene documentos asignados")
                End If
            Else
                If dt.Rows.Count > 0 Then
                    For Each row As DataRow In dt.Rows
                        Dim q1 As Query = dm.NewQuery() _
                        .Sel("em.vencimiento") _
                        .Sel("te.nombre").Top(1) _
                        .FromAs("em", "rho_examenmedico") _
                        .Join("te", "rho_tipoexamenmedico") _
                        .JoinOn("em.id_tipoexamen", "te.id_tipoexamen") _
                        .WhereVal("em.id_tipoexamen", row("id_tipoexamen")) _
                        .Where("em.id_personal", idPersonal) _
                        .OrderBy("em.vencimiento", Constructor.dbOrd.Desc)
                        Dim dt2 As DataTable = dm.ExecTable(q1)
                        'CSCABRALES,26/09/2018, FOLIO: 24965
                        If dt2.Rows.Count > 0 Then
                            If dt2.Rows(0).Item("vencimiento") < System.DateTime.Now Then
                                nVencidos = nVencidos + 1
                                docVencidos = docVencidos & dt2.Rows(0).Item("nombre") & ", "
                            End If
                        End If
                    Next
                Else
                    Throw New Exception("El tipo de servicio " & TipoServicio & " no tiene documentos asignados")
                End If
            End If
        End If
        If nVencidos > 0 Then
            Throw New Exception("No es posible continuar, ya que el personal seleccionado tiene los siguientes documentos por vencer: " & docVencidos)
        Else
            Return False
        End If
        'WhereVal(Constructor.dbCom.LessThan, "em.vencimiento", System.DateTime.Now.ToString("yyyyMMdd")) _
    End Function

    ''' <summary>Devuelve una lista de objetos con los documentos vencidos/por vencer de la unidad</summary>
    Private Function GetListDocumentosUnidadCF(ByVal id_unidad As String) As List(Of Object)
        Dim conn As String = ConfigurationManager.AppSettings("strConn")
        Dim dm As New DataManager(conn)
        Dim difDias As Expression = Expression.DateDiff(Constructor.dbTim.Day, Expression.GetDate(), "du.fecha_vencimiento")
        '                                    jmartin folio: 23450 13/09/2017
        Dim luoParam As New net_b_zamclases.net_b_clasegeneralparametros
        luoParam.str_log = Session("strConn")
        Dim strParm As String = luoParam.GetParametro("bddPrincipal", luoParam.str_log, False).ToString

        Dim dt As New Data.DataTable
        If strParm <> "" Then 'm�ltiples bases de datos que consultan a una base de datos principal
            Try
                Dim caseAviso As Expression = Expression _
                .CaseWhen(tipo_aviso.bloqueada, Constructor.dbCom.LessThanOrEquals, difDias, "td.dias_bloqueo_unidad") _
                .When(tipo_aviso.aviso, Constructor.dbCom.LessThanOrEquals, difDias, "td.dias_inicia_aviso") _
                .Else(tipo_aviso.omitir)
                Dim q As Query = New Query().SelAs("tipo_aviso", caseAviso).Sel("du.fecha_vencimiento", "td.nombre")
                q.FromAs("du", "ctrl_flotas_tipo_documento_unidad", "dbo", strParm)
                q.Join(Constructor.dbJoi.Inner, "td", "ctrl_flotas_tipo_documento", "dbo", strParm).JoinOn("td.id_tipo_doc", "du.id_tipo_doc")
                q.WhereVal("du.estatus", "A").WhereVal("td.estatus", "A")
                q.WhereVal("du.id_unidad", id_unidad)
                dt = dm.ExecTable(q)
            Catch ex As Exception
                Throw ex
            End Try
            '----------------------------------------------------------------------------------------------------------------------------
        Else    'est�ndar
            Dim caseAviso As Expression = Expression _
            .CaseWhen(tipo_aviso.bloqueada, Constructor.dbCom.LessThanOrEquals, difDias, "td.dias_bloqueo_unidad") _
            .When(tipo_aviso.aviso, Constructor.dbCom.LessThanOrEquals, difDias, "td.dias_inicia_aviso") _
            .Else(tipo_aviso.omitir)
            Dim q As Query = New Query().SelAs("tipo_aviso", caseAviso).Sel("du.fecha_vencimiento", "td.nombre")
            q.FromAs("du", "ctrl_flotas_tipo_documento_unidad")
            q.Join("td", "ctrl_flotas_tipo_documento").JoinOn("td.id_tipo_doc", "du.id_tipo_doc")
            q.WhereVal("du.estatus", "A").WhereVal("td.estatus", "A")
            q.WhereVal("du.id_unidad", id_unidad)
            dt = dm.ExecTable(q)
        End If

        Dim result As New List(Of Object)()
        For Each row As DataRow In dt.Rows
            Dim mensaje As String = If(row("tipo_aviso") = tipo_aviso.aviso, "DOCUMENTO PR�XIMO A VENCER: ", "UNIDAD BLOQUEADA POR VENCIMIENTO: ")
            mensaje &= row("nombre") & " - VENCIMIENTO: " & row("fecha_vencimiento") & " - UNIDAD: " + id_unidad
            If row("tipo_aviso") <> tipo_aviso.omitir Then result.Add(New With {.tipo_aviso = row("tipo_aviso"), .mensaje = mensaje})
        Next row
        Return result
    End Function

    ''' <summary>Devuelve una lista de objetos con los documentos vencidos/por vencer del operador''' </summary>
    Public Function GetListDocumentosOperador(ByVal id_operador As String) As List(Of Object)
        Dim conn As String = ConfigurationManager.AppSettings("strConn")
        Dim dm As New DataManager(conn)
        Dim diasAviso As Query = New Query().Sel("valor").From("general_parametros").WhereVal("nombre", "valida_lic_operador_aviso")
        Dim diasBloqueo As Query = New Query().Sel("valor").From("general_parametros").WhereVal("nombre", "valida_lic_operador_bloqueo")
        Dim difDias As Expression = Expression.DateDiff(Constructor.dbTim.Day, Expression.GetDate(), "pp.fecha_venclicencia")
        Dim difDias2 As Expression = Expression.DateDiff(Constructor.dbTim.Day, Expression.GetDate(), "pp.fecha_venclicencia2")
        Dim difDias3 As Expression = Expression.DateDiff(Constructor.dbTim.Day, Expression.GetDate(), "pp.fecha_venclicencia3")

        Dim caseAvisoLic As Expression = Expression.CaseWhen(tipo_aviso.bloqueada, Constructor.dbCom.LessThanOrEquals, difDias, diasBloqueo) _
            .When(tipo_aviso.aviso, Constructor.dbCom.LessThanOrEquals, difDias, diasAviso) _
            .Else(tipo_aviso.omitir)

        Dim caseAvisoLic2 As Expression = Expression.CaseWhen(tipo_aviso.bloqueada, Constructor.dbCom.LessThanOrEquals, difDias2, diasBloqueo) _
            .When(tipo_aviso.aviso, Constructor.dbCom.LessThanOrEquals, difDias, diasAviso) _
            .Else(tipo_aviso.omitir)

        Dim caseAvisoLic3 As Expression = Expression.CaseWhen(tipo_aviso.bloqueada, Constructor.dbCom.LessThanOrEquals, difDias3, diasBloqueo) _
            .When(tipo_aviso.aviso, Constructor.dbCom.LessThanOrEquals, difDias, diasAviso) _
            .Else(tipo_aviso.omitir)

        Dim q As Query = New Query().Sel("pp.folio_licencia", "pp.folio_licencia2", "pp.folio_licencia3", "pp.fecha_venclicencia", "pp.fecha_venclicencia2", "pp.fecha_venclicencia3")
        q.SelAs("tipo_aviso_lic", caseAvisoLic).SelAs("tipo_aviso_lic2", caseAvisoLic2).SelAs("tipo_aviso_lic3", caseAvisoLic3)
        q.FromAs("pp", "personal_personal")
        q.Where("pp.id_personal", id_operador)

        Dim dt As DataTable = dm.ExecTable(q)
        Dim result As New List(Of Object)
        Dim mensaje As String = ""
        For Each row As DataRow In dt.Rows
            If Not row("folio_licencia") Is Nothing AndAlso Not IsDBNull(row("folio_licencia")) AndAlso row("folio_licencia") <> "" Then
                mensaje = ""
                mensaje = If(row("tipo_aviso_lic") = tipo_aviso.aviso, "LICENCIA DEL OPERADOR PR�XIMA A VENCER: ", "LICENCIA DEL OPERADOR VENCIDAD: ")
                mensaje &= row("folio_licencia") & " - " & row("fecha_venclicencia")
                If row("tipo_aviso_lic") <> tipo_aviso.omitir Then result.Add(New With {.tipo_aviso = row("tipo_aviso_lic"), .mensaje = mensaje})
            End If

            If Not row("folio_licencia2") Is Nothing AndAlso Not IsDBNull(row("folio_licencia2")) AndAlso row("folio_licencia2") <> "" Then
                mensaje = ""
                mensaje = If(row("tipo_aviso_lic2") = tipo_aviso.aviso, "LICENCIA DEL OPERADOR PR�XIMA A VENCER: ", "LICENCIA DEL OPERADOR VENCIDAD: ")
                mensaje &= row("folio_licencia2") & " - " & row("fecha_venclicencia2")
                If row("tipo_aviso_lic2") <> tipo_aviso.omitir Then result.Add(New With {.tipo_aviso = row("tipo_aviso_lic2"), .mensaje = mensaje})
            End If

            If Not row("folio_licencia3") Is Nothing AndAlso Not IsDBNull(row("folio_licencia3")) AndAlso row("folio_licencia3") <> "" Then
                mensaje = ""
                mensaje = If(row("tipo_aviso_lic3") = tipo_aviso.aviso, "LICENCIA DEL OPERADOR PR�XIMA A VENCER: ", "LICENCIA DEL OPERADOR VENCIDAD: ")
                mensaje &= row("folio_licencia3") & " - " & row("fecha_venclicencia3")
                If row("tipo_aviso_lic3") <> tipo_aviso.omitir Then result.Add(New With {.tipo_aviso = row("tipo_aviso_lic3"), .mensaje = mensaje})
            End If
        Next row
        Return result
    End Function

    ''' <summary>Devuelve listado JSON con los documentos vencidos o por vencer de la unidad</summary>
    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
    Public Function CheckDocumentosUnidadCF(ByVal id_unidad As String) As String
        Dim lstDocsUnidad As New List(Of Object)()
        lstDocsUnidad = GetListDocumentosUnidadCF(id_unidad)
        Return JsonConvert.SerializeObject(lstDocsUnidad)
    End Function

    ''' <summary>Valida la vigencia de las licencias del operador </summary>
    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
    Public Function CheckLicenciaOperador(ByVal id_operador As String) As String
        Dim lstDocsOperador As New List(Of Object)()
        lstDocsOperador = GetListDocumentosOperador(id_operador)
        Return JsonConvert.SerializeObject(lstDocsOperador)

        'Dim conn As String = ConfigurationManager.AppSettings("strConn")
        'Dim dm As New DataManager(conn)
        'Dim diasAviso As Query = New Query().Sel("valor").From("general_parametros").WhereVal("nombre", "valida_lic_operador_aviso")
        'Dim diasBloqueo As Query = New Query().Sel("valor").From("general_parametros").WhereVal("nombre", "valida_lic_operador_bloqueo")
        'Dim difDias As Expression = Expression.DateDiff(Constructor.dbTim.Day, Expression.GetDate(), "pp.fecha_venclicencia")
        'Dim difDias2 As Expression = Expression.DateDiff(Constructor.dbTim.Day, Expression.GetDate(), "pp.fecha_venclicencia2")
        'Dim difDias3 As Expression = Expression.DateDiff(Constructor.dbTim.Day, Expression.GetDate(), "pp.fecha_venclicencia3")

        'Dim caseAvisoLic As Expression = Expression.CaseWhen(tipo_aviso.bloqueada, Constructor.dbCom.LessThan, difDias, diasBloqueo) _
        '    .When(tipo_aviso.aviso, Constructor.dbCom.LessThanOrEquals, difDias, diasAviso) _
        '    .Else(tipo_aviso.omitir)

        'Dim caseAvisoLic2 As Expression = Expression.CaseWhen(tipo_aviso.bloqueada, Constructor.dbCom.LessThan, difDias2, diasBloqueo) _
        '    .When(tipo_aviso.aviso, Constructor.dbCom.LessThanOrEquals, difDias, diasAviso) _
        '    .Else(tipo_aviso.omitir)

        'Dim caseAvisoLic3 As Expression = Expression.CaseWhen(tipo_aviso.bloqueada, Constructor.dbCom.LessThan, difDias3, diasBloqueo) _
        '    .When(tipo_aviso.aviso, Constructor.dbCom.LessThanOrEquals, difDias, diasAviso) _
        '    .Else(tipo_aviso.omitir)

        'Dim q As Query = New Query().Sel("pp.folio_licencia", "pp.folio_licencia2", "pp.folio_licencia3", "pp.fecha_venclicencia", "pp.fecha_venclicencia2", "pp.fecha_venclicencia3")
        'q.SelAs("tipo_aviso_lic", caseAvisoLic).SelAs("tipo_aviso_lic2", caseAvisoLic2).SelAs("tipo_aviso_lic3", caseAvisoLic3)
        'q.FromAs("pp", "personal_personal")
        'q.Where("pp.id_personal", id_operador)

        'Dim dt As DataTable = dm.ExecTable(q)
        'Dim result As New List(Of Object)
        'Dim mensaje As String = ""
        'For Each row As DataRow In dt.Rows
        '    If Not row("folio_licencia") Is Nothing AndAlso Not IsDBNull(row("folio_licencia")) AndAlso row("folio_licencia") <> "" Then
        '        mensaje = ""
        '        mensaje = If(row("tipo_aviso_lic") = tipo_aviso.aviso, "LICENCIA DEL OPERADOR PR�XIMA A VENCER: ", "LICENCIA DEL OPERADOR VENCIDAD: ")
        '        mensaje &= row("folio_licencia") & " - " & row("fecha_venclicencia")
        '        If row("tipo_aviso_lic") <> tipo_aviso.omitir Then result.Add(New With {.tipo_aviso = row("tipo_aviso_lic"), .mensaje = mensaje})
        '    End If

        '    If Not row("folio_licencia2") Is Nothing AndAlso Not IsDBNull(row("folio_licencia2")) AndAlso row("folio_licencia2") <> "" Then
        '        mensaje = ""
        '        mensaje = If(row("tipo_aviso_lic2") = tipo_aviso.aviso, "LICENCIA DEL OPERADOR PR�XIMA A VENCER: ", "LICENCIA DEL OPERADOR VENCIDAD: ")
        '        mensaje &= row("folio_licencia2") & " - " & row("fecha_venclicencia2")
        '        If row("tipo_aviso_lic2") <> tipo_aviso.omitir Then result.Add(New With {.tipo_aviso = row("tipo_aviso_lic2"), .mensaje = mensaje})
        '    End If

        '    If Not row("folio_licencia3") Is Nothing AndAlso Not IsDBNull(row("folio_licencia3")) AndAlso row("folio_licencia3") <> "" Then
        '        mensaje = ""
        '        mensaje = If(row("tipo_aviso_lic3") = tipo_aviso.aviso, "LICENCIA DEL OPERADOR PR�XIMA A VENCER: ", "LICENCIA DEL OPERADOR VENCIDAD: ")
        '        mensaje &= row("folio_licencia3") & " - " & row("fecha_venclicencia3")
        '        If row("tipo_aviso_lic3") <> tipo_aviso.omitir Then result.Add(New With {.tipo_aviso = row("tipo_aviso_lic3"), .mensaje = mensaje})
        '    End If
        'Next row
        'Return JsonConvert.SerializeObject(result)
    End Function

    <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
    Function GetDescPlaza(ByVal strIdPlaza As String) As ArrayList
        Dim luo_BR As New net_b_zamclasesextended.net_b_claseplazaextended
        Dim arraylist As New ArrayList
        Try

            luo_BR.Str_log = New net_objestandar.str_log((ConfigurationManager.AppSettings("strConn").ToString))
            luo_BR.GetPlazaPorId(strIdPlaza, luo_BR, luo_BR.str_log)
            arraylist.Add(luo_BR.Desc_Plaza)

            Return arraylist
        Catch ex As Exception
            arraylist.Add("Error")
            arraylist.Add(ex.Message)
            Return arraylist
            Throw
        End Try
    End Function

    Public Sub MostrarOrigenDestino(ByRef astr_log As net_objestandar.str_log)
        Dim dtPedidosAsig As New Data.DataTable
        Dim luo_BR As New net_b_zamclasesextended.net_b_claseplazaextended
        Try
            If Request("ParmAsignHdn") <> Nothing Then
                dtPedidosAsig = iBR.GetPedidosDeUnaAsignacion(Request("ParmAsignHdn"), iBR.str_log)
                'Agregar valores a los campos de Origen y Destino
                If iBR.AsignacionClass.Id_remitente <> "0" Then
                    Me.txtOrigen1.Text = iBR.AsignacionClass.Id_remitente
                    luo_BR.GetPlazaPorId(iBR.AsignacionClass.Id_remitente, luo_BR, iBR.str_log)
                    Me.txtOrigenDesc.Text = luo_BR.Desc_Plaza
                End If
                If iBR.AsignacionClass.Id_destinatario <> "0" Then
                    Me.txtDestino1.Text = iBR.AsignacionClass.Id_destinatario
                    luo_BR.GetPlazaPorId(iBR.AsignacionClass.Id_destinatario, luo_BR, iBR.str_log)
                    Me.txtDestinoDesc.Text = luo_BR.Desc_Plaza
                End If
                If dtPedidosAsig.Rows.Count = 0 Then
                    Me.tblOrigenDestino.Style.Remove("display")
                End If
            Else
                If Request("Lista") = "vacio[;]" Then
                    Me.tblOrigenDestino.Style.Remove("display")
                End If
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    'AGONZALEZ 02/08/2018 FOLIO:24709
    Public Sub LlenarContenedores(ByVal idpedido As Integer, ByVal idarea As Integer)
        Dim dtPedido As New DataTable
        dtPedido = iBR.GetNombresContenedoresDeUnPedido(idpedido, idarea, iBR.str_log)

        Me.txtCont1Ped1.Text = dtPedido.Rows(0)("SerieContenedor1").ToString()
        Me.txtCont2Ped1.Text = dtPedido.Rows(0)("SerieContenedor2").ToString()
        Me.txtCont1Ped2.Text = dtPedido.Rows(0)("SerieContenedor3").ToString()
        Me.txtCont2Ped2.Text = dtPedido.Rows(0)("SerieContenedor4").ToString()
        Me.txtIdContenedor1.Text = dtPedido.Rows(0)("IdContenedor1").ToString()
        Me.txtIdContenedor1b.Text = dtPedido.Rows(0)("IdContenedor2").ToString()
        Me.txtIdContenedor2.Text = dtPedido.Rows(0)("IdContenedor3").ToString()
        Me.txtIdContenedor2b.Text = dtPedido.Rows(0)("IdContenedor4").ToString()

        Me.txtIdPedido_Contenedores1.Text = idpedido.ToString()
        Me.txtIdPedido_Contenedores2.Text = idpedido.ToString()
        Me.txtareaPedido_Contenedores1.Text = idarea.ToString()
        Me.txtareaPedido_Contenedores2.Text = idarea.ToString()

        'Llenar Linea 2 y Remolque2
        iBRPedidos.str_log = iBR.str_log
        Dim ldtPedidoRem As New Data.DataTable
        Dim strParms(2) As String
        strParms(0) = idarea
        strParms(1) = idpedido
        strParms(2) = " ISNULL(id_dolly,'') dolly, ISNULL(id_remolque2,'') remolque2, ISNULL(id_linearem2,'') linea2 "
        iBRPedidos.GetPedidobyIdPedido(strParms, ldtPedidoRem)

        Me.txtDolly.Text = ldtPedidoRem.Rows(0)("dolly").ToString()
        Me.txtRemolque2.Text = ldtPedidoRem.Rows(0)("remolque2").ToString()
        Me.txtLineaRem2.Text = ldtPedidoRem.Rows(0)("linea2").ToString()

        If Me.txtDolly.Text <> "" Then
            iBR.AsignacionClass.Dolly = Me.txtDolly.Text
            iBR.ValidaDolly()
        End If
        If Me.txtLineaRem2.Text <> "" Then
            iBR.AsignacionClass.LineaRem2 = Me.txtLineaRem2.Text
        End If
        If Me.txtRemolque2.Text <> "" Then
            iBR.AsignacionClass.Remolque2 = Me.txtRemolque2.Text
            iBR.ValidaRemolque2()
        End If

    End Sub

    'AGONZALEZ 02/08/2018 FOLIO:24709 , Valida Capacidad de Carga de Contenedores
    ' <Ajax.AjaxMethod(Ajax.HttpSessionStateRequirement.ReadWrite)> _
    Public Sub ValidaCargaContenedores(ByVal arrListaPedidos As ArrayList, ByVal arrListaPedidosAreas As ArrayList)
        Try
            iBR.str_log = New net_objestandar.str_log(ConfigurationManager.AppSettings("strConn").ToString)
            Dim dtPedido As New Data.DataTable
            Dim Cargaft As Decimal = 0

            For i As Integer = 0 To arrListaPedidos.Count - 1
                If arrListaPedidos(i).ToString().Contains("vacio") = False Then
                    dtPedido = iBR.GetNombresContenedoresDeUnPedido(arrListaPedidos(i), arrListaPedidosAreas(i), iBR.str_log)
                    Cargaft += dtPedido.Rows(0)("Medida1")
                    Cargaft += dtPedido.Rows(0)("Medida2")
                    Cargaft += dtPedido.Rows(0)("Medida3")
                    Cargaft += dtPedido.Rows(0)("Medida4")
                End If
            Next

            If Cargaft > 80 Then
                Throw New Exception("Se ha excedido la Carga de Contenedores")
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
