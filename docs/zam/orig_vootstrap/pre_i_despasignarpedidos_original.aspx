<%@ Page Language="VB" AutoEventWireup="false" CodeFile="pre_i_despasignarpedidos.aspx.vb"
    Inherits="pridesp01_pre_i_despasignarpedidos" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Asignaci�n</title>
    <link href="../css/textos.css" type="text/css" rel="stylesheet"/>
    <style type="text/css" >
       html, body, #map_canvas {
         margin: 0;
         padding: 0;
         height: 50%;
         width: 50%;
       }
       .lblError {
        margin: 10px;
       }
       .LabelError {
            margin: 3px 0;
       }
     </style>
    <!-- Plugins -->
	<script type="text/jscript" src="../vendors/jquery/jquery-1.11.2.js"></script>
    <script type="text/javascript" src="../vendors/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="../vendors/jquery-ui/jquery-ui.js"></script>
    <script type="text/javascript" src="../vendors/jquery-switch/jquery.switchButton.js"></script>
    <script type="text/javascript" src="../vendors/jquery.ui.touch-punch.min.js"></script>
    <!-- google Maps API -->
    <script type="text/javascript" src="../vendors/lodash_compatibility.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?<%=key%>&libraries=geometry"></script>
    <%--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?AIzaSyAJycY5ZY6aYu-qiwOl7Rm4_TT907IpnSM&libraries=geometry&sensor=false"></script>--%>

    <script language="javascript" type="text/javascript">
        if (!window.JSON) {
            window.JSON = {
                parse: function(sJSON) { return eval("(" + sJSON + ")"); },
                stringify: function(vContent) {
                    if (vContent instanceof Object) {
                        var sOutput = "";
                        if (vContent.constructor === Array) {
                            for (var nId = 0; nId < vContent.length; sOutput += this.stringify(vContent[nId]) + ",", nId++);
                            return "[" + sOutput.substr(0, sOutput.length - 1) + "]";
                        }
                        if (vContent.toString !== Object.prototype.toString) {
                            return "\"" + vContent.toString().replace(/"/g, "\\$&") + "\"";
                        }
                        for (var sProp in vContent) {
                            sOutput += "\"" + sProp.replace(/"/g, "\\$&") + "\":" + this.stringify(vContent[sProp]) + ",";
                        }
                        return "{" + sOutput.substr(0, sOutput.length - 1) + "}";
                    }
                    return typeof vContent === "string" ? "\"" + vContent.replace(/"/g, "\\$&") + "\"" : String(vContent);
                }
            };
        }
    </script>

    <script language="javascript" type="text/javascript">
        //Columnas de la pantalla de proceso Agenda de Viajes
        function ColumnasPedidosAgenda(response, oRow, intcont) {
            var strSubArray
            for (intcont; intcont < response.value.length; ++intcont) {
                strSubArray = response.value[intcont].split(",")
                if (strSubArray[0] != 'NuevoRow') {
                    if (oRow.getCellFromKey(strSubArray[0]) != null) {
                        oRow.getCellFromKey(strSubArray[0]).setValue(strSubArray[1])
                    }
                }
            }
        }

        function Guardar2() {
            /*if (document.getElementById('txtHdnValidaArmado').value == 1 && (document.getElementById('txtRemolque1').value != '' && document.getElementById('txtRemolque2').value != '' && document.getElementById("txtDolly").value == '')) {
            lblErrorAjax.innerText = 'Debe capturar un Dolly, ya que se tienen 2 remolques'
            return;
            }*/

            ValidadEvidenciaOperador(document.getElementById("txtOperador1").value, 1)

            ValidadEvidenciaOperador(document.getElementById("txtOperador2").value, 2)

            document.getElementById("btnGuarda2").disabled = true;
            MuestraCargando(true);
            var Array1 = new Array(); var rowIndex;
            var oRow; var intAsignacion; var grid;
            var DatoAsigAgenda = 0;
            DatoAsigAgenda = document.getElementById('txt_hdPasoAgenda').value;
            //verifica si la asignacion se abrio de la agenda
            if (DatoAsigAgenda == null) {
                DatoAsigAgenda = 0;
            }

            if (DatoAsigAgenda == 1) {
                if (document.getElementById('txtIdGrid').value == 'uwgAgenda') {
                    rowIndex = window.opener.igtbl_getGridById('uwgAgenda').ActiveRow
                    oRow = window.opener.igtbl_getRowById(rowIndex)
                    grid = window.opener.igtbl_getGridById('uwgAgenda');
                    Array1[16] = "0";  //no le mando flota ya que estoy enviando la unidad 0
                }
                if (oRow != null) {
                    intAsignacion = oRow.getCellFromKey("id_asignacion").getValue();
                    if (intAsignacion == null) { intAsignacion = ''; }
                } else {
                    intAsignacion = '';
                }
                Array1[0] = document.getElementById("txt_hdAsignacion").value
                document.getElementById("txtActualiza").value = document.getElementById("txt_hdAsignacion").value
                if (document.getElementById('txtUnidad').value == '') {
                    document.getElementById('txtUnidad').value = '0'
                }
                Array1[1] = document.getElementById('txtUnidad').value;
                //Recorre los pedidos seleccionados
                if (grid != null) {
                    var strLista = ""; var strLista2 = ""; var strLista3 = ""; var aiAreaUsuario
                    var strURL; var winAsignar; var lRow;
                    for (lRow in grid.SelectedRows) {
                        var oRow2; var value;
                        if (document.getElementById('txtIdGrid').value == 'uwgAgenda') {
                            oRow2 = window.opener.igtbl_getRowById(lRow);
                            aiAreaUsuario = window.opener.document.form1.txtAreaUsuario.value;

                        }
                        value = oRow2.getCellFromKey("id_pedido").getValue();
                        if (value != null) { strLista = strLista + value + "[;]"; }
                        value = oRow2.getCellFromKey("id_area").getValue();
                        if (value != null) { strLista2 = strLista2 + value + "[;]"; }
                        value = oRow2.getCellFromKey("id_pedidopk").getValue();
                        if (value != null) { strLista3 = strLista3 + value + "[;]"; }
                        if (oRow2.getCellFromKey("id_statusseg").getValue() != null) {
                            var aiStatusSeg = oRow2.getCellFromKey("id_statusseg").getValue();
                            if (aiStatusSeg == 5 && oRow2.getCellFromKey("id_area").getValue() != aiAreaUsuario) {
                                alert("El Pedido tiene estatus de Transbordo y debe pertenecer a la misma �rea del Usuario para poder asignarlo nuevamente.");
                                return;
                            }
                        }
                    }
                    if (oRow2 == undefined) {
                        var oRow3; var value; var continuaSinPedidos = false;
                        if (grid.getActiveCell() == undefined)
                        { continuaSinPedidos = true; }
                        if (continuaSinPedidos == false) {
                            oRow3 = grid.getActiveCell().Row;
                            value = oRow3.getCellFromKey("id_pedido").getValue();
                            if (value != null) { strLista = strLista + value + "[;]"; }
                            value = oRow3.getCellFromKey("id_area").getValue();
                            if (value != null) { strLista2 = strLista2 + value + "[;]"; }
                            value = oRow3.getCellFromKey("id_pedidopk").getValue();
                            if (value != null) { strLista3 = strLista3 + value + "[;]"; }
                            if ((strLista == "") || (strLista2 == "") || (strLista3 == "")) {
                                alert("Debe seleccionar uno o m�s Pedidos.");
                                return;
                            }
                        }
                    }
                }
            } else {

            }

            Array1[2] = strLista;
            Array1[3] = strLista2;
            Array1[4] = strLista3;
            if (document.getElementById("txtRuta").value == '') {
                document.getElementById("txtRuta").value = '0';
            }
            Array1[5] = document.getElementById("txtRuta").value
            Array1[6] = FormatoFecha(igedit_getById('wdcInicioViaje').getText(), document.getElementById('txtFechaDefault').value)
            Array1[7] = FormatoFecha(igedit_getById('wdcFinViaje').getText(), document.getElementById('txtFechaDefault').value)
            Array1[8] = document.getElementById("txtIngreso").value
            if (document.getElementById("txtHrsEstandar").value == '') {
                document.getElementById("txtHrsEstandar").value = '0'
            }
            Array1[9] = document.getElementById("txtHrsEstandar").value
            Array1[10] = document.getElementById("txtKit").value
            Array1[11] = document.getElementById("txtLineaRem1").value
            Array1[12] = document.getElementById("txtRemolque1").value
            Array1[13] = document.getElementById("txtDolly").value
            Array1[14] = document.getElementById("txtLineaRem2").value
            Array1[15] = document.getElementById("txtRemolque2").value
            Array1[17] = document.getElementById("ddlConfigViaje").value;
            Array1[18] = document.getElementById("txtOperador1").value
            Array1[19] = document.getElementById("txtOperador2").value
            Array1[20] = document.getElementById("txtObservaciones").value
            Array1[21] = document.getElementById("txtSeguimiento").value
            Array1[22] = document.getElementById("txtLineaRem1Original").value
            Array1[23] = document.getElementById("txtRemolque1Original").value
            Array1[24] = document.getElementById("txtLineaRem2Original").value
            Array1[25] = document.getElementById("txtRemolque2Original").value
            Array1[26] = document.getElementById("txtUnidadOriginal").value
            Array1[27] = document.getElementById("txtFechaDefault").value
            document.form1.txtAsignacion2.value = intAsignacion;
            // Asignacion seleccionada del grid Unidades de Despacho
            Array1[28] = document.form1.txtAsignacion2.value;
            //Se agrega la informacion de la Capacidad de Carga KG y M3, emartinez 20/Jun/2012
            if (document.getElementById('txtHdnParCapCarga').value == 'S') {
                Array1[29] = document.getElementById("txtCargarM3").value
                Array1[30] = document.getElementById("txtCargaKG").value
            } else {
                Array1[29] = "0"
                Array1[30] = "0"
            }

            Array1[31] = document.getElementById("txtRemitente").value;
            Array1[32] = document.getElementById("txtDestinatario").value;

            Array1[33] = document.getElementById("lblRemDestHabil").value;
            Array1[34] = document.getElementById("txthdnEsCargado").value;

            // Si esta el cambio de fianzas en la asignacion de pedidos, valida
            if (document.getElementById('DivFianza1').style.visibility == 'visible') {
                var arrFianza = new Array()
                var arrNacionalidad = new Array()
                //valida la fianza de los remolques americanos
                if (Array1[11] != '') { //Si tiene capturada Linea 1
                    // valida que el remolque este activo, siempre y cuando sea impo
                    arrFianza = EditarFianza(Array1[11], Array1[12], '1', false)
                    if (arrFianza.value[0] == 'Error') {
                        MuestraCargando(false);
                        document.getElementById("btnGuarda2").disabled = false;
                        return;
                    }
                    else {
                        //Valida que este capturada la fianza
                        //Se agrega la validacion del tipo de servicio 'E', Jdlcruz, GASA 29-Nov-2012, Folio: 17748.
                        if (arrFianza.value[1] == 'I' || arrFianza.value[1] == 'E') {
                            if (document.getElementById('txtFianza1').value == '') {
                                document.getElementById('lblErrorAjax').innerText = 'El Remolque ' + Array1[12] + ' no tiene una fianza capturada. Favor de capturarla.'
                                MuestraCargando(false);
                                document.getElementById("btnGuarda2").disabled = false;
                                return;
                            }
                            else {
                                if (FianzaNoRtornada(arrFianza.value[2]) == false) {
                                    document.getElementById('lblErrorAjax').innerText = 'El Remolque ' + Array1[12] + ' tiene una fianza retornada. Favor de cambiarla.'
                                    MuestraCargando(false);
                                    document.getElementById("btnGuarda2").disabled = false;
                                    return;
                                }
                            }
                        }
                        else {
                            if (FianzaNoRtornada(arrFianza.value[2]) == false) {
                                document.getElementById('lblErrorAjax').innerText = 'El Remolque ' + Array1[12] + ' tiene una fianza retornada. Favor de cambiarla.'
                                MuestraCargando(false);
                                document.getElementById("btnGuarda2").disabled = false;
                                return;
                            }
                        }
                    }
                }
                if (Array1[14] != '') { //Si tiene capturada Linea 2
                    arrFianza = EditarFianza(Array1[14], Array1[15], '1', false)
                    if (arrFianza.value[0] == 'Error') {
                        MuestraCargando(false);
                        document.getElementById("btnGuarda2").disabled = false;
                        return;
                    }
                    else {
                        //Se agrega la validacion del tipo de servicio 'E', Jdlcruz, GASA 29-Nov-2012, Folio: 17748.
                        if (arrFianza.value[1] == 'I' || arrFianza.value[1] == 'E') {
                            if (document.getElementById('txtFianza2').value == '') {
                                document.getElementById('lblErrorAjax').innerText = 'El Remolque ' + Array1[15] + ' no tiene una fianza capturada. Favor de capturarla.'
                                MuestraCargando(false);
                                document.getElementById("btnGuarda2").disabled = false;
                                return;
                            }
                            else {
                                if (FianzaNoRtornada(arrFianza.value[2]) == false) {
                                    document.getElementById('lblErrorAjax').innerText = 'El Remolque ' + Array1[12] + ' tiene una fianza retornada. Favor de cambiarla.'
                                    MuestraCargando(false);
                                    document.getElementById("btnGuarda2").disabled = false;
                                    return;
                                }
                            }
                        }
                        else {
                            if (FianzaNoRtornada(arrFianza.value[2]) == false) {
                                document.getElementById('lblErrorAjax').innerText = 'El Remolque ' + Array1[12] + ' tiene una fianza retornada. Favor de cambiarla.'
                                MuestraCargando(false);
                                document.getElementById("btnGuarda2").disabled = false;
                                return;
                            }
                        }
                    }
                }

                //Obtengo la nacionalidad para validar la fianza de los remolques propios americanos.
                //Jdlcruz, GASA 29-Nov-2012, Folio: 17748.
                if (Array1[11] == '' && Array1[12] != '') { //Si no tiene capturada la Linea 1 y pero si capturado el remolque 1
                    //Obtengo la nacionalidad del remolque.
                    arrNacionalidad = pridesp01_pre_i_despasignarpedidos.GetNacionalidadRemolque(Array1[12]);
                    if (arrNacionalidad.error != null) {
                        alert("Error:" + arrNacionalidad.error);
                        return;
                    }
                    if (arrNacionalidad.value[0] != 'Error') {
                        //Si la nacionalidad es Americana (2) raliza la validacion de remolque activo y fianza obligada.
                        if (arrNacionalidad.value[0] == 2) {
                            // valida que el remolque este activo, siempre y cuando sea impo
                            arrFianza = EditarFianza(Array1[11], Array1[12], '1', false)
                            if (arrFianza.value[0] == 'Error') {
                                MuestraCargando(false);
                                document.getElementById("btnGuarda2").disabled = false;
                                return;
                            }
                            else {
                                //Valida que este capturada la fianza
                                if (arrFianza.value[1] == 'I' || arrFianza.value[1] == 'E') {
                                    if (document.getElementById('txtFianza1').value == '') {
                                        document.getElementById('lblErrorAjax').innerText = 'El Remolque ' + Array1[12] + ' no tiene una fianza capturada. Favor de capturarla.'
                                        MuestraCargando(false);
                                        document.getElementById("btnGuarda2").disabled = false;
                                        return;
                                    }
                                    else {
                                        if (FianzaNoRtornada(arrFianza.value[2]) == false) {
                                            document.getElementById('lblErrorAjax').innerText = 'El Remolque ' + Array1[12] + ' tiene una fianza retornada. Favor de cambiarla.'
                                            MuestraCargando(false);
                                            document.getElementById("btnGuarda2").disabled = false;
                                            return;
                                        }
                                    }
                                }
                                else {
                                    if (FianzaNoRtornada(arrFianza.value[2]) == false) {
                                        document.getElementById('lblErrorAjax').innerText = 'El Remolque ' + Array1[12] + ' tiene una fianza retornada. Favor de cambiarla.'
                                        MuestraCargando(false);
                                        document.getElementById("btnGuarda2").disabled = false;
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    else {
                        document.getElementById('lblErrorAjax').innerHTML = arrNacionalidad.value[1];
                        return arrControl;
                    }
                }

                //Obtengo la nacionalidad para validar la fianza de los remolques propios americanos.
                //Jdlcruz, GASA 29-Nov-2012, Folio: 17748.
                if (Array1[14] == '' && Array1[15] != '') { //Si no tiene capturada la Linea 2 y pero si capturado el remolque 2
                    //Obtengo la nacionalidad del remolque.
                    arrNacionalidad = pridesp01_pre_i_despasignarpedidos.GetNacionalidadRemolque(Array1[15]);
                    if (arrNacionalidad.error != null) {
                        alert("Error:" + arrNacionalidad.error);
                        return;
                    }
                    if (arrNacionalidad.value[0] != 'Error') {
                        //Si la nacionalidad es Americana (2) raliza la validacion de remolque activo y fianza obligada.
                        if (arrNacionalidad.value[0] == 2) {
                            // valida que el remolque este activo, siempre y cuando sea impo
                            arrFianza = EditarFianza(Array1[14], Array1[15], '1', false)
                            if (arrFianza.value[0] == 'Error') {
                                MuestraCargando(false);
                                document.getElementById("btnGuarda2").disabled = false;
                                return;
                            }
                            else {
                                //Valida que este capturada la fianza
                                if (arrFianza.value[1] == 'I' || arrFianza.value[1] == 'E') {
                                    if (document.getElementById('txtFianza1').value == '') {
                                        document.getElementById('lblErrorAjax').innerText = 'El Remolque ' + Array1[15] + ' no tiene una fianza capturada. Favor de capturarla.'
                                        MuestraCargando(false);
                                        document.getElementById("btnGuarda2").disabled = false;
                                        return;
                                    }
                                    else {
                                        if (FianzaNoRtornada(arrFianza.value[2]) == false) {
                                            document.getElementById('lblErrorAjax').innerText = 'El Remolque ' + Array1[12] + ' tiene una fianza retornada. Favor de cambiarla.'
                                            MuestraCargando(false);
                                            document.getElementById("btnGuarda2").disabled = false;
                                            return;
                                        }
                                    }
                                }
                                else {
                                    if (FianzaNoRtornada(arrFianza.value[2]) == false) {
                                        document.getElementById('lblErrorAjax').innerText = 'El Remolque ' + Array1[12] + ' tiene una fianza retornada. Favor de cambiarla.'
                                        MuestraCargando(false);
                                        document.getElementById("btnGuarda2").disabled = false;
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    else {
                        document.getElementById('lblErrorAjax').innerHTML = arrNacionalidad.value[1];
                        return arrControl;
                    }
                }
            } // Termina cambio de fianzas


            //================================hrflores 20/06/2013 Cambio Contenedores de Pedidos================================//
            Array1[34] = document.getElementById("txtParamContenedores").value;
            Array1[35] = document.getElementById("txtIdPedido_Contenedores1").value;
            Array1[36] = document.getElementById("txtareaPedido_Contenedores1").value;
            Array1[37] = document.getElementById("txtIdPedido_Contenedores2").value;
            Array1[38] = document.getElementById("txtareaPedido_Contenedores2").value;

            Array1[39] = document.getElementById("txtIdContenedor1").value;
            Array1[40] = document.getElementById("txtIdContenedor1b").value;


            Array1[41] = document.getElementById("txtIdContenedor2").value;
            Array1[42] = document.getElementById("txtIdContenedor2b").value;

            //=================================================================================================================//
            //megallegos 08/08/2016 - SIMSA Folio: 22277
            var strTipoOp_AsigVV = document.getElementById("hdnTipoOp_AsigVV").value;
            if (strTipoOp_AsigVV == "S") {
                Array1[43] = document.getElementById("ddlTipoOp").value;
            } else {
                Array1[43] = document.getElementById("ddlOperacion").value;
            }

            //rherrera ETF 24/11/14
            var paramETF = document.getElementById("hdnParamTresFronteras").value;
            if (paramETF == "S") {
                var bolCheckUrgente = document.getElementById("chkPedidoUrgente").checked;
                if (bolCheckUrgente == true) {
                    Array1[44] = '1';
                }
                else {
                    Array1[44] = '0';
                }
            }

            //ereyes 02/12/2015: OS Xpress Internacional Viajes vacios Auto
            if (document.getElementById("hdn_viajevacioauto").value == "S") {
                var TipoSegDetona = document.getElementById("hdn_tiposegdetonador").value;
                //1 :Validar que el tipo de seguimiento alla sido capturado
                if (document.getElementById("txtSeguimiento").value == "") {
                    document.getElementById('lblErrorAjax').innerText = 'Favor de capturar el tipo de seguimiento';
                    MuestraCargando(false)
                    return;
                } else {
                    //1 :Validar el tipo de seguimiento sea exportacion
                    if (document.getElementById("txtSeguimiento").value == TipoSegDetona) {
                        //valida que el ultimo destino coincida con el origen de al ruta actual si no coincide abrir ventana de creacion de viaje vacio.
                        var arreglo = new Array();
                        var arrRegreso = new Array();
                        var Unidad = document.getElementById('txtUnidad').value + "";
                        var RutaNueva = document.getElementById("txtRuta").value + "";
                        var Operador1 = document.getElementById("txtOperador1").value + "";
                        var Linea1 = document.getElementById("txtLineaRem1").value + "";
                        var Rem1 = document.getElementById("txtRemolque1").value + "";

                        arrRegreso = pridesp01_pre_i_despasignarpedidos.UltimoDestinoNuevoOrigen(Unidad, RutaNueva);
                        if (arrRegreso.value[0] == "Error") {
                            alert(arrRegreso.value[1]);
                            MuestraCargando(false)
                            document.getElementById("btnGuarda").disabled = false;
                            return;
                        } else {
                            if (arrRegreso.value[0] > 0) {
                                //compara el utimo destino sea igual al nuevo origen para poder permitir continuar
                                if (arrRegreso.value[0] != arrRegreso.value[1]) {
                                    //abrir ventana de nueva asignacion de viaje vacio
                                    MuestraCargando(false)
                                    var strURL = "";
                                    var winVacio;
                                    strURL = '../pridesp01/pre_i_despviajevacioauto.aspx?Accion=Nuevo&Lista=vacio[;]&Unidad=' + Unidad + '&Lista2=vacio[;]&Flota=0&Lista3=vacio[;]&IdGrid=uwgUnidades&UltDestino=' + arrRegreso.value[0] + '&OrigenRNueva=' + arrRegreso.value[1] + '&FechaIncio=' + arrRegreso.value[2] + "&operador1=" + Operador1 + "&LineaRem1=" + Linea1 + "&Rem1=" + Rem1;
                                    winVacio = window.open(strURL, 'winVacioX', 'width=850,height=500,status=no,scrollbars=no,left=50,top=50,resizable=no');
                                    winVacio.focus();
                                    document.getElementById("btnGuarda").disabled = false;
                                    return;
                                }
                            }
                        }
                    }
                }
            }

            if (document.getElementById('diVentanaTiempos').style.display == 'block') {

                if (document.getElementById('txtIdventana').value == "") {
                    //alert("Es necesario capturar el numero de ventana de tiempos. Ya que el convenio de el pedido asignado lo requiere")
                    document.getElementById('lblErrorAjax').innerText = 'Es necesario capturar el numero de ventana de tiempos. Ya que el convenio de el pedido asignado lo requiere';
                    MuestraCargando(false);
                    document.getElementById("btnGuarda").disabled = false;
                    return;
                } else {
                    Array1[45] = document.getElementById('txtIdventana').value
                }

            } else {
                Array1[45] = 'NULL'
            }
            //=================================================================================================================//
            //oerc 14/11/2016 - VacioAreaTracto Folio: 22592
            var strVacioAreaTracto = document.getElementById("hdnVacioXAreaTracto").value;
            if (strVacioAreaTracto == "S") {
                Array1[46] = 'S';
            } else {
                Array1[46] = 'N';
            }

            //rherrera GAAL
            if (document.getElementById('hdnObtenerGasolineras').value == 'S') {
                if (!document.getElementById("txtRutaRegreso").value == '') {
                    //document.getElementById("txtRutaRegreso").value = '0';
                    Array1[47] = document.getElementById("txtRutaRegreso").value; //Ruta de Regreso
                    Array1[48] = FormatoFecha(igedit_getById('wdcInicioViaje').getText(), document.getElementById('txtFechaDefault').value); //F_prog_ini_viaje
                    Array1[49] = FormatoFecha(igedit_getById('wdcFinViaje').getText(), document.getElementById('txtFechaDefault').value); //F_prog_fin_viaje
                    if (document.getElementById("txtHrsEstandar").value == '') {
                        document.getElementById("txtHrsEstandar").value = '0';
                    }
                    Array1[50] = document.getElementById("txtHrsEstandar").value; //HrsEstandar
                    Array1[51] = "1";  //viaje_regreso
                    wayPoints_regreso = pridesp01_pre_i_despasignarpedidos.getPuntosInteres(Array1[47]);
                }
                else {
                    Array1[51] = "0";  //viaje_regreso
                }
                //Se trae los waypoints de las rutas
                wayPoints = pridesp01_pre_i_despasignarpedidos.getPuntosInteres(Array1[5]);

                //Obtiene el rendimiento de la Unidad, comparando si est� capturado el remolque 2
                if (Array1[15] != '') { //si es full
                    rendimientoUnidad = 1.5
                }
                else {
                    rendimientoUnidad = 2.0
                }

            }

             if (document.getElementById('hdnValidaDocumentos').value == 'S') {
                Array1[52] = document.getElementById("ddlTipoServicio").value;
            }

            //FOLIO: 24328, GAFIGUEROA 28/04/2018
            if (document.getElementById("HdnCambiosOpeAlmex").value == "S") {
                Array1[53] = '0';
                Array1[54] = document.getElementById("HdnIdRecorrido").value;
                Array1[55] = document.getElementById("HdnIdRecorridoOrigen").value;
                Array1[56] = document.getElementById("HdnIdRecorridoDestino").value;
            }

            // AGONZALEZ FOLIO:24443 30/05/2018
            if (document.getElementById('hdnLigaMonitoreo').value == 'S') {
                if (!document.getElementById("txtVelMax").value == '') {
                    Array1[58] = document.getElementById("txtVelMax").value;
                }
                else {
                    Array1[58] = "0";
                    document.getElementById('lblErrorAjax').innerText = 'Es necesario capturar la velocidad maxima';
                    MuestraCargando(false);
                    document.getElementById("btnGuarda").disabled = false;
                    return;
                }
            } else {
                Array1[58] = "0";
            }

            pridesp01_pre_i_despasignarpedidos.Guardar2(Array1, Guardar_CallBack2);
         }

            function Guardar_CallBack2(response) {
                if (response.error != null) { return; }
                if (response.value[0] == 'Error') {
                    document.getElementById("btnGuarda2").disabled = false;
                    lblErrorAjax.innerText = response.value[1]
                    MuestraCargando(false)
                } else {
                    try {
                        if (window.opener.igtbl_getGridById('uwgAgenda') != null) {
                            var rowIndex; var oRow;
                            rowIndex = window.opener.igtbl_getGridById('uwgAgenda').ActiveRow
                            oRow = window.opener.igtbl_getRowById(rowIndex)
                            if (response.value[0] == 'MAS') {
                                var obtnConsulta = window.opener.document.getElementById("imgbtnConsultar");
                                obtnConsulta.click();
                            } else {
                                if (window.opener.igtbl_getGridById('uwgAgenda').GroupByBox.groups.length == 0) {
                                    ColumnasPedidosAgenda(response, oRow, 1)
                                } else {
                                    var oRowId = window.opener.igtbl_getGridById('uwgAgenda').oActiveRow.Id
                                    var oRow = window.opener.igtbl_getRowById(oRowId)
                                    ColumnasPedidosAgenda(response, oRow, 1)
                                }
                            }
                            oRow.setSelected(true);
                        }
                    } catch (er) {
                        alert("Error Ajax: " + er.messege)
                        window.close()
                    }
                    MuestraCargando(false)
                    window.close()
                    lblErrorAjax.innerText = ""
                }
            }

    </script>

        <script language="javascript">

        // -- =============================== Add ========================================== --
        // function validaOperator(){
        // 	var response;
        	// alert(document.getElementById('txtOperador1').value);
        	// pridesp01_pre_i_despasignarpedidos.GetData(numberOp,GetData_CallBack);
        // };



        function GetData(numberOp) {
        	var response;
          // alert(document.getElementById('txtOperador1').value.length);
          // alert(numberOp);

        	pridesp01_pre_i_despasignarpedidos.GetData(numberOp,GetData_CallBack);

        }

        function GetData_CallBack(response) {
          var response=response.value;
          // alert(response);
          // document.getElementById('node-id').innerHTML = response;
          var pieces = response.split(",");
          var blocks = parseInt(pieces[0]);
          var restDays = pieces[1];
          var namae = pieces[2];
          var id_operador = pieces[3];
          var folio = pieces[4];
          var vence = pieces[5];
          // alert("block => " + blocks);
          // alert(typeof(blocks));
          // alert(vence);
          	if(blocks == 0) {
            alert("Licencia expirada desde " + restDays + " dias");
            document.getElementById("btnGuarda").disabled = true;
            // lblError.innerText = "";
            lblErrorAjax.innerText = "Licencia del operador "+ namae +" expirada desde " + restDays + " dias";
        	} else if (blocks == 1) {
            alert("Licencia Vence en " + restDays + " dias");
            document.getElementById("btnGuarda").disabled = true;
            // lblError.innerText = "";
            lblErrorAjax.innerText = "La Licencia del Operador tiene menos de 10 dias para vencer";
          } else if (blocks == 2) {
            alert("Licencia sin Actualizar");
            document.getElementById("btnGuarda").disabled = true;
            // lblError.innerText = "";
            lblErrorAjax.innerText = "La Licencia del Operador necesita Actualizarse";
          } else if (blocks == 3) { // Entre 10 y 30 dias
            alert("Licencia Vence en " + restDays + " dias");
            // if button is disabled just enable
            if (document.getElementById("btnGuarda").disabled == true) {
              document.getElementById("btnGuarda").disabled = false;
            }
            document.getElementById('txtOperador1Nombre').length=0;
            document.getElementById('txtOperador1Nombre').value=namae;
            // lblError.innerText = "";
            lblErrorAjax.innerText = "La Licencia del Operador Vence en " + restDays + " dias";
          } else if (blocks == 4) {
            alert("El Operador esta dado de Baja");
            document.getElementById("btnGuarda").disabled = true;
            // lblError.innerText = "";
            lblErrorAjax.innerText = "El Operador " + namae + " esta dado de baja";
        	} else { // mayor a un mes
            // if button is disabled just enable
            if (document.getElementById("btnGuarda").disabled == true) {
              document.getElementById("btnGuarda").disabled = false;
            }
            // Clear current data
        		document.getElementById('txtOperador1Nombre').length=0;
            // set the new value
            document.getElementById('txtOperador1Nombre').value=namae;
            lblErrorAjax.innerText = ""
            // lblError.innerText = ""
        	}
        } // NOTE End Callback
            // -- =============================== NOTE Add  END========================================== --
    </script>
    <script language="javascript" type="text/javascript">
        function BuscaRemDes(as_campo, as_descr, as_domi, as_plazac, as_idplaza) {
            var strURL; var winCliente;
            strURL = '../busqueda_estandar.aspx?Search=/srv_i_busquedas/ctrlsearch_clientes.ascx&Campo=' + as_campo + '&Descr=' + as_descr + '&Dom=' + as_domi + '&PlazaC=' + as_plazac + '&Opc=C' + '&IdPlaza=' + as_idplaza;
            winCliente = window.open(strURL, 'BuscaRemDes', 'width=640,height=480,status=no,scrollbars=no');
            winCliente.focus();
        }
        function ValidaRemitente(intRemitente) {
            if (intRemitente == "") {
                document.form1.txtRemitente.value = ""
                document.form1.txtRemitenteNom.value = ""
                document.form1.txtRemitenteDom.value = ""
                document.form1.txtPlazaOrigenNom.value = ""
                document.form1.txtOrigen.value = ""
                lblErrorAjax.innerText = "Favor de capturar un dato en el campo Remitente"
                return;
            }
            else
            { pridesp01_pre_i_despasignarpedidos.ValidaRemitente(intRemitente,Remitente_CallBack); }
        }
        function Remitente_CallBack(response) {
            if (response.error != null) { return; }
            if (response.value[0] == 'Error') {
                document.form1.txtRemitente.value = ""
                document.form1.txtRemitenteNom.value = ""
                document.form1.txtRemitenteDom.value = ""
                document.form1.txtPlazaOrigenNom.value = ""
                document.form1.txtOrigen.value = ""
                lblErrorAjax.innerText = response.value[1]
            }
            else {
                document.form1.txtRemitente.value = response.value[0]
                document.form1.txtRemitenteNom.value = response.value[1]
                lblErrorAjax.innerText = ""
            }
        }
        function ValidaDestinatario(valor) {
            pridesp01_pre_i_despasignarpedidos.ValidaDestinatario(valor,Destinatario_CallBack);
        }
        function Destinatario_CallBack(response){
             if (response.error != null){return;}
                if (response.value[0] == 'Error')
                    {
                      lblErrorAjax.innerText = response.value[1]
                      document.form1.txtDestinatario.value = ""
                      document.form1.txtDestinatarioNom.value = ""
                      document.form1.txtDestinatarioDom.value = ""
                      document.form1.txtPlazaDestinoNom.value = ""
                      document.form1.txtDestino.value = ""
                     }
                else
                {
                    document.form1.txtDestinatario.value = response.value[0]
                    document.form1.txtDestinatarioNom.value = response.value[1]
                    lblErrorAjax.innerText = ""
                }
             }

             function BuscaContenedor(as_campo, as_cont) {
                 var strURL; var winBusCont;
                 strURL = '../busquedas/busqueda_contenedores.aspx?Campo=' + as_campo + '&Contenedor=' + as_cont;
                 winBusCont = window.open(strURL, 'BuscConte', 'width=640,height=480,status=0,scrollbars=0');
                 winBusCont.focus();
             }

             //Valida Contenedor
             function ValidaContenedor(strContenedor, strID) {
                 pridesp01_pre_i_desppedidos.ValidaContenedor(strContenedor, strID, Contenedor_CallBack);
             }

             function Contenedor_CallBack(response) {
                 if (response.error != null) { return; }
                 if (response.value[0] == 'Error') {
                     lblErrorAjax.innerText = response.value[1]
                     if (response.value[2] == 'Contenedor 1') {
                         document.form1.txtCont1Ped1.value = ""
                         document.form1.txtIdContenedor1.value = ""
                     }
                     else if (response.value[2] == 'Contenedor 1b') {
                        document.form1.txtCont2Ped1.value = ""
                        document.form1.txtIdContenedor1b.value = ""
                     }
                     else if (response.value[2] == 'Contenedor 2') {
                     document.form1.txtCont1Ped2.value = ""
                     document.form1.txtIdContenedor2.value = ""
                     }
                     else {
                         document.form1.txtCont2Ped2.value = ""
                         document.form1.txtIdContenedor2b.value = ""
                     }
                 }
                 else {
                     if (response.value[0] == 'Contenedor 1') {
                         document.form1.txtCont1Ped1.value = response.value[1]
                         document.form1.txtIdContenedor1.value = response.value[2]
                     }
                     else if (response.value[0] == 'Contenedor 1b') {
                     document.form1.txtCont2Ped1.value = response.value[1]
                     document.form1.txtIdContenedor1b.value = response.value[2]
                     }
                     else if (response.value[0] == 'Contenedor 2') {
                     document.form1.txtCont1Ped2.value = response.value[1]
                     document.form1.txtIdContenedor2.value = response.value[2]
                     }
                     else {
                         document.form1.txtCont2Ped2.value = response.value[1]
                         document.form1.txtIdContenedor2b.value = response.value[2]
                     }
                     lblErrorAjax.innerText = ""
                 }
             }

             function BuscaPlaza(as_campo,as_desc){
                    var strURL; var winBuscPlaza;
                    strURL = '../busqueda_estandar.aspx?Search=/srv_i_busquedas/ctrlsearch_plazas.ascx&Campo=' + as_campo + '&Descr=' + as_desc + '&Opc=Geo';
                    winBuscPlaza = window.open(strURL,'BuscPlaza','width=600,height=480,status=no,scrollbars=no,left=150,top=150');
                    winBuscPlaza.focus();
                }

             function GetPlazaOrigen(valor) {
                    if (valor == "") {
                        window.document.form1.WebPanel1_txtOrigenDesc.value = ""
                        return
                    }
                    else {
                        var strtest = '' + valor;
                        pridesp01_pre_i_despasignarpedidos.GetDescPlaza(strtest, GetPlazaOrigen_CallBack);
                    }
                    return
                }

            function GetPlazaOrigen_CallBack(response) {
                if (response.error != null) { return; }

                if (response.value[0] == 'Error') {
                    window.document.form1.txtOrigenDesc.value = "";
                }

                else {
                    window.document.form1.txtOrigenDesc.value = response.value[0];
                }
            }

            function GetPlazaDestino(valor) {
                if (valor == "") {
                    window.document.form1.txtDestinoDesc.value = "";
                    return
                }
                else {
                    var strtest = '' + valor;
                    pridesp01_pre_i_despasignarpedidos.GetDescPlaza(strtest, GetPlazaDestino_CallBack);
                }
                return
            }

            function GetPlazaDestino_CallBack(response) {
                if (response.error != null) { return; }

                if (response.value[0] == 'Error') {
                    window.document.form1.txtDestinoDesc.value = "";
                }

                else {
                    window.document.form1.txtDestinoDesc.value = response.value[0];
                }
            }
            // GPEREZ F:24656 11/07/18 -- Busqueda de clientes del destino de la ruta.
            function BuscaCliente(as_campo, as_descr) {
                var ruta = document.form1.txtRuta.value;
                if (ruta != "0" && ruta != "") {
                    var data = pridesp01_pre_i_despasignarpedidos.GetDestinoRuta(ruta);
                    var datos = JSON.parse(data.value);
                    var idPlazaDestino = datos[0].destino_ruta;
                    var strURL; var winCliente;
                    strURL = '../busqueda_estandar.aspx?Search=/srv_i_busquedas/ctrlsearch_clientes.ascx&Campo=' + as_campo + '&Descr=' + as_descr + '&Opc=C' + '&Plaza=' + idPlazaDestino;
                    winCliente = window.open(strURL, 'BuscaCliente', 'width=640,height=480,status=no,scrollbars=no,top=150,left=150');
                    winCliente.focus();
                    lblErrorAjax.innerText = ""
                }
                else {
                    lblErrorAjax.innerText = "Favor de seleccionar una ruta"
                }
            }
    </script>
    <style type="text/css">
        .style1{
            height: 26px;
        }
        .style2{
            height: 22px;
        }
        .style3
        {
            width: 191px;
        }
    </style>
</head>
<body leftmargin="5" topmargin="5" onload="Load()">
  <div class="oper">
    <asp:Label id="lblOutputOp" runat="server" style="display:none;"></asp:Label>
  </div>
    <form id="form1" method="post" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div id="contenido">
            <table border="0">
                <tr>
                    <td colspan="1">
                        <table style="background-position: right center; background-repeat: no-repeat" height="30"
                            background="../Imagenes/backs/fondoCh.gif" border="0">
                            <tr>
                                <td>
                                    Num. Asignaci�n</td>
                                <td width="120">
                                    <asp:TextBox ID="txtNumAsignacion" TabIndex="-1" runat="server" CssClass="FechaHora"
                                        ReadOnly="True" Height="20px" BackColor="Transparent"></asp:TextBox></td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table>
                             <tr>
                                <td>
                                    <input id="hdnStatus" type="hidden" style="width: 15px; height: 15px" runat="server" />
                                </td>
                             </tr>
                        </table>
                    </td>
                    <td>
                        <table border="0">
                            <tr>
                                <td>
                                </td>
                                <td>
                                   <!-- <div id="DivConfigViaje" style="display: none"> -->
                                   <div id="DivConfigViaje" runat="server" style="display: none"><!--AMONTIEL FOLIO:25313  11/12/2018 -->
                                        <asp:DropDownList ID="ddlConfigViaje" TabIndex="50" runat="server" CssClass="labels">
                                            <asp:ListItem Value="1">Unidad Motriz</asp:ListItem>
                                            <asp:ListItem Value="2">Sencillo</asp:ListItem>
                                            <asp:ListItem Value="3">Full</asp:ListItem>
                                            <asp:ListItem Value="0">Sin Asignar</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </td>
                                <td>
                                    <input type="hidden" runat="server" id="txthdnEsCargado" value="0" />
                                    <input type="hidden" runat="server" id="txtHdnValidaRem1" value="0" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width = "325" align="right">
                        <input id="txtHdnParCapCarga" runat=server type="hidden" />
                        <asp:HiddenField ID="txt_hdPasoAgenda" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                    </td>
                    <td>
                        <input id="hdn_viajevacioauto" runat="server" value="N" type="hidden" />
                        <input id="hdn_tiposegdetonador" runat="server" value="" type="hidden" />
                    </td>
                    <td>
                    </td>
                    <td width = "325" align="right">
                        <asp:CheckBox ID="chkPedidoUrgente" AutoPostBack="true" OnCheckedChanged="chkPedidoUrgente_CheckedChanged" visible="true"  runat="server" Text="Pedido urgente" ToolTip="Marcar pedido como urgente"  />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table style="background-position: right center; background-repeat: no-repeat" height="30"
                                    background="../Imagenes/backs/FONDOMdGde2.gif" border="0">
                                    <tr>
                                        <td width="50">
                                            Ruta
                                        </td>
                                        <td>
                                            <asp:TextBox onkeypress="return IsNum(event);" ID="txtRuta" TabIndex="10" runat="server"
                                                CssClass="ID" MaxLength="9" onchange="ValidaRuta(this.value,document.form1.txtUnidad.value,igedit_getById('wdcInicioViaje').getText()); validarDocumentos();"
                                                Height="20px" AutoPostBack="false"></asp:TextBox>&nbsp;
                                            <asp:TextBox ID="txtRutaDesc" TabIndex="-1" runat="server" CssClass="desc" ReadOnly="True"
                                                BackColor="Transparent" Height="20px" Width="200px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvRuta" runat="server" Display="Dynamic" ControlToValidate="txtRuta"
                                                ErrorMessage="Favor de capturar un dato en el campo Ruta">*</asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="cvRuta" runat="server" Display="Dynamic" ControlToValidate="txtRuta"
                                                ErrorMessage="El dato capturado en el campo Ruta no es del tipo de dato v�lido."
                                                Type="Integer" Operator="DataTypeCheck">*</asp:CompareValidator>
                                            <asp:CustomValidator ID="cusvRuta" runat="server" Display="Dynamic" ControlToValidate="txtRuta"
                                                ErrorMessage="Dato inv�lido en el campo Ruta">*</asp:CustomValidator>
                                        </td>
                                        <td>
                                            <img id="btnBuscarJefeFlota"  runat="server"  style="cursor: hand" onclick="BuscaRuta('form1.txtRuta','form1.txtRutaDesc');"
                                                alt="Buscar Ruta" src="../Imagenes/BTbusc2.gif" align="absMiddle">
                                        </td>
                                    </tr>
                                </table >
                                <table border="0" width="100%">
                                    <tr>
                                        <td valign="top">
                                            <table style="background-position: right center; background-repeat: no-repeat" height="31"
                                                background="../Imagenes/backs/fondoCh.gif" border="0">
                                                <tr>
                                                    <td>
                                                        Inicio Viaje</td>
                                                    <td>
                                                        <igtxt:WebDateTimeEdit ID="wdcInicioViaje" TabIndex="20" runat="server" CssClass="FechaHora"
                                                            Height="20px">
                                                            <ClientSideEvents ValueChange="ValidaFechas(igedit_getById('wdcInicioViaje').getText(),igedit_getById('wdcFinViaje').getText())" />
                                                        </igtxt:WebDateTimeEdit>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td valign="top">
                                            <table style="background-position: right center; background-repeat: no-repeat" height="31"
                                                background="../Imagenes/backs/fondoCh.gif" border="0" class="style1">
                                                <tr>
                                                    <td width="70" align="right" class="style2">
                                                        Fin Viaje</td>
                                                    <td class="style2">
                                                        <igtxt:WebDateTimeEdit ID="wdcFinViaje" TabIndex="30" runat="server" CssClass="FechaHora"
                                                            Height="20px">
                                                            <ClientSideEvents ValueChange="ValidaFechas(igedit_getById('wdcInicioViaje').getText(),igedit_getById('wdcFinViaje').getText())" />
                                                        </igtxt:WebDateTimeEdit>
                                                    </td>
                                                    <td class="style2">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td valign="top">
                                            <table style="background-position: right center; background-repeat: no-repeat" height="31"
                                                background="../Imagenes/backs/fondoCh.gif" border="0">
                                                <tr>
                                                    <td width="70">
                                                        Hrs. Estandar</td>
                                                    <td>
                                                        <asp:TextBox onkeypress="return IsNum(event);" ID="txtHrsEstandar" TabIndex="40"
                                                            runat="server" CssClass="FechaFiltro" MaxLength="7" onchange="ValidaHora(this.value,igedit_getById('wdcInicioViaje').getText())"
                                                            Height="20px"></asp:TextBox><asp:RequiredFieldValidator ID="rfvHrsEstandar" runat="server"
                                                                ControlToValidate="txtHrsEstandar" ErrorMessage="Favor de capturar un dato en el campo Hrs. Estandar">*</asp:RequiredFieldValidator></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td valign="top">
                                            <div id="divVelMax" runat="server" visible="false">
                                                <table style="background-position: right center; background-repeat: no-repeat" height="31"
                                                    background="../Imagenes/backs/fondoCh.gif" border="0">
                                                    <tr>
                                                        <td>Velocidad M�xima.</td>
                                                        <td>
                                                        <%-- AMONTIEL FOLIO:25313  13/12/2018, Se agrega evento onchange  --%>
                                                        <asp:TextBox onchange="ValidaVelocidad()" onkeypress="return IsNum(event);" ID="txtVelMax" TabIndex="50" runat="server"
                                                            CssClass="ID" MaxLength="3" Height="20px" AutoPostBack="false"></asp:TextBox>&nbsp;
                                                        <td width="70">Kms/h.</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>

                                </table>
                                <asp:Label ID="lblErrorUP1" runat="server" CssClass="labelop" ForeColor="Red" Visible="False"></asp:Label>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="wdcInicioViaje" EventName="ValueChange" />
                                <asp:AsyncPostBackTrigger ControlID="wdcFinViaje" EventName="ValueChange" />
                                <asp:AsyncPostBackTrigger ControlID="txtHrsEstandar" EventName="TextChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <%--GPEREZ F:24656 11/07/18  -- Se agrega destinatario --%>
                        <table id="tblUbicacionDiasRemolque" runat="server" style="display:none;">
                            <tr>
                                <td valign="top">
                                    <table style="background-position: right center; background-image: url(../imagenes/backs/fondoMdGde.gif);
										                    background-repeat: no-repeat">
					                    <tr>
						                    <td width="60">
							                    Destinatario</td>
						                    <td>
							                    <asp:TextBox ID="txtDestinat" onkeypress="return IsInt(event);" runat="server" Height="20"
								                    CssClass="ID" onchange="ValidaDestinatario(this.value);" TabIndex="42" MaxLength="9"></asp:TextBox>
							                    <asp:TextBox ID="txtDestinatNom" runat="server" CssClass="desc" Width="250px" Height="20"
								                    TabIndex="-1" BackColor="Transparent"></asp:TextBox></td>
						                    <td>
							                    <asp:RequiredFieldValidator ID="rfvDestinat" runat="server" ControlToValidate="txtDestinat"
								                    Display="Dynamic" ErrorMessage="Debe capturar un dato en el campo Destinatario">*</asp:RequiredFieldValidator><asp:CompareValidator
									                    ID="cvDestinat" runat="server" ControlToValidate="txtDestinat" Display="Dynamic"
									                    ErrorMessage="El dato capturado en el campo Destinatario no es v�lido." Operator="DataTypeCheck">*</asp:CompareValidator>
							                    <img id="imgBuscDestinat" onclick="BuscaCliente('form1.txtDestinat','form1.txtDestinatNom','form1.txtRuta');"
								                    alt="Buscar Destinatario" name="imgBuscDestinat" src="../imagenes/BTBusc2.gif" style="cursor: hand"
								                    title="Buscar Destinatario" />
								            </td>
					                    </tr>
				                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td colspan="3">
                     <!-- <table id="tablaRutaSiguiente" runat="server" style="background-position: right center; background-repeat: no-repeat" height="30" background="../Imagenes/backs/FONDOMdGde2.gif" border="0">
	                    <tr>
		                    <td >
		                        <asp:Label ID="lblRutaSiguiente" runat="server" Visible="True">Ruta Siguiente </asp:Label>
		                    </td>
		                    <td >
			                    <asp:TextBox onkeypress="return IsNum(event);" ID="txtRutaRegreso" TabIndex="10" runat="server"
			                        onchange="ValidaRutaRegreso(this.value,document.form1.txtUnidad.value,igedit_getById('wdcInicioViaje').getText())"
				                    CssClass="ID" MaxLength="9"
				                    Height="20px" AutoPostBack="false"></asp:TextBox>&nbsp;
			                    <asp:TextBox ID="txtRutaRegresoDesc" TabIndex="-1" runat="server" CssClass="desc" ReadOnly="True"
				                    BackColor="Transparent" Height="20px" Width="200px"></asp:TextBox>
			                    <asp:CompareValidator ID="cvRutaRegreso" runat="server" Display="Dynamic" ControlToValidate="txtRutaRegreso"
				                    ErrorMessage="El dato capturado en el campo Ruta no es del tipo de dato v�lido."
				                    Type="Integer" Operator="DataTypeCheck">*</asp:CompareValidator>
		                    </td>
		                    <td >
			                    <img id="Img4" style="cursor: hand" onclick="BuscaRuta('form1.txtRutaRegreso','form1.txtRutaRegresoDesc');"
				                    alt="Buscar Ruta" src="../Imagenes/BTbusc2.gif" align="absMiddle" runat="server" />
		                    </td>
	                    </tr>
                    </table> --> &nbsp;
                    <div id="dvClasfMvto" style="display:none" runat="server">
                        <table border="0" width="100%" height="65px">
                            <tr>
                                <td width="70" align="right" valign="bottom">
                                   <asp:DropDownList  ID="ddlClasf" runat="server" CssClass="aspecttxt"  ></asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <asp:Label ID="lblErrorUP2" runat="server" CssClass="labelop" ForeColor="Red" Visible="False"></asp:Label>
                      <%--rherrera GAAL AQUI IBA--%>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <div id="diVentanaTiempos" runat="server" style="display:none;">
                            <table>
                                <tr>
                                    <td colspan="3" >
                                      VENTANA DE TIEMPOS
                                    </td>
                                 </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtIdventana" Enabled="false" runat="server" Width="58px"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtDescventana" Enabled="false" runat="server" Width="238px"></asp:TextBox></td>
                                    <td><img id="imgBuscaVentana" style="cursor: hand" onclick="BuscaVentana('form1.txtIdventana','form1.txtDescventana');"
                                                    alt="Buscar Ruta" src="../Imagenes/BTbusc2.gif" align="absMiddle"></td>
                                </tr>
                                <tr>
                                    <td colspan="3" >
                                        <input id="hdn_pedidopk" runat="server" type="hidden" />
                                    </td>
                                 </tr>
                             </table>
                        </div>
                    </td>
                    <td>
                     <div id="divTiposervicio" runat="server" style="display:none;">
                        <table>
                            <tr>
                            <td width = "325" align="right" valign= "bottom">
                                <table border="0" style="background-position: right center; background-image: url(../imagenes/backs/fondoCh.gif);
                            background-repeat: no-repeat" height="30" id="Table2">
                            <tr>
                                <td>
                                    Tipo servicio:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlTipoServicio" Visible="false" Font-Size="11px" runat="server"></asp:DropDownList>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                            </td>
                                <td width = "325" align="right" valign= "bottom">
                                     <img id="Img50" style="cursor: hand" Height="25" src="../imagenes/add_hover.gif" Width="25" onclick = "AgregaTraslado();" runat="server" visible="false" />
                                 </td>
                            </tr>
                        </table>
                        </div>
                    </td>
                    <td>
                    </td>
                    <td >
                        <table>
                            <tr>
                                <td>
                                    <%--------%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%--------%>
                                </td>
                            </tr>
                         </table>
                    </td>
                </tr>
            </table>
            <asp:TextBox ID="lblRemDestHabil" runat="server" style="display:none" Text="0"></asp:TextBox>
                 <table id="tblRemDestOper" style="display:none;" width="840px" runat="server" cellpadding="0" cellspacing="0" border="0">
	                <tr>
		                <td>
			                <table id="tblRemDest" style="display:none;" width="840px" runat="server" cellpadding="0" cellspacing="0" border="0">
				                <tr>
					                <td align="left">
						                <div style="display:none;">
						                <asp:TextBox ID="txtOrigen" runat="server" TabIndex="-1" Width="5px"></asp:TextBox></div>
						                <table border="0" cellpadding="0" cellspacing="0">
							                <tr>
								                <td colspan="2">
									                <table border="0" style="background-position: right center; background-image: url(../imagenes/backs/fondoMdGde.gif);
										                background-repeat: no-repeat">
										                <tr>
											                <td width="60">
												                Remitente</td>
											                <td>
												                <asp:TextBox ID="txtRemitente" onchange="ValidaRemitente(this.value);" onkeypress="return IsInt(event);"
													                runat="server" Height="20px" CssClass="ID" TabIndex="41" MaxLength="9"></asp:TextBox>
												                <asp:TextBox ID="txtRemitenteNom" runat="server" CssClass="desc" Width="250px" TabIndex="-1" Height="20"
													                BackColor="Transparent"></asp:TextBox></td>
											                <td>
												                <asp:RequiredFieldValidator ID="rfvRemitente" runat="server" ControlToValidate="txtRemitente"
													                Display="Dynamic" ErrorMessage="Debe capturar un dato en el campo Remitente">*</asp:RequiredFieldValidator><asp:CompareValidator
														                ID="cvRemitente" runat="server" ControlToValidate="txtRemitente" Display="Dynamic"
														                ErrorMessage="El dato capturado en el campo Remitente no es v�lido." Operator="DataTypeCheck"
														                Type="Integer">*</asp:CompareValidator>
												                <img id="imgBuscRem" onclick="BuscaRemDes('form1.txtRemitente','form1.txtRemitenteNom','form1.txtRemitenteDom','form1.txtPlazaOrigenNom','form1.txtOrigen');"
													                alt="Buscar Remitente" name="imgBuscRem" src="../imagenes/BTBusc2.gif" style="cursor: hand"
													                title="Buscar Remitente" />
											                </td>
										                </tr>
									                </table>
								                </td>
							                </tr>
							                <tr style="display:none">
								                <td>
								                Plaza</td>
								                <td>
									                <table border="0" style="background-position: right center; background-image: url(../imagenes/backs/fondoMdGde.gif);
										                background-repeat: no-repeat">
										                <tr>
											                <td>
												                <asp:TextBox ID="txtPlazaOrigenNom" runat="server" CssClass="labelop" Width="320px"
													                TabIndex="-1" BackColor="Transparent"></asp:TextBox>
											                </td>
											                <td>
											                </td>
										                </tr>
									                </table>
								                </td>
							                </tr>
							                <tr style="display:none">
								                <td>
								                Domicilio</td>
								                <td>
								                <asp:TextBox ID="txtRemitenteDom" runat="server" CssClass="labelop" Width="320px"
									                TabIndex="-1" Height="30px" Rows="3" TextMode="MultiLine"></asp:TextBox></td>
							                </tr>
							                <tr style="display:none">
								                <td>
								                Recoger En</td>
								                <td>
								                <asp:TextBox ID="txtRecogerEn" onkeydown="TextCount(this,240);" onkeyup="TextCount(this,240);"
									                runat="server" CssClass="labelop" Rows="3" TextMode="MultiLine" Width="320px"
									                Height="30px" TabIndex="5"></asp:TextBox></td>
							                </tr>
						                </table>
					                </td>
				                </tr>
				                <tr>
					                <td align="left">
							                <div style="display:none;">
						                <asp:TextBox ID="txtDestino" runat="server" TabIndex="-1" Width="5px"></asp:TextBox></div>
						                <table border="0" cellpadding="0" cellspacing="0">
							                <tr>
								                <td colspan="2">
									                <table style="background-position: right center; background-image: url(../imagenes/backs/fondoMdGde.gif);
										                background-repeat: no-repeat">
										                <tr>
											                <td width="60">
												                Destinatario</td>
											                <td>
												                <asp:TextBox ID="txtDestinatario" onkeypress="return IsInt(event);" runat="server" Height="20"
													                CssClass="ID" onchange="ValidaDestinatario(this.value);" TabIndex="42" MaxLength="9"></asp:TextBox>
												                <asp:TextBox ID="txtDestinatarioNom" runat="server" CssClass="desc" Width="250px" Height="20"
													                TabIndex="-1" BackColor="Transparent"></asp:TextBox></td>
											                <td>
												                <asp:RequiredFieldValidator ID="rfvDestinatario" runat="server" ControlToValidate="txtDestinatario"
													                Display="Dynamic" ErrorMessage="Debe capturar un dato en el campo Destinatario">*</asp:RequiredFieldValidator><asp:CompareValidator
														                ID="cvDestinatario" runat="server" ControlToValidate="txtDestinatario" Display="Dynamic"
														                ErrorMessage="El dato capturado en el campo Destinatario no es v�lido." Operator="DataTypeCheck">*</asp:CompareValidator>
												                <img id="imgBuscDes" onclick="BuscaRemDes('form1.txtDestinatario','form1.txtDestinatarioNom','form1.txtDestinatarioDom','form1.txtPlazaDestinoNom','form1.txtDestino');"
													                alt="Buscar Destinatario" name="imgBuscDes" src="../imagenes/BTBusc2.gif" style="cursor: hand"
													                title="Buscar Destinatario" /></td>
										                </tr>
									                </table>
								                </td>
							                </tr>
							                <tr style="display:none">
								                <td>
								                Plaza</td>
								                <td align="left" style="width: 341px">
									                <table cellspacing="0" cellpadding="0" border="0" style="background-position: right center;
										                background-image: url(../imagenes/backs/fondoMdGde.gif); background-repeat: no-repeat"
										                height="30">
										                <tr>
											                <td>
												                <asp:TextBox ID="txtPlazaDestinoNom" runat="server" CssClass="labelop" Width="320px"
													                TabIndex="-1" BackColor="Transparent"></asp:TextBox>
											                </td>
											                <td width="5">
											                </td>
										                </tr>
									                </table>
								                </td>
							                </tr>
							                <tr style="display:none">
								                <td>
								                Domicilio</td>
								                <td style="width: 341px">
								                <asp:TextBox ID="txtDestinatarioDom" runat="server" CssClass="labelop" Width="320px"
									                TabIndex="-1" Height="30px" Rows="3" TextMode="MultiLine"></asp:TextBox></td>
							                </tr>
							                <tr style="display:none">
								                <td>
								                Entregar En</td>
								                <td style="width: 341px">
								                <asp:TextBox ID="txtEntregarEn" onkeydown="TextCount(this,240);" onkeyup="TextCount(this,240);"
									                runat="server" CssClass="labelop" Rows="3" TextMode="MultiLine" Width="320px"
									                Height="30px" TabIndex="7"></asp:TextBox></td>
							                </tr>
						                </table>
					                </td>
				                </tr>
			                </table>
		                </td>
	                </tr>
	                <tr>
		                <td align="left">
			                <table border="0" style="background-position: right center; background-image: url(../imagenes/backs/fondoCh.gif);
			                background-repeat: no-repeat" height="30" id="tblTipoOper">
				                <tr>
					                <td>
					                Tipo Operaci&oacute;n
					                </td>
					                <td>
					                <asp:DropDownList ID="ddlOperacion" CssClass="labels" onchange="ddlOperacionChange()"
						                runat="server" Font-Size="11px" Enabled =false  >
					                </asp:DropDownList>
					                </td>
					                <td>
					                </td>
				                </tr>
			                </table>
		                </td>
	                </tr>
                </table>
                 <table id="tblOrigenDestino" runat="server" style="display:none">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        Origen
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtOrigen1" onkeypress="return IsInt(event);" TabIndex="5" runat="server"
                                            Height="21px" MaxLength="9" CssClass="ID" onchange="GetPlazaOrigen(this.value);"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtOrigenDesc" TabIndex="-1" runat="server" Height="20px" CssClass="desc"
                                            ReadOnly="True" BackColor="Transparent"></asp:TextBox>
                                    </td>
                                    <td>
                                        <img style="cursor: hand" id="Img6" title="Buscar Zona" onclick="BuscaPlaza('form1.txtOrigen1','form1.txtOrigenDesc');"
                                            alt="Buscar Zona" src="../Imagenes/BTbusc2.gif" align="absMiddle" border="0"
                                            name="imgZona" />&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table>
                                <tr>
                                     <td>
                                        Destino
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDestino1" onkeypress="return IsInt(event);" TabIndex="5" runat="server"
                                            Height="21px" MaxLength="9" CssClass="ID" onchange="GetPlazaDestino(this.value);"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDestinoDesc" TabIndex="-1" runat="server" Height="20px" CssClass="desc"
                                            ReadOnly="True" BackColor="Transparent"></asp:TextBox>
                                    </td>
                                    <td>
                                        <img style="cursor: hand" id="Img8" title="Buscar Zona" onclick="BuscaPlaza('form1.txtDestino1','form1.txtDestinoDesc');"
                                            alt="Buscar Zona" src="../Imagenes/BTbusc2.gif" align="absMiddle" border="0"
                                            name="imgZona" />&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                 <div  id="DivDatosPermUnidad" runat="server" style="display:none">
                                    <table id="Table1" border="0" runat="server" style="background-position: right center;
                                        background-image: url(../imagenes/backs/FondoCH.gif); background-repeat: no-repeat"
                                        height="30">
                                        <tr>
                                            <td>Permisionario</td>
                                            <td><asp:TextBox onkeypress="return IsStr(event);" ID="txtPermisionario" onblur=""
                                                TabIndex="60" runat="server" CssClass="IDvch" Height="20px" onchange="" Width="230px"></asp:TextBox></td>
                                        </tr>
                                    </table>
                 </div>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <table>
                        <tr>
                            <td id="TD1" runat="server">
                            <div id="DivKit" runat="server" style="display:none">
                            <table id="tblkit" border="0" runat="server" style="background-position: right center;
                                    background-image: url(../imagenes/backs/FondoCH.gif); background-repeat: no-repeat"
                                    height="30">
                                    <tr>
                                        <td width="50">
                                            No. Kit</td>
                                        <td>
                                            <asp:TextBox onkeypress="return IsStr(event);" ID="txtKit" TabIndex="50" runat="server"
                                                CssClass="IDvch" MaxLength="10" onchange="ValidaKit(this.value);" Height="20px"></asp:TextBox>
                                            <asp:CompareValidator ID="Comparevalidator1" runat="server" Display="Dynamic" ControlToValidate="txtKit"
                                                ErrorMessage="El dato capturado en el campo No. Kit no es del tipo de dato v�lido."
                                                Operator="DataTypeCheck">*</asp:CompareValidator><asp:CustomValidator ID="cusvKit"
                                                    runat="server" Display="Dynamic" ControlToValidate="txtKit" ErrorMessage="Dato inv�lido en el campo No. Kit">*</asp:CustomValidator></td>
                                        <td style="color: #000000">
                                            <img id="ImgKit" style="cursor: hand" onclick="BuscaKit();" alt="Buscar Kit" src="../Imagenes/BTbusc2.gif"
                                                align="absMiddle"></td>
                                        <td style="color: #000000">
                                        </td>
                                    </tr>
                                </table>
                                </div>
                                <!-- div peromisionarios aortiz cambio vimifos 2013-06-28 -->
                            </td>
                            <td colspan="3" style="color: #000000">
                                <img id="imgTracto" alt="" src="<%= strImgTractor %>"><img id="imgRemolque1" alt=""
                                    src="<%= strImgRemolque1 %>"><img id="imgDolly" alt="" src="<%= strImgDolly %>"><img
                                        id="imgRemolque2" alt="" src="<%= strImgRemolque2 %>"></td>
                        </tr>
                        <tr style="color: #000000">
                            <td valign="top">
                                <table border="0" style="background-position: right center; background-image: url(../imagenes/backs/FondoCH.gif);
                                    background-repeat: no-repeat">
                                    <tr>
                                        <td width="50">
                                            Tractor</td>
                                        <td>
                                            <asp:TextBox onkeypress="return IsStr(event);" ID="txtUnidad" onblur="modifunidad();"
                                                TabIndex="60" runat="server" CssClass="IDvch" MaxLength="10" Height="20px" onchange="ValidaUnidad(this.value); validarDocumentos();"></asp:TextBox></td>
                                        <td>
                                            <asp:CompareValidator ID="cvUnidad" runat="server" Display="Dynamic" ControlToValidate="txtUnidad"
                                                ErrorMessage="El dato capturado en el campo Unidad no es del tipo de dato v�lido."
                                                Operator="DataTypeCheck">*</asp:CompareValidator>
                                            <asp:CustomValidator ID="cusvUnidad" runat="server" Display="Dynamic" ControlToValidate="txtUnidad" ErrorMessage="Dato inv�lido en el campo Unidad">*</asp:CustomValidator>
                                            <img id="Img3" style="cursor: hand" onclick="BuscaTractor();" alt="Buscar Tractor" src="../Imagenes/BTbusc2.gif" align="absMiddle" runat="server"/>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                                <div id="DivFianza1" runat="server" style="visibility:hidden">
                                <table border="0" style="background-position: right center; background-image: url(../imagenes/backs/FondoCH.gif);
                                    background-repeat: no-repeat">
                                <tr>
                                <td>Fianza 1</td>
                                <td colspan="2">
                                    <asp:TextBox runat="server" ID="txtFianza1" ReadOnly="true" Height="20px"  CssClass="IDvch"></asp:TextBox>
                                </td>
                                 <td>
                                <img id="img1" style="cursor: hand" onclick="EditarFianza(document.getElementById('txtLineaRem1').value,document.getElementById('txtRemolque1').value,'1',true);"
                                alt="Editar Fianza" src="../Imagenes/editar.gif" align="absMiddle"></td>
                                </tr>
                                </table>
                                </div>
                            </td>
                            <td class="style3">
                                <table border="0" style="background-position: right center; background-image: url(../imagenes/backs/FondoCH.gif);
                                    background-repeat: no-repeat">
                                    <tr>
                                        <td width="85">
                                            L�nea Rem. 1</td>
                                        <td>
                                            <asp:TextBox onkeypress="return IsStr(event);" ID="txtLineaRem1" TabIndex="70" runat="server"
                                                CssClass="IDvch" MaxLength="10" onchange="ValidaLineasyRemolques(this.value,document.getElementById('txtRemolque1').value,1);"
                                                Height="20px"></asp:TextBox>
                                            <asp:CompareValidator ID="cvLineaRem1" runat="server" Display="Dynamic" ControlToValidate="txtLineaRem1"
                                                ErrorMessage="El dato capturado en el campo Linea Rem. 1 no es del tipo de dato v�lido."
                                                Operator="DataTypeCheck">*</asp:CompareValidator><asp:CustomValidator ID="cusvLineaRem1"
                                                    runat="server" Display="Dynamic" ControlToValidate="txtLineaRem1" ErrorMessage="Dato inv�lido en el campo Linea Rem. 1">*</asp:CustomValidator></td>
                                        <td style="color: #000000">
                                            <div id="divImgLinea" runat="server">
                                              <img id="Img5" style="cursor: hand" onclick="BuscaLinea('form1.txtLineaRem1');" alt="Buscar L�nea Remolque"
                                                src="../Imagenes/BTbusc2.gif" align="absMiddle" runat="server" />
                                            </div>
                                          </td>
                                        <td style="color: #000000">
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" style="background-position: right center; background-image: url(../imagenes/backs/FondoCH.gif);
                                    background-repeat: no-repeat; color: #000000;">
                                    <tr>
                                        <td width="85">
                                            Remolque 1</td>
                                        <td>
                                            <asp:TextBox onkeypress="return IsStr(event);" ID="txtRemolque1" onchange="ValidaLineasyRemolques(document.getElementById('txtLineaRem1').value,this.value,1); validarDocumentos();"
                                                onblur="modifrem1();" TabIndex="80" runat="server" CssClass="IDvch" MaxLength="20"
                                                Height="20px"></asp:TextBox>
                                            <asp:CompareValidator ID="cvRemolque1" runat="server" Display="Dynamic" ControlToValidate="txtRemolque1"
                                                ErrorMessage="El dato capturado en el campo Remolque 1 no es del tipo de dato v�lido."
                                                Operator="DataTypeCheck">*</asp:CompareValidator><asp:CustomValidator ID="cusvRemolque1"
                                                    runat="server" Display="Dynamic" ControlToValidate="txtRemolque1" ErrorMessage="Dato inv�lido en el campo Remolque 1">*</asp:CustomValidator></td>
                                        <td style="color: #000000">
                                         <div id="divImgRem1" runat="server">
                                              <img id="ImgSearchRem1" style="cursor: hand" onclick="BuscaRemolque('form1.txtRemolque1','form1.txtLineaRem1');"
                                                alt="Buscar Remolque 1" src="../Imagenes/BTbusc2.gif" align="absMiddle" runat="server" />
                                         </div>
                                          </td>
                                        <td style="color: #000000">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" style="color: #000000">
                                <table border="0" style="background-position: right center; background-image: url(../imagenes/backs/FondoCH.gif);
                                    background-repeat: no-repeat">
                                    <tr>
                                        <td>
                                            Dolly</td>
                                        <td>
                                            <asp:TextBox onkeypress="return IsStr(event);" ID="txtDolly" onblur="modifdolly();"
                                                TabIndex="90" runat="server" onchange="ValidaDolly(this.value); validarDocumentos();" CssClass="IDvch"
                                                MaxLength="10" Height="20px"></asp:TextBox>
                                            <asp:CompareValidator ID="cvDolly" runat="server" Display="Dynamic" ControlToValidate="txtDolly"
                                                ErrorMessage="El dato capturado en el campo Dolly no es del tipo de dato v�lido."
                                                Operator="DataTypeCheck">*</asp:CompareValidator><asp:CustomValidator ID="cusvDolly"
                                                    runat="server" Display="Dynamic" ControlToValidate="txtDolly" ErrorMessage="Dato inv�lido en el campo Dolly">*</asp:CustomValidator></td>
                                        <td style="color: #000000">
                                            <img id="imgBDolly" style="cursor: hand" onclick="BuscaDolly('form1.txtDolly');" runat="server"
                                                alt="Buscar Dolly" src="../Imagenes/BTbusc2.gif" align="absMiddle" name="imgBDolly" />
                                        </td>
                                        <td style="color: #000000">
                                        </td>
                                    </tr>
                                </table>
                                 <input type="hidden" id="txthdnOpcion" runat="server" value="" />
                                 <div id="DivFianza2" runat="server" style="visibility:hidden">
                                 <table border="0" style="background-position: right center; background-image: url(../imagenes/backs/FondoCH.gif);
                                    background-repeat: no-repeat">
                                <tr>
                                <td><a>Fianza 2 </a></td>
                                <td colspan="2">
                                    <asp:TextBox runat="server" ID="txtFianza2" ReadOnly="true"  Height="20px" CssClass="IDvch"></asp:TextBox>
                                </td>
                                <td>  <img id="img2" style="cursor: hand" onclick="EditarFianza(document.getElementById('txtLineaRem2').value,document.getElementById('txtRemolque2').value,'2',true);"
                                alt="Editar Fianza" src="../Imagenes/editar.gif" align="absMiddle"></td>
                                </tr>
                                </table>
                                 </div>
                            </td>
                            <td valign="top" style="color: #000000">
                                <table border="0" style="background-position: right center; background-image: url(../imagenes/backs/FondoCH.gif);
                                    background-repeat: no-repeat">
                                    <tr>
                                        <td width="85">
                                            L�nea Rem 2</td>
                                        <td>
                                            <asp:TextBox onkeypress="return IsStr(event);" ID="txtLineaRem2" TabIndex="100" runat="server"
                                                CssClass="IDvch" MaxLength="10" onchange="ValidaLineasyRemolques(this.value,document.getElementById('txtRemolque2').value,2);"
                                                Height="20px"></asp:TextBox>
                                            <asp:CompareValidator ID="cvLineaRem2" runat="server" Display="Dynamic" ControlToValidate="txtLineaRem2"
                                                ErrorMessage="El dato capturado en el campo Linea Rem. 2 no es del tipo de dato v�lido."
                                                Operator="DataTypeCheck">*</asp:CompareValidator><asp:CustomValidator ID="cusvLineaRem2"
                                                    runat="server" Display="Dynamic" ControlToValidate="txtLineaRem2" ErrorMessage="Dato inv�lido en el campo Linea Rem. 2">*</asp:CustomValidator></td>
                                        <td style="color: #000000">
                                            <div id="divImgLinea2" runat="server">
                                              <img id="Img7" style="cursor: hand" onclick="BuscaLinea('form1.txtLineaRem2');" alt="Buscar L�nea Remolque"
                                                src="../Imagenes/BTbusc2.gif" align="absMiddle" runat="server" />
                                            </div>
                                          </td>
                                        <td style="color: #000000">
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" style="background-position: right center; background-image: url(../imagenes/backs/FondoCH.gif);
                                    background-repeat: no-repeat; color: #000000;">
                                    <tr>
                                        <td width="85">
                                            Remolque 2</td>
                                        <td>
                                            <asp:TextBox onkeypress="return IsStr(event);" ID="txtRemolque2" onblur="modifrem2();"
                                                TabIndex="110" onchange="ValidaLineasyRemolques(document.getElementById('txtLineaRem2').value,this.value,2); validarDocumentos();"
                                                runat="server" CssClass="IDvch" MaxLength="20" Height="20px"></asp:TextBox>
                                            <asp:CompareValidator ID="cvRemolque2" runat="server" Display="Dynamic" ControlToValidate="txtRemolque2"
                                                ErrorMessage="El dato capturado en el campo Remolque 2 no es del tipo de dato v�lido."
                                                Operator="DataTypeCheck">*</asp:CompareValidator><asp:CustomValidator ID="cusvRemolque2"
                                                    runat="server" Display="Dynamic" ControlToValidate="txtRemolque2" ErrorMessage="Dato inv�lido en el campo Remolque 2">*</asp:CustomValidator></td>
                                        <td style="color: #000000">
                                            <div id="divImgRem2" runat="server">
                                              <img id="ImgSearchRem2" style="cursor: hand" onclick="BuscaRemolque('form1.txtRemolque2','form1.txtLineaRem2');"
                                                alt="Buscar Remolque 2" src="../Imagenes/BTbusc2.gif" align="absMiddle" runat="server" />
                                            </div>
                                          </td>
                                        <td style="color: #000000">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id=trInfoCapacidadCarga runat=server>
                            <td align=right>
                                <table id=tblTUTracto style="background-position: right center; background-image: url(../imagenes/backs/FondoCH.gif);
                                    background-repeat: no-repeat">
                                    <tr>
                                        <td align="left">Tipo</td>
                                        <td>
                                            <asp:TextBox ID="txtTipoUTracto" runat="server" CssClass="IDvch" Height="20px"
                                                ReadOnly="True" Width="120px" TabIndex="-1" BackColor="Transparent"></asp:TextBox>
                                        </td>
                                        <td>
                                            <div style="visibility:hidden">
                                                <asp:TextBox ID="txtIdTipoUTracto" runat="server"
                                                    Width="20px" CssClass="IDvch">0</asp:TextBox></div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align=right class="style3">
                                <table border=0 id=tblTURem1 style="background-position: right center; background-image: url(../imagenes/backs/FondoCH.gif);
                                    background-repeat: no-repeat">
                                    <tr>
                                        <td width="85" align="right">Tipo</td>
                                        <td>
                                            <asp:TextBox ID="txtTipoURem1" runat="server" CssClass="IDvch" Height="20px"
                                                ReadOnly="True" Width="120px" TabIndex="-1" BackColor="Transparent"></asp:TextBox>
                                        </td>
                                        <td>
                                            <div style="visibility:hidden">
                                                <asp:TextBox ID="txtIdTipoUR1" runat="server"
                                                    Width="20px" CssClass="IDvch">0</asp:TextBox></div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align=right>
                                <table border=0 id=tblTUDolly runat=server style="background-position: right center; background-image: url(../imagenes/backs/FondoCH.gif);
                                    background-repeat: no-repeat">
                                    <tr>
                                        <td>Tipo</td>
                                        <td>
                                            <asp:TextBox ID="txtTipoUDolly" runat="server" ReadOnly="True" TabIndex="-1"
                                                Width="120px" CssClass="IDvch" Height="20px" BackColor="Transparent"></asp:TextBox>
                                        </td>
                                        <td>
                                            <div style="visibility:hidden">
                                                <asp:TextBox ID="txtIdTipoUD" runat="server"
                                                    Width="20px" CssClass="IDvch">0</asp:TextBox></div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align=right>
                                <table border=0 id=tblTURem2 runat=server style="background-position: right center; background-image: url(../imagenes/backs/FondoCH.gif);
                                    background-repeat: no-repeat">
                                    <tr>
                                        <td>Tipo</td>
                                        <td>
                                            <asp:TextBox ID="txtTipoURem2" runat="server" CssClass="IDvch" Height="20px"
                                                ReadOnly="True" TabIndex="-1" Width="120px" BackColor="Transparent"></asp:TextBox>
                                        </td>
                                        <td>
                                            <div style="visibility:hidden">
                                                <asp:TextBox ID="txtIdTipoUR2" runat="server"
                                                    Width="20px" CssClass="IDvch">0</asp:TextBox></div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr id="trInfoContenedores" runat="server" visible="false">
                            <td colspan="2" align="center" id="trInfoContenedor1" runat="server" visible="false">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="right">
                                        Contenedor 1&nbsp;
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtCont1Ped1" runat="server" CssClass="labelop" MaxLength="20"  Height="20"
                                                     onchange="ValidaContenedor(this.value,'Contenedor 1');"></asp:TextBox>
                                        <img id="imgBuscContA" onclick="BuscaContenedor('form1.txtCont1Ped1','form1.txtIdContenedor1');"
                                             alt="Buscar Contenedor 1 Pedido 1" name="imgBuscContA" src="../imagenes/BTBusc2.gif" style="cursor: hand"
                                             title="Buscar Contenedor 1 Pedido 1" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        Contenedor 2&nbsp;
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtCont2Ped1" runat="server" CssClass="labelop" MaxLength="20"   Height="20"
                                                     onchange="ValidaContenedor(this.value,'Contenedor 1b');"></asp:TextBox>
                                        <img id="imgBuscContA1" onclick="BuscaContenedor('form1.txtCont2Ped1','form1.txtIdContenedor1b');"
                                             alt="Buscar Contenedor 2 Pedido 1" name="imgBuscContA1" src="../imagenes/BTBusc2.gif" style="cursor: hand"
                                             title="Buscar Contenedor 2 Pedido 1" />
                                    </td>
                                </tr>
                            </table>
                            </td>
                            <td colspan="2" align="center" id="trInfoContenedor2" runat="server" visible="false">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="right">
                                        Contenedor 1&nbsp;
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtCont1Ped2" runat="server" CssClass="labelop" MaxLength="20"
                                                     onchange="ValidaContenedor(this.value,'Contenedor 2');"></asp:TextBox>
                                        <img id="imgBuscContB" onclick="BuscaContenedor('form1.txtCont1Ped2','form1.txtIdContenedor2');"
                                             alt="Buscar Contenedor 1 Pedido 2" name="imgBuscContB" src="../imagenes/BTBusc2.gif" style="cursor: hand"
                                             title="Buscar Contenedor 1 Pedido 2" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        Contenedor 2&nbsp;
                                    </td>
                                   <td align="left">
                                        <asp:TextBox ID="txtCont2Ped2" runat="server" CssClass="labelop" MaxLength="20"
                                                     onchange="ValidaContenedor(this.value,'Contenedor 2b');"></asp:TextBox>
                                        <img id="imgBuscContB1" onclick="BuscaContenedor('form1.txtCont2Ped2','form1.txtIdContenedor2b');"
                                             alt="Buscar Contenedor 2 Pedido 2" name="imgBuscContB1" src="../imagenes/BTBusc2.gif" style="cursor: hand"
                                             title="Buscar Contenedor 2 Pedido 2" />
                                    </td>
                                </tr>
                            </table>
                            </td>
                        </tr>

                        <tr id="trConfigMTC" runat="server">
                            <td align=center><asp:Label ID="lblConfigMTC" runat="server" Text="Configuraci�n MTC: "></asp:Label></td>
                            <%--<td align=center>Configuraci&oacute;n MTC</td>--%>
                            <td align=right class="style3">
                                <table border=0 cellpadding=0 cellspacing=0>
                                    <tr>
                                        <td></td>
                                        <td align=center>Capacidad M3</td>
                                        <td align=center>M3 Cargar</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <asp:TextBox ID="txtCapacidadM3" runat="server" CssClass="IDvch" Height="20px"
                                                ReadOnly="True"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCargarM3" runat="server" CssClass="IDvch" Height="20px"
                                                ReadOnly="True"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align=right>
                                <table border=0 cellpadding=0 cellspacing=0>
                                    <tr>
                                        <td></td>
                                        <td align=center>Capacidad KG</td>
                                        <td align=center>KG Cargar</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <asp:TextBox ID="txtCapacidadKG" runat="server" CssClass="IDvch" Height="20px"
                                                ReadOnly="True"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCargaKG" runat="server" CssClass="IDvch" Height="20px"
                                                ReadOnly="True"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align=right>
                                <table cellpadding=0 cellspacing=0 id=tblInfoPallets style="visibility:hidden" border=0>
                                    <tr>
                                        <td></td>
                                        <td align=center>Capacidad Pallets</td>
                                        <td align=center>Pallets Cargar</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <asp:TextBox ID="txtCapacidadPallets" runat="server" CssClass="IDvch"
                                                Height="20px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCargaPallets" runat="server" CssClass="IDvch" Height="20px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <caption>
                            <br />
                            <%--PHERNANDEZ 23/08/2018 FOLIO: 24834 --%>
                            <tr>
                                <td>
                                    <div ID="DivTipoRuta" runat="server" style="display: none">
                                        <asp:DropDownList ID="ddTipoRuta" runat="server" AppendDataBoundItems="true">
                                            <asp:ListItem Text="Seleccione Tipo de Ruta..." Value="SL"></asp:ListItem>
                                            <asp:ListItem Text="Pista de Ida y Regreso" Value="PIPR"></asp:ListItem>
                                            <asp:ListItem Text="Libre Ida y Libre Regreso" Value="LILR"></asp:ListItem>
                                            <asp:ListItem Text="Pista Ida y Libre regreso" Value="PILR"></asp:ListItem>
                                            <asp:ListItem Text="Libre Ida y Pista regreso" Value="LIPR"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </td>
                                <td>
                                    <div ID="DivTipoRutaModoCarga" runat="server" style="display: none">
                                        <asp:DropDownList ID="ddModoCarga" runat="server" AppendDataBoundItems="true">
                                            <asp:ListItem Text="Seleccione Modo Carga..." Value="SL"></asp:ListItem>
                                            <asp:ListItem Text="Carga Ida/Carga Regreso" Value="CICR"></asp:ListItem>
                                            <asp:ListItem Text="Carga Ida/Sin Carga Regreso" Value="CISR"></asp:ListItem>
                                            <asp:ListItem Text="Libre Ida/Carga Regreso" Value="LICR"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </td>
                            </tr>
                            <tr style="color: #000000">
                                <td colspan="2">
                                    <table ID="Table3" background="../Imagenes/backs/FONDOMdGde2.gif" border="0"
                                        cellpadding="1" cellspacing="1" height="31"
                                        style="background-position: right center; background-repeat: no-repeat">
                                        <tr>
                                            <td width="60">
                                                <asp:Label ID="lblOperador1" runat="server" CssClass="labelop" Font-Size="8pt"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtOperador1" runat="server" CssClass="ID" Height="20px"
                                                    MaxLength="9" onchange="ValidaOperador(this.value,1);
                                                    GetData(this.value); validarDocumentos();"
                                                    onkeypress="return IsInt(event);" TabIndex="120"></asp:TextBox>
                                                &nbsp;<asp:TextBox ID="txtOperador1Nombre" runat="server" BackColor="Transparent"
                                                    CssClass="desc" Height="20px" ReadOnly="True" TabIndex="-1" Width="200px"></asp:TextBox>
                                                &nbsp;
                                                <img ID="imgBscaOperador" align="absMiddle" alt="Buscar Operador 1"
                                                    onclick="BuscaOperador('form1.txtOperador1','form1.txtOperador1Nombre');"
                                                    src="../Imagenes/BTbusc2.gif" style="cursor: hand">
                                                <asp:RequiredFieldValidator ID="rfvOperador1" runat="server"
                                                    ControlToValidate="txtOperador1" Display="Dynamic"
                                                    ErrorMessage="Favor de capturar un dato en el campo Operador 1">*</asp:RequiredFieldValidator>
                                                <asp:CompareValidator ID="cvOperador1" runat="server"
                                                    ControlToValidate="txtOperador1" Display="Dynamic"
                                                    ErrorMessage="El dato capturado en el campo Operador 1 no es del tipo de dato v�lido."
                                                    Operator="DataTypeCheck" Type="Integer">*</asp:CompareValidator>
                                                <asp:CustomValidator ID="cusvOperador1" runat="server"
                                                    ControlToValidate="txtOperador1" Display="Dynamic"
                                                    ErrorMessage="El Operador no existe">*</asp:CustomValidator>
                                                </img></td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="right" colspan="2">
                                    <table ID="TableOp2" border="0" cellpadding="1" cellspacing="1" height="31"
                                        style="background-position: right center; background-repeat: no-repeat; background-image: url(../Imagenes/backs/FONDOMdGde2.gif);">
                                        <tr>
                                            <td width="60">
                                                <asp:Label ID="lblOperador2" runat="server" CssClass="labelop" Font-Size="8pt"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtOperador2" runat="server" CssClass="ID" Height="20px"
                                                    MaxLength="9" onchange="ValidaOperador(this.value,2); validarDocumentos();"
                                                    onkeypress="return IsInt(event);" TabIndex="130"></asp:TextBox>
                                                &nbsp;<asp:TextBox ID="txtOperador2Nombre" runat="server" BackColor="Transparent"
                                                    CssClass="desc" Height="20px" ReadOnly="True" TabIndex="-1" Width="200px"></asp:TextBox>
                                                &nbsp;<img ID="imgBscaOperador2" align="absMiddle" alt="Buscar Operador 2"
                                                    onclick="BuscaOperador('form1.txtOperador2','form1.txtOperador2Nombre');"
                                                    src="../Imagenes/BTbusc2.gif" style="cursor: hand"><asp:CompareValidator
                                                    ID="cvOperador2" runat="server" ControlToValidate="txtOperador2"
                                                    Display="Dynamic"
                                                    ErrorMessage="El dato capturado en el campo Operador 2 no es del tipo de dato v�lido."
                                                    Operator="DataTypeCheck" Type="Integer">*</asp:CompareValidator>
                                                <asp:CustomValidator ID="cusvOperador2" runat="server"
                                                    ControlToValidate="txtOperador2" Display="Dynamic"
                                                    ErrorMessage="El Operador 2 no existe">*</asp:CustomValidator>
                                                </img></td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table ID="Table7" border="0" cellpadding="1" cellspacing="1" height="31"
                                        style="background-position: right center; background-image: url(../Imagenes/backs/FONDOMdGde2.gif); background-repeat: no-repeat;">
                                        <tr>
                                            <td width="60">
                                                <asp:Label ID="lblSeguimiento" runat="server">Seguimiento</asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSeguimiento" runat="server" CssClass="ID" Height="20px"
                                                    MaxLength="9" onchange="ValidaSeguimiento(this.value);" TabIndex="140"></asp:TextBox>
                                                &nbsp;<asp:TextBox ID="txtSeguimientoDesc" runat="server" BackColor="Transparent"
                                                    CssClass="desc" Height="20px" ReadOnly="True" TabIndex="-1" Width="200px"></asp:TextBox>
                                                &nbsp;<img ID="imgBscaSeguimiento" align="absMiddle" alt="Buscar Seguimiento"
                                                    onclick="BuscaSeguimiento();" src="../Imagenes/BTbusc2.gif"
                                                    style="cursor: hand">
                                                <asp:CompareValidator ID="cvSeguimiento" runat="server"
                                                    ControlToValidate="txtSeguimiento" Display="Dynamic"
                                                    ErrorMessage="El dato capturado en el campo Seguimiento no es del tipo de dato v�lido."
                                                    Operator="DataTypeCheck">*</asp:CompareValidator>
                                                <asp:RequiredFieldValidator ID="rfvSeguimiento" runat="server"
                                                    ControlToValidate="txtSeguimiento" Display="Dynamic"
                                                    ErrorMessage="Debe capturar un dato en el campo Seguimiento">*</asp:RequiredFieldValidator>
                                                <asp:CustomValidator ID="cusvSeguimiento" runat="server"
                                                    ControlToValidate="txtSeguimiento" Display="Dynamic"
                                                    ErrorMessage="El Seguimiento no existe en la base de datos.">*</asp:CustomValidator>
                                                </img></td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table ID="tbTipoOp" runat="server" border="0" height="30"
                                        style="background-position: right center; background-image: url(../imagenes/backs/fondoCh.gif); background-repeat: no-repeat">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblTipoOp" runat="server" CssClass="labelop" Font-Size="8pt">Tipo Operaci�n</asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlTipoOp" runat="server" CssClass="labels"
                                                    Font-Size="11px" onchange="ddlTipoOpChange()">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td colspan="2">
                                    <asp:HiddenField ID="hdn_tipoopertpx" runat="server" Value="N" />
                                    <asp:HiddenField ID="hdn_valida_tipoOperacion" runat="server" Value="N" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <table border="0" cellpadding="1" cellspacing="1"
                                        style="width: 500px; height: 58px">
                                        <tr>
                                            <td>
                                                <p align="right">
                                                    <asp:Label ID="Label1" runat="server">Observaciones</asp:Label>
                                                </p>
                                            </td>
                                            <td background="../Imagenes/backs/FONDOObs.gif" height="60"
                                                style="background-position: right center; background-repeat: no-repeat"
                                                width="490">
                                                <asp:TextBox ID="txtObservaciones" runat="server" CssClass="labels"
                                                    Height="46px" MaxLength="255" onkeydown="TextCount(this,250);"
                                                    onkeypress="return IsStr(event);" onkeyup="TextCount(this,250);"
                                                    onpaste="PasteStr(this);" Rows="3" TabIndex="150" TextMode="MultiLine"
                                                    Width="478px"></asp:TextBox>
                                                &nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="right" valign="top">
                                    <table background="../Imagenes/backs/fondoCh.gif" border="0" height="31"
                                        style="background-position: right center; background-repeat: no-repeat">
                                        <tr>
                                            <td align="right" width="70">
                                                Ingres�</td>
                                            <td>
                                                <asp:TextBox ID="txtIngreso" runat="server" BackColor="Transparent"
                                                    CssClass="IDVch" Height="20px" ReadOnly="True" TabIndex="-1"> </asp:TextBox>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr id="rowProspectoPedido" runat="server" style="padding-top: 3px; display: none;">
                                <td>
                                    <table background="../Imagenes/backs/fondoCh.gif" border="0" height="20"
                                        style="background-position: right center; background-repeat: no-repeat">
                                        <tr>
                                            <td align="right" width="150">Tipo Armado</td>
                                            <td>
                                                <asp:DropDownList ID="ddlTipoArmado1" runat="server" CssClass="labels" Onchange="">
                                            </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:Label ID="lblError" runat="server" CssClass="LabelError" Visible="False"></asp:Label>
                                    <%--<asp:Label ID="lblErrorAjax" runat="server" CssClass="LabelError"></asp:Label>--%>
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:Label ID="lblErrorAjax" runat="server" CssClass="LabelError"></asp:Label>
                                    <div ID="ls-error-div">
                                    </div>
                                </td>
                            </tr>
                        </caption>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
            <table>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td valign="top">
                                    &nbsp;</td>
                                <td valign="top">
                                  <%--  <asp:ImageButton ID="btnGuardar" AccessKey="G" TabIndex="160" runat="server" ImageUrl="../Imagenes/GUARDAR_b.gif"
                                        AlternateText="Guardar informaci�n (Alt+G)" ToolTip="Guardar informaci�n (Alt+G)">
                                    </asp:ImageButton>--%>
                                        <img id="btnGuarda2" runat="server" style="display:none; cursor: hand" title="Guardar" src="../Imagenes/GUARDAR_b.gif"
                                            onclick="Guardar2();" />
                                    <div id="DivGuardar" runat="server" style="display:block;">
                                    <img id="btnGuarda" style="cursor: hand" title="Guardar" src="../Imagenes/GUARDAR_b.gif"
                                        onclick="Guardar();" />
                                        </div>
                                </td>
                                <td valign="bottom">
                                    <igtxt:WebImageButton ID="wibSalir" runat="server" AccessKey="S" AutoSubmit="False"
                                        CausesValidation="False" ToolTip="Regresar al listado (Alt+S)" TabIndex="170">
                                        <Appearance>
                                            <Image AlternateText="Regresar al listado (Alt+S)" Url="../imagenes/B_salir_z.gif" />
                                           <ButtonStyle BackColor="Transparent" BorderStyle="None" BorderWidth="0px" Cursor="Hand">
                                                <BorderDetails ColorLeft="Transparent" ColorTop="Transparent" ColorRight="Transparent" ColorBottom="Transparent" StyleLeft="None" StyleRight="None" StyleTop="None" StyleBottom="None">
                                                </BorderDetails>
                                            </ButtonStyle>
                                            <InnerBorder StyleBottom="None" StyleLeft="None" StyleRight="None" StyleTop="None" />
                                        </Appearance>
                                        <ClientSideEvents Click="wibSalir_Click" />
                                        <Alignments VerticalAll="Bottom" />
                                    </igtxt:WebImageButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br />
            <div style="visibility: hidden">
                <asp:TextBox ID="txtOperador1Original" runat="server" CssClass="ID" Visible="False"
                    BackColor="Transparent" Height="20px"></asp:TextBox><asp:TextBox ID="txtOperador2Original"
                    runat="server" CssClass="ID" Visible="False" BackColor="Transparent" Height="20px"></asp:TextBox>
                <input class="ID" id="Hidden1" type="hidden" name="txtHdnIndice" runat="server" style="height: 20px">&nbsp;
                <input class="ID" id="txtHdnValidaArmado" type="hidden" name="txtHdnValidaArmado" runat="server" style="height: 20px" value="1" />
                <asp:TextBox ID="txtIdContenedor1" runat="server" CssClass="ID" Width="10px"></asp:TextBox>
                <asp:TextBox ID="txtIdContenedor1b" runat="server" CssClass="ID" Width="10px"></asp:TextBox>
                <asp:TextBox ID="txtIdPedido_Contenedores1" runat="server" CssClass="ID" Width="10px"></asp:TextBox>
                <asp:TextBox ID="txtareaPedido_Contenedores1" runat="server" CssClass="ID" Width="10px"></asp:TextBox>
                <asp:TextBox ID="txtIdContenedor2" runat="server" CssClass="ID" Width="10px"></asp:TextBox>
                <asp:TextBox ID="txtIdContenedor2b" runat="server" CssClass="ID" Width="10px"></asp:TextBox>
                <asp:TextBox ID="txtIdPedido_Contenedores2" runat="server" CssClass="ID" Width="10px"></asp:TextBox>
                <asp:TextBox ID="txtareaPedido_Contenedores2" runat="server" CssClass="ID" Width="10px"></asp:TextBox>
                <asp:TextBox ID="txtParamContenedores" runat="server" CssClass="ID" Width="10px"></asp:TextBox>
                <input type="hidden" id="txtParamDocumentosPorUnidad" class="ID" runat="server" />
                <%-- FOLIO: 24266,  GAFIGUEROA 18/04/2018--%>
                <input id="HdnCambiosOpeAlmex" runat="server" name="HdnCambiosOpeAlmex" class="desc" type="hidden" />
                <%-- FOLIO: 24328,  GAFIGUEROA 27/04/2018--%>
                <input id="HdnIdRecorrido" runat="server" class="ID" name="HdnIdRecorrido" type="hidden" />
                <input id="HdnIdRecorridoOrigen" runat="server" class="ID" name="HdnIdRecorridoOrigen" type="hidden" />
                <input id="HdnIdRecorridoDestino" runat="server" class="ID" name="HdnIdRecorridoDestino" type="hidden" />
                <table border="0">
                    <tr>
                        <td>
                            <igtbl:UltraWebGrid ID="uwgPaso" runat="server" Width="100%" Height="80px">
                                <DisplayLayout AutoGenerateColumns="False" AllowSortingDefault="Yes" RowHeightDefault="5px"
                                    Version="3.00" SelectTypeRowDefault="Single" AllowColumnMovingDefault="OnServer"
                                    HeaderClickActionDefault="SortMulti" BorderCollapseDefault="Separate" AllowColSizingDefault="Free"
                                    EnableInternalRowsManagement="True" Name="uwgPaso" CellClickActionDefault="RowSelect"
                                    NoDataMessage="No hay registros a desplegar">
                                    <Pager PrevText="Anterior" NextText="Siguiente" PageSize="50" AllowPaging="True"
                                        StyleMode="ComboBox">
                                    </Pager>
                                    <HeaderStyleDefault BorderWidth="0px" BorderStyle="Solid" Height="25px" CssClass="TRHead"
                                        Font-Bold="True">
                                    </HeaderStyleDefault>
                                    <GroupByRowStyleDefault HorizontalAlign="Left">
                                    </GroupByRowStyleDefault>
                                    <FrameStyle Width="100%" Height="80px" Wrap="True">
                                    </FrameStyle>
                                    <GroupByBox Prompt="Arrastre aqu&#237; las columnas para crear grupos">
                                    </GroupByBox>
                                    <SelectedHeaderStyleDefault BackColor="#804040">
                                    </SelectedHeaderStyleDefault>
                                    <SelectedRowStyleDefault CssClass="TRRowSel">
                                    </SelectedRowStyleDefault>
                                    <RowAlternateStyleDefault CssClass="TRRowAlt">
                                    </RowAlternateStyleDefault>
                                    <RowStyleDefault Wrap="True" CssClass="TRRow">
                                    </RowStyleDefault>
                                    <Images ImageDirectory="../imagenes/WebGrid3/">
                                    </Images>
                                    <ActivationObject BorderColor="" BorderWidth="">
                                    </ActivationObject>
                                </DisplayLayout>
                                <Bands>
                                    <igtbl:UltraGridBand>
                                        <Columns>
                                            <igtbl:UltraGridColumn Key="id_unidad" Width="70px" CellButtonDisplay="Always" BaseColumnName="id_unidad"
                                                AllowResize="Fixed">
                                                <Header Caption="Unidad">
                                                </Header>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="id_remolque1" Width="75px" BaseColumnName="id_remolque1">
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                                <Header Caption="Remolque 1">
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="nombre_status_viaje" Width="120px" BaseColumnName="nombre_status_viaje">
                                                <Header Caption="Status Viaje">
                                                    <RowLayoutColumnInfo OriginX="2" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="2" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="no_viaje" Width="75px" BaseColumnName="no_viaje">
                                                <CellStyle Font-Bold="True" HorizontalAlign="Center">
                                                </CellStyle>
                                                <Header Caption="No. Viaje">
                                                    <RowLayoutColumnInfo OriginX="3" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="3" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="MensajesNuevos" Width="25px" BaseColumnName="MensajesNuevos">
                                                <CellStyle VerticalAlign="Top" Font-Bold="True" HorizontalAlign="Center">
                                                </CellStyle>
                                                <Header Caption="M">
                                                    <RowLayoutColumnInfo OriginX="4" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="4" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="AlarmasNuevas" Width="25px" Format=" " BaseColumnName="AlarmasNuevas">
                                                <CellStyle VerticalAlign="Top" Font-Bold="True" HorizontalAlign="Center">
                                                </CellStyle>
                                                <Header Caption="A">
                                                    <RowLayoutColumnInfo OriginX="5" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="5" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="Pend" Width="35px" Format="" BaseColumnName="Pend">
                                                <CellStyle Font-Bold="True" HorizontalAlign="Center">
                                                </CellStyle>
                                                <Header Caption="VP">
                                                    <RowLayoutColumnInfo OriginX="6" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="6" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="viajeactual" Width="15px" Hidden="True" BaseColumnName="viajeactual">
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                                <Header Caption="Desp">
                                                    <RowLayoutColumnInfo OriginX="7" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="7" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="f_ini_status" Width="120px" BaseColumnName="f_ini_status">
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                                <Header Caption="F. Status">
                                                    <RowLayoutColumnInfo OriginX="8" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="8" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="desp_status_nombre" Width="90px" Hidden="True" BaseColumnName="desp_status_nombre">
                                                <Header Caption="Status Unidad">
                                                    <RowLayoutColumnInfo OriginX="9" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="9" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="num_asignacion" Width="90px" BaseColumnName="num_asignacion">
                                                <CellStyle Font-Bold="True" HorizontalAlign="Center">
                                                </CellStyle>
                                                <Header Caption="Num. Asignaci&#243;n">
                                                    <RowLayoutColumnInfo OriginX="10" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="10" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="tiempos" Width="25px" BaseColumnName="tiempos">
                                                <CellStyle Font-Bold="True">
                                                </CellStyle>
                                                <Header Caption="Mod.">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="siguiente" Width="25px" BaseColumnName="siguiente">
                                                <CellStyle Cursor="Hand">
                                                </CellStyle>
                                                <Header Caption="Sig.">
                                                    <RowLayoutColumnInfo OriginX="12" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="12" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="Remitente" Width="225px" BaseColumnName="Remitente">
                                                <Header Caption="Remitente">
                                                    <RowLayoutColumnInfo OriginX="13" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="13" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="Destinatario" Width="225px" BaseColumnName="Destinatario">
                                                <Header Caption="Destinatario">
                                                    <RowLayoutColumnInfo OriginX="14" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="14" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="mostrar_operador2" Width="25px" AllowGroupBy="No" CellButtonDisplay="Always"
                                                BaseColumnName="mostrar_operador2" AllowResize="Fixed">
                                                <Header>
                                                    <RowLayoutColumnInfo OriginX="15" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="15" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="operadorunidad" Width="200px" BaseColumnName="operadorunidad">
                                                <Header Caption="Operador">
                                                    <RowLayoutColumnInfo OriginX="16" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="16" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="tipo_serv" Width="45px" BaseColumnName="tipo_serv">
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                                <Header Caption="Tipo Serv.">
                                                    <RowLayoutColumnInfo OriginX="17" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="17" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="id_linearem1" Width="75px" BaseColumnName="id_linearem1">
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                                <Header Caption="L&#237;nea Rem. 1">
                                                    <RowLayoutColumnInfo OriginX="18" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="18" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="id_dolly" Width="75px" BaseColumnName="id_dolly">
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                                <Header Caption="Dolly">
                                                    <RowLayoutColumnInfo OriginX="19" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="19" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="id_linearem2" Width="75px" BaseColumnName="id_linearem2">
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                                <Header Caption="L&#237;nea Rem. 2">
                                                    <RowLayoutColumnInfo OriginX="20" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="20" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="id_remolque2" Width="75px" BaseColumnName="id_remolque2">
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                                <Header Caption="Remolque 2">
                                                    <RowLayoutColumnInfo OriginX="21" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="21" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="plaza_origen" Width="170px" BaseColumnName="plaza_origen">
                                                <Header Caption="Origen">
                                                    <RowLayoutColumnInfo OriginX="22" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="22" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="plaza_destino" Width="170px" BaseColumnName="plaza_destino">
                                                <Header Caption="Destino">
                                                    <RowLayoutColumnInfo OriginX="23" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="23" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="posicion" Width="220px" BaseColumnName="posicion">
                                                <Header Caption="Posici&#243;n">
                                                    <RowLayoutColumnInfo OriginX="24" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="24" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="posdate" Width="100px" BaseColumnName="posdate">
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                                <Header Caption="F. Posici&#243;n">
                                                    <RowLayoutColumnInfo OriginX="25" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="25" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="f_prog_ini_viaje" Width="100px" BaseColumnName="f_prog_ini_viaje">
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                                <Header Caption="F. Ini. Prog.">
                                                    <RowLayoutColumnInfo OriginX="26" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="26" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="fecha_real_viaje" Width="100px" BaseColumnName="fecha_real_viaje">
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                                <Header Caption="F. Ini. Real">
                                                    <RowLayoutColumnInfo OriginX="27" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="27" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="f_prog_fin_viaje" Width="100px" BaseColumnName="f_prog_fin_viaje">
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                                <Header Caption="F. Fin Prog.">
                                                    <RowLayoutColumnInfo OriginX="28" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="28" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="observacionesviaje" Width="200px" BaseColumnName="observacionesviaje">
                                                <Header Caption="Obs. Viaje">
                                                    <RowLayoutColumnInfo OriginX="29" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="29" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="id_area" Hidden="True" BaseColumnName="id_area">
                                                <Header Caption="id_area">
                                                    <RowLayoutColumnInfo OriginX="30" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="30" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="UltimoDestino" Width="170px" BaseColumnName="UltimoDestino">
                                                <Header Caption="&#218;ltimo Destino">
                                                    <RowLayoutColumnInfo OriginX="31" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="31" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="zonanombre" BaseColumnName="zonanombre">
                                                <Header Caption="Zona">
                                                    <RowLayoutColumnInfo OriginX="32" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="32" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="operadorviaje" Hidden="True" BaseColumnName="operadorviaje">
                                                <Header Caption="operadorviaje">
                                                    <RowLayoutColumnInfo OriginX="33" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="33" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="fecha_servicio_sig" Width="60px" BaseColumnName="fecha_servicio_sig">
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                                <Header Caption="Dias p/ Mtto.">
                                                    <RowLayoutColumnInfo OriginX="34" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="34" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="fecha_disponible" BaseColumnName="fecha_disponible">
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                                <Header Caption="Fecha Disponible">
                                                    <RowLayoutColumnInfo OriginX="35" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="35" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="fecha_sale_vac" Width="80px" BaseColumnName="fecha_sale_vac">
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                                <Header Caption="Dias Vacaciones">
                                                    <RowLayoutColumnInfo OriginX="36" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="36" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="no_circula" Width="60px" BaseColumnName="no_circula">
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                                <Header Caption="D&#237;a No Circula">
                                                    <RowLayoutColumnInfo OriginX="37" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="37" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="placas" Width="60px" BaseColumnName="placas">
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                                <Header Caption="Placas Unidad">
                                                    <RowLayoutColumnInfo OriginX="38" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="38" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="fecha_venclicencia" Width="60px" BaseColumnName="fecha_venclicencia">
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                                <Header Caption="Vence Licencia">
                                                    <RowLayoutColumnInfo OriginX="39" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="39" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="tipo_licencia" BaseColumnName="tipo_licencia">
                                                <Header Caption="Tipo Licencia">
                                                    <RowLayoutColumnInfo OriginX="40" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="40" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="operadorunidad2" Width="200px" BaseColumnName="operadorunidad2">
                                                <Header Caption="Operador 2">
                                                    <RowLayoutColumnInfo OriginX="41" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="41" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="no_guia" Hidden="True" BaseColumnName="no_guia">
                                                <Header Caption="no_guia">
                                                    <RowLayoutColumnInfo OriginX="42" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="42" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="num_guia" BaseColumnName="num_guia">
                                                <CellStyle Font-Bold="True" HorizontalAlign="Center">
                                                </CellStyle>
                                                <Header Caption="Num. Gu&#237;a">
                                                    <RowLayoutColumnInfo OriginX="43" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="43" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="mostrar_guias" Width="25px" AllowGroupBy="No" CellButtonDisplay="Always"
                                                AllowResize="Fixed">
                                                <Header>
                                                    <RowLayoutColumnInfo OriginX="44" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="44" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="CantGuias" Hidden="True" AllowGroupBy="No" CellButtonDisplay="Always"
                                                BaseColumnName="CantGuias" AllowResize="Fixed">
                                                <Header Caption="">
                                                    <RowLayoutColumnInfo OriginX="45" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="45" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="id_pedido" BaseColumnName="id_pedido">
                                                <CellStyle Font-Bold="True" HorizontalAlign="Center">
                                                </CellStyle>
                                                <Header Caption="Pedido">
                                                    <RowLayoutColumnInfo OriginX="46" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="46" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="mostrar_pedidos" Width="25px" AllowGroupBy="No" CellButtonDisplay="Always"
                                                AllowResize="Fixed">
                                                <Header>
                                                    <RowLayoutColumnInfo OriginX="47" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="47" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="CantPedidos" Hidden="True" AllowGroupBy="No" CellButtonDisplay="Always"
                                                BaseColumnName="CantPedidos" AllowResize="Fixed">
                                                <Header Caption="">
                                                    <RowLayoutColumnInfo OriginX="48" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="48" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="id_responsable" Hidden="True" BaseColumnName="id_responsable">
                                                <Header Caption="id_responsable">
                                                    <RowLayoutColumnInfo OriginX="49" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="49" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="tipo_operacion" Width="120px" BaseColumnName="tipo_operacion">
                                                <Header Caption="Tipo Operaci&#243;n">
                                                    <RowLayoutColumnInfo OriginX="50" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="50" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="id_asignacion" Hidden="True" BaseColumnName="id_asignacion">
                                                <Header Caption="id_asignacion">
                                                    <RowLayoutColumnInfo OriginX="51" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="51" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="status_asignacion" Hidden="True" BaseColumnName="status_asignacion">
                                                <Header Caption="status_asignacion">
                                                    <RowLayoutColumnInfo OriginX="52" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="52" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="id_personal" Hidden="True" BaseColumnName="id_personal">
                                                <Header Caption="No. Personal Viaje">
                                                    <RowLayoutColumnInfo OriginX="53" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="53" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="op_unidad" Hidden="True" BaseColumnName="op_unidad">
                                                <Header Caption="Op. Unidad">
                                                    <RowLayoutColumnInfo OriginX="54" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="54" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="duracion_aviso" Hidden="True" BaseColumnName="duracion_aviso">
                                                <Header Caption="duracion_aviso">
                                                    <RowLayoutColumnInfo OriginX="55" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="55" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="duracion_maxima" Hidden="True" BaseColumnName="duracion_maxima">
                                                <Header Caption="duracion_maxima">
                                                    <RowLayoutColumnInfo OriginX="56" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="56" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="color" Hidden="True" BaseColumnName="color">
                                                <Header Caption="color">
                                                    <RowLayoutColumnInfo OriginX="57" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="57" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="color_aviso" Hidden="True" BaseColumnName="color_aviso">
                                                <Header Caption="color_aviso">
                                                    <RowLayoutColumnInfo OriginX="58" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="58" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="color_maxima" Hidden="True" BaseColumnName="color_maxima">
                                                <Header Caption="color_maxima">
                                                    <RowLayoutColumnInfo OriginX="59" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="59" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="id_statusviaje" Hidden="True" BaseColumnName="id_statusviaje">
                                                <Header Caption="id_statusviaje">
                                                    <RowLayoutColumnInfo OriginX="60" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="60" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="f_status_unidad" Hidden="True" BaseColumnName="f_status_unidad">
                                                <Header Caption="f_status_unidad">
                                                    <RowLayoutColumnInfo OriginX="61" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="61" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                        </Columns>
                                        <AddNewRow View="NotSet" Visible="NotSet">
                                        </AddNewRow>
                                    </igtbl:UltraGridBand>
                                </Bands>
                            </igtbl:UltraWebGrid></td>
                    </tr>
                </table>
            </div>

            <div style="display: none">
                <asp:TextBox ID="txtIDPedido" runat="server"  ></asp:TextBox>
                <asp:TextBox ID="txtRespValOrigCont" runat="server">0</asp:TextBox>
                <asp:TextBox ID="txtUnidadOriginal" runat="server" CssClass="ID" BackColor="Transparent" Height="20px"></asp:TextBox>
                <asp:TextBox ID="txtLineaRem1Original" runat="server" CssClass="ID" BackColor="Transparent" Height="20px"></asp:TextBox>
                <asp:TextBox ID="txtRemolque1Original" runat="server" CssClass="ID" BackColor="Transparent" Height="20px"></asp:TextBox>
                <asp:TextBox ID="txtLineaRem2Original" runat="server" CssClass="ID" BackColor="Transparent" Height="20px"></asp:TextBox>
                <asp:TextBox ID="txtRemolque2Original" runat="server" CssClass="ID" BackColor="Transparent" Height="20px"></asp:TextBox>
                <asp:TextBox ID="txt_hdAsignacion" runat="server"></asp:TextBox>
                <asp:TextBox ID="txtAsignacion2" runat="server"></asp:TextBox>
                <asp:TextBox ID="txtFechaDefault" runat="server" Width="8px"></asp:TextBox>
                <asp:TextBox ID="RequestArea" runat="server"></asp:TextBox>
                <asp:TextBox ID="RequestViaje" runat="server"></asp:TextBox>
                <asp:TextBox ID="txtIdGrid" runat="server" Width="10px"></asp:TextBox>
                <asp:TextBox ID="txtListaPedidosAreas" runat="server" CssClass="ID"></asp:TextBox>
                <asp:TextBox ID="txtListaPedidos" runat="server" CssClass="ID"></asp:TextBox>
                <asp:TextBox ID="txtActualiza" runat="server" CssClass="ID" Width="0px"></asp:TextBox>
                <asp:TextBox ID="hfDINET" runat="server" Text="0"></asp:TextBox>
                <asp:TextBox id="idFlota" name="idflota" runat="server" />
                <input type="hidden" runat="server" id="FianzaRetornada" name="FianzaRetornada"  />
                <asp:HiddenField ID="hdnParamTresFronteras" Value="0" runat="server" />
                <asp:HiddenField ID="hdnTiempoRuta" Value="0" runat="server" />
                <asp:HiddenField ID="hdnTiempoUrgente" Value="0" runat="server" />
                <asp:HiddenField ID="hdnTipoOp_AsigVV" Value="0" runat="server" />
                <asp:HiddenField ID="hdnVacioXAreaTracto" Value="" runat="server" />
                <asp:HiddenField ID="hdnObtenerGasolineras" runat="server" />
                <asp:HiddenField ID="hdnIdAsignacion" runat="server" />
                <asp:HiddenField ID="hdnIdAreaAsignacion" runat="server" />
                <asp:HiddenField ID="hdnIdAsignacionRegreso" runat="server" />
                <asp:HiddenField ID="hdnIdAreaAsignacionRegreso" runat="server" />
                <asp:HiddenField ID="hdnIdUnidad" runat="server" />
                <asp:HiddenField ID="hdnresult" runat="server" />
                <asp:HiddenField ID="hdnCombustibleMinimo" runat="server" />
                <asp:HiddenField ID="hdnRutaRegreso" runat="server" />
                <asp:HiddenField ID="hdnValidaDocumentos" runat="server" />
                <asp:HiddenField ID = "hdnValidaOperadorPermEnUnidadPerm" runat="server" />
                <asp:HiddenField ID="hdnLigaMonitoreo" runat="server" />
                <asp:HiddenField ID="hdnShowOrigenDestinoVacio" runat="server" />
                <asp:HiddenField ID="hdnUbicacionDiasRemolque" runat="server" /> <%--GPEREZ F:24656--%>
                <asp:HiddenField ID="hdn4Contenedores" runat="server" />
                <asp:HiddenField ID="hdnClasificaTipoRuta" runat="server" /> <%-- PHERNANDEZ 23/08/2018 --%>
                <asp:HiddenField ID="hdn_razon_social" runat="server" Value="" /><%-- AMONTIEL FOLIO:25313  13/12/2018 --%>
            </div>
        </div>

        <%--style="float:inherit" --%>
        <div style="display: none;"  id="map_canvas" ></div> <%--rherrera GAAL--%>

    </form>
    <script language="javascript" src='<%= String.Concat(Application("myRoot")) %>/js/validaciones.js' type="text/javascript"></script>
    <script language="javascript" src='<%= String.Concat(Application("myRoot")) %>/pridesp01/pre_i_despasignarpedidosajaxvalidaciones.js' type="text/javascript"></script>

    <script type="text/javascript">
        // window.onload = function () {
// alert(document.getElementById("lblOutputOp").innerHTML);
//           if (document.getElementById("lblOutputOp").innerHTML != undefined) {
//
//           var raspon = document.getElementById("lblOutputOp").innerHTML;
//
//             var response = raspon;
//             var pieces = response.split(",");
//             var blocks = parseInt(pieces[0]);
//             var restDays = pieces[1];
//             var namae = pieces[2];
//             var id_operador = pieces[3];
//             var folio = pieces[4];
//             var vence = pieces[5];
//
// // alert(vence);
//
//             if(blocks == 0) {
//               alert("Licencia expirada desde " + restDays + " dias");
//               document.getElementById("btnGuarda").disabled = true;
//               // lblError.innerText = "";
//               lblErrorAjax.innerText = "Licencia del operador "+ namae +" expirada desde " + restDays + " dias";
//             } else if (blocks == 1) {
//               alert("Licencia Vence en " + restDays + " dias");
//               document.getElementById("btnGuarda").disabled = true;
//               // lblError.innerText = "";
//               lblErrorAjax.innerText = "La Licencia del Operador tiene menos de 10 dias para vencer";
//             } else if (blocks == 2) {
//               alert("Licencia sin Actualizar");
//               document.getElementById("btnGuarda").disabled = true;
//               // lblError.innerText = "";
//               lblErrorAjax.innerText = "La Licencia del Operador necesita Actualizarse";
//             } else if (blocks == 3) { // Entre 10 y 30 dias
//               alert("Licencia Vence en " + restDays + " dias");
//               // if button is disabled just enable
//               if (document.getElementById("btnGuarda").disabled == true) {
//                 document.getElementById("btnGuarda").disabled = false;
//               }
//               document.getElementById('txtOperador1Nombre').length=0;
//               document.getElementById('txtOperador1Nombre').value=namae;
//               // lblError.innerText = "";
//               lblErrorAjax.innerText = "La Licencia del Operador Vence en " + restDays + " dias";
//             } else if (blocks == 4) {
//               alert("El Operador esta dado de Baja");
//               document.getElementById("btnGuarda").disabled = true;
//               // lblError.innerText = "";
//               lblErrorAjax.innerText = "El Operador " + namae + " esta dado de baja";
//             } else { // mayor a un mes
//               // if button is disabled just enable
//               if (document.getElementById("btnGuarda").disabled == true) {
//                 document.getElementById("btnGuarda").disabled = false;
//               }
//               // Clear current data
//               document.getElementById('txtOperador1Nombre').length=0;
//               // set the new value
//               document.getElementById('txtOperador1Nombre').value=namae;
//               lblErrorAjax.innerText = ""
//             }
//         }
      // }
    </script>
</body>
</html>
